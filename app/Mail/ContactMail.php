<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;

    public $request;
    public $theme;

    public function __construct($request, $theme)
    {
        $this->request = $request;
        $this->theme = $theme;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to($this->request->email)
                    ->from('no-reply@einhell.com', 'Einhell')
                    ->subject($this->theme)
                    ->view('mail.contact')
                    ->with('data', $this->request->except(['_token', 'theme_id']));
    }
}
