<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class OrderConfirmationMail extends Mailable
{
    use Queueable, SerializesModels;

    public $order;
    public $pdf;

    public function __construct($order, $pdf)
    {
        $this->order = $order;
        $this->pdf = $pdf;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail = $this->to($this->order->email)
                    ->from('no-reply@einhell.com', 'Einhell')
                    ->subject('Успешна поръчка')
                    ->view('mail.order-confirmation')
                    ->with('order', $this->order);

        if (!is_null($this->pdf)) {
            $mail->attachData($this->pdf, 'invoice.pdf', [
                'mime' => 'application/pdf',
            ]);
        }
        
        return $mail;
    }
}
