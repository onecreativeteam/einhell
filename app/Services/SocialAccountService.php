<?php
namespace App\Services;

use Laravel\Socialite\Contracts\User as ProviderUser;

use App\Models\User;
use App\Models\SocialAccount;

class SocialAccountService
{
    public function createOrGetUser(ProviderUser $providerUser, $provider)
    {
        $account = SocialAccount::whereProvider($provider)
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        }

        $account = new SocialAccount([
            'provider_user_id' => $providerUser->getId(),
            'provider' => $provider
        ]);

        $user = User::whereEmail($providerUser->getEmail())->first();

        if (!$user) {

            $user = User::create([
                'first_name' => $providerUser->getName(),
                'email' => $providerUser->getEmail(),
                'phone' => '',
                'role' => 'basic',
                'password' => bcrypt($providerUser->getEmail()),
            ]);
        }

        $account->user()->associate($user);
        $account->save();

        return $user;
    }

    public function validateExistingUser($user)
    {
        $user = User::where('email', '=', $user->email)->first();

        if (!is_null($user->social) || is_null($user)) {
            return true;
        }

        return false;
    }
}