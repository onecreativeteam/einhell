<?php
namespace App\Services;

use Carbon\Carbon;
use File;
use Image;
use Auth;

use App\Repositories\ProductRepository;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\ProductSeriesRepository;

class SearchService
{
    private $productRepo;
    private $categoryRepo;
    private $seriesRepo;

    public function __construct(
        ProductRepository $productRepo,
        ProductCategoryRepository $categoryRepo,
        ProductSeriesRepository $seriesRepo
    ) {
        $this->productRepo  = $productRepo;
        $this->categoryRepo = $categoryRepo;
        $this->seriesRepo   = $seriesRepo;
    }

    public function search($request)
    {
        if (strlen($request->search) < 2) return [ 'products' => [], 'category' => [], 'series' => [] ];

        $products = $this->productRepo->getSearchProducts(['name', 'like', '%'.$request->search.'%']);
        $category = $this->categoryRepo->getTranslatedWhereWith(['name', 'like', '%'.$request->search.'%'], [], 'get');
        $series   = $this->seriesRepo->getTranslatedWhereWith(['name', 'like', '%'.$request->search.'%'], [], 'get');

        return compact(['products', 'category', 'series']);
    }
}