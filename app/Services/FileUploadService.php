<?php
namespace App\Services;

use Carbon\Carbon;
use File;
use Image;
use Auth;

class FileUploadService
{
    /**
     * @param mixed $file;
     * @param string $path;
     * @return string
     */
    public static function uploadFile($file, $path, $dimensions = null)
    {
        $fileExt  = $file->getClientOriginalExtension();
        $fileName = strtolower($file->getClientOriginalName());
        $fileName = str_slug(pathinfo($fileName, PATHINFO_FILENAME)) . '-' . rand(10, 100) . '-' . Auth::user()->id;
        $filePrep = sprintf('%s.%s', $fileName, $fileExt);
        $file->move($path, $filePrep);
        $filePath = $path . $filePrep;
        if (!is_null($dimensions)) {
            $image = Image::make($filePath);
            $image->fit($dimensions[0], $dimensions[1], function ($constraint) {
                $constraint->upsize();
            });
            $image->save($filePath);
        }
        
        return $filePath;
    }

    public static function uploadProductExcelPicture($file, $path, $dimensions = null)
    {
        $fileExt  = $file->getClientOriginalExtension();
        $fileName = $file->getClientOriginalName();
        $fileName = pathinfo($fileName, PATHINFO_FILENAME);
        $filePrep = sprintf('%s.%s', $fileName, $fileExt);
        $file->move($path, $filePrep);
        $filePath = $path . $filePrep;
        if (!is_null($dimensions)) {
            $image = Image::make($filePath);
            $image->fit($dimensions[0], $dimensions[1], function ($constraint) {
                $constraint->upsize();
            });
            $image->save($filePath);
        }
        
        return $filePath;
    }
}