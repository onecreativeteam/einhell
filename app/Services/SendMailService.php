<?php
namespace App\Services;

use Carbon\Carbon;
use File;
use Image;
use Mail;
use Auth;

use App\Mail\RegistrationMail;
use App\Mail\ContactMail;
use App\Mail\OrderConfirmationMail;
use App\Mail\OrderAdminMail;

class SendMailService
{
    public static function sendRegistrationMail($user)
    {
        Mail::to($user->email)->send(new RegistrationMail($user));
    }

    public static function sendContactMail($request, $theme)
    {
        Mail::to($request->email)->send(new ContactMail($request, $theme));
    }

    public static function sendOrderConfirmationMail($order, $pdf = null)
    {
        Mail::to($order->email)->send(new OrderConfirmationMail($order, $pdf));
    }

    public static function sendOrderAdminMail($order)
    {
        Mail::to(['internet@einhell.bg', 'RadevS@einhell.bg'])->send(new OrderAdminMail($order));
    }
}