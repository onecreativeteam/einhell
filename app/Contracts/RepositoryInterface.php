<?php

namespace App\Contracts;

/**
 * Interface RepositoryInterface
 *
 * @package App\Contracts
 */
interface RepositoryInterface
{

    /**
     * Find a record
     *
     * @param int $id The object ID to find
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function find($id);

    /**
     * Get all records
     *
     * @return \Illuminate\Database\Eloquent\Collection;
     */
    public function all();

    /**
     * Create new record
     *
     * @param array $data Data that the object will be populated with
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create($data);

    /**
     * Update given object
     *
     * @param int $id The object ID
     * @param array $data The data that will be set to the object
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update($id, $data);

    /**
     * Delete row from the DB
     *
     * @param int $id The object ID
     * @return boolean
     */
    public function delete($id);

    /**
     * Paginate records
     *
     * @param array $search
     * @param int $perPage
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function paginate($search = [], $perPage = 15);
}
