<?php
namespace App\Repositories;

use App\Models\ContactTranslation;

class ContactTranslationRepository extends BaseRepository
{
    /**
    * @var App\Models\ContactTranslation $modelClass;
    */
    protected $modelClass = ContactTranslation::class;
}
