<?php
namespace App\Repositories;

use App\Models\InformationPage;

class InformationPageRepository extends BaseRepository
{
    /**
    * @var App\Models\InformationPage $modelClass;
    */
    protected $modelClass = InformationPage::class;
}
