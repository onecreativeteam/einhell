<?php
namespace App\Repositories;

use App\Models\Order;

class OrderRepository extends BaseRepository
{
    /**
    * @var App\Models\Order $modelClass;
    */
    protected $modelClass = Order::class;

    public function getOrder($id)
    {
        return $this->getModel()->find($id);
    }

    public function updateAdminOrder($id, array $data)
    {
    	$order = $this->getModel()->find($id);

    	foreach ($data as $k => $value) {
    		$order->$k = $value;
    	}
    	
    	$order->save();
    }

    public function deleteOrderAndReturnQuantity($id)
    {
    	$order = $this->getModel()->find($id);
    	$orderProducts = $order->orderProducts;

    	if (count($orderProducts)) {
    		foreach ($orderProducts as $k => $orderProduct) {
    			$product = $orderProduct->product;
    			$product->quantity = $product->quantity + $orderProduct->product_quantity;

    			$product->save();
    		}
    	}
    }

    public function getOrderListDataTable()
    {
        return $this->getModel()->orderBy('id', 'desc')->get();
    }

    public function getOrderListDataTableWhereAll(array $where)
    {
        return $this->getModel()->where($where[0], $where[1], $where[2])->orderBy('id', 'desc')->get();
    }
}
