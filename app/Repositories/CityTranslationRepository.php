<?php
namespace App\Repositories;

use App\Models\CityTranslation;

class CityTranslationRepository extends BaseRepository
{
    /**
    * @var App\Models\CityTranslation $modelClass;
    */
    protected $modelClass = CityTranslation::class;
}
