<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Contracts\RepositoryInterface;

/**
 * Class Repository
 *
 * @codeCoverageIgnore
 * @package App\Repositories
 */
abstract class Repository implements RepositoryInterface
{
    /**
     * The Model class that this repository will use to
     * retrieve/update/create or delete records
     *
     * @var null
     */
    protected $modelClass = null;

    /**
     * Find a record
     *
     * @param int $id The object ID to find
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function find($id)
    {
        return $this->getModel()->findOrFail($id);
    }

    public function findBy($field, $value)
    {
        return $this->getModel()->where($field, '=', $value)->first();
    }

    /**
     * Get all records
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all()
    {
        return $this->getModel()->all();
    }

    /**
     * Insert many new records
     *
     * @param array $data Data that the objects will be populated with
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function insertMany(array $data)
    {
        return $this->getModel()->insert($data);
    }

    /**
     * Get records where has relation
     *
     * @param string $relation
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function whereHas($relation)
    {
        return $this->getModel()->whereHas($relation);
    }

    /**
     * Create new record
     *
     * @param array $data Data that the object will be populated with
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create($data)
    {
        return $this->getModel()->create($data);
    }

    /**
     * Update given object
     *
     * @param int $id The object ID
     * @param array $data The data that will be set to the object
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update($id, $data)
    {
        $model = $this->find($id);
        $model->update($data);

        return $model;
    }

    /**
     * Delete row form the DB
     *
     * @param int $id The object ID
     * @return boolean
     */
    public function delete($id)
    {
        return $this->find($id)->delete();
    }

    /**
     * Get new model instance
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getModel()
    {
        return app($this->modelClass);
    }

    /**
     * Paginate records
     *
     * @param array $search
     * @param int $perPage
     * @return LengthAwarePaginator
     */
    public function paginate($search = [], $perPage = 15)
    {
        return $this->getModel()->paginate($perPage);
    }
}