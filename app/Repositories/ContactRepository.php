<?php
namespace App\Repositories;

use App\Models\Contact;

class ContactRepository extends BaseRepository
{
    /**
    * @var App\Models\Contact $modelClass;
    */
    protected $modelClass = Contact::class;

    public function getContact($id)
    {
        return $this->getModel()->find($id);
    }
}
