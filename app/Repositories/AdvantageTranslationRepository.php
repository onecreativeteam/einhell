<?php
namespace App\Repositories;

use App\Models\AdvantageTranslation;

class AdvantageTranslationRepository extends BaseRepository
{
    /**
    * @var App\Models\AdvantageTranslation $modelClass;
    */
    protected $modelClass = AdvantageTranslation::class;
}
