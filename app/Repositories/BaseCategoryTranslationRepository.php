<?php
namespace App\Repositories;

use App\Models\BaseCategoryTranslation;

class BaseCategoryTranslationRepository extends BaseRepository
{
    /**
    * @var App\Models\BaseCategoryTranslation $modelClass;
    */
    protected $modelClass = BaseCategoryTranslation::class;
}
