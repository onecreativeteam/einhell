<?php
namespace App\Repositories;

use App\Models\City;
use Config;

class CityRepository extends BaseRepository
{
    /**
    * @var App\Models\City $modelClass;
    */
    protected $modelClass = City::class;

    public function getCity($id)
    {
        return $this->getModel()->find($id);
    }

    public function getAllWithTranslation()
    {
        $defaultLocale  = Config::get('app.default_locale');

        if ($defaultLocale == app()->getLocale()) {

            $records = $this->getModel()->orderBy('name', 'asc')->get();

        } else {

            $records = $this->getModel()
                        ->whereHas('translated')
                        ->with(['translated'])
                        ->orderBy('name', 'asc')
                        ->get()->map(function ($item) {
                            foreach ($item->translatableFields as $field) {
                                $item->$field = $item->translated->$field;
                            }
                            return $item;
                        });

        }

        return $records;
    }

    public function pluck($value, $key = null)
    {
        return ($key) ? $this->getModel()->orderBy('name', 'asc')->pluck($value, $key)->all() : $this->getModel()->orderBy('name', 'asc')->pluck($value)->all();
    }
}
