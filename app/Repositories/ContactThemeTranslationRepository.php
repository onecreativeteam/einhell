<?php
namespace App\Repositories;

use App\Models\ContactThemeTranslation;

class ContactThemeTranslationRepository extends BaseRepository
{
    /**
    * @var App\Models\ContactThemeTranslation $modelClass;
    */
    protected $modelClass = ContactThemeTranslation::class;
}
