<?php
namespace App\Repositories;

use App\Models\XchangePageTranslation;

class XchangePageTranslationRepository extends BaseRepository
{
    /**
    * @var App\Models\XchangePageTranslation $modelClass;
    */
    protected $modelClass = XchangePageTranslation::class;
}
