<?php
namespace App\Repositories;

use App\Models\PartnerTranslation;

class PartnerTranslationRepository extends BaseRepository
{
    /**
    * @var App\Models\PartnerTranslation $modelClass;
    */
    protected $modelClass = PartnerTranslation::class;
}
