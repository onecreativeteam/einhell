<?php
namespace App\Repositories;

use App\Models\BaseCategory;

class BaseCategoryRepository extends BaseRepository
{
    /**
    * @var App\Models\BaseCategory $modelClass;
    */
    protected $modelClass = BaseCategory::class;

    public function getBaseCategory($id)
    {
        return $this->getModel()->find($id);
    }
}
