<?php
namespace App\Repositories;

use App\Models\DistributorTranslation;

class DistributorTranslationRepository extends BaseRepository
{
    /**
    * @var App\Models\DistributorTranslation $modelClass;
    */
    protected $modelClass = DistributorTranslation::class;
}
