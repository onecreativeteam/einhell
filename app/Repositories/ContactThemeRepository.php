<?php
namespace App\Repositories;

use App\Models\ContactTheme;

class ContactThemeRepository extends BaseRepository
{
    /**
    * @var App\Models\ContactTheme $modelClass;
    */
    protected $modelClass = ContactTheme::class;

    public function getContactTheme($id)
    {
        return $this->getModel()->find($id);
    }
}
