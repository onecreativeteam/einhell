<?php
namespace App\Repositories;

use App\Models\ProductCategoryTranslation;

class ProductCategoryTranslationRepository extends BaseRepository
{
    /**
    * @var App\Models\ProductCategoryTranslation $modelClass;
    */
    protected $modelClass = ProductCategoryTranslation::class;
}
