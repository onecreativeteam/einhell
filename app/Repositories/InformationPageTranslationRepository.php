<?php
namespace App\Repositories;

use App\Models\InformationPageTranslation;

class InformationPageTranslationRepository extends BaseRepository
{
    /**
    * @var App\Models\InformationPageTranslation $modelClass;
    */
    protected $modelClass = InformationPageTranslation::class;
}
