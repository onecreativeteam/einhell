<?php
namespace App\Repositories;

use App\Models\CoverPhoto;
use App\Services\FileUploadService;

class CoverPhotoRepository extends BaseRepository
{
	private $fileUploadService;

    public function __construct(
        FileUploadService $fileUploadService
    ) {
        $this->fileUploadService = $fileUploadService;
    }

    /**
    * @var App\Models\CoverPhoto $modelClass;
    */
    protected $modelClass = CoverPhoto::class;

    public function getFilesByType()
    {
        return $this->getModel()->get()->keyBy('type');
    }

    public function scopeType($type)
    {
        return $this->getModel()->$type()->first();
    }

    public function createOrUpdateFiles($request)
    {
        if ($request->hasFile('file')) {
            
            $filesArray         = [];
            $filePath           = $this->getModel()->filePathAttribute;
            $filesArray['file'] = $this->fileUploadService->uploadFile($request->file, $filePath);
            $filesArray['type'] = $request->type;

        }

        $existingFile = $this->scopeType($request->type);

        if (!is_null($existingFile)) {
            if (file_exists($existingFile->file)) {
                unlink($existingFile->file);
            }
            $this->updateRecord($existingFile, $filesArray);
        } else {
            $this->getModel()->create($filesArray);
        }
    }
}
