<?php
namespace App\Repositories;

use App\Models\ProductSliderTranslation;

class ProductSliderTranslationRepository extends BaseRepository
{
    /**
    * @var App\Models\ProductSliderTranslation $modelClass;
    */
    protected $modelClass = ProductSliderTranslation::class;
}
