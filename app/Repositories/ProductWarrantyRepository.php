<?php
namespace App\Repositories;

use App\Models\ProductWarranty;

class ProductWarrantyRepository extends BaseRepository
{
    public function __construct(

    ) {

    }

    /**
    * @var App\Models\ProductWarranty $modelClass;
    */
    protected $modelClass = ProductWarranty::class;

    public function getProductWarranty($product_id)
    {
        return $this->getModel()->where('products_id', '=', $product_id)->get();
    }

    public function createWarranty($request, $product_id)
    {
        $data                   = $request->except(['_token']);
        $data['products_id']    = $product_id;

        $this->getModel()->create($data);
    }
}
