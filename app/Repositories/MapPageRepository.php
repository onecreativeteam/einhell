<?php
namespace App\Repositories;

use App\Models\MapPage;

class MapPageRepository extends BaseRepository
{
    /**
    * @var App\Models\MapPage $modelClass;
    */
    protected $modelClass = MapPage::class;
}
