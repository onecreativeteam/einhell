<?php
namespace App\Repositories;

use App\Models\ProductTranslation;

class ProductTranslationRepository extends BaseRepository
{
    /**
    * @var App\Models\ProductTranslation $modelClass;
    */
    protected $modelClass = ProductTranslation::class;
}
