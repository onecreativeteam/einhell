<?php
namespace App\Repositories;

use App\Models\ProductCategory;

class ProductCategoryRepository extends BaseRepository
{
    /**
    * @var App\Models\ProductCategory $modelClass;
    */
    protected $modelClass = ProductCategory::class;

    public function getProductCategory($id)
    {
        return $this->getModel()->find($id);
    }
}
