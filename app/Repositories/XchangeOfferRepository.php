<?php
namespace App\Repositories;

use App\Models\XchangeOffer;

class XchangeOfferRepository extends BaseRepository
{
    /**
    * @var App\Models\XchangeOffer $modelClass;
    */
    protected $modelClass = XchangeOffer::class;
}
