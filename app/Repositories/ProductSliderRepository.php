<?php
namespace App\Repositories;

use App\Models\ProductSlider;

class ProductSliderRepository extends BaseRepository
{
    /**
    * @var App\Models\ProductSlider $modelClass;
    */
    protected $modelClass = ProductSlider::class;
}
