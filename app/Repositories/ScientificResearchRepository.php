<?php
namespace App\Repositories;

use App\Models\ScientificResearch;

class ScientificResearchRepository extends BaseRepository
{
    /**
    * @var App\Models\ScientificResearch $modelClass;
    */
    protected $modelClass = ScientificResearch::class;
}
