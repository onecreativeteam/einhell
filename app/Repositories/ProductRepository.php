<?php
namespace App\Repositories;

use App\Models\Product;

use Config;
use DB;

use App\Services\FileUploadService;

class ProductRepository extends BaseRepository
{
    /**
    * @var App\Models\Product $modelClass;
    */
    protected $modelClass = Product::class;

    public function getProduct($id)
    {
        return $this->getModel()->find($id);
    }

    public function getUserSearch($string)
    {
        $productQuery = $this->getModel()
                            ->where('product_number', 'like', '%' . $string . '%')
                            ->orWhere('name', 'like', '%' . $string . '%');

        $productQuery = self::applyFilters($productQuery);

        return $productQuery->with('pictures')->orderBy('price', 'asc')->paginate(9);
    }

    public function applyFilters($query)
    {
        $filters        = session('filters');

        if (!is_null($filters)) {

            if (isset($filters['price']['from']) || isset($filters['price']['to'])) {

                $query->where(function ($q) use ($filters) {

                    $q->where('promo_product', '=', '0')->where(function ($q2) use ($filters) {
                        $q2->where('price', '>=', $filters['price']['from'])->where('price', '<=', $filters['price']['to']);
                    })->orWhere('promo_product', '=', '1')->where(function ($qq2) use ($filters) {
                        $qq2->where('promo_price', '>=', $filters['price']['from'])->where('promo_price', '<=', $filters['price']['to']);
                    });

                });

            }

            if (isset($filters['serie']) && count($filters['serie'])) {

                $query->whereIn('product_series_id', $filters['serie']);

            }

            if (isset($filters['xchange'])) {

                $query->where('xchange', 1);

            }

        }

        return $query;
    }

    public function paginateProducts($paginate = null)
    {
        $defaultLocale  = Config::get('app.default_locale');

        $productQuery = $this->getModel()->newQuery();

        $productQuery = self::applyFilters($productQuery);

        if ($defaultLocale == app()->getLocale()) {
            
            $records = $productQuery->paginate($paginate);

        } else {

            $records = $productQuery
                            ->paginate($paginate)->map(function ($item) {
                                foreach ($item->translatableFields as $field) {
                                    $item->$field = $item->translated->$field;
                                }
                                return $item;
                            });

        }

        return $records;
    }

    public function getCategoryProducts($slug, $paginate)
    {
        $cat_id = DB::table('product_categories')->where('slug', $slug)->first()->id;

        $productQuery = $this->getModel()->newQuery()->where('product_categories_id', '=', $cat_id);

        $productQuery = self::applyFilters($productQuery);

        return $productQuery->with('pictures')->paginate($paginate);
    }

    public function getXchangeCategoryProducts($slug, $paginate)
    {
        $cat_id = DB::table('product_categories')->where('slug', $slug)->first()->id;

        $productQuery = $this->getModel()->newQuery()->where('xchange', '=', '1')->where('product_categories_id', '=', $cat_id);

        $productQuery = self::applyFilters($productQuery);

        return $productQuery->paginate($paginate);
    }

    public function getBaseCategoryProducts($slug, $paginate)
    {
        $cat_id = DB::table('base_categories')->where('slug', '=', $slug)->first()->id;

        $subCategoriesArr = DB::table('product_categories')->where('base_categories_id', '=', $cat_id)->pluck('id');

        if (count($subCategoriesArr) > 0) {
            $productQuery = $this->getModel()->whereIn('product_categories_id', $subCategoriesArr)->orWhere('base_categories_id', '=', $cat_id);
        } else {
            $productQuery = $this->getModel()->where('base_categories_id', '=', $cat_id);
        }

        $productQuery = self::applyFilters($productQuery);
        
        return $productQuery->paginate($paginate);
    }

    public function getPromotionProducts($paginate)
    {
        $productQuery = $this->getModel()->newQuery()->where('promo_product', '=', '1');

        $productQuery = self::applyFilters($productQuery);

        return $productQuery->with('pictures')->paginate($paginate);
    }

    public function getSearchProducts(array $where)
    {
        $defaultLocale  = Config::get('app.default_locale');

        if ($defaultLocale == app()->getLocale()) {

            $records = $this->getModel()
                        ->where($where[0], $where[1], $where[2])
                        ->whereHas('pictures')
                        ->with(['pictures'])
                        ->get();

        } else {

            $records = $this->getModel()
                        ->where($where[0], $where[1], $where[2])
                        ->whereHas('translated')
                        ->whereHas('pictures')
                        ->with(['translated', 'pictures'])
                        ->get()->map(function ($item) {
                            foreach ($item->translatableFields as $field) {
                                $item->$field = $item->translated->$field;
                            }
                            return $item;
                        });

        }

        return $records;
    }

    public function getDataTableProducts()
    {
        return $this->getModel()->with(['category', 'serie'])->get();
    }

    public function getDataTableOrderProducts()
    {
        return $this->getModel()->where('quantity', '>', '1')->get();
    }

    public function createProductFiles($request, $id)
    {
        if ($request->hasFile('file')) {

            $files = [];
            $path = $this->getModel()->filePath;

            foreach ($request->file as $k => $file) {
                
                $files[$k]['resource']    = FileUploadService::uploadFile($file, $path);
                $files[$k]['type']        = 'file';
                $files[$k]['products_id'] = $id;

            }

            return $files;
        }
    }

    public function getExcelExportData()
    {
        return $this->getModel()->select(
                                'id',
                                'product_categories_id',
                                'product_series_id',
                                'base_warranty_id',
                                'name',
                                'description',
                                'technical_description',
                                'price',
                                'quantity',
                                'product_number',
                                'top_product',
                                'promo_product',
                                'promo_price',
                                'xchange',
                                'picture')->get();
    }

    public function uploadProductPicturesExcel($request)
    {
        if ($request->hasFile('excel_pictures')) {

            $path = $this->getModel()->filePathAttribute;

            foreach ($request->excel_pictures as $picture) {
                FileUploadService::uploadProductExcelPicture($picture, $path);
            }

        }
    }
    
}
