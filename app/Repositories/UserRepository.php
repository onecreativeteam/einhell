<?php
namespace App\Repositories;

use App\Models\User;

use Auth;

class UserRepository extends BaseRepository
{
    protected $modelClass = User::class;

    public function getUser()
    {
        return $this->getModel()->find(Auth::user()->id);
    }

    /**
     *  Get Current User Email
     */
    public function getUserEmail()
    {
        return Auth::user()->email;
    }

    /**
     *  Get User Type [admin, basic']
     */
    public function getUserType()
    {
        return $this->get()->role;
    }

    /**
     *  Get Current User Email
     */
    public function getUsersList($role)
    {
        return $this->getModel()->where(['role' => $role])->get();
    }

    /**
     *  Create Admin Profile
     */
    public function createUser($request)
    {
        $data = $request->all();
        
        return $this->getModel()->create(
            [
                'first_name' => $data['first_name'] ? $data['first_name'] : '',
                'last_name' => $data['last_name'] ? $data['last_name'] : '',
                'phone' => $data['phone'] ? $data['phone'] : '',
                'adress' => $data['adress'] ? $data['adress'] : '',
                'accepted_tc' => isset($data['accepted_tc']) ? $data['accepted_tc'] : 1,
                'has_user_discount' => isset($data['has_user_discount']) ? @$data['has_user_discount'] : 0,
                'user_discount_value' => $data['user_discount_value'] ? @$data['user_discount_value'] : 0,
                'role' => 'basic',
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
            ]
        );
    }

    /**
     *  Update User Profile
     */
    public function updateProfile($request, $user)
    {
        $data = [];

        if ($request->old_password != '') {

            if (!auth()->attempt(['email' => Auth::user()->email, 'password' => $request->old_password])) {
                return redirect()->route('profile')->withErrors(['Въвели сте грешна парола.']);
            }

            $data['password']   = bcrypt($request->password);
        }

        $data['first_name'] = $request->first_name;
        $data['last_name']  = $request->last_name;
        $data['phone']      = $request->phone;
        $data['adress']     = $request->adress;
        
        $this->update($user->id, $data);

        return true;
    }

}
