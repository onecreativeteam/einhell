<?php
namespace App\Repositories;

use App\Models\XchangePage;

class XchangePageRepository extends BaseRepository
{
    /**
    * @var App\Models\XchangePage $modelClass;
    */
    protected $modelClass = XchangePage::class;
}
