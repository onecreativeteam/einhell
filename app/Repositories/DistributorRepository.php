<?php
namespace App\Repositories;

use App\Models\Distributor;

use Config;

class DistributorRepository extends BaseRepository
{
    /**
    * @var App\Models\Distributor $modelClass;
    */
    protected $modelClass = Distributor::class;

    public function getAllWithTranslationWheres(array $where, array $where2)
    {
        $defaultLocale  = Config::get('app.default_locale');

        if ($defaultLocale == app()->getLocale()) {

            $records = $this->getModel()->where($where[0],$where[1],$where[2])->where($where2[0],$where2[1],$where2[2])->get();

        } else {

            $records = $this->getModel()
                        ->whereHas('translated')
                        ->with(['translated'])
                        ->where($where[0],$where[1],$where[2])
                        ->where($where2[0],$where2[1],$where2[2])
                        ->get()->map(function ($item) {
                            foreach ($item->translatableFields as $field) {
                                $item->$field = $item->translated->$field;
                            }
                            return $item;
                        });

        }

        return $records;
    }
}
