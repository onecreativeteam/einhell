<?php
namespace App\Repositories;

use App\Models\WorldMap;

class WorldMapRepository extends BaseRepository
{
    /**
    * @var App\Models\WorldMap $modelClass;
    */
    protected $modelClass = WorldMap::class;
}
