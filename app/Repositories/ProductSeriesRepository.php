<?php
namespace App\Repositories;

use App\Models\ProductSeries;

class ProductSeriesRepository extends BaseRepository
{
    /**
    * @var App\Models\ProductSeries $modelClass;
    */
    protected $modelClass = ProductSeries::class;

    public function getProductSeries($id)
    {
        return $this->getModel()->find($id);
    }
}
