<?php
namespace App\Repositories;

use App\Models\Partner;

class PartnerRepository extends BaseRepository
{
    /**
    * @var App\Models\Partner $modelClass;
    */
    protected $modelClass = Partner::class;
}
