<?php
namespace App\Repositories;

use App\Models\InformationFile;
use App\Services\FileUploadService;

class InformationFileRepository extends BaseRepository
{
	private $fileUploadService;

    public function __construct(
        FileUploadService $fileUploadService
    ) {
        $this->fileUploadService = $fileUploadService;
    }

    /**
    * @var App\Models\ProductGallery $modelClass;
    */
    protected $modelClass = InformationFile::class;

    public function getFiles()
    {
        return $this->getModel()->all();
    }

    public function createFiles($request)
    {
        if ($request->hasFile('file')) {

            $filesArray = [];
            $filePath   = $this->getModel()->filePathAttribute;

            foreach ($request->file as $k => $file)
            {
                $filesArray[$k]['file'] = $this->fileUploadService->uploadFile($file, $filePath);
            }

        }

        $this->getModel()->insert($filesArray);
    }
}
