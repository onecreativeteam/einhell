<?php
namespace App\Repositories;

use App\Models\ProductGallery;
use App\Services\FileUploadService;

class ProductGalleryRepository extends BaseRepository
{
	private $fileUploadService;

    public function __construct(
        FileUploadService $fileUploadService
    ) {
        $this->fileUploadService = $fileUploadService;
    }

    /**
    * @var App\Models\ProductGallery $modelClass;
    */
    protected $modelClass = ProductGallery::class;

    public function getProductGallery($product_id)
    {
        return $this->getModel()->where('products_id', '=', $product_id);
    }

    public function createEmbedGallery($request, $product_id)
    {
    	$type 			= $request->type;
    	$arrayInsert 	= [];

    	switch ($type) {
		    case 'picture':

		        if ($request->hasFile('resource')) {

		        	$filePath 		= $this->getModel()->filePathAttribute;

		        	foreach ($request->resource as $k => $picture) {
            			$arrayInsert[$k]['resource'] 	= $this->fileUploadService->uploadFile($picture, $filePath);
                        $arrayInsert[$k]['type']        = $type;
            			$arrayInsert[$k]['view_order'] 	= 1;
            			$arrayInsert[$k]['products_id'] = $product_id;
		        	}

		        }

		    	break;
		    case 'video':

	        		$arrayInsert[0]['resource']    = $request->resource;
        			$arrayInsert[0]['type'] 	   = $type;
        			$arrayInsert[0]['products_id'] = $product_id;

		        break;
		}

		$this->getModel()->insert($arrayInsert);
    }

    public function getExcelExportData()
    {
        return $this->getModel()->select(
                                'id',
                                'products_id',
                                'resource')->get();
    }

    public function uploadProductGalleryPicturesExcel($request)
    {
        if ($request->hasFile('excel_gallery_pictures')) {

            $path = $this->getModel()->filePathAttribute;

            foreach ($request->excel_gallery_pictures as $picture) {
                FileUploadService::uploadProductExcelPicture($picture, $path);
            }

        }
    }

    public function getOrderedPictures($product_id)
    {
        return $this->getModel()->where('products_id', '=', $product_id)->orderBy('view_order', 'desc')->get();
    }

}
