<?php
namespace App\Repositories;

use App\Models\ScientificResearchTranslation;

class ScientificResearchTranslationRepository extends BaseRepository
{
    /**
    * @var App\Models\ScientificResearchTranslation $modelClass;
    */
    protected $modelClass = ScientificResearchTranslation::class;
}
