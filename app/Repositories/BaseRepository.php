<?php
namespace App\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

use App\Services\FileUploadService;

use Config;
use Carbon\Carbon;
use DB;

/**
 * Class BaseRepository
 *
 * @package App\Repositories
 */
class BaseRepository extends Repository
{
    public function first()
    {
        return $this->getModel()->first();
    }

    public function firstOrNew($params)
    {
        return $this->getModel()->firstOrNew($params);
    }

    public function firstOrCreate($params)
    {
        return $this->getModel()->firstOrCreate($params);
    }

    public function where(array $where)
    {
        return $this->getModel()->where($where[0], $where[1], $where[2])->firstOrFail();
    }

    public function whereAll(array $where)
    {
        return $this->getModel()->where($where[0], $where[1], $where[2])->get();
    }

    public function getTranslatedWhereWith(array $where, $with = [], $method = 'first')
    {
        $defaultLocale  = Config::get('app.default_locale');
        
        if (app()->getLocale() == $defaultLocale) {

            $records = $this->getModel()->where($where[0], $where[1], $where[2])->with($with)->$method();

        } else {

            $records = $this->getModel()
                        ->where($where[0], $where[1], $where[2])
                        ->with($with)
                        ->whereHas('translated')
                        ->$method()->map(function ($item) {
                            foreach ($item->translatableFields as $field) {
                                $item->$field = $item->translated->$field;
                            }
                            return $item;
                        });

            return $records;
        }

        return $records;
    }

    public function getRecord($array, $method = 'get')
    {
        return $this->where($array)->$method();
    }

    public function getRecordWith($where = [], $reletions = [])
    {
        $defaultLocale  = Config::get('app.default_locale');
        
        if (app()->getLocale() == $defaultLocale) {

            $record = $this->getModel()->where($where[0],$where[1],$where[2])->with($reletions)->firstOrFail();

        } else {

            $record = $this->getModel()
                        ->where($where[0],$where[1],$where[2])
                        ->whereHas('translated')
                        ->with($reletions)
                        ->get()->map(function ($item) {
                            foreach ($item->translatableFields as $field) {
                                $item->$field = $item->translated->$field;
                            }
                            return $item;
                        });

            return $record[0];
        }

        return $record;
    }

    public function updateRecord($obj, $array = [])
    {
        if (!is_object($obj)) {
            $attributes = $obj;
            $obj = $this->getRecord($attributes, 'firstOrFail');
        }

        foreach ($array as $key => $value) {
            $obj->$key = $value;
        }

        $obj->save();

        return $obj;
    }
    
    public function pluck($value, $key = null)
    {
        return ($key) ? $this->getModel()->pluck($value, $key)->all() : $this->getModel()->pluck($value)->all();
    }

    public function destroyWithFile($id)
    {
        $record     = $this->getModel()->find($id);
        $attribute  = $this->getModel()->fileAttribute;

        $object = DB::transaction(function () use ($record, $attribute) {

            if (file_exists($record->$attribute)) {
                unlink($record->$attribute);
            }

            $deleted = $record->delete();

            return $deleted;
        });

        return $object;
    }

    /**
     * Get all records with relevant translations
     * Setup :
     * In the model in need to have defined -
     * @var translatableFields
     * @var translatableKey
     */
    public function getAllWithTranslation()
    {
        $defaultLocale  = Config::get('app.default_locale');

        if ($defaultLocale == app()->getLocale()) {

            $records = $this->getModel()->get();

        } else {

            $records = $this->getModel()
                        ->whereHas('translated')
                        ->with(['translated'])
                        ->get()->map(function ($item) {
                            foreach ($item->translatableFields as $field) {
                                $item->$field = $item->translated->$field;
                            }
                            return $item;
                        });

        }

        return $records;
    }

    /**
     * Create with relevant translations
     * Setup :
     * In the model in need to have defined -
     * @var translatableFields
     * @var translatableKey
     * @var filePathAttribute - needed if u have file in main table
     */
    public function createWithTranslations($request, $sluggable = 'name')
    {
        $parentData         = $request->except(['translations','_token']);
        $translationData    = array_get($request->only(['translations']), 'translations');
        $defaultLocale      = Config::get('app.default_locale');

        if (!array_key_exists($defaultLocale, $translationData)) {
            return redirect()->back()->withErrors(['Невъзможно създаване на ресурс. Липсват преводи.']);
        }

        $parentData         = array_merge($parentData, $translationData[$defaultLocale]);

        if (array_key_exists($sluggable, $parentData)) {
            $parentData['slug']     = str_slug($parentData[$sluggable].'_'.Carbon::now()->format('i:s'));
            $parentData['locale']   = $defaultLocale;
            unset($translationData[$defaultLocale]);
        } else {
            return redirect()->back()->withErrors['Невъзможно създаването на уникален линк към ресурса.'];
        }

        if ($request->hasFile($this->getModel()->fileAttribute)) {
            $file = $this->getModel()->fileAttribute;
            $filePath = $this->getModel()->filePathAttribute;
            $parentData[$this->getModel()->fileAttribute] = FileUploadService::uploadFile($request->$file, $filePath);
        }
        
        $createdParent = DB::transaction(function () use ($parentData, $translationData, $sluggable) {

            $createdParent = $this->create($parentData);

            if (count($translationData)) {

                foreach ($translationData as $locale => $translation) {
                    $translationData[$locale]['locale'] = $locale;
                    $translationData[$locale]['slug'] = str_slug($translation[$sluggable].'_'.Carbon::now()->format('i:s'));
                    $translationData[$locale][$this->getModel()->translatableKey] = $createdParent->id;

                    $createdParent->translations()->create($translationData[$locale]);
                }

            }

            return $createdParent;
        });

        return $createdParent;
    }

    public function updateWithTranslations($request, $id, $sluggable = 'name')
    {
        $parentData         = $request->except(['translations','_token','_method','file', 'pictogram']);
        $translationData    = array_get($request->only(['translations']), 'translations');
        $defaultLocale      = Config::get('app.default_locale');

        $parent = $this->find($id);
        if (is_null($parent)) return redirect()->back()->withErrors['Невъзможно редактиране. Не е намерен запис.'];

        if (!array_key_exists($defaultLocale, $translationData)) {
            return redirect()->back()->withErrors(['Невъзможно редактиране на ресурс. Липсват преводи.']);
        }

        $parentData         = array_merge($parentData, $translationData[$defaultLocale]);

        if (array_key_exists($sluggable, $parentData)) {
            $parentData['slug']     = str_slug($parentData[$sluggable].'_'.Carbon::now()->format('i:s'));
            $parentData['locale']   = $defaultLocale;
            unset($translationData[$defaultLocale]);
        } else {
            return redirect()->back()->withErrors['Невъзможно създаване на уникален линк към ресурса.'];
        }

        if ($request->hasFile($this->getModel()->fileAttribute)) {

            $file = $this->getModel()->fileAttribute;
            $filePath = $this->getModel()->filePathAttribute;

            if ($parent->$file && file_exists($parent->$file)) {
                unlink($parent->$file);
            }
            
            $parentData[$this->getModel()->fileAttribute] = FileUploadService::uploadFile($request->$file, $filePath);

        }

        $updatedParent = DB::transaction(function () use ($parent, $parentData, $translationData, $sluggable) {

            $updatedParent = $this->updateRecord($parent, $parentData);
            
            if (count($translationData)) {

                foreach ($translationData as $locale => $translation) {

                    $translationData[$locale]['locale'] = $locale;
                    $translationData[$locale]['slug'] = str_slug($translation[$sluggable].'_'.Carbon::now()->format('i:s'));
                    $translationData[$locale][$this->getModel()->translatableKey] = $updatedParent->id;

                    if (isset($updatedParent->translations[$locale])) {
                        $this->updateRecord($updatedParent->translations[$locale], $translationData[$locale]);
                    } else {
                        $updatedParent->translations()->create($translationData[$locale]);
                    }

                }

            }

            return $updatedParent;
        });
    }

}