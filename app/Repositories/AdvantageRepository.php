<?php
namespace App\Repositories;

use App\Models\Advantage;

class AdvantageRepository extends BaseRepository
{
    /**
    * @var App\Models\Advantage $modelClass;
    */
    protected $modelClass = Advantage::class;
}
