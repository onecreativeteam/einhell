<?php
namespace App\Repositories;

use App\Models\OrderProduct;

class OrderProductRepository extends BaseRepository
{
    /**
    * @var App\Models\OrderProduct $modelClass;
    */
    protected $modelClass = OrderProduct::class;
}
