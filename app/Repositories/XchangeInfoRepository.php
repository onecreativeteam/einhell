<?php
namespace App\Repositories;

use App\Models\XchangeInfo;

use App\Services\FileUploadService;

use File;

class XchangeInfoRepository extends BaseRepository
{
	public function __construct(FileUploadService $fileService)
    {
        $this->fileService = $fileService;
    }

    /**
    * @var App\Models\XchangeInfo $modelClass;
    */
    protected $modelClass = XchangeInfo::class;

    public function createSlider($request)
    {
        $data = $request->all();

        if ($request->hasFile('slider_picture')) {
            $data['slider_picture'] = $this->fileService->uploadFile($data['slider_picture'], $this->getModel()->filePathAttribute);
        }

        $record = $this->getModel()->create($data);

        return $record;
    }

    public function updateSlider($request, $slider)
    {
        $data = $request->all();

        if ($request->hasFile('slider_picture')) {

            if (!is_null($slider->slider_picture) && file_exists($slider->slider_picture)) {
                File::delete($slider->slider_picture);
            }

            $data['slider_picture'] = $this->fileService->uploadFile($data['slider_picture'], $this->getModel()->filePathAttribute);

        }

        $record = $slider->update($data);

        return $record;
    }
}
