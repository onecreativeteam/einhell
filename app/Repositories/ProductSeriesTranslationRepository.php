<?php
namespace App\Repositories;

use App\Models\ProductSeriesTranslation;

class ProductSeriesTranslationRepository extends BaseRepository
{
    /**
    * @var App\Models\ProductSeriesTranslation $modelClass;
    */
    protected $modelClass = ProductSeriesTranslation::class;
}
