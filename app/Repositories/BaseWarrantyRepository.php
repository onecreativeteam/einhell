<?php
namespace App\Repositories;

use App\Models\BaseWarranty;

class BaseWarrantyRepository extends BaseRepository
{
    public function __construct(

    ) {

    }

    /**
    * @var App\Models\BaseWarranty $modelClass;
    */
    protected $modelClass = BaseWarranty::class;

    public function createWarranty($request)
    {
        $data = $request->except(['_token']);

        $this->getModel()->create($data);
    }
}
