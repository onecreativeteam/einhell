<?php
namespace App\Repositories;

use App\Models\Test;

class TestRepository extends BaseRepository
{
    /**
    * @var App\Models\Test $modelClass;
    */
    protected $modelClass = Test::class;

    public function getTest($id)
    {
        return $this->getModel()->find($id);
    }
}
