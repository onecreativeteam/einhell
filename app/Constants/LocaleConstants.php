<?php
namespace App\Constants;

class LocaleConstants
{
    const LOCALE = 'bg';
    const FALLBACK_LOCALE = 'en';
}