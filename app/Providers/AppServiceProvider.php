<?php

namespace App\Providers;

use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;
use Config;

use App\Constants\LocaleConstants;
use App\Models\BaseCategory;
use App\Models\SystemFile;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Request $request)
    {
        if (!array_key_exists($request->segment(1), config('app.locales'))) {
            app()->setLocale(config('app.locale'));
        } else {
            app()->setLocale($request->segment(1));
        }

        $admin_options = Config::get('admin_nav');
        $supportedLocales = Config::get('app.locales');

        view()->composer('layouts.parts.nav_admin', function($view) use ($admin_options){
            $view->with('admin_options',$admin_options);
        });

        view()->composer('layouts.app_admin', function($view) use ($supportedLocales) {
            $view->with('supportedLocales', $supportedLocales);
        });

        view()->composer('layouts.*', function($view)
        {
            $view->with('baseCategories', BaseCategory::where('show_in_menu', '=', 1)->take(2)->get())
                 ->with('filesTC', SystemFile::where('type', '=', 'tc')->first())
                 ->with('filesFAQ', SystemFile::where('type', '=', 'faq')->first());
        });

        view()->composer('product', function($view)
        {
            $view->with('baseCategories', BaseCategory::where('show_in_menu', '=', 1)->take(2)->get());
        });

        view()->composer('admin.*', function($view) {
            $view->with('default_locale', Config::get('app.default_locale'))
                 ->with('fallback_locale', Config::get('app.fallback_locale'));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
