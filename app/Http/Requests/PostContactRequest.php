<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'theme_id' => 'required|exists:contact_themes,id',
            'first_name' => 'required|string|max:255|min:3',
            'last_name' => 'string|max:255|min:3',
            'phone' => 'max:20|min:6',
            'adress' => 'string|max:255|min:1',
            'email' => 'required|email',
            'product_number' => 'string|max:255|min:1',
            'message' => 'required',
        ];
    }
}
