<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'translations.*.name' => 'required|max:255|min:1',
            'translations.*.owner' => 'required|max:255|min:1',
            'translations.*.email' => 'required|max:255|min:1',
            'translations.*.phone' => 'required|max:255|min:1',
            'translations.*.mobile_phone' => 'required|max:255|min:1'
        ];
    }
}
