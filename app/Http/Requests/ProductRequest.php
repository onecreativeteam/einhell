<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_categories_id' => 'required|exists:product_categories,id',
            'product_series_id' => 'required|exists:product_series,id',
            'translations.*.name' => 'required|max:255|min:1',
            'translations.*.description' => 'sometimes',
            'translations.*.technical_description' => 'sometimes',
            'price' => 'required|numeric',
            'promo_price' => 'required_if:promo_product,1|numeric',
            'quantity' => 'required|integer',
            'product_number' => 'required'
        ];
    }
}
