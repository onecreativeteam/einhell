<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostOrderInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'email' => 'sometimes|required|email',
            'phone' => 'required|regex:/^[0-9\-\+\s\(\)]{10,15}$/|min:10|max:15',
            'adress' => 'required|min:3',
            'postcode' => 'required|min:3',
            'payment_method' => 'sometimes|required|in:cash,vpos',
            'order_comment' => '',
            'is_company' => 'sometimes|boolean',
            'company_name' => 'required_if:is_company,1',
            'company_mol' => 'required_if:is_company,1',
            'company_bulstat' => 'required_if:is_company,1',
            'company_adress' => 'required_if:is_company,1',
            'add_dds' => 'sometimes|boolean'
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => 'Името е задължително поле',
            'last_name.required'  => 'Фамилията е задължително поле',
            'email.required'  => 'Email е задължително поле',
            'email.email'  => 'Невалиден Email формат',
            'phone.required'  => 'Телефон е задължително поле',
            'phone.regex'  => 'Невалиден Телефонен формат',
            'phone.min'  => 'Телефонът неможе да е по-къс от 10 символа',
            'phone.max'  => 'Телефонът неможе да е по-дълъг от 15 символа',
            'adress.required'  => 'Адрес е задължително поле',
            'adress.min'  => 'Адрес неможе да е по-къс от 3 символа',
            'postcode.required'  => 'Пощенски код е задължително поле',
            'postcode.min'  => 'Пощенски код неможе да е по-къс от 3 символа',
            'payment_method.required' => 'Метода на плащане е задължителен. Изберете опция.',
            'payment_method.in' => 'Невалиден метод на плащане. Изберете опция.',
            'company_name.required_if' => 'Името на компанията е задължително поле.',
            'company_mol.required_if' => 'МОЛ е задължително поле.',
            'company_bulstat.required_if' => 'Булстат е задължително поле.',
            'company_adress.required_if' => 'Адрес на компанията е задължително поле.',
        ];
    }
}
