<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DistributorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'translations.*.name' => 'required|max:255|min:1',
            'translations.*.adress' => 'max:255|min:1',
            'translations.*.phone' => 'max:255|min:1',
            'translations.*.email' => 'max:255|min:1',
            // 'cities_id' => 'sometimes|exists:cities,id',
            'type' => 'in:shop,service,diller',
            'gps_lat' => 'numeric',
            'gps_lon' => 'numeric',
            'gps_zoom' => 'sometimes|numeric',
            'show_in_contact' => 'sometimes|boolean',
            'show_in_contact_only' => 'sometimes|boolean'
        ];
    }
}
