<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:255|min:2',
            'last_name' => 'required|max:255|min:2',
            'phone' => 'required|regex:/^[0-9\-\+\s\(\)]{10,15}$/|min:10|max:15',
            'adress' => 'sometimes|max:255|min:1',
            'email' => 'sometimes|email|max:255|unique:users',
            'old_password' => 'required_with_all:password,password_confirmation|min:6',
            'password' => 'required_with_all:old_password|min:6|confirmed',
            'password_confirmation' => 'required_with_all:old_password,password|min:6',
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => 'Въведете име.',
            'first_name.max' => 'Името е прекалено дълго.',
            'first_name.min' => 'Името е прекалено кратко.',
            'last_name.required' => 'Въведете фамилия.',
            'last_name.max' => 'Фамилията е прекалено дълга.',
            'last_name.min' => 'Фамилията е прекалено кратка.',
            'phone.required' => 'Въведете телефон.',
            'phone.regex' => 'Телефонният номер е в неправилен формат.',
            'phone.min' => 'Телефонният номер е прекалено кратък.',
            'phone.max' => 'Телефонният номер е прекалено дълъг.',
            'email.email' => 'Невалиден емайл формат.',
            'email.unique' => 'Вече съществува потребител с този емайл.',
            'old_password.required_with_all' => 'Не е въведена стара парола.',
            'old_password.min' => 'Старата Парола трябва да е поне 6 символа.',
            'password.required' => 'Паролата е задължителна.',
            'password.confirmed' => 'Моля въведете еднакви пароли.',
            'password.min' => 'Паролата трябва да е поне 6 символа.',
            'password_confirmation.min' => 'Потвърдената парола трябва да е поне 6 символа.',
            //'password_confirmation.same' => 'Нова парола и повтори нова парола не съвпадат.',
        ];
    }
}
