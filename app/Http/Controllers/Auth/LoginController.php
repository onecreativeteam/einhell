<?php
namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        logout as performLogout;
    }

    protected function validateLogin(Request $request)
    {
        $message = array(
            'email.required' => 'Моля въведете емайл.',
            'email.email' => 'Невалиден емайл формат.',
            'email.exists' => 'Не е намерен запис с този емайл.',
            'password.required' => 'Паролата е задължителна.',
        );

        $this->validate($request, [
            $this->username() => 'required|exists:users,email|email', 'password' => 'required',
        ], $message);
    }

    /**
     * Where to redirect users after login / registration.
     */
    protected function authenticated(Request $request, $user)
    {
        if ( $user->role == 'admin' ) {
            return redirect()->route('admin.home');
        }

        return redirect()->route('home');
    }

    public function logout(Request $request)
    {
        $this->performLogout($request);
        
        return redirect()->route('home');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }
}
