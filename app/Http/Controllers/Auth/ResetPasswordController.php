<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Get the password reset validation error messages.
     *
     * @return array
     */
    protected function validationErrorMessages()
    {
        return [
            'password.required' => 'Паролата е задължителна.',
            'password.confirmed' => 'Моля въведете еднакви пароли.',
            'password.min' => 'Паролата трябва да е минимум 6 символа.',
            'email.email' => 'Невалиден емайл формат.',
            'email.required' => 'Емайл е задължителен.',
            'token.required' => 'Липсва токен. Моля повторете процеса за промяна на парола.',
        ];
    }

    protected function redirectTo()
    {
        return route('home');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
}
