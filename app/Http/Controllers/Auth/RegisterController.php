<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Validator;

use App\Services\SendMailService;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected function redirectTo()
    {
        return route('home');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $message = array(
            // 'first_name.required' => 'Въведете име.',
            // 'first_name.max' => 'Името е прекалено дълго.',
            // 'first_name.min' => 'Името е прекалено кратко.',
            // 'last_name.required' => 'Въведете фамилия.',
            // 'last_name.max' => 'Фамилията е прекалено дълга.',
            // 'last_name.min' => 'Фамилията е прекалено кратка.',
            // 'phone.required' => 'Въведете телефон.',
            // 'phone.regex' => 'Телефонният номер е в неправилен формат.',
            // 'phone.min' => 'Телефонният номер е прекалено кратък.',
            // 'phone.max' => 'Телефонният номер е прекалено дълъг.',
            'accepted_tc.required' => 'Моля приемете условията на сайта.',
            'register_email.required' => 'Моля въведете емайл.',
            'register_email.email' => 'Невалиден емайл формат.',
            'register_email.unique' => 'Вече съществува потребител с този емайл.',
            'password.required' => 'Паролата е задължителна.',
            'password.confirmed' => 'Моля въведете еднакви пароли.',
            'password.min' => 'Паролата трябва да е поне 6 символа.',
        );

        return Validator::make($data, [
            // 'first_name' => 'required|max:255|min:2',
            // 'last_name' => 'required|max:255|min:2',
            // 'phone' => 'required|regex:/^[0-9\-\+\s\(\)]{10,15}$/|min:10|max:15',
            //'adress' => 'sometimes|max:255|min:1',
            'accepted_tc' => 'required',
            'role' => 'sometimes|in:basic,admin',
            'register_email' => 'required|email|max:255|unique:users,email',
            'password' => 'required|min:6|confirmed',
        ], $message);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            // 'first_name' => $data['first_name'] ? $data['first_name'] : '',
            // 'last_name' => $data['last_name'] ? $data['last_name'] : '',
            // 'phone' => $data['phone'] ? $data['phone'] : '',
            //'adress' => $data['adress'] ? $data['adress'] : '',
            'accepted_tc' => $data['accepted_tc'] ? $data['accepted_tc'] : 1,
            'role' => 'basic',
            'email' => $data['register_email'],
            'password' => bcrypt($data['password']),
        ]);

        SendMailService::sendRegistrationMail($user);

        session()->flash('success', 'Успешна регистрация. Благодарим Ви!');

        return $user;
    }
}
