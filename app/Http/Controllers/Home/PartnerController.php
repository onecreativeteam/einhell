<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;

use App\Repositories\DistributorRepository;
use App\Repositories\ContactRepository;

use Illuminate\Http\Request;

class PartnerController extends Controller
{
    private $distributorsRepo;
    private $contactRepository;

    public function __construct(
        DistributorRepository $distributorsRepo,
        ContactRepository $contactRepository
    ) {
        $this->distributorsRepo = $distributorsRepo;
        $this->contactRepository = $contactRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('partners')
                ->with('mapData', $this->distributorsRepo->getAllWithTranslation()->toArray())
                ->with('mainShops', $this->distributorsRepo->getAllWithTranslationWheres(['type', '=', 'shop'], ['show_in_contact', '=', '1']))
                ->with('mainServices', $this->distributorsRepo->getAllWithTranslationWheres(['type', '=', 'service'], ['show_in_contact', '=', '1']))
                ->with('mainDillers', $this->distributorsRepo->getAllWithTranslationWheres(['type', '=', 'diller'], ['show_in_contact', '=', '1']))
                ->with('otherDistributors', $this->distributorsRepo->getTranslatedWhereWith(['show_in_contact', '=', '0'], [], 'get'));
    }
}
