<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\UpdateUserProfileRequest;

use Auth;

use App\Repositories\UserRepository;
use App\Repositories\OrderRepository;

use Illuminate\Database\Eloquent\Collection;

class UserProfileController extends Controller
{
    private $userRepo;
    private $orderRepo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        UserRepository $userRepo,
        OrderRepository $orderRepo
    ) {
        $this->userRepo = $userRepo;
        $this->orderRepo = $orderRepo;

        return Auth::check();
    }

    /**
     * Show user profile page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = [];
        $counter = 1;
        foreach ($this->orderRepo->whereAll(['user_id', '=', Auth::user()->id]) as $order)
        {
            foreach ($order->orderProducts as $k => $orderProduct) {
                $counter += $counter;

                $orderProduct->product->order_id = $order->id;
                $orderProduct->product->product_order_quantity = $orderProduct->product_quantity;
                $orderProduct->product->product_order_s_price = $orderProduct->p_single_price;
                $orderProduct->product->product_order_t_price = $orderProduct->p_total_price;

                $products[$counter] = $orderProduct->product;
            }
        }

        return view('client.profile.index')
                        ->with('orderProducts', $products)
                        ->with('user', Auth::user());
    }

    public function update(UpdateUserProfileRequest $request)
    {
        $updated = $this->userRepo->updateProfile($request, Auth::user());
        
        if ($updated === true) {
            session()->flash('flash_success', 'Успешно променихте информацията в профила си.');
        }

        return redirect()->route('profile');
    }

    public function indexShipping()
    {
        return view('client.profile.shipping')->with('user', Auth::user());
    }

    public function viewSingleOrder($id)
    {
        return view('view-order')->with('order', $this->orderRepo->find($id));
    }
}
