<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;

use App\Repositories\ContactRepository;
use App\Repositories\ContactThemeRepository;

use App\Repositories\DistributorRepository;

use App\Http\Requests\PostContactRequest;

use App\Services\SendMailService;

use Illuminate\Http\Request;

class ContactController extends Controller
{
    private $contactRepository;
    private $contactThemeRepository;
    private $distributorsRepo;
    private $sendMailService;

    public function __construct(
        ContactRepository $contactRepository,
        ContactThemeRepository $contactThemeRepository,
        DistributorRepository $distributorsRepo,
        SendMailService $sendMailService
    ) {
        $this->contactRepository = $contactRepository;
        $this->contactThemeRepository = $contactThemeRepository;
        $this->distributorsRepo = $distributorsRepo;
        $this->sendMailService = $sendMailService;
    }

    public function index()
    {
        return view('contact')
                    ->with('themeSelect', $this->contactThemeRepository->pluck('name', 'id'))
                    ->with('mainShops', $this->distributorsRepo->getAllWithTranslationWheres(['type', '=', 'shop'], ['show_in_contact_only', '=', '1']))
                    ->with('mainServices', $this->distributorsRepo->getAllWithTranslationWheres(['type', '=', 'service'], ['show_in_contact_only', '=', '1']))
                    ->with('contacts', $this->contactRepository->getAllWithTranslation());
    }

    public function store(PostContactRequest $request)
    {
        $this->sendMailService->sendContactMail($request, $this->contactThemeRepository->find($request->theme_id)->name);

        session()->flash('success', 'Вашето запитване беше изпратено успешно.');

        return redirect()->back();
    }
}
