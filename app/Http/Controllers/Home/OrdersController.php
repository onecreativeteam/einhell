<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;

use App\Repositories\ProductRepository;
use App\Repositories\OrderRepository;
use App\Repositories\OrderProductRepository;

use App\Services\SendMailService;

use Illuminate\Http\Request;

use Auth;
use DB;
use Session;
use Cart;
use PDF;

class OrdersController extends Controller
{
    private $productRepo;
    private $orderRepo;
    private $orderProductRepo;
    private $sendMailService;

    public function __construct(
        ProductRepository $productRepo,
        OrderRepository $orderRepo,
        OrderProductRepository $orderProductRepo,
        SendMailService $sendMailService
    ) {
        $this->productRepo = $productRepo;
        $this->orderRepo = $orderRepo;
        $this->orderProductRepo = $orderProductRepo;
        $this->sendMailService = $sendMailService;
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function postOrder()
    {
        $cartContent    = Cart::content();
        $orderInfo      = session()->get('orderInfo');
        $orderObj       = self::constructOrderObject($orderInfo, Cart::subtotal());

        $createdOrder   = DB::transaction(function () use ($orderObj, $cartContent) {

            $order = $this->orderRepo->create($orderObj);

            if (count($cartContent)) {
                foreach ($cartContent as $row) {

                    $orderProductRow                         = array();
                    $orderProductRow['orders_id']            = $order->id;
                    $orderProductRow['products_id']          = $row->id;
                    $orderProductRow['base_warranty_id']     = $row->options->base_warranty_id ? $row->options->base_warranty_id : 1;
                    $orderProductRow['has_product_warranty'] = '0';
                    $orderProductRow['product_warranty_id']  = '';
                    $orderProductRow['product_quantity']     = $row->qty;
                    $orderProductRow['promo_product']        = $row->options->promo;
                    $orderProductRow['p_single_price']       = $row->price;
                    $orderProductRow['p_total_price']        = $row->price * $row->qty;

                    $order->orderProducts()->create($orderProductRow);

                    $product = $this->productRepo->find($row->id);
                    $product->quantity = $product->quantity - $row->qty;
                    $product->save();
                }
            }

            return $order;
        });
        
        if ($createdOrder->is_company == '1') {
            $pdfStream = PDF::loadView('facture', ['order' => $createdOrder])->stream();
        } else {
            $pdfStream = null;
        }
        
        $this->sendMailService->sendOrderConfirmationMail($createdOrder, $pdfStream);
        $this->sendMailService->sendOrderAdminMail($createdOrder);

        session()->forget('orderInfo');

        Cart::destroy();

        return redirect()->route('cart.thankyou');
    }

    public function constructOrderObject($data, $orderTotal)
    {
        $orderObject = array();

        if (Auth::check()) {
            if (Auth::user()->has_user_discount == '1') {

                $discount = number_format( ($orderTotal * (Auth::user()->user_discount_value / 100) ), 2 );
                $totalWithDiscount = $orderTotal - $discount;

                $orderObject['has_user_discount']    = 1;
                $orderObject['user_discount_value']  = Auth::user()->user_discount_value;
                $orderObject['order_discount']       = $discount;
                $orderObject['order_discount_total'] = $totalWithDiscount;
            }
        }

        $orderObject['user_id']         = Auth::user() ? Auth::user()->id : 'guest';
        $orderObject['status']          = 'new';
        $orderObject['first_name']      = $data['first_name'];
        $orderObject['last_name']       = $data['last_name'];
        $orderObject['email']           = $data['email'];
        $orderObject['phone']           = $data['phone'];
        $orderObject['adress']          = $data['adress'];
        $orderObject['postcode']        = $data['postcode'];
        $orderObject['payment_method']  = $data['payment_method'];
        $orderObject['order_comment']   = $data['order_comment'];

        if ($data['is_company'] == '1') {
            $orderObject['is_company']      = $data['is_company'];
            $orderObject['company_name']    = $data['company_name'];
            $orderObject['company_bulstat'] = $data['company_bulstat'];
            $orderObject['company_mol']     = $data['company_mol'];
            $orderObject['company_adress']  = $data['company_adress'];
            $orderObject['add_dds']         = @$data['add_dds'] ? $data['add_dds'] : 0;
        }

        $orderObject['order_total']     = $orderTotal;

        return $orderObject;
    }

}
