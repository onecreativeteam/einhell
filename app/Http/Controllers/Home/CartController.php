<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;

use App\Repositories\ProductRepository;

use Illuminate\Http\Request;

use App\Http\Requests\PostOrderInfoRequest;

use Session;
use Auth;
use Cart;

class CartController extends Controller
{
    private $productRepo;

    public function __construct(
        ProductRepository $productRepo
    ) {
        $this->productRepo  = $productRepo;
    }

    public function index()
    {
        if (!count(Cart::content())) {
            return redirect()->route('home');
        }

        $categories = array();
        foreach (Cart::content() as $row) {
            if (!array_key_exists($row->options->product_categories_id, $categories)) {
                $categories[$row->options->product_categories_id] = $row->options->product_categories_id;
            }
        }

        return view('cart')
                ->with('topProducts', $this->productRepo->getModel()->newQuery()->whereIn('product_categories_id', $categories)->get()->take(20))
                ->with('cart', Cart::content());
    }

    public function add($product_id)
    {
        $product = $this->productRepo->find($product_id);
        
        if ($product->quantity > 0) {
            
            Cart::add([
                'id' => $product->id,
                'name' => $product->name,
                'qty' => 1,
                'price' => ($product->promo_product == '1') ? $product->promo_price : $product->price,
                'options' => [
                    'product_number' => $product->product_number,
                    'picture' => count($product->pictures) ? $product->pictures[0]->resource : '',
                    'promo' => $product->promo_product,
                    'quantity' => $product->quantity,
                    // 'base_warranty_id' => $product->baseWarranty->id,
                    'has_product_warranty' => '',
                    'product_warranty_id' => '',
                    'product_categories_id' => $product->product_categories_id,
                ]
            ]);

            session()->flash('success', 'Успешно добавен продукт в количка.');

            return redirect()->back();
        }

        return redirect()->back()->withErrors(['Недостатъчна наличност']);
    }

    public function remove($row_id)
    {
        Cart::remove($row_id);

        session()->flash('success', 'Успешно премахнат продукт от количка.');
                
        return redirect()->back();
    }

    public function updateQuantity($row_id, $qty)
    {
        Cart::update($row_id, $qty);
                
        return redirect()->back();
    }

    public function orderInfo()
    {
        if (!count(Cart::content())) {
            return redirect()->back()->withErrors(['Нямате добавени продукти в количка.']);
        }

        return view('order-info')->with('orderInfo', session('orderInfo'));
    }

    public function postOrderInfo(PostOrderInfoRequest $request)
    {
        if (!count(Cart::content())) {
            return redirect()->back()->withErrors(['Нямате добавени продукти в количка.']);
        }

        session()->put('orderInfo', $request->except(['_token']));

        return redirect()->route('cart.checkout');
    }

    public function checkout()
    {
        if (is_null(session()->get('orderInfo')) || !count(Cart::content())) {
            return redirect()->route('home')->withErrors(['Не са попълнени данни за поръчка']);
        }

        return view('checkout')->with('orderInfo', session()->get('orderInfo'));
    }

    public function thankYouPage()
    {
        return view('thankyou');
    }
}
