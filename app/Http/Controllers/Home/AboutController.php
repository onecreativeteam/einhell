<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;

use App\Repositories\InformationPageRepository;
use App\Repositories\CoverPhotoRepository;

use App\Repositories\PartnerRepository;
use App\Repositories\WorldMapRepository;
use App\Repositories\InformationFileRepository;
use App\Repositories\MapPageRepository;

use Illuminate\Http\Request;

class AboutController extends Controller
{
    private $pageRepo;
    private $coverPhotoRepo;

    private $partnerRepo;
    private $mapRepo;
    private $informationFileRepo;
    private $mapPageRepo;

    public function __construct(
        InformationPageRepository $pageRepo,
        CoverPhotoRepository $coverPhotoRepo,
        PartnerRepository $partnerRepo,
        WorldMapRepository $mapRepo,
        InformationFileRepository $informationFileRepo,
        MapPageRepository $mapPageRepo
    ) {
        $this->pageRepo = $pageRepo;
        $this->coverPhotoRepo = $coverPhotoRepo;
        $this->partnerRepo = $partnerRepo;
        $this->mapRepo = $mapRepo;
        $this->informationFileRepo = $informationFileRepo;
        $this->mapPageRepo = $mapPageRepo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('about')
                ->with('partners', $this->partnerRepo->getAllWithTranslation())
                ->with('map', $this->mapRepo->pluck('code'))
                ->with('mapPage', $this->mapPageRepo->first())
                ->with('files', $this->informationFileRepo->getAllWithTranslation())
                ->with('coverPhoto', $this->coverPhotoRepo->scopeType('about'))
                ->with('pages', $this->pageRepo->getAllWithTranslation());
    }
}
