<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;

use App\Repositories\ProductRepository;
use App\Repositories\BaseCategoryRepository;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\ProductSeriesRepository;

use Illuminate\Http\Request;

class ProductsController extends Controller
{
    private $productRepo;
    private $baseCategoryRepo;
    private $productCategoryRepo;
    private $seriesRepo;

    public function __construct(
        ProductRepository $productRepo,
        BaseCategoryRepository $baseCategoryRepo,
        ProductCategoryRepository $productCategoryRepo,
        ProductSeriesRepository $seriesRepo
    ) {
        $this->productRepo   = $productRepo;
        $this->baseCategoryRepo = $baseCategoryRepo;
        $this->productCategoryRepo = $productCategoryRepo;
        $this->seriesRepo  = $seriesRepo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('product')
                ->with('maxPrice', $this->productRepo->getModel()->max('price'))
                ->with('categories', $this->baseCategoryRepo->getAllWithTranslation())
                ->with('category', null)
                ->with('baseCategory', null)
                ->with('series', $this->seriesRepo->getAllWithTranslation()->where('add_to_filter', '=', 1))
                ->with('products', $this->productRepo->paginateProducts(9));
    }

    public function inner($slug)
    {
        $product = @$this->productRepo->getTranslatedWhereWith(['slug', '=', $slug], ['files', 'category.category']);
        $similar = @$this->productRepo->getTranslatedWhereWith(['product_categories_id', '=', $product->product_categories_id], ['files'], 'get');

        return view('product-inner')
                ->with('maxPrice', $this->productRepo->getModel()->max('price'))
                ->with('product', $product)
                ->with('category', $product->category)
                ->with('baseCategory', $product->category->category)
                ->with('similarProducts', $similar)
                ->with('series', $this->seriesRepo->getAllWithTranslation()->where('add_to_filter', '=', 1))
                ->with('categories', $this->baseCategoryRepo->getAllWithTranslation());
    }

    public function category($slug)
    {
        $category = $this->productCategoryRepo->getTranslatedWhereWith(['slug', '=', $slug], ['category']);

        return view('product')
                ->with('maxPrice', $this->productRepo->getModel()->max('price'))
                ->with('category', $category)
                ->with('baseCategory', $category->category)
                ->with('products', $this->productRepo->getCategoryProducts($slug, 9))
                ->with('series', $this->seriesRepo->getAllWithTranslation()->where('add_to_filter', '=', 1))
                ->with('categories', $this->baseCategoryRepo->getAllWithTranslation());
    }

    public function xchangeCategory($slug)
    {
        $category = $this->productCategoryRepo->getTranslatedWhereWith(['slug', '=', $slug], ['category']);

        return view('product')
                ->with('maxPrice', $this->productRepo->getModel()->max('price'))
                ->with('category', $category)
                ->with('baseCategory', $category->category)
                ->with('products', $this->productRepo->getXchangeCategoryProducts($slug, 9))
                ->with('series', $this->seriesRepo->getAllWithTranslation()->where('add_to_filter', '=', 1))
                ->with('categories', $this->baseCategoryRepo->getAllWithTranslation());
    }

    public function baseCategory($slug)
    {
        return view('product')
                ->with('maxPrice', $this->productRepo->getModel()->max('price'))
                ->with('baseCategory',$this->baseCategoryRepo->getTranslatedWhereWith(['slug', '=', $slug], ['category']))
                ->with('category', null)
                ->with('products', $this->productRepo->getBaseCategoryProducts($slug, 9))
                ->with('series', $this->seriesRepo->getAllWithTranslation()->where('add_to_filter', '=', 1))
                ->with('categories', $this->baseCategoryRepo->getAllWithTranslation());
    }

    public function series()
    {
        return view('product')
                ->with('maxPrice', $this->productRepo->getModel()->max('price'))
                ->with('products', $this->productRepo->paginateProducts(9))
                ->with('category', null)
                ->with('baseCategory', null)
                ->with('series', $this->seriesRepo->getAllWithTranslation()->where('add_to_filter', '=', 1))
                ->with('categories', $this->baseCategoryRepo->getAllWithTranslation());
    }

    public function promotions()
    {
        return view('product')
                ->with('maxPrice', $this->productRepo->getModel()->max('price'))
                ->with('products', $this->productRepo->getPromotionProducts(9))
                ->with('category', null)
                ->with('baseCategory', null)
                ->with('series', $this->seriesRepo->getAllWithTranslation()->where('add_to_filter', '=', 1))
                ->with('categories', $this->baseCategoryRepo->getAllWithTranslation());
    }
}
