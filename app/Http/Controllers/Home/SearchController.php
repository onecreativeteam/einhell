<?php
namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests\AjaxSearchRequest;

use App\Repositories\ProductRepository;

use Response;

class SearchController extends Controller
{
    private $productRepo;

    public function __construct(
        ProductRepository $productRepo
    ) {
        $this->productRepo = $productRepo;
    }

    public function setFilter(AjaxSearchRequest $request)
    {
        $data = $request->all();

        $filters = session()->get('filters');

        if ($request->source == 'serie') {

            if (isset($filters[$data['source']]) && !empty($filters[$data['source']])) {
                $k = max(array_keys($filters[$data['source']])) + 1;
            } else {
                $k = 1;
            }

            $filters[$data['source']][$k] = $data['filter'];

        }

        if ($request->source == 'price') {

            $filters[$data['source']] = $data['filter'];

        }

        if ($request->source == 'xchange') {

            $filters[$data['source']] = $data['filter'];

        }

        session()->put('filters', $filters);

        //$results = self::search($filters);

        return Response::json([
            'status'        => 'success',
            'data'          => $filters,
            //'referer'       => self::getPreviousRouteName($request),
            // 'results'       => $results
        ], 200);
    }

    public function unsetFilter(AjaxSearchRequest $request)
    {
        $data = $request->all();

        $filters = session()->get('filters');

        if (isset($filters[$data['source']])) {

            if ($data['source'] == 'xchange') {
                unset($filters[$data['source']]);
            } else {

                if(($key = array_search($data['filter'], $filters[$data['source']])) !== false) {
                    unset($filters[$data['source']][$key]);
                }

            }

            
        }

        session()->put('filters', $filters);

        //$results = self::search($filters);

        return Response::json([
            'status'        => 'success',
            'data'          => $filters,
            //'referer'       => self::getPreviousRouteName($request),
            // 'results'       => $results
        ], 200);
    }

    // public function loadFilters()
    // {
    //     $filters = session('filters');
        
    //     $results = self::search($filters);

    //     return Response::json([
    //         'status'        => 'success',
    //         'data'          => $filters,
    //         'results'       => $results
    //     ], 200);
    // }

    public function clearFilters(Request $request)
    {
        session()->forget('filters');

        $filters = [];
        //$results = self::search($filters);

        return Response::json([
            'status'        => 'success',
            'data'          => $filters,
            // 'results'       => $results
        ], 200);
    }

    public function getPreviousRouteName($request)
    {
        $previous_url = app('Illuminate\Routing\UrlGenerator')->previous();

        $previous_request = app('request')->create($previous_url);

        $parameters = app('router')->getRoutes()->match($previous_request)->parameters();

        $route_name = app('router')->getRoutes()->match($previous_request)->getName();

        return route($route_name, $parameters);
    }

}
