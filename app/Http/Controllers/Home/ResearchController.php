<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;

use App\Repositories\ScientificResearchRepository;
use App\Repositories\CoverPhotoRepository;

use Illuminate\Http\Request;

class ResearchController extends Controller
{
    private $researchRepo;
    private $coverPhotoRepo;

    public function __construct(
        ScientificResearchRepository $researchRepo,
        CoverPhotoRepository $coverPhotoRepo
    ) {
        $this->researchRepo = $researchRepo;
        $this->coverPhotoRepo = $coverPhotoRepo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('research')
                ->with('coverPhoto', $this->coverPhotoRepo->scopeType('research'))
                ->with('research', $this->researchRepo->getAllWithTranslation());
    }
}
