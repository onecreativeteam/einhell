<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;

use App\Repositories\WorldMapRepository;
use App\Repositories\ProductRepository;
use App\Repositories\BaseCategoryRepository;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\AdvantageRepository;
use App\Repositories\XchangePageRepository;
use App\Repositories\ProductSliderRepository;
use App\Repositories\ProductSeriesRepository;
use App\Services\SearchService;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    private $worldMap;
    private $productRepo;
    private $baseCategoryRepo;
    private $productCategoryRepo;
    private $advantageRepo;
    private $xchangePageRepo;
    private $sliderRepo;
    private $seriesRepo;
    private $searchService;

    public function __construct(
        WorldMapRepository $worldMap,
        ProductRepository $productRepo,
        BaseCategoryRepository $baseCategoryRepo,
        ProductCategoryRepository $productCategoryRepo,
        AdvantageRepository $advantageRepo,
        XchangePageRepository $xchangePageRepo,
        ProductSliderRepository $sliderRepo,
        ProductSeriesRepository $seriesRepo,
        SearchService $searchService
    ) {
        $this->worldMap      = $worldMap;
        $this->productRepo   = $productRepo;
        $this->baseCategoryRepo = $baseCategoryRepo;
        $this->productCategoryRepo = $productCategoryRepo;
        $this->advantageRepo = $advantageRepo;
        $this->xchangePageRepo = $xchangePageRepo;
        $this->sliderRepo = $sliderRepo;
        $this->seriesRepo = $seriesRepo;
        $this->searchService = $searchService;
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home')
                ->with('sliders', $this->sliderRepo->getAllWithTranslation())
                ->with('baseCategories', $this->baseCategoryRepo->getTranslatedWhereWith(['show_in_homepage', '=', '1'], [], 'get')->take(2))
                ->with('promoProducts', $this->productRepo->getTranslatedWhereWith(['promo_product', '=', '1'], [], 'get'))
                ->with('topProducts', $this->productRepo->getTranslatedWhereWith(['top_product', '=', '1'], [], 'get'));
    }

    public function facture()
    {
        return view('facture')->with('order', \App\Models\Order::find(63));
    }

    public function search(Request $request)
    {
        return $this->searchService->search($request);
    }

    public function searchPage(Request $request)
    {
        return view('product')
                ->with('maxPrice', $this->productRepo->getModel()->max('price'))
                ->with('categories', $this->baseCategoryRepo->getAllWithTranslation())
                ->with('category', null)
                ->with('baseCategory', null)
                ->with('series', $this->seriesRepo->getAllWithTranslation()->where('add_to_filter', '=', 1))
                ->with('products', $this->productRepo->getUserSearch($request->user_search_string));
    }

    public function xChangePage()
    {
        return view('power_Xchange')
                ->with('sliders', $this->sliderRepo->getTranslatedWhereWith(['xchange', '=', '1'], ['product'], 'get'))
                ->with('categories', $this->productCategoryRepo->getTranslatedWhereWith(['xchange', '=', '1'], [], 'get'))
                ->with('advantages', $this->advantageRepo->getAllWithTranslation())
                ->with('xchange', $this->xchangePageRepo->getAllWithTranslation());
    }
}
