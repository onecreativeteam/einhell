<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Socialite;

use App\Services\SocialAccountService;

class SocialAuthController extends Controller
{
    private $socialAccountService;

    public function __construct(
        SocialAccountService $socialAccountService
    ) {
        $this->socialAccountService = $socialAccountService;
    }

    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }   

    public function callback(Request $request, SocialAccountService $service, $provider)
    {
        $socialiteUser = Socialite::driver($provider)->user();

        $check = $this->socialAccountService->validateExistingUser($socialiteUser);
        
        if ($check === false) {
            return redirect()->route('login')->withErrors(['Този емайл вече е използван за регистрация. Моля опитайте да влезете в профила си.']);
        }

        try{

            $user = $this->socialAccountService->createOrGetUser($socialiteUser, $provider);

        } catch (\Exception $e) {

            return redirect()->route('login')->withErrors(['Something went wrong or You have rejected the app!']);

        }

        auth()->login($user);

        return redirect()->route('home');
    }

}