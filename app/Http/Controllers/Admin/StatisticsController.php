<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Repositories\ProductRepository;
use App\Repositories\OrderRepository;

use Illuminate\Http\Request;

class StatisticsController extends Controller
{
    private $productsRepo;
    private $ordersRepo;

    public function __construct(
        ProductRepository $productsRepo,
        ProductRepository $ordersRepo
    ) {
        $this->productsRepo = $productsRepo;
        $this->ordersRepo = $ordersRepo;
    }

    public function productQuantity()
    {
        return view('admin.statistics.product_quantity')
                ->with('products', $this->productsRepo->whereAll(['quantity', '<', '3']));
    }

    public function newOrders()
    {
        return view('admin.statistics.new_orders');
    }
}
