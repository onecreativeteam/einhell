<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\PartnersRequest;

use App\Repositories\PartnerRepository;

use Config;

class PartnersController extends Controller
{
    private $partnerRepo;

    public function __construct(
        PartnerRepository $partnerRepo
    ) {
        $this->partnerRepo = $partnerRepo;
    }

    public function index()
    {
        return view('admin.partners.index')
                ->with('partners', $this->partnerRepo->getAllWithTranslation());
    }

    public function create()
    {
        return view('admin.partners.create')
                ->with('supportedLocales', Config::get('app.locales'));
    }

    public function store(PartnersRequest $request)
    {
        $this->partnerRepo->createWithTranslations($request);

        session()->flash('success', 'Успешно създадено партньор.');

        return redirect()->route('admin.list.partners');
    }

    public function edit($id)
    {
        return view('admin.partners.edit')
            ->with('partner', $this->partnerRepo->find($id))
            ->with('supportedLocales', Config::get('app.locales'));
    }

    public function update(PartnersRequest $request, $id)
    {
        $this->partnerRepo->updateWithTranslations($request, $id);

        session()->flash('success', 'Успешно редактиран партньор.');

        return redirect()->route('admin.list.partners');
    }

    public function destroy($id)
    {
        $this->partnerRepo->delete($id);

        return redirect()->route('admin.list.partners');
    }
}
