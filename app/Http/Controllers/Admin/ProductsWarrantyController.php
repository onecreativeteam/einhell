<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
// use App\Http\Requests\ProductWarrantyRequest;

use App\Repositories\ProductRepository;
use App\Repositories\ProductWarrantyRepository;

use Config;

class ProductsWarrantyController extends Controller
{
    private $productsRepo;
    private $productsWarrantyRepo;

    public function __construct(
        ProductRepository $productsRepo,
        ProductWarrantyRepository $productsWarrantyRepo
    ) {
        $this->productsRepo        = $productsRepo;
        $this->productsWarrantyRepo = $productsWarrantyRepo;
    }

    public function create($product_id)
    {
        $product = $this->productsRepo->find($product_id);

        return view('admin.products.warranty.create')->with('product', $product);
    }

    public function store(Request $request, $product_id)
    {
        $this->productsWarrantyRepo->createWarranty($request, $product_id);

        session()->flash('success', 'Успешно добавена гаранция.');

        return redirect()->route('admin.create.product.warranty', [$product_id]);
    }

    public function update(Request $request, $product_id, $id)
    {
        $this->productsWarrantyRepo->update($id, $request->except(['_method','_token']));

        session()->flash('success', 'Успешно редактирана гаранция.');

        return redirect()->route('admin.create.product.warranty', [$product_id]);
    }

    public function destroy($product_id, $id)
    {
        $this->productsWarrantyRepo->delete($id);

        return redirect()->route('admin.create.product.warranty', [$product_id]);
    }
}
