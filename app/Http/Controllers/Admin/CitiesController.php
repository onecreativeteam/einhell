<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\CityRequest;

use App\Repositories\CityRepository;

use Config;

class CitiesController extends Controller
{
    private $cityRepo;

    public function __construct(
        CityRepository $cityRepo
    ) {
        $this->cityRepo = $cityRepo;
    }

    public function index()
    {
        return view('admin.cities.index')
                ->with('cities', $this->cityRepo->getAllWithTranslation());
    }

    public function create()
    {
        return view('admin.cities.create')
                ->with('supportedLocales', Config::get('app.locales'));
    }

    public function store(CityRequest $request)
    {
        $this->cityRepo->createWithTranslations($request);

        session()->flash('success', 'Успешно създаден град.');

        return redirect()->route('admin.list.cities');
    }

    public function edit($id)
    {
        return view('admin.cities.edit')
            ->with('city', $this->cityRepo->find($id))
            ->with('supportedLocales', Config::get('app.locales'));
    }

    public function update(CityRequest $request, $id)
    {
        $this->cityRepo->updateWithTranslations($request, $id);

        session()->flash('success', 'Успешно редактиран град.');

        return redirect()->route('admin.list.cities');
    }

    public function destroy($id)
    {
        $this->cityRepo->delete($id);

        return redirect()->route('admin.list.cities');
    }
}
