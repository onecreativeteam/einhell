<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
// use App\Http\Requests\ContactThemeRequest;

use App\Repositories\ContactThemeRepository;

use Config;

class ContactThemesController extends Controller
{
    private $contactThemeRepo;

    public function __construct(
        ContactThemeRepository $contactThemeRepo
    ) {
        $this->contactThemeRepo = $contactThemeRepo;
    }

    public function index()
    {
        return view('admin.contact_theme.index')
                ->with('themes', $this->contactThemeRepo->getAllWithTranslation());
    }

    public function create()
    {
        return view('admin.contact_theme.create')
                ->with('supportedLocales', Config::get('app.locales'));
    }

    public function store(Request $request)
    {
        $this->contactThemeRepo->createWithTranslations($request);

        session()->flash('success', 'Успешно създадена базова категория.');

        return redirect()->route('admin.list.contact.theme');
    }

    public function edit($id)
    {
        return view('admin.contact_theme.edit')
            ->with('theme', $this->contactThemeRepo->find($id))
            ->with('supportedLocales', Config::get('app.locales'));
    }

    public function update(Request $request, $id)
    {
        $this->contactThemeRepo->updateWithTranslations($request, $id);

        session()->flash('success', 'Успешно редактирана базова категория.');

        return redirect()->route('admin.list.contact.theme');
    }

    public function destroy($id)
    {
        $this->contactThemeRepo->delete($id);

        return redirect()->route('admin.list.contact.theme');
    }
}
