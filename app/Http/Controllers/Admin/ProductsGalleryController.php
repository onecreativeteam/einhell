<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\ProductGalleryRequest;

use App\Repositories\ProductRepository;
use App\Repositories\ProductGalleryRepository;

use Config;

class ProductsGalleryController extends Controller
{
    private $productsRepo;
    private $productsGalleryRepo;

    public function __construct(
        ProductRepository $productsRepo,
        ProductGalleryRepository $productsGalleryRepo
    ) {
        $this->productsRepo        = $productsRepo;
        $this->productsGalleryRepo = $productsGalleryRepo;
    }

    public function create($product_id)
    {
        $product = $this->productsRepo->find($product_id);

        return view('admin.products.gallery.create')
                ->with('pictures', $this->productsGalleryRepo->getOrderedPictures($product_id))
                ->with('videos', $product->videos)
                ->with('product', $product);
    }

    public function store(ProductGalleryRequest $request, $product_id)
    {
        $this->productsGalleryRepo->createEmbedGallery($request, $product_id);

        session()->flash('success', 'Успешно добавени елементи в галерия.');

        return redirect()->route('admin.create.product.gallery', [$product_id]);
    }

    public function update(ProductGalleryRequest $request, $product_id, $id)
    {
        $this->productsGalleryRepo->update($id, $request->except(['_method','_token']));

        session()->flash('success', 'Успешно редактирано видео.');

        return redirect()->route('admin.create.product.gallery', [$product_id]);
    }

    public function updateOrder(Request $request, $id)
    {
        $this->productsGalleryRepo->update($id, $request->except(['_method','_token']));

        session()->flash('success', 'Успешно редактирана подредба.');

        return redirect()->back();
    }

    public function destroy($product_id, $id)
    {
        $this->productsGalleryRepo->destroyWithFile($id);

        return redirect()->route('admin.create.product.gallery', [$product_id]);
    }
}
