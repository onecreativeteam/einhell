<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\ProductSeriesRequest;

use App\Repositories\ProductSeriesRepository;

use Config;

class ProductSeriesController extends Controller
{
    private $productSeriesRepo;

    public function __construct(
        ProductSeriesRepository $productSeriesRepo
    ) {
        $this->productSeriesRepo = $productSeriesRepo;
    }

    public function index()
    {
        return view('admin.product_series.index')
                ->with('series', $this->productSeriesRepo->getAllWithTranslation());
    }

    public function create()
    {
        return view('admin.product_series.create')
                ->with('supportedLocales', Config::get('app.locales'));
    }

    public function store(ProductSeriesRequest $request)
    {
        if (!isset($request->add_to_filter)) {
            $request->request->add(['add_to_filter' => 0]);
        }
        
        $this->productSeriesRepo->createWithTranslations($request);

        session()->flash('success', 'Успешно създадена серия.');

        return redirect()->route('admin.list.product.series');
    }

    public function edit($id)
    {
        return view('admin.product_series.edit')
            ->with('serie', $this->productSeriesRepo->find($id))
            ->with('supportedLocales', Config::get('app.locales'));
    }

    public function update(ProductSeriesRequest $request, $id)
    {
        if (!isset($request->add_to_filter)) {
            $request->request->add(['add_to_filter' => 0]);
        }

        $this->productSeriesRepo->updateWithTranslations($request, $id);

        session()->flash('success', 'Успешно редактирана серия.');

        return redirect()->route('admin.list.product.series');
    }

    public function destroy($id)
    {
        $this->productSeriesRepo->delete($id);

        return redirect()->route('admin.list.product.series');
    }
}
