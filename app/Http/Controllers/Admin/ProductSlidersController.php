<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\ProductSliderRequest;

use App\Repositories\ProductRepository;
use App\Repositories\ProductSliderRepository;

use Config;

class ProductSlidersController extends Controller
{
    private $productsRepo;
    private $productsSliderRepo;

    public function __construct(
        ProductRepository $productsRepo,
        ProductSliderRepository $productsSliderRepo
    ) {
        $this->productsRepo       = $productsRepo;
        $this->productsSliderRepo = $productsSliderRepo;
    }

    public function index()
    {
        return view('admin.products.sliders.index')
                ->with('sliders', $this->productsSliderRepo->getAllWithTranslation());
    }

    public function create()
    {
        // $autoCompleteSelectValues = '[';
        // foreach ($this->productsRepo->pluck('name', 'id') as $k => $v) {
        //     $autoCompleteSelectValues .= '{ label: "'.$v.'", value: "'.$k.'" },';
        // }
        // $autoCompleteSelectValues .= ']';

        return view('admin.products.sliders.create')
                ->with('productsSelect', $this->productsRepo->pluck('name', 'id'))
                ->with('supportedLocales', Config::get('app.locales'));
    }

    public function store(ProductSliderRequest $request)
    {
        $this->productsSliderRepo->createWithTranslations($request);

        session()->flash('success', 'Успешно създаден слайдер.');

        return redirect()->route('admin.list.product.sliders');
    }

    public function edit($id)
    {
        return view('admin.products.sliders.edit')
            ->with('slider', $this->productsSliderRepo->find($id))
            ->with('productsSelect', $this->productsRepo->pluck('name', 'id'))
            ->with('supportedLocales', Config::get('app.locales'));
    }

    public function update(ProductSliderRequest $request, $id)
    {
        if (!isset($request->xchange)) $request->request->add(['xchange' => 0]);

        $this->productsSliderRepo->updateWithTranslations($request, $id);

        session()->flash('success', 'Успешно редактиран слайдер.');

        return redirect()->route('admin.list.product.sliders');
    }

    public function destroy($id)
    {
        $this->productsSliderRepo->delete($id);

        return redirect()->route('admin.list.product.sliders');
    }
}
