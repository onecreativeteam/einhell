<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
// use App\Http\Requests\XchangePageRequest;

use App\Repositories\XchangePageRepository;

use Config;

class XChangePageController extends Controller
{
    private $pageRepo;

    public function __construct(
        XchangePageRepository $pageRepo
    ) {
        $this->pageRepo = $pageRepo;
    }

    public function index()
    {
        return view('admin.xchange.index')
                ->with('slides', $this->pageRepo->getAllWithTranslation());
    }

    public function create()
    {
        return view('admin.xchange.create')
                // ->with('page', $this->pageRepo->first())
                ->with('supportedLocales', Config::get('app.locales'));
    }

    public function store(Request $request)
    {
        // $resource = $this->pageRepo->first();

        // if (is_null($resource)) {
            $this->pageRepo->createWithTranslations($request);
        // } else {
            // $this->pageRepo->updateWithTranslations($request, $resource->id);
        // }

        session()->flash('success', 'Успешно добавени данни.');

        return redirect()->route('admin.list.xchange');
    }

    public function edit($id)
    {
        return view('admin.xchange.edit')
            ->with('slide', $this->pageRepo->find($id))
            ->with('supportedLocales', Config::get('app.locales'));
    }

    public function update(Request $request, $id)
    {
        $this->pageRepo->updateWithTranslations($request, $id);

        session()->flash('success', 'Успешно редактиран слайд.');

        return redirect()->route('admin.list.xchange');
    }

    public function destroy($id)
    {
        $this->pageRepo->destroyWithFile($id);

        return redirect()->route('admin.list.xchange');
    }

}
