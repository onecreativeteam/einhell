<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Repositories\UserRepository;

use Config;

class UsersController extends Controller
{
    private $userRepo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        UserRepository $userRepo
    ) {
        $this->userRepo = $userRepo;
    }

    public function index()
    {
        return view('admin.users.index')->with('users', $this->userRepo->all());
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(Request $request)
    {
        $this->userRepo->createUser($request);

        session()->flash('success', 'Успешно създаден потребител.');

        return redirect()->route('admin.list.users');
    }

    public function edit($id)
    {
        return view('admin.users.edit')
            ->with('user', $this->userRepo->find($id));
    }

    public function update(Request $request, $id)
    {   
        if (!isset($request->has_user_discount)) $request->request->add(['has_user_discount' => 0]);
        
        $user = $this->userRepo->find($id);

        $this->userRepo->updateRecord($user, $request->except(['_method', '_token']));

        session()->flash('success', 'Успешно редактиран потребител.');

        return redirect()->route('admin.list.users');
    }

    public function destroy($id)
    {
        $this->userRepo->delete($id);

        return redirect()->route('admin.list.users');
    }
}
