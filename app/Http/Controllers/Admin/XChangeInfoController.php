<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Repositories\XchangeInfoRepository;

class XChangeInfoController extends Controller
{
    private $xchangeInfoRepo;

    public function __construct(
        XchangeInfoRepository $xchangeInfoRepo
    ) {
        $this->xchangeInfoRepo = $xchangeInfoRepo;
    }

    public function create()
    {
        return view('admin.xchange_info.index')
                ->with('data', @$this->xchangeInfoRepo->first());
    }

    public function store(Request $request)
    {
        if (is_null($this->xchangeInfoRepo->first())) {
            $this->xchangeInfoRepo->create($request->all());
        } else {
            $this->xchangeInfoRepo->update($this->xchangeInfoRepo->first()->id, $request->all());
        }
        
        session()->flash('success', 'Success.');

        return redirect()->route('admin.create.xchangeinfo');
    }

    public function getSliderInfo()
    {
        return view('admin.xchange_info.slider')
                ->with('data', @$this->xchangeInfoRepo->first());
    }

    public function postSliderInfo(Request $request)
    {
        if (is_null($this->xchangeInfoRepo->first())) {
            $this->xchangeInfoRepo->createSlider($request);
        } else {
            $this->xchangeInfoRepo->updateSlider($request, $this->xchangeInfoRepo->first());
        }
        
        session()->flash('success', 'Success.');

        return redirect()->route('admin.xchangeinfo.slider');
    }
}
