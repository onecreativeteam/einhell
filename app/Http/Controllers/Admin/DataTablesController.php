<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Repositories\ProductRepository;
use App\Repositories\UserRepository;
use App\Repositories\DistributorRepository;
use App\Repositories\OrderRepository;

use Illuminate\Http\Request;

use Datatables;

class DataTablesController extends Controller
{
    private $productsRepo;
    private $userRepo;
    private $distributorRepo;
    private $orderRepo;

    public function __construct(
        ProductRepository $productsRepo,
        UserRepository $userRepo,
        DistributorRepository $distributorRepo,
        OrderRepository $orderRepo
    ) {
        $this->productsRepo = $productsRepo;
        $this->userRepo = $userRepo;
        $this->distributorRepo = $distributorRepo;
        $this->orderRepo = $orderRepo;
    }

    public function productDataTable()
    {
        return Datatables::of($this->productsRepo->getDataTableProducts())->add_column('operations',
            '<a class="btn btn-sm blue pull-left" href="products/{{$id}}/edit"><i class="fa fa-edit"></i> Редактирай</a>
            <a class="btn btn-sm blue pull-left" href="products/{{$id}}/embed-gallery/create"><i class="fa fa-edit"></i> Галерия</a>
            <form class="pull-left" method="POST" action="products/{{$id}}" accept-charset="UTF-8">
                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                <input name="_method" type="hidden" value="DELETE">
                <button class="btn btn-sm red deleteRowBttn" type="submit"><i class="fa fa-trash-o"></i> Изтрий</button>
            </form>')->make(true);
    }

    public function editOrderProductDataTable(Request $request)
    {
        $order_id = $request->order_id;

        return Datatables::of($this->productsRepo->getDataTableOrderProducts())->add_column('operations',
            '<form class="pull-left" method="POST" action="{{route("admin.order.add.product", ['.$order_id.', $id])}}" accept-charset="UTF-8">
                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                <input name="_method" type="hidden" value="POST">
                <button class="btn btn-sm red deleteRowBttn" type="submit"><i class="fa fa-trash-o"></i> Добави към поръчка</button>
            </form>')->make(true);
    }

    public function userDataTable()
    {
        return Datatables::of($this->userRepo->getUsersList('basic'))->add_column('operations',
            '<a class="btn btn-sm blue pull-left" href="users/{{$id}}/edit"><i class="fa fa-edit"></i> Редактирай</a>')->make(true);
    }

    public function distributorDataTable()
    {
        return Datatables::of($this->distributorRepo->getAllWithTranslation())->add_column('operations',
            '<a class="btn btn-sm blue pull-left" href="distributors/{{$id}}/edit"><i class="fa fa-edit"></i> Редактирай</a>
            <form class="pull-left" method="POST" action="distributors/{{$id}}" accept-charset="UTF-8">
                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                <input name="_method" type="hidden" value="DELETE">
                <button class="btn btn-sm red deleteRowBttn" type="submit"><i class="fa fa-trash-o"></i> Изтрий</button>
            </form>')->make(true);
    }

    public function orderDataTable()
    {
        return Datatables::of($this->orderRepo->getOrderListDataTable())
            ->add_column('operations',
                '<a class="btn btn-sm blue pull-left" href="{{route("admin.edit.order", [$id])}}"><i class="fa fa-edit"></i> Редактирай</a>
                <form class="pull-left" method="POST" action="{{route("admin.destroy.order", [$id])}}" accept-charset="UTF-8">
                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                    <input name="_method" type="hidden" value="DELETE">
                    <button class="btn btn-sm red deleteRowBttn" type="submit"><i class="fa fa-trash-o"></i> Изтрий</button>
                </form>')
            ->add_column('statuses',
                '
                <form class="pull-left" method="POST" action="{{route("admin.order.status.change", [$id,"cancelled"])}}" accept-charset="UTF-8">
                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                    <input name="_method" type="hidden" value="POST">
                    <button class="btn btn-sm btn-block btn-danger" {{ $status == "cancelled" ? "disabled" : "" }} type="submit"><i class="fa fa-trash-o"></i> Отхвърлена</button>
                </form>
                <form class="pull-left" method="POST" action="{{route("admin.order.status.change", [$id,"processed"])}}" accept-charset="UTF-8">
                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                    <input name="_method" type="hidden" value="POST">
                    <button class="btn btn-sm btn-block btn-success" {{ $status == "processed" ? "disabled" : "" }} type="submit"><i class="fa fa-exchange"></i> Обработва се</button>
                </form>
                <form class="pull-left" method="POST" action="{{route("admin.order.status.change", [$id,"finished"])}}" accept-charset="UTF-8">
                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                    <input name="_method" type="hidden" value="POST">
                    <button class="btn btn-sm btn-block btn-info" {{ $status == "finished" ? "disabled" : "" }} type="submit"><i class="fa fa-check-circle"></i> Завършена</button>
                </form>
                '
            )->make(true);
    }

    public function newOrdersDataTable()
    {
        return Datatables::of($this->orderRepo->getOrderListDataTableWhereAll(['status', '=', 'new']))
            ->add_column('operations',
            '<a class="btn btn-sm blue pull-left" href="{{route("admin.edit.order", [$id])}}"><i class="fa fa-edit"></i> Редактирай</a>
            <form class="pull-left" method="POST" action="{{route("admin.destroy.order", [$id])}}" accept-charset="UTF-8">
                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                <input name="_method" type="hidden" value="DELETE">
                <button class="btn btn-sm red deleteRowBttn" type="submit"><i class="fa fa-trash-o"></i> Изтрий</button>
            </form>')
            ->add_column('statuses',
                '
                <form class="pull-left" method="POST" action="{{route("admin.order.status.change", [$id,"cancelled"])}}" accept-charset="UTF-8">
                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                    <input name="_method" type="hidden" value="POST">
                    <button class="btn btn-sm btn-block btn-danger" {{ $status == "cancelled" ? "disabled" : "" }} type="submit"><i class="fa fa-trash-o"></i> Отхвърлена</button>
                </form>
                <form class="pull-left" method="POST" action="{{route("admin.order.status.change", [$id,"processed"])}}" accept-charset="UTF-8">
                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                    <input name="_method" type="hidden" value="POST">
                    <button class="btn btn-sm btn-block btn-success" {{ $status == "processed" ? "disabled" : "" }} type="submit"><i class="fa fa-exchange"></i> Обработва се</button>
                </form>
                <form class="pull-left" method="POST" action="{{route("admin.order.status.change", [$id,"finished"])}}" accept-charset="UTF-8">
                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                    <input name="_method" type="hidden" value="POST">
                    <button class="btn btn-sm btn-block btn-info" {{ $status == "finished" ? "disabled" : "" }} type="submit"><i class="fa fa-check-circle"></i> Завършена</button>
                </form>
                '
            )->make(true);
    }
}
