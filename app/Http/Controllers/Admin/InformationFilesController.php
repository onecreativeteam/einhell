<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\InformationFileRequest;

use App\Repositories\InformationFileRepository;

use Config;

class InformationFilesController extends Controller
{
    private $informationFileRepo;

    public function __construct(
        InformationFileRepository $informationFileRepo
    ) {
        $this->informationFileRepo = $informationFileRepo;
    }

    public function create()
    {
        return view('admin.information_files.create')
                ->with('supportedLocales', Config::get('app.locales'))
                ->with('files', $this->informationFileRepo->getFiles());
    }

    public function store(InformationFileRequest $request)
    {
        $this->informationFileRepo->createWithTranslations($request);

        session()->flash('success', 'Успешно добавени файлове.');

        return redirect()->route('admin.create.information.files');
    }

    public function destroy($id)
    {
        $this->informationFileRepo->destroyWithFile($id);

        session()->flash('success', 'Успешно изтрит файл.');

        return redirect()->route('admin.create.information.files');
    }

}
