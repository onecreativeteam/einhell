<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Repositories\ProductRepository;
use App\Repositories\XchangeOfferRepository;

use Config;

class XChangeOffersController extends Controller
{
    private $productsRepo;
    private $offersRepo;

    public function __construct(
        ProductRepository $productsRepo,
        XchangeOfferRepository $offersRepo
    ) {
        $this->productsRepo = $productsRepo;
        $this->offersRepo = $offersRepo;
    }

    public function index()
    {
        return view('admin.xchange_offers.index')
                ->with('offers', $this->offersRepo->all());
    }

    public function create()
    {
        return view('admin.xchange_offers.create')
                ->with('productsSelect', $this->productsRepo->pluck('name', 'id'))
                ->with('supportedLocales', Config::get('app.locales'));
    }

    public function store(Request $request)
    {
        $this->offersRepo->create($request->all());

        session()->flash('success', 'Успешно създадено предложение.');

        return redirect()->route('admin.list.xchangeoffers');
    }

    public function edit($id)
    {
        return view('admin.xchange_offers.edit')
            ->with('offer', $this->offersRepo->find($id))
            ->with('productsSelect', $this->productsRepo->pluck('name', 'id'))
            ->with('supportedLocales', Config::get('app.locales'));
    }

    public function update(Request $request, $id)
    {
        $this->offersRepo->update($id, $request->all());

        session()->flash('success', 'Успешно редактирано предложение.');

        return redirect()->route('admin.list.xchangeoffers');
    }

    public function destroy($id)
    {
        $this->offersRepo->delete($id);

        return redirect()->route('admin.list.xchangeoffers');
    }
}
