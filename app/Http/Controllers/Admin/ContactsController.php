<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;

use App\Repositories\ContactRepository;

use Config;

class ContactsController extends Controller
{
    private $contactsRepo;

    public function __construct(
        ContactRepository $contactsRepo
    ) {
        $this->contactsRepo = $contactsRepo;
    }

    public function index()
    {
        return view('admin.contacts.index')
                ->with('contacts', $this->contactsRepo->getAllWithTranslation());
    }

    public function create()
    {
        return view('admin.contacts.create')
                ->with('supportedLocales', Config::get('app.locales'));
    }

    public function store(ContactRequest $request)
    {
        $this->contactsRepo->createWithTranslations($request);

        session()->flash('success', 'Успешно създаден контакт.');

        return redirect()->route('admin.list.contacts');
    }

    public function edit($id)
    {
        return view('admin.contacts.edit')
            ->with('contact', $this->contactsRepo->find($id))
            ->with('supportedLocales', Config::get('app.locales'));
    }

    public function update(ContactRequest $request, $id)
    {
        $this->contactsRepo->updateWithTranslations($request, $id);

        session()->flash('success', 'Успешно редактиран контакт.');

        return redirect()->route('admin.list.contacts');
    }

    public function destroy($id)
    {
        $this->contactsRepo->delete($id);

        return redirect()->route('admin.list.contacts');
    }
}
