<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\DistributorRequest;

use App\Repositories\DistributorRepository;
use App\Repositories\CityRepository;

use Config;

class DistributorsController extends Controller
{
    private $distributorRepo;
    private $cityRepo;

    public function __construct(
        DistributorRepository $distributorRepo,
        CityRepository $cityRepo
    ) {
        $this->distributorRepo  = $distributorRepo;
        $this->cityRepo         = $cityRepo;
    }

    public function index()
    {
        return view('admin.distributors.index')
                ->with('distributors', $this->distributorRepo->getAllWithTranslation());
    }

    public function create()
    {
        return view('admin.distributors.create')
                ->with('citySelect', $this->cityRepo->pluck('name', 'id'))
                ->with('typeSelect', Config::get('distributor_types'))
                ->with('supportedLocales', Config::get('app.locales'));
    }

    public function store(DistributorRequest $request)
    {
        if (!isset($request->show_in_contact)) {
            $request->request->add(['show_in_contact' => 0]);
        }

        if (!isset($request->show_in_contact_only)) {
            $request->request->add(['show_in_contact_only' => 0]);
        }

        $this->distributorRepo->createWithTranslations($request);

        session()->flash('success', 'Успешно създаден представител.');

        return redirect()->route('admin.list.distributors');
    }

    public function edit($id)
    {
        return view('admin.distributors.edit')
                ->with('distributor', $this->distributorRepo->find($id))
                ->with('citySelect', $this->cityRepo->pluck('name', 'id'))
                ->with('typeSelect', Config::get('distributor_types'))
                ->with('supportedLocales', Config::get('app.locales'));
    }

    public function update(DistributorRequest $request, $id)
    {
        if (!isset($request->show_in_contact)) {
            $request->request->add(['show_in_contact' => 0]);
        }

        if (!isset($request->show_in_contact_only)) {
            $request->request->add(['show_in_contact_only' => 0]);
        }
        
        $this->distributorRepo->updateWithTranslations($request, $id);

        session()->flash('success', 'Успешно редактиран представител.');

        return redirect()->route('admin.list.distributors');
    }

    public function destroy($id)
    {
        $this->distributorRepo->delete($id);

        return redirect()->route('admin.list.distributors');
    }
}
