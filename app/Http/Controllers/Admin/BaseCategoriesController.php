<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\BaseCategoryRequest;

use App\Repositories\BaseCategoryRepository;
use App\Services\FileUploadService;

use Config;

class BaseCategoriesController extends Controller
{
    private $baseCategoryRepo;
    private $fileUploadService;

    public function __construct(
        BaseCategoryRepository $baseCategoryRepo,
        FileUploadService $fileUploadService
    ) {
        $this->baseCategoryRepo = $baseCategoryRepo;
        $this->fileUploadService = $fileUploadService;
    }

    public function index()
    {
        return view('admin.base_category.index')
                ->with('categories', $this->baseCategoryRepo->getAllWithTranslation());
    }

    public function create()
    {
        return view('admin.base_category.create')
                ->with('supportedLocales', Config::get('app.locales'));
    }

    public function store(BaseCategoryRequest $request)
    {
        if ($request->hasFile('pictogram')) {

            $request->request->add([
                'pictogram_picture' => $this->fileUploadService->uploadFile($request->pictogram, $this->baseCategoryRepo->getModel()->pictogramPathAttribute)
            ]);

        }

        if (isset($request->show_in_menu)) $request->request->add(['show_in_menu' => 1]);
        if (isset($request->show_in_homepage)) $request->request->add(['show_in_homepage' => 1]);

        $this->baseCategoryRepo->createWithTranslations($request);

        session()->flash('success', 'Успешно създадена базова категория.');

        return redirect()->route('admin.list.base.category');
    }

    public function edit($id)
    {
        return view('admin.base_category.edit')
            ->with('category', $this->baseCategoryRepo->find($id))
            ->with('supportedLocales', Config::get('app.locales'));
    }

    public function update(BaseCategoryRequest $request, $id)
    {   
        if ($request->hasFile('pictogram')) {

            $record = $this->baseCategoryRepo->find($id);

            if ($record->pictogram && file_exists($record->pictogram)) {
                unlink($record->pictogram);
            }

            $request->request->add([
                'pictogram_picture' => $this->fileUploadService->uploadFile($request->pictogram, $this->baseCategoryRepo->getModel()->pictogramPathAttribute)
            ]);
        }

        if (!isset($request->show_in_menu)) $request->request->add(['show_in_menu' => 0]);
        if (!isset($request->show_in_homepage)) $request->request->add(['show_in_homepage' => 0]);

        $this->baseCategoryRepo->updateWithTranslations($request, $id);

        session()->flash('success', 'Успешно редактирана базова категория.');

        return redirect()->route('admin.list.base.category');
    }

    public function destroy($id)
    {
        $this->baseCategoryRepo->delete($id);

        return redirect()->route('admin.list.base.category');
    }
}
