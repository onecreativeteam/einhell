<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\ScientificResearchsRequest;

use App\Repositories\ScientificResearchRepository;
use App\Repositories\CoverPhotoRepository;

use Config;

class ScientificResearchsController extends Controller
{
    private $scientificResearchRepo;
    private $coverPhotoRepo;

    public function __construct(
        ScientificResearchRepository $scientificResearchRepo,
        CoverPhotoRepository $coverPhotoRepo
    ) {
        $this->scientificResearchRepo = $scientificResearchRepo;
        $this->coverPhotoRepo = $coverPhotoRepo;
    }

    public function index()
    {
        return view('admin.scientific_researchs.index')
                ->with('researchs', $this->scientificResearchRepo->getAllWithTranslation());
    }

    public function coverPhoto()
    {
        return view('admin.scientific_researchs.cover_photo')
                ->with('cover', $this->coverPhotoRepo->findBy('type', 'research'));
    }

    public function create()
    {
        return view('admin.scientific_researchs.create')
                ->with('supportedLocales', Config::get('app.locales'));
    }

    public function store(ScientificResearchsRequest $request)
    {
        $this->scientificResearchRepo->createWithTranslations($request);

        session()->flash('success', 'Успешно създадено изследване.');

        return redirect()->route('admin.list.scientific.researchs');
    }

    public function edit($id)
    {
        return view('admin.scientific_researchs.edit')
            ->with('research', $this->scientificResearchRepo->find($id))
            ->with('supportedLocales', Config::get('app.locales'));
    }

    public function update(ScientificResearchsRequest $request, $id)
    {
        $this->scientificResearchRepo->updateWithTranslations($request, $id);

        session()->flash('success', 'Успешно редактирано изследване.');

        return redirect()->route('admin.list.scientific.researchs');
    }

    public function destroy($id)
    {
        $this->scientificResearchRepo->delete($id);

        return redirect()->route('admin.list.scientific.researchs');
    }
}
