<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
// use App\Http\Requests\XchangePageRequest;

use App\Repositories\MapPageRepository;

use Config;

class MapPageController extends Controller
{
    private $pageRepo;

    public function __construct(
        MapPageRepository $pageRepo
    ) {
        $this->pageRepo = $pageRepo;
    }

    public function store(Request $request)
    {
        $resource = $this->pageRepo->first();

        if (is_null($resource)) {
            $this->pageRepo->createWithTranslations($request, 'slug');
        } else {
            $this->pageRepo->updateWithTranslations($request, $resource->id, 'slug');
        }

        session()->flash('success', 'Успешно добавени данни.');

        return redirect()->route('information.page.map');
    }

}
