<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Repositories\WorldMapRepository;
use App\Repositories\ProductRepository;
use App\Repositories\OrderRepository;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    private $worldMap;
    private $productsRepo;
    private $ordersRepo;

    public function __construct(
        WorldMapRepository $worldMap,
        ProductRepository $productsRepo,
        OrderRepository $ordersRepo
    ) {
        $this->worldMap = $worldMap;
        $this->productsRepo = $productsRepo;
        $this->ordersRepo = $ordersRepo;
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.home')
                ->with('productsLowQuantityCount', $this->productsRepo->whereAll(['quantity', '<', '3'])->count())
                ->with('newOrdersCount', $this->ordersRepo->whereAll(['status', '=', 'new'])->count())
                ->with('worldMap', $this->worldMap->pluck('code'));
    }

    public function listOrders()
    {
        return view('admin.orders-list');
    }

    public function updateMap(Request $request, $method)
    {
        if ($method == 'set') {
            $this->worldMap->create($request->except(['_token']));
        } elseif ($method == 'unset') {
            $code = $this->worldMap->findBy('code', $request->code)->delete();
        }
    }
}
