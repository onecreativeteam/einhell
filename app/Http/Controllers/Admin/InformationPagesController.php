<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\InformationPagesRequest;

use App\Repositories\InformationPageRepository;
use App\Repositories\WorldMapRepository;
use App\Repositories\CoverPhotoRepository;
use App\Repositories\MapPageRepository;

use Config;

class InformationPagesController extends Controller
{
    private $informationPageRepo;
    private $worldMap;
    private $coverPhotoRepo;
    private $mapPageRepo;

    public function __construct(
        InformationPageRepository $informationPageRepo,
        WorldMapRepository $worldMap,
        CoverPhotoRepository $coverPhotoRepo,
        MapPageRepository $mapPageRepo
    ) {
        $this->informationPageRepo = $informationPageRepo;
        $this->worldMap = $worldMap;
        $this->coverPhotoRepo = $coverPhotoRepo;
        $this->mapPageRepo = $mapPageRepo;
    }

    public function index()
    {
        return view('admin.information_pages.index')
                ->with('pages', $this->informationPageRepo->getAllWithTranslation());
    }

    public function coverPhoto()
    {
        return view('admin.information_pages.cover_photo')
                ->with('cover', $this->coverPhotoRepo->findBy('type', 'about'));
    }

    public function map()
    {
        return view('admin.information_pages.map')
                ->with('mapPage', $this->mapPageRepo->first())
                ->with('worldMap', $this->worldMap->pluck('code'))
                ->with('supportedLocales', Config::get('app.locales'));
    }

    public function create()
    {
        return view('admin.information_pages.create')
                ->with('supportedLocales', Config::get('app.locales'));
    }

    public function store(InformationPagesRequest $request)
    {
        $this->informationPageRepo->createWithTranslations($request);

        session()->flash('success', 'Успешно създадена страница.');

        return redirect()->route('admin.list.information.pages');
    }

    public function edit($id)
    {
        return view('admin.information_pages.edit')
            ->with('page', $this->informationPageRepo->find($id))
            ->with('supportedLocales', Config::get('app.locales'));
    }

    public function update(InformationPagesRequest $request, $id)
    {
        $this->informationPageRepo->updateWithTranslations($request, $id);

        session()->flash('success', 'Успешно редактирана страница.');

        return redirect()->route('admin.list.information.pages');
    }

    public function destroy($id)
    {
        $this->informationPageRepo->delete($id);

        return redirect()->route('admin.list.information.pages');
    }
}
