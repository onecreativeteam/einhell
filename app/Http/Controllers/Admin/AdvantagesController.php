<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
// use App\Http\Requests\AdvantageRequest;

use App\Repositories\AdvantageRepository;

use Config;

class AdvantagesController extends Controller
{
    private $advantageRepo;

    public function __construct(
        AdvantageRepository $advantageRepo
    ) {
        $this->advantageRepo = $advantageRepo;
    }

    public function index()
    {
        return view('admin.advantages.index')
                ->with('advantages', $this->advantageRepo->getAllWithTranslation());
    }

    public function create()
    {
        return view('admin.advantages.create')
                ->with('supportedLocales', Config::get('app.locales'));
    }

    public function store(Request $request)
    {
        $this->advantageRepo->createWithTranslations($request);

        session()->flash('success', 'Успешно създадено предимство.');

        return redirect()->route('admin.list.advantages');
    }

    public function edit($id)
    {
        return view('admin.advantages.edit')
            ->with('advantage', $this->advantageRepo->find($id))
            ->with('supportedLocales', Config::get('app.locales'));
    }

    public function update(Request $request, $id)
    {
        $this->advantageRepo->updateWithTranslations($request, $id);

        session()->flash('success', 'Успешно редактирано предимство.');

        return redirect()->route('admin.list.advantages');
    }

    public function destroy($id)
    {
        $this->advantageRepo->delete($id);

        return redirect()->route('admin.list.advantages');
    }
}
