<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\ProductCategoryRequest;

use App\Repositories\ProductCategoryRepository;
use App\Repositories\BaseCategoryRepository;

use Config;

class ProductCategoriesController extends Controller
{
    private $productCategoryRepo;
    private $baseCategoryRepo;

    public function __construct(
        ProductCategoryRepository $productCategoryRepo,
        BaseCategoryRepository $baseCategoryRepo
    ) {
        $this->productCategoryRepo  = $productCategoryRepo;
        $this->baseCategoryRepo     = $baseCategoryRepo;
    }

    public function index()
    {
        return view('admin.product_category.index')
                ->with('categories', $this->productCategoryRepo->getAllWithTranslation());
    }

    public function create()
    {
        return view('admin.product_category.create')
                ->with('supportedLocales', Config::get('app.locales'))
                ->with('baseCategories', $this->baseCategoryRepo->pluck('name', 'id'));
    }

    public function store(ProductCategoryRequest $request)
    {
        $this->productCategoryRepo->createWithTranslations($request);

        session()->flash('success', 'Успешно създадена продуктова категория.');

        return redirect()->route('admin.list.product.category');
    }

    public function edit($id)
    {
        return view('admin.product_category.edit')
            ->with('category', $this->productCategoryRepo->find($id))
            ->with('baseCategories', $this->baseCategoryRepo->pluck('name', 'id'))
            ->with('supportedLocales', Config::get('app.locales'));
    }

    public function update(ProductCategoryRequest $request, $id)
    {
        if (!isset($request->xchange)) $request->request->add(['xchange' => 0]);
        
        $this->productCategoryRepo->updateWithTranslations($request, $id);

        session()->flash('success', 'Успешно редактирана продуктова категория.');

        return redirect()->route('admin.list.product.category');
    }

    public function destroy($id)
    {
        $this->productCategoryRepo->delete($id);

        return redirect()->route('admin.list.product.category');
    }
}
