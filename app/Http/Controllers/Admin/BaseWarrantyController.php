<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\BaseWarrantyRequest;

use App\Repositories\BaseWarrantyRepository;

use Config;

class BaseWarrantyController extends Controller
{
    private $baseWarrantyRepo;

    public function __construct(
        BaseWarrantyRepository $baseWarrantyRepo
    ) {
        $this->baseWarrantyRepo = $baseWarrantyRepo;
    }

    public function index()
    {
        return view('admin.base_warranty.index')
            ->with('warrantys', $this->baseWarrantyRepo->all());
    }

    public function create()
    {
        return view('admin.base_warranty.create')
            ->with('typeSelect', Config::get('warranty_types'));
    }

    public function store(BaseWarrantyRequest $request)
    {
        $this->baseWarrantyRepo->createWarranty($request);

        session()->flash('success', 'Успешно добавена гаранция.');

        return redirect()->route('admin.list.base.warranty');
    }

    public function edit($id)
    {
        return view('admin.base_warranty.edit')
            ->with('typeSelect', Config::get('warranty_types'))
            ->with('warranty', $this->baseWarrantyRepo->find($id));
    }

    public function update(BaseWarrantyRequest $request, $id)
    {
        $this->baseWarrantyRepo->update($id, $request->except(['_method','_token']));

        session()->flash('success', 'Успешно редактирана гаранция.');

        return redirect()->route('admin.list.base.warranty');
    }

    public function destroy($id)
    {
        $this->baseWarrantyRepo->delete($id);

        return redirect()->route('admin.list.base.warranty');
    }
}
