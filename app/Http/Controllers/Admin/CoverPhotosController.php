<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
// use App\Http\Requests\SystemFileRequest;

use App\Repositories\CoverPhotoRepository;

use Config;

class CoverPhotosController extends Controller
{
    private $coverPhotoRepo;

    public function __construct(
        CoverPhotoRepository $coverPhotoRepo
    ) {
        $this->coverPhotoRepo = $coverPhotoRepo;
    }

    public function store(Request $request)
    {
        $this->coverPhotoRepo->createOrUpdateFiles($request);

        session()->flash('success', 'Успешно добавени файлове.');

        return redirect()->back();
    }
}
