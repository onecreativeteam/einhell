<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\SystemFileRequest;

use App\Repositories\SystemFileRepository;

use Config;

class SystemFilesController extends Controller
{
    private $systemFileRepo;

    public function __construct(
        SystemFileRepository $systemFileRepo
    ) {
        $this->systemFileRepo = $systemFileRepo;
    }

    public function create()
    {
        return view('admin.system_files.create')
                ->with('files', $this->systemFileRepo->getFilesByType());
    }

    public function store(SystemFileRequest $request)
    {
        $this->systemFileRepo->createOrUpdateFiles($request);

        session()->flash('success', 'Успешно добавени файлове.');

        return redirect()->route('admin.create.system.files');
    }

    public function destroy($id)
    {
        $this->systemFileRepo->destroyWithFile($id);

        session()->flash('success', 'Успешно изтрит файл.');

        return redirect()->route('admin.create.system.files');
    }
}
