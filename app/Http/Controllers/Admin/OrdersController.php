<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests\PostOrderInfoRequest;

use App\Repositories\OrderRepository;
use App\Repositories\OrderProductRepository;
use App\Repositories\ProductRepository;

use Config;
use DB;

class OrdersController extends Controller
{
    private $orderRepo;
    private $orderProductRepo;
    private $productRepo;

    public function __construct(
        OrderRepository $orderRepo,
        OrderProductRepository $orderProductRepo,
        ProductRepository $productRepo
    ) {
        $this->orderRepo = $orderRepo;
        $this->orderProductRepo = $orderProductRepo;
        $this->productRepo = $productRepo;
    }

    public function edit($id)
    {
        return view('admin.orders.edit')
            ->with('order', $this->orderRepo->find($id));
    }

    public function update(PostOrderInfoRequest $request, $id)
    {
        $this->orderRepo->updateAdminOrder($id, $request->except(['_method','_token']));

        session()->flash('success', 'Успешно редактирана поръчка.');

        return redirect()->back();
    }

    public function destroy($id)
    {
        $this->orderRepo->deleteOrderAndReturnQuantity($id);

        $this->orderRepo->delete($id);

        return redirect()->back();
    }

    public function removeOrderProduct($order_product_id)
    {
        DB::transaction(function () use ($order_product_id) {

            $orderProduct   = $this->orderProductRepo->find($order_product_id);
            $order          = $this->orderRepo->find($orderProduct->orders_id);
            $product        = $this->productRepo->find($orderProduct->products_id);

            $orderTotal = $order->order_total - $orderProduct->p_total_price;

            if ($order->has_user_discount == '1') {

                $orderDiscount = $order->order_discount - ($orderProduct->p_total_price * $order->user_discount_value / 100);
                $orderDiscountTotal = $order->order_discount_total - ($orderProduct->p_total_price - ($orderProduct->p_total_price * $order->user_discount_value / 100));

                $order->update(['order_total' => $orderTotal, 'order_discount' => $orderDiscount, 'order_discount_total' => $orderDiscountTotal]);

            } else {
                $order->update(['order_total' => $orderTotal]);
            }



            $productQuantity = $product->quantity + $orderProduct->product_quantity;
            $product->update(['quantity' => $productQuantity]);

            $this->orderProductRepo->delete($order_product_id);

        });

        session()->flash('success', 'Успешно премахнат продукт от поръчка.');

        return redirect()->back();
    }

    public function addNewOrderProduct($order_id, $product_id)
    {
        $order      = $this->orderRepo->find($order_id);
        $product    = $this->productRepo->find($product_id);

        if ($product->quantity < 1) {
            return redirect()->back()->withErrors(['Няма достатъчна наличност от този продукт.']);
        }

        $insertData = [];

        $insertData['orders_id']            = $order_id;
        $insertData['products_id']          = $product_id;
        $insertData['base_warranty_id']     = $product->base_warranty_id;
        $insertData['has_product_warranty'] = 0;
        $insertData['product_warranty_id']  = 0;
        $insertData['product_quantity']     = 1;
        $insertData['promo_product']        = $product->promo_product == '1' ? 1 : 0;
        $insertData['p_single_price']       = $product->promo_product == '1' ? $product->promo_price : $product->price;
        $insertData['p_total_price']        = $product->promo_product == '1' ? $product->promo_price : $product->price;

        DB::transaction(function () use ($insertData, $order, $product) {

            $orderTotal = $order->order_total + $insertData['p_total_price'];

            if ($order->has_user_discount == '1') {

                $orderDiscount = $order->order_discount + ($insertData['p_total_price'] * $order->user_discount_value / 100);
                $orderDiscountTotal = $order->order_discount_total + ($insertData['p_total_price'] - ($insertData['p_total_price'] * $order->user_discount_value / 100));

                $order->update(['order_total' => $orderTotal, 'order_discount' => $orderDiscount, 'order_discount_total' => $orderDiscountTotal]);

            } else {
                $order->update(['order_total' => $orderTotal]);
            }

            $product->update(['quantity' => $product->quantity - 1]);

            $order->orderProducts()->create($insertData);

        });

        session()->flash('success', 'Успешно добавен продукт към поръчка.');

        return redirect()->back();
    }

    public function changeOrderStatus($order_id, $status)
    {
        $order = $this->orderRepo->find($order_id);

        $order->update(['status' => $status]);

        session()->flash('success', 'Успешно сменен статус на поръчка.');

        return redirect()->back();
    }
}
