<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;

use App\Repositories\ProductRepository;
use App\Repositories\BaseCategoryRepository;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\ProductSeriesRepository;
use App\Repositories\ProductGalleryRepository;
use App\Repositories\BaseWarrantyRepository;

use DB;
use Config;
use Carbon\Carbon;
use Excel;

class ProductsController extends Controller
{
    private $productsRepo;
    private $baseCategoryRepo;
    private $productsCategoryRepo;
    private $productsSeriesRepo;
    private $productsGalleryRepo;
    private $baseWarrantyRepo;

    public function __construct(
        ProductRepository $productsRepo,
        BaseCategoryRepository $baseCategoryRepo,
        ProductCategoryRepository $productsCategoryRepo,
        ProductSeriesRepository $productsSeriesRepo,
        ProductGalleryRepository $productsGalleryRepo,
        BaseWarrantyRepository $baseWarrantyRepo
    ) {
        $this->productsRepo         = $productsRepo;
        $this->baseCategoryRepo     = $baseCategoryRepo;
        $this->productsCategoryRepo = $productsCategoryRepo;
        $this->productsSeriesRepo   = $productsSeriesRepo;
        $this->productsGalleryRepo  = $productsGalleryRepo;
        $this->baseWarrantyRepo     = $baseWarrantyRepo;
    }

    public function index()
    {
        return view('admin.products.index')
                ->with('products', $this->productsRepo->getAllWithTranslation());
    }

    public function create()
    {
        return view('admin.products.create')
                ->with('baseWarrantySelect', $this->baseWarrantyRepo->getModel()->all()->sortBy('type')->pluck('full_type', 'id')->toArray())
                ->with('baseCategorySelect', $this->baseCategoryRepo->pluck('name', 'id'))
                ->with('seriesSelect', $this->productsSeriesRepo->pluck('name', 'id'))
                ->with('categorySelect', $this->productsCategoryRepo->pluck('name', 'id'))
                ->with('supportedLocales', Config::get('app.locales'));
    }

    public function store(ProductRequest $request)
    {
        if ($request->base_categories_id == 'null') {
            $request->request->add(['base_categories_id' => $this->productsCategoryRepo->find($request->product_categories_id)->category->id]);
        }

        $created = $this->productsRepo->createWithTranslations($request);

        $files = $this->productsRepo->createProductFiles($request, $created->id);
        if ($files) {
            $this->productsGalleryRepo->getModel()->insert($files);
        }

        session()->flash('success', 'Успешно създаден продукт.');

        return redirect()->route('admin.list.products');
    }

    public function edit($id)
    {
        return view('admin.products.edit')
            ->with('product', $this->productsRepo->find($id))
            ->with('files', $this->productsRepo->find($id))
            ->with('baseWarrantySelect', $this->baseWarrantyRepo->getModel()->all()->sortBy('type')->pluck('full_type', 'id')->toArray())
            ->with('seriesSelect', $this->productsSeriesRepo->pluck('name', 'id'))
            ->with('baseCategorySelect', $this->baseCategoryRepo->pluck('name', 'id'))
            ->with('categorySelect', $this->productsCategoryRepo->pluck('name', 'id'))
            ->with('supportedLocales', Config::get('app.locales'));
    }

    public function update(ProductRequest $request, $id)
    {
        if (!isset($request->top_product)) $request->request->add(['top_product' => 0]);
        if (!isset($request->promo_product)) $request->request->add(['promo_product' => 0]);
        if (!isset($request->xchange)) $request->request->add(['xchange' => 0]);
        
        if ($request->base_categories_id == 'null') {
            $request->request->add(['base_categories_id' => $this->productsCategoryRepo->find($request->product_categories_id)->category->id]);
        }

        $this->productsRepo->updateWithTranslations($request, $id);

        $files = $this->productsRepo->createProductFiles($request, $id);
        if ($files) {
            $this->productsGalleryRepo->getModel()->insert($files);
        }

        session()->flash('success', 'Успешно редактиран продукт.');

        return redirect()->route('admin.list.products');
    }

    public function destroy($id)
    {
        $this->productsRepo->delete($id);

        return redirect()->route('admin.list.products');
    }

    // PRODUCT EXCELL FUNCTIONS 
    public function export()
    {
        $products = $this->productsRepo->getExcelExportData();
        $category = $this->productsCategoryRepo->getAllWithTranslation();
        $series = $this->productsSeriesRepo->getAllWithTranslation();
        $warrantys = $this->baseWarrantyRepo->getModel()->all();

        Excel::create('ProductsExportEinhell', function($excel) use ($products, $category, $series, $warrantys) {

            // Our first sheet
            $excel->sheet('Products', function($sheet) use ($products) {
                $sheet->cells('B1:J1', function($cells) {

                    $cells->setBackground('#ff0000')->setFontColor('#ffffff')->setFontWeight('bold');

                })->fromArray($products);
            });

            // Our second sheet
            $excel->sheet('Category-Legend', function($sheet) use ($category) {
                $sheet->fromArray($category);
            });

            // Our third sheet
            $excel->sheet('Series-Legend', function($sheet) use ($series) {
                $sheet->fromArray($series);
            });

            // Our third sheet
            $excel->sheet('Warranty-Legend', function($sheet) use ($warrantys) {
                $sheet->fromArray($warrantys);
            });


        })->export('xls');

        return redirect()->back();
    }

    public function import(Request $request)
    {
        if (!$request->file('products_excel')) {
            return redirect()->back()->withErrors(['Липсва избран файл.']);
        }

        $excelFile      = $request -> file('products_excel');
        $fileName       = time() . '.' . $excelFile -> getClientOriginalExtension();
        \Storage::disk('local') -> put( $fileName, \File::get( $excelFile ) );

        Excel::load( storage_path( 'app/' . $fileName ), function ( $reader ) {

            $excelResults   = $reader->all();

            if ($excelResults[0]) {

                foreach ($excelResults[0]->chunk(10) as $chunk) {

                    $insertArray = array();

                    foreach ($chunk as $k => $product) {
                        $insertArray[$k]['product_categories_id'] = $product->product_categories_id;
                        $insertArray[$k]['product_series_id'] = $product->product_series_id;
                        $insertArray[$k]['base_warranty_id'] = $product->base_warranty_id;
                        $insertArray[$k]['name'] = $product->name;
                        $insertArray[$k]['description'] = $product->description;
                        $insertArray[$k]['technical_description'] = $product->technical_description;
                        $insertArray[$k]['price'] = $product->price;
                        $insertArray[$k]['quantity'] = $product->quantity;
                        $insertArray[$k]['product_number'] = $product->product_number;
                        $insertArray[$k]['top_product'] = $product->top_product;
                        $insertArray[$k]['promo_product'] = $product->promo_product;
                        $insertArray[$k]['promo_price'] = $product->promo_price;
                        $insertArray[$k]['xchange'] = $product->xchange;
                        $insertArray[$k]['picture'] = $product->picture;
                        $insertArray[$k]['slug'] = str_slug($product->name.'_'.Carbon::now()->format('i:s'));
                        $insertArray[$k]['locale'] = Config::get('app.default_locale');
                    }

                    $createdArray = DB::transaction(function () use ($insertArray) {

                        $createdArray = $this->productsRepo->insertMany($insertArray);

                        return $createdArray;

                    });
                }
                
            }

        });

        return redirect()->back();
    }

    public function uploadExcellPictures(Request $request)
    {
        $this->productsRepo->uploadProductPicturesExcel($request);

        return redirect()->route('admin.list.products');
    }
    // END PRODUCT EXCELL FUNCTIONS

    // PRODUCT GALLERY EXCELL FUNCTIONS
    public function exportGallery()
    {
        $gallery = $this->productsGalleryRepo->getExcelExportData();

        if (count($gallery) < 1) {
            return redirect()->back()->withErrors(['Моля качете поне 1 снимка в галерия към продукт преди да свалите файла.']);
        }

        Excel::create('ProductsGalleryExportEinhell', function($excel) use ($gallery) {

            $excel->sheet('Gallery', function($sheet) use ($gallery) {
                $sheet->fromArray($gallery);
            });

        })->export('xls');

        return redirect()->back();
    }

    public function importGallery(Request $request)
    {
        if (!$request->file('products_gallery_excel')) {
            return redirect()->back()->withErrors(['Липсва избран файл.']);
        }

        $excelFile      = $request -> file('products_gallery_excel');
        $fileName       = time() . '.' . $excelFile -> getClientOriginalExtension();
        \Storage::disk('local') -> put( $fileName, \File::get( $excelFile ) );

        Excel::load( storage_path( 'app/' . $fileName ), function ( $reader ) {

            $excelResults   = $reader->all();

            if ($excelResults) {

                foreach ($excelResults->chunk(10) as $chunk) {

                    $insertArray = array();

                    foreach ($chunk as $k => $gallery) {
                        $insertArray[$k]['products_id'] = $gallery->products_id;
                        $insertArray[$k]['type'] = 'picture';
                        $insertArray[$k]['resource'] = $gallery->resource;
                    }

                    $createdArray = DB::transaction(function () use ($insertArray) {

                        $createdArray = $this->productsGalleryRepo->insertMany($insertArray);

                        return $createdArray;

                    });
                }
                
            }

        });

        return redirect()->back();
    }

    public function uploadExcellGalleryPictures(Request $request)
    {
        $this->productsGalleryRepo->uploadProductGalleryPicturesExcel($request);

        return redirect()->route('admin.list.products');
    }
    // END PRODUCT GALLERY EXCELL FUNCTIONS

    public function manualChangeCategory()
    {
        dd('function is stopped and not performing anything now.');
        // write your custom logic to loop through products and change their category or other properties.
        
        // $productsArray = $this->productsRepo->whereAll(['promo_product', '=', true]);

        // foreach ($productsArray as $product) {
        //     $product->base_categories_id = 15;
        //     $product->save();
        // }
        // dd('done');
    }

    public function manualChangeSeries(Request $request)
    {
        if (!$request->file('series_excell')) {
            return redirect()->back()->withErrors(['Липсва избран файл.']);
        }

        $excelFile      = $request->file('series_excell');
        $fileName       = time() . '.' . $excelFile -> getClientOriginalExtension();
        \Storage::disk('local') -> put( $fileName, \File::get( $excelFile ) );

        Excel::load( storage_path( 'app/' . $fileName ), function ( $reader ) {

            $excelResults   = $reader->all();

            if ($excelResults[0]) {

                foreach ($excelResults[0]->chunk(10) as $chunk) {

                    foreach ($chunk as $k => $product) {

                        $dbProduct = $this->productsRepo->find($product->id);
                        $dbProduct->product_series_id = $product->product_series_id;

                        $dbProduct->save();
                    }

                }
                
            }

        });

        return redirect()->back();
    }

    public function trim()
    {
        // TRIM FILES AND DATABASE RECORDS
        // WITH WHITE SPACES

        // foreach ($this->productsGalleryRepo->all() as $record)
        // {
        //     $record->resource = str_replace(' ', '_', $record->resource);
        //     $record->save();
        // }

        // foreach (glob("uploads/products/gallery/*.*") as $file) {
        //     \File::move($file, str_replace(' ', '_', $file));
        // }

        // dd('You\'re DONE !');
    }

    // public function updateCategories(Request $request)
    // {
    //     if (!$request->file('products_categories_excel')) {
    //         return redirect()->back()->withErrors(['Липсва избран файл.']);
    //     }

    //     $excelFile      = $request -> file('products_categories_excel');
    //     $fileName       = time() . '.' . $excelFile -> getClientOriginalExtension();
    //     \Storage::disk('local') -> put( $fileName, \File::get( $excelFile ) );

    //     Excel::load( storage_path( 'app/' . $fileName ), function ( $reader ) {

    //         $excelResults   = $reader->all();

    //         if ($excelResults[0]) {

    //             foreach ($excelResults[0] as $product) {
                    
    //                 $id = intval($product->id);
    //                 $cat = intval($product->product_categories_id);

    //                 $dbProduct = $this->productsRepo->find($id);
    //                 $dbProduct->product_categories_id = $cat;
    //                 $dbProduct->save();
    //             }
                
    //         }

    //     });

    //     return redirect()->back();
    // }
}
