<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

class LanguageMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!array_key_exists($request->segment(1), config('app.locales'))) {

            $segments = $request->segments();

            unset($segments[0]);

            $segments = array_prepend($segments, config('app.locale'));

            return redirect()->to(implode('/', $segments));
        }

        return $next($request);
    }
}
