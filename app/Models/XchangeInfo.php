<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class XchangeInfo extends Model
{
    protected $table = 'xchange_info';

    public $timestamps = true;

    public $filePathAttribute = 'uploads' . DIRECTORY_SEPARATOR . 'xchange_slider' . DIRECTORY_SEPARATOR;

    protected $fillable = [
        'section_one_title',
        'section_one_text',
        'section_two_title',
        'section_two_text',
        'section_two_link',
        'section_three_title',
        'section_three_text',
        'section_four_title',
        'section_four_text',
        'slider_title',
        'slider_text',
        'slider_picture',
        'created_at',
        'updated_at'
    ];
}
