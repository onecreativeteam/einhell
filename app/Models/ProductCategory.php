<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $table = 'product_categories';

    public $timestamps = true;

    protected $fillable = [
        'base_categories_id',
        'name',
        'picture',
        'xchange',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    // This is configuration for translated resources FK
    public $fileAttribute = 'picture';

    // This is configuration for model files path
    public $filePathAttribute = 'uploads' . DIRECTORY_SEPARATOR . 'category' . DIRECTORY_SEPARATOR;

    // This is configuration for translated resources FK
    public $translatableKey = 'product_categories_id';

    // This is configuration for translatable fields for model
    public $translatableFields = [
        'name',
        'slug',
        'locale',
    ];

    // This is querying the translated instance depend on locale
    public function getTranslationsAttribute() {
        return $this->getRelationValue('translations')->keyBy('locale');
    }

    public function category()
    {
        return $this->belongsTo(BaseCategory::class, 'base_categories_id');
    }

    public function translated()
    {
        return $this->hasOne(ProductCategoryTranslation::class, 'product_categories_id')->where('locale', app()->getLocale());
    }

    public function translations()
    {
        return $this->hasMany(ProductCategoryTranslation::class, 'product_categories_id');
    }

    public function baseCategory()
    {
        return $this->belongsTo(BaseCategory::class, 'base_categories_id');
    }
}
