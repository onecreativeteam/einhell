<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MapPage extends Model
{
    protected $table = 'map_page';

    protected $fillable = [
        'text',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    // This is configuration for translated resources FK
    public $fileAttribute = 'picture';

    // This is configuration for model files path
    public $filePathAttribute = 'uploads' . DIRECTORY_SEPARATOR . 'map' . DIRECTORY_SEPARATOR;

    // This is configuration for related TranslationModel resources FK
    public $translatableKey = 'map_page_id';

    // This is configuration for translatable fields for related TranslationModel
    public $translatableFields = [
        'text',
        'slug',
        'locale',
    ];

    // This is querying the translated instance depend on locale
    public function getTranslationsAttribute() {
        return $this->getRelationValue('translations')->keyBy('locale');
    }

    public function translated()
    {
        return $this->hasOne(MapPageTranslation::class, 'map_page_id')->where('locale', app()->getLocale());
    }

    public function translations()
    {
        return $this->hasMany(MapPageTranslation::class, 'map_page_id');
    }
}
