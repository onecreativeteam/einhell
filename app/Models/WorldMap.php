<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorldMap extends Model
{
    protected $table = 'world_map';

    public $timestamps = true;

    protected $fillable = [
        'code',
        'created_at',
        'updated_at'
    ];
}
