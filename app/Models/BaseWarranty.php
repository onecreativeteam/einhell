<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseWarranty extends Model
{
    protected $table = 'base_warranty';

    public $timestamps = true;

    protected $fillable = [
        'range',
        'type',
        'created_at',
        'updated_at'
    ];

    public function product()
    {
        return $this->hasMany(Product::class, 'products_id');
    }

    public function getFullTypeAttribute()
    {
        return $this->range . " " . $this->type;
    }
}
