<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseCategoryTranslation extends Model
{
    public $timestamps = true;

    protected $table = 'base_categories_translations';

    protected $fillable = [
        'base_categories_id',
        'name',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    public function parent()
    {
        return $this->belongsTo(BaseCategory::class, 'base_categories_id');
    }
}
