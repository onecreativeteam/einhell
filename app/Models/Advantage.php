<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Advantage extends Model
{
    protected $table = 'advantages';

    protected $fillable = [
        'name',
        'text',
        'picture',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    // This is configuration for translated resources FK
    public $fileAttribute = 'picture';

    // This is configuration for model files path
    public $filePathAttribute = 'uploads' . DIRECTORY_SEPARATOR . 'advantages' . DIRECTORY_SEPARATOR;

    // This is configuration for related TranslationModel resources FK
    public $translatableKey = 'advantages_id';

    // This is configuration for translatable fields for related TranslationModel
    public $translatableFields = [
        'name',
        'text',
        'slug',
        'locale',
    ];

    // This is querying the translated instance depend on locale
    public function getTranslationsAttribute() {
        return $this->getRelationValue('translations')->keyBy('locale');
    }

    public function translated()
    {
        return $this->hasOne(AdvantageTranslation::class, 'advantages_id')->where('locale', app()->getLocale());
    }

    public function translations()
    {
        return $this->hasMany(AdvantageTranslation::class, 'advantages_id');
    }
}
