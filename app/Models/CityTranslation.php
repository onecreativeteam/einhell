<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CityTranslation extends Model
{
    public $timestamps = true;

    protected $table = 'cities_translations';

    protected $fillable = [
        'cities_id',
        'name',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    public function parent()
    {
        return $this->belongsTo(City::class, 'cities_id');
    }
}
