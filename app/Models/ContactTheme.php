<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactTheme extends Model
{
    protected $table = 'contact_themes';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    // This is configuration for translated resources FK
    public $translatableKey = 'contact_themes_id';

    // This is configuration for translatable fields for model
    public $translatableFields = [
        'name',
        'slug',
        'locale',
    ];

    // This is querying the translated instance depend on locale
    public function getTranslationsAttribute() {
        return $this->getRelationValue('translations')->keyBy('locale');
    }

    public function translated()
    {
        return $this->hasOne(ContactThemeTranslation::class, 'contact_themes_id')->where('locale', app()->getLocale());
    }

    public function translations()
    {
        return $this->hasMany(ContactThemeTranslation::class, 'contact_themes_id');
    }
}
