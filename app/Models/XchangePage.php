<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class XchangePage extends Model
{
    protected $table = 'xchange_page';

    protected $fillable = [
        'name',
        'text',
        'picture',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    // This is configuration for translated resources FK
    public $fileAttribute = 'picture';

    // This is configuration for model files path
    public $filePathAttribute = 'uploads' . DIRECTORY_SEPARATOR . 'xchange' . DIRECTORY_SEPARATOR;

    // This is configuration for related TranslationModel resources FK
    public $translatableKey = 'xchange_page_id';

    // This is configuration for translatable fields for related TranslationModel
    public $translatableFields = [
        'name',
        'text',
        'slug',
        'locale',
    ];

    // This is querying the translated instance depend on locale
    public function getTranslationsAttribute() {
        return $this->getRelationValue('translations')->keyBy('locale');
    }

    public function translated()
    {
        return $this->hasOne(XchangePageTranslation::class, 'xchange_page_id')->where('locale', app()->getLocale());
    }

    public function translations()
    {
        return $this->hasMany(XchangePageTranslation::class, 'xchange_page_id');
    }
}
