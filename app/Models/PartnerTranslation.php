<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerTranslation extends Model
{
    public $timestamps = true;

    protected $table = 'partners_translations';

    protected $fillable = [
        'partners_id',
        'name',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    public function parent()
    {
        return $this->belongsTo(Partner::class, 'partners_id');
    }
}
