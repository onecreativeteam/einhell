<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class XchangeOffer extends Model
{
    protected $table = 'xchange_offers';

    public $timestamps = true;

    protected $fillable = [
        'product_id',
        'title',
        'description',
        'created_at',
        'updated_at'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
