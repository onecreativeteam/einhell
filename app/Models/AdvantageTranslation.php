<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdvantageTranslation extends Model
{
    public $timestamps = true;

    protected $table = 'advantages_translations';

    protected $fillable = [
        'advantages_id',
        'name',
        'text',
        'locale',
        'slug',
        'created_at',
        'updated_at'
    ];

    public function parent()
    {
        return $this->belongsTo(Advantage::class, 'advantages_id');
    }
}
