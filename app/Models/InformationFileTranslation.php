<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InformationFileTranslation extends Model
{
    public $timestamps = true;

    protected $table = 'information_files_translations';

    protected $fillable = [
        'information_files_id',
        'name',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    public function parent()
    {
        return $this->belongsTo(InformationFile::class, 'information_files_id');
    }
}
