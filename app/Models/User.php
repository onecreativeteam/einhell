<?php

namespace App\Models;

use App\Notifications\PasswordReset;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'phone',
        'adress',
        'accepted_tc',
        'has_user_discount',
        'user_discount_value',
        'role',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token'
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordReset($token));
    }

    public function isAdmin()
    {
        return $this->role === 'admin';
    }

    public function hasDiscount()
    {
        return $this->has_user_discount === 1;
    }

    public function discountValue()
    {
        return $this->user_discount_value;
    }

    public function social()
    {
        return $this->hasOne(SocialAccount::class, 'user_id');
    }
}
