<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DistributorTranslation extends Model
{
    public $timestamps = true;

    protected $table = 'distributors_translations';

    protected $fillable = [
        'distributors_id',
        'name',
        'adress',
        'phone',
        'mobile_phone',
        'fax',
        'owner',
        'email',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    public function parent()
    {
        return $this->belongsTo(Distributor::class, 'distributors_id');
    }
}
