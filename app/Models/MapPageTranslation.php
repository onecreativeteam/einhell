<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MapPageTranslation extends Model
{
    public $timestamps = true;

    protected $table = 'map_page_translations';

    protected $fillable = [
        'map_page_id',
        'text',
        'locale',
        'slug',
        'created_at',
        'updated_at'
    ];

    public function parent()
    {
        return $this->belongsTo(MapPage::class, 'map_page_id');
    }
}
