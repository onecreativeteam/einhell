<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactTranslation extends Model
{
    public $timestamps = true;

    protected $table = 'contacts_translations';

    protected $fillable = [
        'contacts_id',
        'name',
        'owner',
        'email',
        'phone',
        'mobile_phone',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    public function parent()
    {
        return $this->belongsTo(Contact::class, 'contacts_id');
    }
}
