<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductTranslation extends Model
{
    public $timestamps = true;

    protected $table = 'products_translations';

    protected $fillable = [
        'products_id',
        'name',
        'description',
        'technical_description',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    public function parent()
    {
        return $this->belongsTo(Product::class, 'products_id');
    }
}
