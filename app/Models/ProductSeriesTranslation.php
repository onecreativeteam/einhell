<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSeriesTranslation extends Model
{
    public $timestamps = true;

    protected $table = 'product_series_translations';

    protected $fillable = [
        'product_series_id',
        'name',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    public function parent()
    {
        return $this->belongsTo(ProductSeries::class, 'product_series_id');
    }
}
