<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScientificResearchTranslation extends Model
{
    public $timestamps = true;

    protected $table = 'scientific_researchs_translations';

    protected $fillable = [
        'scientific_researchs_id',
        'name',
        'description',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    public function parent()
    {
        return $this->belongsTo(ScientificResearch::class, 'scientific_researchs_id');
    }
}
