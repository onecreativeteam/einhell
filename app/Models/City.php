<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'postcode',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    // This is configuration for translated resources FK
    public $translatableKey = 'cities_id';

    // This is configuration for translatable fields for model
    public $translatableFields = [
        'name',
        'slug',
        'locale',
    ];

    // This is querying the translated instance depend on locale
    public function getTranslationsAttribute() {
        return $this->getRelationValue('translations')->keyBy('locale');
    }

    public function translated()
    {
        return $this->hasOne(CityTranslation::class, 'cities_id')->where('locale', app()->getLocale());
    }

    public function translations()
    {
        return $this->hasMany(CityTranslation::class, 'cities_id');
    }
}
