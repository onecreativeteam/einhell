<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InformationFile extends Model
{
    protected $table = 'information_files';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'file',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    // This is configuration for translated resources FK
    public $fileAttribute = 'file';

    // This is configuration for model files path
    public $filePathAttribute = 'uploads' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'information' . DIRECTORY_SEPARATOR;

    // This is configuration for translated resources FK
    public $translatableKey = 'information_files_id';

    // This is configuration for translatable fields for model
    public $translatableFields = [
        'name',
        'slug',
        'locale',
    ];

    // This is querying the translated instance depend on locale
    public function getTranslationsAttribute() {
        return $this->getRelationValue('translations')->keyBy('locale');
    }

    public function translated()
    {
        return $this->hasOne(InformationFileTranslation::class, 'information_files_id')->where('locale', app()->getLocale());
    }

    public function translations()
    {
        return $this->hasMany(InformationFileTranslation::class, 'information_files_id');
    }
}
