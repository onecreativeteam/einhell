<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScientificResearch extends Model
{
    protected $table = 'scientific_researchs';

    protected $fillable = [
        'name',
        'description',
        'picture',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    // This is configuration for translated resources FK
    public $fileAttribute = 'picture';

    // This is configuration for model files path
    public $filePathAttribute = 'uploads' . DIRECTORY_SEPARATOR . 'researchs' . DIRECTORY_SEPARATOR;

    // This is configuration for related TranslationModel resources FK
    public $translatableKey = 'scientific_researchs_id';

    // This is configuration for translatable fields for related TranslationModel
    public $translatableFields = [
        'name',
        'description',
        'slug',
        'locale',
    ];

    // This is querying the translated instance depend on locale
    public function getTranslationsAttribute() {
        return $this->getRelationValue('translations')->keyBy('locale');
    }

    public function translated()
    {
        return $this->hasOne(ScientificResearchTranslation::class, 'scientific_researchs_id')->where('locale', app()->getLocale());
    }

    public function translations()
    {
        return $this->hasMany(ScientificResearchTranslation::class, 'scientific_researchs_id');
    }
}
