<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    protected $table = 'order_product';

    public $timestamps = true;

    protected $fillable = [
        'orders_id',
        'products_id',
        'base_warranty_id',
        'has_product_warranty',
        'product_warranty_id',
        'product_quantity',
        'promo_product',
        'p_single_price',
        'p_total_price',
        'created_at',
        'updated_at'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'products_id');
    }
}
