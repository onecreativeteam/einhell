<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    public $timestamps = true;

    protected $fillable = [
        'product_categories_id',
        'product_series_id',
        'base_categories_id',
        'base_warranty_id',
        'name',
        'description',
        'technical_description',
        'price',
        'quantity',
        'product_number',
        'picture',
        'top_product',
        'promo_product',
        'promo_price',
        'xchange',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    // This is configuration for translated resources FK
    public $translatableKey = 'products_id';

    // This is configuration for translated resources FK
    public $fileAttribute = 'picture';

    // This is configuration for model files path
    public $filePathAttribute = 'uploads' . DIRECTORY_SEPARATOR . 'product' . DIRECTORY_SEPARATOR;

    public $filePath = 'uploads' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'product' . DIRECTORY_SEPARATOR;

    // This is configuration for translatable fields for model
    public $translatableFields = [
        'name',
        'description',
        'technical_description',
        'slug',
        'locale',
    ];

    // This is querying the translated instance depend on locale
    public function getTranslationsAttribute() {
        return $this->getRelationValue('translations')->keyBy('locale');
    }

    public function translated()
    {
        return $this->hasOne(ProductTranslation::class, 'products_id')->where('locale', app()->getLocale());
    }

    public function translations()
    {
        return $this->hasMany(ProductTranslation::class, 'products_id');
    }

    public function category()
    {
        return $this->belongsTo(ProductCategory::class, 'product_categories_id');
    }

    public function baseCategory()
    {
        return $this->belongsTo(BaseCategory::class, 'base_category_id');
    }

    public function serie()
    {
        return $this->belongsTo(ProductSeries::class, 'product_series_id');
    }

    public function gallery()
    {
        return $this->hasMany(ProductGallery::class, 'products_id');
    }

    public function videos()
    {
        return $this->hasMany(ProductGallery::class, 'products_id')->where('type', '=', 'video');
    }

    public function pictures()
    {
        return $this->hasMany(ProductGallery::class, 'products_id')->where('type', '=', 'picture')->orderBy('view_order', 'desc');
    }

    public function files()
    {
        return $this->hasMany(ProductGallery::class, 'products_id')->where('type', '=', 'file');
    }

    public function warranty()
    {
        return $this->hasMany(ProductWarranty::class, 'products_id');
    }

    public function baseWarranty()
    {
        return $this->belongsTo(BaseWarranty::class, 'base_warranty_id');
    }
}
