<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contacts';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'owner',
        'email',
        'phone',
        'mobile_phone',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    // This is configuration for translated resources FK
    public $translatableKey = 'contacts_id';

    // This is configuration for translatable fields for model
    public $translatableFields = [
        'name',
        'owner',
        'email',
        'phone',
        'mobile_phone',
        'slug',
        'locale',
    ];

    // This is querying the translated instance depend on locale
    public function getTranslationsAttribute() {
        return $this->getRelationValue('translations')->keyBy('locale');
    }

    public function translated()
    {
        return $this->hasOne(ContactTranslation::class, 'contacts_id')->where('locale', app()->getLocale());
    }

    public function translations()
    {
        return $this->hasMany(ContactTranslation::class, 'contacts_id');
    }
}
