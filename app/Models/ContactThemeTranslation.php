<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactThemeTranslation extends Model
{
    public $timestamps = true;

    protected $table = 'contact_themes_translations';

    protected $fillable = [
        'contact_themes_id',
        'name',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    public function parent()
    {
        return $this->belongsTo(ContactTheme::class, 'contact_themes_id');
    }
}
