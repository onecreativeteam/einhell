<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductWarranty extends Model
{
    protected $table = 'products_warranty';

    public $timestamps = true;

    protected $fillable = [
        'products_id',
        'warranty_months',
        'warranty_price',
        'created_at',
        'updated_at'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'products_id');
    }
}
