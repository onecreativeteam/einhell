<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategoryTranslation extends Model
{
    public $timestamps = true;

    protected $table = 'product_categories_translations';

    protected $fillable = [
        'product_categories_id',
        'name',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    public function parent()
    {
        return $this->belongsTo(ProductCategory::class, 'product_categories_id');
    }
}
