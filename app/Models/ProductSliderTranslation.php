<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSliderTranslation extends Model
{
    public $timestamps = true;

    protected $table = 'products_sliders_translations';

    protected $fillable = [
        'products_sliders_id',
        'name',
        'locale',
        'slug',
        'created_at',
        'updated_at'
    ];

    public function parent()
    {
        return $this->belongsTo(ProductSlider::class, 'products_sliders_id');
    }
}
