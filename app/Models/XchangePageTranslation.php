<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class XchangePageTranslation extends Model
{
    public $timestamps = true;

    protected $table = 'xchange_page_translations';

    protected $fillable = [
        'xchange_page_id',
        'name',
        'text',
        'locale',
        'slug',
        'created_at',
        'updated_at'
    ];

    public function parent()
    {
        return $this->belongsTo(XchangePage::class, 'xchange_page_id');
    }
}
