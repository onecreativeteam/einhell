<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CoverPhoto extends Model
{
    protected $table = 'cover_photos';

    public $timestamps = true;

    public $filePathAttribute = 'uploads' . DIRECTORY_SEPARATOR . 'cover_photos' . DIRECTORY_SEPARATOR;

    public $fileAttribute = 'file';

    protected $fillable = [
        'file',
        'type',
        'created_at',
        'updated_at'
    ];

    public function scopeAbout($query)
    {
        return $query->where('type', '=', 'about');
    }

    public function scopeResearch($query)
    {
        return $query->where('type', '=', 'research');
    }
}
