<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InformationPage extends Model
{
    protected $table = 'information_pages';

    protected $fillable = [
        'name',
        'description',
        'picture',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    // This is configuration for translated resources FK
    public $fileAttribute = 'picture';

    // This is configuration for model files path
    public $filePathAttribute = 'uploads' . DIRECTORY_SEPARATOR . 'pages' . DIRECTORY_SEPARATOR;

    // This is configuration for related TranslationModel resources FK
    public $translatableKey = 'information_pages_id';

    // This is configuration for translatable fields for related TranslationModel
    public $translatableFields = [
        'name',
        'description',
        'slug',
        'locale',
    ];

    // This is querying the translated instance depend on locale
    public function getTranslationsAttribute() {
        return $this->getRelationValue('translations')->keyBy('locale');
    }

    public function translated()
    {
        return $this->hasOne(InformationPageTranslation::class, 'information_pages_id')->where('locale', app()->getLocale());
    }

    public function translations()
    {
        return $this->hasMany(InformationPageTranslation::class, 'information_pages_id');
    }
}
