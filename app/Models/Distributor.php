<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Distributor extends Model
{
    protected $table = 'distributors';

    public $timestamps = true;

    protected $fillable = [
        'cities_id',
        'name',
        'type',
        'adress',
        'phone',
        'mobile_phone',
        'fax',
        'owner',
        'email',
        'gps_lat',
        'gps_lon',
        'gps_zoom',
        'show_in_contact',
        'show_in_contact_only',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    // This is configuration for translated resources FK
    public $translatableKey = 'distributors_id';

    // This is configuration for translatable fields for model
    public $translatableFields = [
        'name',
        'adress',
        'phone',
        'mobile_phone',
        'fax',
        'owner',
        'email',
        'slug',
        'locale',
    ];

    // This is querying the translated instance depend on locale
    public function getTranslationsAttribute() {
        return $this->getRelationValue('translations')->keyBy('locale');
    }

    public function translated()
    {
        return $this->hasOne(DistributorTranslation::class, 'distributors_id')->where('locale', app()->getLocale());
    }

    public function translations()
    {
        return $this->hasMany(DistributorTranslation::class, 'distributors_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'cities_id');
    }

}
