<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SystemFile extends Model
{
    protected $table = 'system_files';

    public $timestamps = true;

    public $filePathAttribute = 'uploads' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'system' . DIRECTORY_SEPARATOR;

    public $fileAttribute = 'file';

    protected $fillable = [
        'file',
        'type',
        'created_at',
        'updated_at'
    ];

    public function scopeFaq($query)
    {
        return $query->where('type', '=', 'faq');
    }

    public function scopeTc($query)
    {
        return $query->where('type', '=', 'tc');
    }
}
