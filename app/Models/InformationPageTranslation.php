<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InformationPageTranslation extends Model
{
    public $timestamps = true;

    protected $table = 'information_pages_translations';

    protected $fillable = [
        'information_pages_id',
        'name',
        'description',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    public function parent()
    {
        return $this->belongsTo(InformationPage::class, 'information_pages_id');
    }
}
