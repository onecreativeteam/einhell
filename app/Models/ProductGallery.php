<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductGallery extends Model
{
    protected $table = 'products_embed_gallery';

    public $timestamps = true;

    public $filePathAttribute = 'uploads' . DIRECTORY_SEPARATOR . 'products' . DIRECTORY_SEPARATOR . 'gallery' . DIRECTORY_SEPARATOR;

    public $fileAttribute = 'resource';

    protected $fillable = [
        'products_id',
        'type',
        'resource',
        'view_order',
        'created_at',
        'updated_at'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'products_id');
    }
}
