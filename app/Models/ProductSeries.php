<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSeries extends Model
{
    protected $table = 'product_series';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'add_to_filter',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    // This is configuration for translated resources FK
    public $translatableKey = 'product_series_id';

    // This is configuration for translatable fields for model
    public $translatableFields = [
        'name',
        'slug',
        'locale',
    ];

    // This is querying the translated instance depend on locale
    public function getTranslationsAttribute() {
        return $this->getRelationValue('translations')->keyBy('locale');
    }

    public function translated()
    {
        return $this->hasOne(ProductSeriesTranslation::class, 'product_series_id')->where('locale', app()->getLocale());
    }

    public function translations()
    {
        return $this->hasMany(ProductSeriesTranslation::class, 'product_series_id');
    }
}
