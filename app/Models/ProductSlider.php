<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSlider extends Model
{
    protected $table = 'products_sliders';

    protected $fillable = [
        'name',
        'description',
        'products_id',
        'xchange',
        'picture',
        'link',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    // This is configuration for translated resources FK
    public $fileAttribute = 'picture';

    // This is configuration for model files path
    public $filePathAttribute = 'uploads' . DIRECTORY_SEPARATOR . 'sliders' . DIRECTORY_SEPARATOR;

    // This is configuration for related TranslationModel resources FK
    public $translatableKey = 'products_sliders_id';

    // This is configuration for translatable fields for related TranslationModel
    public $translatableFields = [
        'name',
        'description',
        'slug',
        'locale',
    ];

    // This is querying the translated instance depend on locale
    public function getTranslationsAttribute() {
        return $this->getRelationValue('translations')->keyBy('locale');
    }

    public function translated()
    {
        return $this->hasOne(ProductSliderTranslation::class, 'products_sliders_id')->where('locale', app()->getLocale());
    }

    public function translations()
    {
        return $this->hasMany(ProductSliderTranslation::class, 'products_sliders_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'products_id');
    }
}
