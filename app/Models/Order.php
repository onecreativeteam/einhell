<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'status',
        'first_name',
        'last_name',
        'email',
        'phone',
        'adress',
        'postcode',
        'payment_method',
        'order_comment',
        'is_company',
        'company_name',
        'company_bulstat',
        'company_mol',
        'company_adress',
        'add_dds',
        'has_user_discount',
        'user_discount_value',
        'order_discount',
        'order_discount_total',
        'order_total',
        'created_at',
        'updated_at'
    ];

    public function orderProducts()
    {
        return $this->hasMany(OrderProduct::class, 'orders_id');
    }
}
