<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductEmbedGalleryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_embed_gallery', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('products_id')->nullable()->unsigned();
            $table->foreign('products_id')->references('id')->on('products')->onDelete('cascade');
            $table->enum('type', ['picture','video'])->default('picture');
            $table->string('resource')->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products_embed_gallery', function (Blueprint $table) {
            $table->dropForeign(['products_id']);
        });

        Schema::dropIfExists('products_embed_gallery');
    }
}
