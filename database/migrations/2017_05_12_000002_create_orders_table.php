<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id')->default('guest')->nullable(false);
            $table->string('status')->default('new')->nullable(false);
            $table->string('first_name')->nullable(false);
            $table->string('last_name')->nullable(false);
            $table->string('email')->nullable(false);
            $table->string('phone')->nullable(false);
            $table->string('adress')->nullable(false);
            $table->string('postcode')->nullable(false);
            $table->enum('payment_method', ['vpos', 'cash'])->default('cash')->nullable(false);
            $table->mediumText('order_comment')->nullable();
            $table->boolean('is_company')->default(0)->nullable(false);
            $table->string('company_name')->nullable();
            $table->string('company_bulstat')->nullable();
            $table->string('company_mol')->nullable();
            $table->string('company_adress')->nullable();
            $table->boolean('add_dds')->default(0)->nullable(false);

            $table->boolean('has_user_discount')->default(0)->nullable(false);
            $table->integer('user_discount_value')->default(0)->nullable(false);
            $table->decimal('order_discount', 10, 2)->default(0)->nullable(false);
            $table->decimal('order_discount_total', 10, 2)->default(0)->nullable(false);

            $table->decimal('order_total', 10, 2)->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
