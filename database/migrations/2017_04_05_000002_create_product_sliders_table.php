<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_sliders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable(false);
            $table->string('description')->nullable();
            $table->integer('products_id')->nullable()->unsigned();
            $table->foreign('products_id')->references('id')->on('products')->onDelete('cascade');
            $table->string('picture')->nullable(false);
            $table->boolean('xchange')->default(0);
            $table->string('link')->nullable(false);
            $table->string('slug')->nullable(false);
            $table->string('locale')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products_sliders', function (Blueprint $table) {
            $table->dropForeign(['products_id']);
        });

        Schema::dropIfExists('products_sliders');
    }
}
