<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistributorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distributors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable(false);
            $table->integer('cities_id')->nullable()->unsigned();
            $table->foreign('cities_id')->references('id')->on('cities')->onDelete('set null');
            $table->enum('type', ['shop', 'service', 'diller'])->nullable(false);
            $table->string('adress')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile_phone')->nullable();
            $table->string('fax')->nullable();
            $table->string('owner')->nullable();
            $table->string('email')->nullable();
            $table->string('gps_lat')->nullable();
            $table->string('gps_lon')->nullable();
            $table->string('gps_zoom')->nullable();
            $table->boolean('show_in_contact')->nullable(false)->default(0);
            $table->string('slug')->nullable(false);
            $table->string('locale')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('distributors', function (Blueprint $table) {
            $table->dropForeign(['cities_id']);
        });
        
        Schema::dropIfExists('distributors');
    }
}
