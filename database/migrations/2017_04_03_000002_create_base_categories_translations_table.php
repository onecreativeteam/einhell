<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaseCategoriesTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('base_categories_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('base_categories_id')->nullable()->unsigned();
            $table->foreign('base_categories_id')->references('id')->on('base_categories')->onDelete('cascade');
            $table->string('name')->nullable(false);
            $table->string('slug')->nullable(false);
            $table->string('locale')->index();
            $table->unique(['base_categories_id', 'locale']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('base_categories_translations', function (Blueprint $table) {
            $table->dropForeign(['base_categories_id']);
        });

        Schema::dropIfExists('base_categories_translations');
    }
}
