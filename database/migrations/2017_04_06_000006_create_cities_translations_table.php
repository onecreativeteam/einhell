<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cities_id')->nullable()->unsigned();
            $table->foreign('cities_id')->references('id')->on('cities')->onDelete('cascade');
            $table->string('name')->nullable(false);
            $table->string('slug')->nullable(false);
            $table->string('locale')->index();
            $table->unique(['cities_id', 'locale']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cities_translations', function (Blueprint $table) {
            $table->dropForeign(['cities_id']);
        });

        Schema::dropIfExists('cities_translations');
    }
}
