<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductSlidersTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_sliders_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable(false);
            $table->string('description')->nullable();
            $table->integer('products_sliders_id')->nullable()->unsigned();
            $table->foreign('products_sliders_id')->references('id')->on('products_sliders')->onDelete('cascade');
            $table->string('slug')->nullable(false);
            $table->string('locale')->index();
            $table->unique(['products_sliders_id', 'locale']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products_sliders_translations', function (Blueprint $table) {
            $table->dropForeign(['products_sliders_id']);
        });

        Schema::dropIfExists('products_sliders_translations');
    }
}
