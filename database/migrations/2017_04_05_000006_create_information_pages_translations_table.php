<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformationPagesTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('information_pages_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('information_pages_id')->nullable()->unsigned();
            $table->foreign('information_pages_id', 'ip_foreign_id')->references('id')->on('information_pages')->onDelete('cascade');
            $table->string('name')->nullable(false);
            $table->mediumText('description')->nullable(false);
            $table->string('slug')->nullable(false);
            $table->string('locale')->index();
            $table->unique(['information_pages_id', 'locale'], 'ip_unique_locale');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('information_pages_translations', function (Blueprint $table) {
            $table->dropForeign(['information_pages_id']);
        });

        Schema::dropIfExists('information_pages_translations');
    }
}
