<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScientificResearchsTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scientific_researchs_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('scientific_researchs_id')->nullable()->unsigned();
            $table->foreign('scientific_researchs_id', 'sr_foreign_id')->references('id')->on('scientific_researchs')->onDelete('cascade');
            $table->string('name')->nullable(false);
            $table->mediumText('description')->nullable(false);
            $table->string('slug')->nullable(false);
            $table->string('locale')->index();
            $table->unique(['scientific_researchs_id', 'locale'], 'sr_unique_locale');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scientific_researchs_translations', function (Blueprint $table) {
            $table->dropForeign(['scientific_researchs_id']);
        });

        Schema::dropIfExists('scientific_researchs_translations');
    }
}
