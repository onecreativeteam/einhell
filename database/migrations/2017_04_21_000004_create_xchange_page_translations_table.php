<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateXchangePageTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xchange_page_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('xchange_page_id')->nullable()->unsigned();
            $table->foreign('xchange_page_id')->references('id')->on('xchange_page')->onDelete('cascade');
            $table->string('name')->nullable(false);
            $table->mediumText('text')->nullable(false);
            $table->string('slug')->nullable(false);
            $table->string('locale')->index();
            $table->unique(['xchange_page_id', 'locale']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('xchange_page_translations', function (Blueprint $table) {
            $table->dropForeign(['xchange_page_id']);
        });

        Schema::dropIfExists('xchange_page_translations');
    }
}
