<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductSeriesTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_series_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_series_id')->nullable()->unsigned();
            $table->foreign('product_series_id')->references('id')->on('product_series')->onDelete('cascade');
            $table->string('name')->nullable(false);
            $table->string('slug')->nullable(false);
            $table->string('locale')->index();
            $table->unique(['product_series_id', 'locale']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_series_translations', function (Blueprint $table) {
            $table->dropForeign(['product_series_id']);
        });

        Schema::dropIfExists('product_series_translations');
    }
}
