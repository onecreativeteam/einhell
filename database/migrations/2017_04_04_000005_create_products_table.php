<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_categories_id')->nullable()->unsigned();
            $table->foreign('product_categories_id', 'ppc_id_foreign')->references('id')->on('product_categories')->onDelete('set null');
            $table->integer('base_categories_id')->nullable()->unsigned();
            $table->foreign('base_categories_id', 'pbc_id_foreign')->references('id')->on('base_categories')->onDelete('set null');
            $table->integer('product_series_id')->nullable()->unsigned();
            $table->foreign('product_series_id', 'pps_id_foreign')->references('id')->on('product_series')->onDelete('set null');
            $table->integer('base_warranty_id')->nullable()->unsigned();
            $table->foreign('base_warranty_id', 'pbw_id_foreign')->references('id')->on('base_warranty')->onDelete('set null');
            $table->string('name')->nullable(false);
            $table->mediumText('description')->nullable();
            $table->mediumText('technical_description')->nullable();
            $table->decimal('price', 10, 2)->nullable(false);
            $table->integer('quantity')->nullable(false);
            $table->string('product_number')->nullable();
            $table->string('picture')->nullable(false);
            $table->boolean('top_product')->default(0);
            $table->boolean('promo_product')->default(0);
            $table->decimal('promo_price', 10, 2)->nullable(false);
            $table->boolean('xchange')->default(0);
            $table->string('slug')->nullable(false);
            $table->string('locale')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign(['product_categories_id']);
            $table->dropForeign(['product_series_id']);
            $table->dropForeign(['base_warranty_id']);
        });

        Schema::dropIfExists('products');
    }
}
