<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('partners_id')->nullable()->unsigned();
            $table->foreign('partners_id', 'partners_foreign_id')->references('id')->on('partners')->onDelete('cascade');
            $table->string('name')->nullable(false);
            $table->string('slug')->nullable(false);
            $table->string('locale')->index();
            $table->unique(['partners_id', 'locale'], 'partners_unique_locale');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('partners_translations', function (Blueprint $table) {
            $table->dropForeign(['partners_id']);
        });

        Schema::dropIfExists('partners_translations');
    }
}
