<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvantagesTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advantages_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('advantages_id')->nullable()->unsigned();
            $table->foreign('advantages_id')->references('id')->on('advantages')->onDelete('cascade');
            $table->string('name')->nullable(false);
            $table->string('text')->nullable(false);
            $table->string('slug')->nullable(false);
            $table->string('locale')->index();
            $table->unique(['advantages_id', 'locale']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('advantages_translations', function (Blueprint $table) {
            $table->dropForeign(['advantages_id']);
        });

        Schema::dropIfExists('advantages_translations');
    }
}
