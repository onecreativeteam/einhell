<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductCategoriesTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_categories_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_categories_id')->nullable()->unsigned();
            $table->foreign('product_categories_id', 'pc_id_foreign')->references('id')->on('product_categories')->onDelete('cascade');
            $table->string('name')->nullable(false);
            $table->string('slug')->nullable(false);
            $table->string('locale')->index();
            $table->unique(['product_categories_id', 'locale'], 'pc_unique_locale');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_categories_translations', function (Blueprint $table) {
            $table->dropForeign(['product_categories_id']);
        });

        Schema::dropIfExists('product_categories_translations');
    }
}
