<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('base_categories_id')->nullable()->unsigned();
            $table->foreign('base_categories_id', 'bc_id_foreign')->references('id')->on('base_categories')->onDelete('cascade');
            $table->string('name')->nullable(false);
            $table->string('picture')->nullable(false);
            $table->boolean('xchange')->default(0);
            $table->string('slug')->nullable(false);
            $table->string('locale')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_categories', function (Blueprint $table) {
            $table->dropForeign(['base_categories_id']);
        });

        Schema::dropIfExists('product_categories');
    }
}
