<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactThemesTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_themes_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contact_themes_id')->nullable()->unsigned();
            $table->foreign('contact_themes_id')->references('id')->on('contact_themes')->onDelete('cascade');
            $table->string('name')->nullable(false);
            $table->string('slug')->nullable(false);
            $table->string('locale')->index();
            $table->unique(['contact_themes_id', 'locale']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contact_themes_translations', function (Blueprint $table) {
            $table->dropForeign(['contact_themes_id']);
        });

        Schema::dropIfExists('contact_themes_translations');
    }
}
