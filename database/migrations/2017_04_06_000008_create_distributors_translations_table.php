<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistributorsTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distributors_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('distributors_id')->nullable()->unsigned();
            $table->foreign('distributors_id')->references('id')->on('distributors')->onDelete('cascade');
            $table->string('name')->nullable(false);
            $table->string('adress')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile_phone')->nullable();
            $table->string('fax')->nullable();
            $table->string('owner')->nullable();
            $table->string('email')->nullable();
            $table->string('slug')->nullable(false);
            $table->string('locale')->index();
            $table->unique(['distributors_id', 'locale']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('distributors_translations', function (Blueprint $table) {
            $table->dropForeign(['distributors_id']);
        });

        Schema::dropIfExists('distributors_translations');
    }
}
