<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('products_id')->nullable()->unsigned();
            $table->foreign('products_id', 'product_id_foreign')->references('id')->on('products')->onDelete('cascade');
            $table->string('name')->nullable(false);
            $table->mediumText('description')->nullable();
            $table->mediumText('technical_description')->nullable();
            $table->string('slug')->nullable(false);
            $table->string('locale')->index();
            $table->unique(['products_id', 'locale'], 'product_unique_locale');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products_translations', function (Blueprint $table) {
            $table->dropForeign(['products_id']);
        });

        Schema::dropIfExists('products_translations');
    }
}
