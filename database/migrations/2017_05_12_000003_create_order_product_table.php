<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_product', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('orders_id')->nullable()->unsigned();
            $table->foreign('orders_id')->references('id')->on('orders')->onDelete('cascade');
            $table->integer('products_id')->nullable()->unsigned();
            $table->foreign('products_id')->references('id')->on('products')->onDelete('cascade');
            $table->integer('base_warranty_id')->nullable(false);
            $table->boolean('has_product_warranty')->default(0)->nullable(false);
            $table->integer('product_warranty_id')->nullable();
            $table->integer('product_quantity')->default(1)->nullable(false);
            $table->integer('promo_product')->default(0)->nullable(false);
            $table->decimal('p_single_price', 10, 2)->nullable(false);
            $table->decimal('p_total_price', 10, 2)->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_product', function (Blueprint $table) {
            $table->dropForeign(['orders_id']);
            $table->dropForeign(['products_id']);
        });

        Schema::dropIfExists('order_product');
    }
}
