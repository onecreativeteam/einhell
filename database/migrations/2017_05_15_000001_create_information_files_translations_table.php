<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformationFilesTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('information_files_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('information_files_id')->nullable()->unsigned();
            $table->foreign('information_files_id', 'if_foreign_id')->references('id')->on('information_files')->onDelete('cascade');
            $table->string('name')->nullable(false);
            $table->string('slug')->nullable(false);
            $table->string('locale')->index();
            $table->unique(['information_files_id', 'locale']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('information_files_translations', function (Blueprint $table) {
            $table->dropForeign(['information_files_id']);
        });

        Schema::dropIfExists('information_files_translations');
    }
}
