<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Паролата трябва да бъде поне 6 символа и да е потвърдена.',
    'reset' => 'Паролата беше сменена.',
    'sent' => 'Изпратен е линк за смяна на парола на вашият емайл.',
    'token' => 'Кодът е грешен. Моля опитайте отново.',
    'user' => "Не е намерен потребител с този емайл адрес.",

];
