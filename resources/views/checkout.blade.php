@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h3 class="page-title"><span></span>Данни за поръчка</h3>
            </div>
        </div>
        <div class="credential-page order-summary">
            <div class="bg-border">
                <div class="row">
                    <div class="col-md-3 col-md-offset-2">
                        <div class="section">
                            <p>Име и фамилия:</p>
                            <p><strong>{{ $orderInfo['first_name'] . ' ' . $orderInfo['last_name'] }}</strong></p>
                        </div>
                        <div class="section">
                            <p>Email:</p>
                            <p><strong>{{ $orderInfo['email'] }}</strong></p>
                        </div>
                        <div class="section">
                            <p>Телефон:</p>
                            <p><strong>{{ $orderInfo['phone'] }}</strong></p>
                        </div>
                        <div class="section">
                            <p>Адрес за доставка:</p>
                            <p><strong>{{ $orderInfo['adress'] }}</strong></p>
                        </div>
                    </div>
                    <div class="col-md-4 col-md-offset-1">
                        <div class="section">
                            <p>Пощенски код:</p>
                            <p><strong>{{ $orderInfo['postcode'] }}</strong></p>
                        </div>
                        <div class="section">
                            <p>Метод на плащане:</p>
                            <p><strong>{{ $orderInfo['payment_method'] == 'cash' ? 'Наложен платеж' : 'Онлайн Плащане Vpos' }}</strong></p>
                        </div>
                        <div class="section">
                            <p>Коментар:</p>
                            <p>
                                <strong>{{ $orderInfo['order_comment'] }}</strong>
                            </p>
                        </div>
                        <div class="section">
                            <div class="form-group">
                                <div class="radio">
                                    <input id="person" checked="" value="person" type="radio" name="ivoice">
                                    <label for="person"> {{ $orderInfo['is_company'] == '1' ? 'Юридическо лице' : 'Физическо лице' }} </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if ($orderInfo['is_company'] == '1')
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 bottom-separator">

                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h3 class="page-title-inner"><span></span>Данни за фактура:</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-md-offset-2">
                            <div class="section">
                                <p>Име на фирма:</p>
                                <p><strong>{{ $orderInfo['company_name'] }}</strong></p>
                            </div>
                            <div class="section">
                                <p>МОЛ:</p>
                                <p><strong>{{ $orderInfo['company_mol'] }}</strong></p>
                            </div>
                        </div>
                        <div class="col-md-4 col-md-offset-1">
                            <div class="section">
                                <p>Булстат:</p>
                                <p><strong>{{ $orderInfo['company_bulstat'] }}</strong></p>
                            </div>
                            <div class="section">
                                <p>Адрес на фирма:</p>
                                <p><strong>{{ $orderInfo['company_adress'] }}</strong></p>
                            </div>
                            @if (@$orderInfo['add_dds'] == '1')
                            <div class="section">
                                <p>По ЗДДС: <strong>{{ @$orderInfo['add_dds'] == '1' ? 'Да' : 'Не' }}</strong></p>
                            </div>
                            @endif
                        </div>
                    </div>
                @endif
                <br>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <table class="card-product-list">
                            <tr>
                                <th>Продукт:</th>
                                <th></th>
                                <th>Брой:</th>
                                <th>Арт. №: </th>
                                <th>Eд. цена:</th>
                                <th>Крайна цена:</th>
                            </tr>
                            @if (\Cart::content())
                              @foreach (\Cart::content() as $row)
                              <tr>
                                  <td data-th="Продукт:">
                                      <div class="">
                                          <span class="product-image" style="background-image:url(/{{ $row->options->picture }})">
                                          </span>
                                      </div>
                                 </td>
                                 <td>
                                     <div class="title">
                                         {{ $row->name }}
                                     </div>
                                 </td>
                                 <td data-th="Брой:">
                                     <div>
                                         <div class="number-input-wrapper">
                                            <strong>{{ $row->qty }}</strong>
                                         </div>

                                     </div>
                                 </td>
                                  <td data-th="Арт. №:">
                                      <div>
                                          {{ $row->product_number }}
                                      </div>
                                  </td>
                                  <td data-th="Eд. цена:">
                                      <div>
                                          <strong>{{ $row->price() }}лв.</strong>
                                      </div>
                                  </td>
                                  <td data-th="Крайна цена:">
                                      <div>
                                          <strong>{{ $row->subtotal() }}лв.</strong>
                                      </div>
                                  </td>
                              </tr>
                              @endforeach
                              @if (Auth::check() && Auth::user()->has_user_discount == '1')
                                <tr>
                                  <td data-th="Продукт:">
                                      <div class="">
                                        
                                      </div>
                                 </td>
                                 <td>
                                     <div class="title">
                                         ОТСТЪПКА ЗА ПОТРЕБИТЕЛЯ
                                     </div>
                                 </td>
                                 <td data-th="Брой:">
                                     <div>
                                        Процент: {{ Auth::user()->user_discount_value }} %
                                     </div>
                                 </td>
                                  <td data-th="Арт. №:">
                                      <div>
                                          
                                      </div>
                                  </td>
                                  <td data-th="Eд. цена:">
                                      <div>
                                          <strong></strong>
                                      </div>
                                  </td>
                                  <td data-th="Крайна цена:">
                                      <div>
                                          <strong>{{ number_format( (\Cart::subtotal() * (Auth::user()->user_discount_value / 100) ), 2 ) }} лв.</strong>
                                      </div>
                                  </td>
                                </tr>
                              @endif
                            @endif
                        </table>
                        @if (Auth::check() && Auth::user()->has_user_discount == '1')
                          <p class="total text-right">Общо: {{ number_format( \Cart::subtotal() - ( \Cart::subtotal() * (Auth::user()->user_discount_value / 100) ), 2 ) }} лв.<span>(c ДДС)</span></p>
                        @else
                          <p class="total text-right">Общо: {{ \Cart::subtotal() }}лв.<span>(c ДДС)</span></p>
                        @endif
                        <br>
                        <br>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-3 col-md-offset-2">
                        <a href="{{ route('cart.order.info') }}" class="submit"><img src="/img/back-icon.png" alt="">  Обратно</a>
                    </div>
                    <div class="col-md-4 col-md-offset-1 text-right">
                        <a href="{{ route('cart.order.post') }}" class="submit">Поръчай</a>
                    </div>
                </div>
            </div>
        </div>
        <ul class="breadcrumb-wrapper clearfix">
            <li><a href="{{ route('home') }}"><span class="home-breadcrumb"></span></a></li>
            <li><a href="{{ route('cart.index') }}">Моята количка</a></li>
            <li><a href="{{ route('cart.order.info') }}">Данни за поръчка</a></li>
        </ul>
    </div>
@endsection

@section('page-scripts')
@endsection
