@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                    <h3 class="page-title"><span></span>Данни за поръчка</h3>
            </div>
        </div>
        <div class="credential-page ">
            <div class="bg-border text-center thank-you">
                <p>Вашата поръчка беше приета успешно!</p>
                <p>Моля, проверете имейл адреса си.</p> 
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')

@endsection
