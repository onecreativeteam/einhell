<!DOCTYPE html>
<html lang="bg">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Преглед на фактура</title>
    <!-- Bootstrap CSS -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="invoice-pdf-preview-page">
    <div class="container">
      <main>
        <section class="preview container-fluid center-block text-center">
          <table class="table table-bordered">
            <thead>
              <tr>
                <td class="text-center"><b>Получател</b></td>
                <td class="text-center"><b>Оригинал</b></td>
                <td class="text-center"><b>Доставчик</b></td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class="text-left" rowspan="4" width="38%">
                  <div class="form-group">
                    <div><b>Идент. №:</b> 
                      {{ $order->company_bulstat }}
                    </div>
                  </div>
                  <div class="form-group">
                    
                    <div><b>Идент. № по ДДС:</b>
                      {{ $order->company_bulstat }}
                    </div>
                  </div>
                  <div><b>Име на фирмата:</b> {{ $order->company_name }}</div>
                  <div><b>Адрес:</b> {{ $order->company_adress }}</div>
                  <div><b>МОЛ:</b> {{ $order->company_mol }}</div>
                </td>
                <td class="text-center">
                  <div class="main-title text-center"><b>Фактура</b></div>
                    <div class="text-center"><b>№ generirana123</b></div>
                    <div class="text-center">{{ \Carbon\Carbon::now()->format('m.d.Y') }} г.</div>
                </td>
                <td class="text-left" rowspan="4" width="38%">
                  <div class="form-group">
                    <div><b>Идент. №</b> 103566033</div>
                  </div>
                  <div class="form-group">
                  <div><b>Идент. № по ДДС:</b> BG103566033</div>
                  </div>
                  <div><b>Име на фирмата:</b> Айнхел България ООД</div>
                  <div><b>Адрес:</b>гр. Варна, бул. Цар Освободител 331</div>
                  <div><b>МОЛ:</b>Румен Радев</div>
                </td>
              </tr>
              <tr>
                <td class="text-center">
                  <small>Дебитно известие към факт. №</small>
                </td>
              </tr>
              <tr>
                <td class="text-center">
                  <small>Кредитно известие към факт. №</small>
                </td>
              </tr>
              <tr>
                <td class="text-center">
                  <b>гр. Варна</b>
                </td>
              </tr>
            </tbody>
          </table>

          <table class="table table-bordered">
            <thead>
              <tr>
                <td colspan="6" height="40" class="reason"><small class="text-muted">Причина за издаване на известието</small></td>
              </tr>
              <tr>
                <th><b>Номер</b></th>
                <th><b>Наименование на стоките и услугите</b></th>
                <th><b>Мярка</b></th>
                <th><b>Количество</b></th>
                <th ><b>Ед. цена</b></th>
                <th><b>Стойност</b></th>
              </tr>
            </thead>
            <tbody>
              @foreach ($order->orderProducts as $k => $orderProduct)
              <tr>
                <td>{{ $k+1 }}</td>
                <td><b>{{ $orderProduct->product->name }}</b></td>
                <td><b>бр.</b></td>
                <td><b>{{ $orderProduct->product_quantity }}</b></td>
                <td>{{ number_format($orderProduct->p_single_price - ($orderProduct->p_single_price * 0.2), 2) }}</td>
                <td>{{ number_format($orderProduct->p_total_price - ($orderProduct->p_total_price * 0.2), 2) }}</td>
              </tr>
              @endforeach
              @if ($order->has_user_discount)
              <tr>
                <td></td>
                <td><b>Отстъпка: </b></td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{ $order->order_discount_total }}</td>
              </tr>
              @endif
              <tr>
                <td></td>
                <td><b>Данъчна основа: </b></td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{ number_format( (($order->order_total) - ($order->order_discount_total)) - ((($order->order_total) - ($order->order_discount_total)) * 0.2), 2 ) }}</td>
              </tr>
              <tr>
                <td></td>
                <td><b>ДДС 20%:</b></td>
                <td></td>
                <td></td>
                <td></td>
                <td> {{ number_format($order->order_total * 0.2, 2) }}</td>
              </tr>
              <tr>
                <td><b>Словом:</b></td>
                <td>{{ \App\Services\NumberToLettersService::numberToLetters($order->order_total) }}</td>
                <td></td>
                <td></td>
                <td><b>Сума за плащане:</b></td>
                <td>{{ $order->order_total }}</td>
              </tr>
            </tbody>
          </table>

          <div class="avoid-page-break">
            <table class="table table-bordered">
              <tbody>
                <tr>
                  <td>
                    <div class="text-center form-group">Дата на данъчното събитие/плащане</div>
                    <div class="text-center">{{ \Carbon\Carbon::now()->format('m.d.Y') }} г. </div>
                  </td>
                  <td colspan="2" class="text-left">
                    <div class="form-group">
                      <div class="choice"><img src="{{ asset('img/bullet-unselected.png') }}" alt=""> Неначисляване на ДДС;</div>
                      <div class="choice"><img src="{{ asset('img/bullet-unselected.png') }}" alt=""> Нулева ставка на ДДС;</div>
                      <div class="choice"><img src="{{ asset('img/bullet-selected.png') }}" alt=""> ДДС</div>
                    </div>
                    <div class="small">Изискуем от получателя</div>
                  </td>
                </tr>
                <tr>
                  <td><b>Получател:</b></td>
                  <td>Основание:</td>
                  <td><b>Съставител:</b></td>
                </tr>
                <tr>
                  <td rowspan="4" style="vertical-align: bottom; padding-left: 5%; padding-right: 5%; padding-bottom: 2.5%;" class="text-right text-muted">
                    <div class="xxsmall text-left" style="display: inline-block; width: 60%;">/име и фамилия/</div>
                    <div class="xxsmall text-right" style="display: inline-block; width: 40%;">/подпис/</div>
                  </td>
                  <td>
                    <div class="form-group">
                      <div class="text-left">Начин на плащане:</div>
                        <!-- <div class="choice"><img src="{{ asset('img/bullet-unselected.png') }}" alt=""> в брой</div> -->
                        <div class="choice"><img src="{{ asset('img/bullet-selected.png') }}" alt=""> Наложен платеж</div>
                    </div>
                    <!-- <div>Моля преведете сумата към:</div> -->
                  </td>
                  <td style="vertical-align: bottom; padding-left: 5%; padding-right: 5%;" class="text-right text-muted">
                    <div class="xxsmall text-left" style="display: inline-block; width: 60%;">/име и фамилия/</div>
                    <div class="xxsmall text-right" style="display: inline-block; width: 40%;">/подпис/</div>
                  </td>
                </tr>
<!--                 <tr class="text-left">
                  <td colspan="2">
                    Прокредит Банк
                  </td>
                </tr>
                <tr class="text-left">
                  <td colspan="2">
                    <b>Сметка в лева:</b> IBAN xxxxxx
                  </td>
                </tr>
                <tr class="text-left">
                  <td colspan="2">
                    <b>BIC:</b> xxxxx
                  </td>
                </tr> -->
              </tbody>
            </table>
          </div>
        </section>
      </main>
    </div>
    <!-- jQuery -->
    <script src="//code.jquery.com/jquery.js"></script>
    <!-- Bootstrap JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  </body>
</html>
