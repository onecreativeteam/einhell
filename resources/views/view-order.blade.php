@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h3 class="page-title"><span></span>Данни за поръчка</h3>
            </div>
        </div>
        <div class="credential-page order-summary">
            <div class="bg-border">
                <div class="row">
                    <div class="col-md-3 col-md-offset-2">
                        <div class="section">
                            <p>Име и фамилия:</p>
                            <p><strong>{{ $order->first_name .' '. $order->last_name  }}</strong></p>
                        </div>
                        <div class="section">
                            <p>Email:</p>
                            <p><strong>{{ $order->email }}</strong></p>
                        </div>
                        <div class="section">
                            <p>Телефон:</p>
                            <p><strong>{{ $order->phone }}</strong></p>
                        </div>
                        <div class="section">
                            <p>Адрес за доставка:</p>
                            <p><strong>{{ $order->adress }}</strong></p>
                        </div>
                    </div>
                    <div class="col-md-4 col-md-offset-1">
                        <div class="section">
                            <p>Пощенски код:</p>
                            <p><strong>{{ $order->postcode }}</strong></p>
                        </div>
                        <div class="section">
                            <p>Метод на плащане:</p>
                            <p><strong>{{ $order->payment_method == 'cash' ? 'Наложен платеж' : 'Онлайн Плащане Vpos' }}</strong></p>
                        </div>
                        <div class="section">
                            <p>Коментар:</p>
                            <p>
                                <strong>{{ $order->order_comment }}</strong>
                            </p>
                        </div>
                        <div class="section">
                            <div class="form-group">
                                <div class="radio">
                                    <input id="person" checked="" value="person" type="radio" name="ivoice">
                                    <label for="person"> {{ $order->is_company == '1' ? 'Юридическо лице' : 'Физическо лице' }} </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if ($order->is_company == '1')
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 bottom-separator">

                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h3 class="page-title-inner"><span></span>Данни за фактура:</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-md-offset-2">
                            <div class="section">
                                <p>Име на фирма:</p>
                                <p><strong>{{ $order->company_name }}</strong></p>
                            </div>
                            <div class="section">
                                <p>МОЛ:</p>
                                <p><strong>{{ $order->company_mol }}</strong></p>
                            </div>
                        </div>
                        <div class="col-md-4 col-md-offset-1">
                            <div class="section">
                                <p>Булстат:</p>
                                <p><strong>{{ $order->company_bulstat }}</strong></p>
                            </div>
                            <div class="section">
                                <p>Адрес на фирма:</p>
                                <p><strong>{{ $order->company_adress }}</strong></p>
                            </div>
                            @if (@$order->add_dds == '1')
                            <div class="section">
                                <p>По ЗДДС: {{ $order->add_dds == 1 ? 'Да' : 'Не' }}</p>
                            </div>
                            @endif
                        </div>
                    </div>
                @endif
                <br>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <table class="card-product-list">
                            <tr>
                                <th>Продукт:</th>
                                <th></th>
                                <th>Брой:</th>
                                <th>Арт. №: </th>
                                <th>Eд. цена:</th>
                                <th>Крайна цена:</th>

                            </tr>
                            @if (count($order->orderProducts))
                              @foreach ($order->orderProducts as $orderProduct)
                              <tr>
                                  <td data-th="Продукт:">
                                      <div class="">
                                          <span class="product-image" style="background-image: url(/{{ ( !is_null($orderProduct->product->picture) || $orderProduct->product->picture != '' ) ? $orderProduct->product->picture : ( count($orderProduct->product->pictures) ? $orderProduct->product->pictures[0]->resource : '' )  }})">
                                          </span>
                                      </div>
                                 </td>
                                 <td>
                                     <div class="title">
                                         {{ $orderProduct->product->name }}
                                     </div>
                                 </td>
                                 <td data-th="Брой:">
                                     <div>
                                         <div class="number-input-wrapper">
                                            <strong>{{ $orderProduct->product_quantity }}</strong>
                                         </div>

                                     </div>
                                 </td>
                                  <td data-th="Арт. №:">
                                      <div>
                                          {{ $orderProduct->product->product_number }}
                                      </div>
                                  </td>
                                  <td data-th="Eд. цена:">
                                      <div>
                                          <strong> {{ $orderProduct->p_single_price }} лв.</strong>
                                      </div>
                                  </td>
                                  <td data-th="Крайна цена:">
                                      <div>
                                          <strong> {{ $orderProduct->p_total_price }} лв.</strong>
                                      </div>
                                  </td>
                              </tr>
                              @endforeach
                            @endif
                        </table>
                        <p class="total text-right">Общо: {{ $order->order_total }} лв. <span>(c ДДС)</span></p>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </div>
        <ul class="breadcrumb-wrapper clearfix">
            <li><a href="/"><span class="home-breadcrumb"></span></a></li>
            <li><a href="{{ route('cart.index') }}">Моята количка</a></li>
            <li>Данни за поръчка</li>
        </ul>
    </div>
@endsection

@section('page-scripts')
@endsection
