@extends('layouts.app')

@section('content')
<div class="container credential-page ">
    <div class="bg-border">
        <div class="row">
            <div class="col-md-4 col-md-offset-1 colum-separator">
                <p class="title">Регистрация:</p>
                <form role="form" method="POST" action="{{ route('register.post') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="formicon formicon-user"></i></span>
                            <input pattern="^[a-zA-Z\u0400-\u04ff\s]{2,}$" title="Полето трябва да съдържа минимум 2 символа. Позволени са само букви." autocomplete="off" placeholder="Име" id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required autofocus>
                        </div>
                        @if ($errors->has('first_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('first_name') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="formicon formicon-user"></i></span>
                            <input pattern="^[a-zA-Z\u0400-\u04ff\s]{2,}$" title="Полето трябва да съдържа минимум 2 символа. Позволени са само букви." autocomplete="off" placeholder="Фамилия" id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required autofocus>
                        </div>
                        @if ($errors->has('last_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="formicon formicon-moblie"></i></span>
                            <input pattern="^[0-9\-\+\s\(\)]{10,15}$" title="Полето трябва да съдържа между 10 и 15 символа. Позволени са +-() и интервал." autocomplete="off" placeholder="Телефон" id="phone" type="tel" class="form-control" name="phone" value="{{ old('phone') }}" required autofocus>
                        </div>
                        @if ($errors->has('phone'))
                            <span class="help-block">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
                    </div>

                    {{-- <div class="form-group{{ $errors->has('adress') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="formicon formicon-mail"></i></span>
                            <input id="adress" type="text" class="form-control" name="adress" value="{{ old('adress') }}" required autofocus>
                        </div>
                        @if ($errors->has('adress'))
                            <span class="help-block">
                                <strong>{{ $errors->first('adress') }}</strong>
                            </span>
                        @endif
                    </div> --}}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="formicon formicon-mail"></i></span>
                            <input autocomplete="off" placeholder="Email" id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                        </div>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="formicon formicon-pass"></i></span>
                            <input autocomplete="off" placeholder="Парола" id="password" type="password" class="form-control" name="password" required>
                        </div>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="formicon formicon-pass"></i></span>
                            <input pattern=".{6,}" title="Полето трябва да съдържа минимум 6 символа." autocomplete="off" placeholder="Повтори парола"  id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        </div>
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('accepted_tc') ? ' has-error' : '' }}">
                        <div class="checkbox">
                            <input id="accepted_tc" type="checkbox" name="accepted_tc" value="1">
                            <label for="accepted_tc"> Приемам общите условия </label>
                        </div>
                        @if ($errors->has('accepted_tc'))
                            <span class="help-block">
                                <strong>{{ $errors->first('accepted_tc') }}</strong>
                            </span>
                        @endif
                    </div>

                        <button type="submit" class="submit">
                            Регистрация
                        </button>
                </form>

            </div>
            <div class="col-md-4 col-md-offset-2">
                <p class="title">Имаш профил?</p>
                <a href="{{ route('login') }}" class="register">Вход в сайта</a>
                    <a href="{{ url(app()->getLocale() . '/redirect/facebook') }}" class="fb-login register"><span><img src="\img\fb-register.png" alt=""></span>Вход с Facebook</a>
            </div>
        </div>
    </div>
    <ul class="breadcrumb-wrapper clearfix">
        <li><a href="/"><span class="home-breadcrumb"></span></a></li>
        <li><a href="#">Регистрация</a></li>
    </ul>
</div>
@endsection
