@extends('layouts.app')

@section('content')
<div class="container credential-page ">
    <div class="bg-border">
        <div class="row">

            @if (count($errors) > 0)
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            @endif
            <div class="col-md-4 col-md-offset-1 colum-separator">
                <p class="title">Вход в сайта:</p>
                <form id="login-form" role="form" method="POST" action="{{ route('login.post') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="formicon formicon-mail"></i></span>
                            <input placeholder="Email" id="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                        </div>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                            <div class="input-group">
                                <span class="input-group-addon"><i class="formicon formicon-pass"></i></span>
                                <input pattern=".{6,}" title="Полето трябва да съдържа минимум 6 символа." placeholder="Парола" id="password" type="password" class="form-control" name="password" required>

                            </div>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                    </div>

                    <div class="row remember-me-wrapper">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <div class="checkbox">
                                    <input id="remember-me"type="checkbox" name="remember">
                                    <label for="remember-me"> Запомни ме </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 text-right">
                            <a class="forgot-pass-link" href="{{ route('password.reset') }}">
                                Забравена парола?
                            </a>
                        </div>
                    </div>
                    <div class="row custom-buttons">
                        <div class="col-xs-12">
                            <button class="submit" type="submit">
                                Вход
                            </button>
                        </div>
                        <div class="col-xs-12">
                            <a href="{{ url(app()->getLocale() . '/redirect/facebook') }}" class="fb-login"><span><img src="\img\fb-register.png" alt=""></span>Вход с Facebook</a>
                        </div>
                    </div>

                </form>
            </div>
            <div class="col-md-4 col-md-offset-2">
                <p class="title">Регистрация:</p>
                <form role="form" method="POST" action="{{ route('register.post') }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('register_email') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="formicon formicon-mail"></i></span>
                            <input autocomplete="off" placeholder="Email" id="register_email" type="email" class="form-control" name="register_email" value="{{ old('register_email') }}" required>
                        </div>
                        @if ($errors->has('register_email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('register_email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="formicon formicon-pass"></i></span>
                            <input autocomplete="off" placeholder="Парола" id="password" type="password" class="form-control" name="password" required>
                        </div>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="formicon formicon-pass"></i></span>
                            <input pattern=".{6,}" title="Полето трябва да съдържа минимум 6 символа." autocomplete="off" placeholder="Повтори парола"  id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        </div>
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('accepted_tc') ? ' has-error' : '' }}">
                        <div class="checkbox">
                            <input id="accepted_tc" type="checkbox" name="accepted_tc" value="1">
                            <label for="accepted_tc"> Приемам общите условия </label>
                        </div>
                        @if ($errors->has('accepted_tc'))
                            <span class="help-block">
                                <strong>{{ $errors->first('accepted_tc') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="custom-buttons">
                        <button type="submit" class="submit">
                            Регистрация
                        </button>
                    </div>

                </form>

            </div>
        </div>
    </div>
    <ul class="breadcrumb-wrapper clearfix">
        <li><a href="/"><span class="home-breadcrumb"></span></a></li>
        <li><a href="#">Вход</a></li>
    </ul>
</div>

@endsection
