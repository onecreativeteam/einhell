@extends('layouts.app')

@section('content')
<div class="container credential-page">
    <div class="bg-border">
        <div class="row">
            <div class="col-md-4 col-md-offset-1 colum-separator">

                <p class="title">Промени парола:</p>
                <form  role="form" method="POST" action="{{ route('password.reset.post') }}">
                {{ csrf_field() }}
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="formicon formicon-mail"></i></span>
                        <input placeholder="Email"  id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>
                    </div>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="formicon formicon-pass"></i></span>
                        <input placeholder="Парола" id="password" type="password" class="form-control" name="password" required>
                    </div>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="formicon formicon-pass"></i></span>
                        <input placeholder="Повтори парола"  id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </div>
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>

                <button type="submit" class="submit">
                    Изпрати
                </button>
            </form>
            </div>
        </div>
    </div>
</div>
@endsection
