@extends('layouts.app')

<!-- Main Content -->
@section('content')
<div class="container credential-page">
    <div class="bg-border">
        <div class="row">
            <div class="col-md-4 col-md-offset-1 colum-separator">
                <p class="title">Възстановяване на парола:</p>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <form id="reset-pass" role="form" method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="formicon formicon-mail"></i></span>
                            <input placeholder="Email"  id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                        </div>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <button class="submit" type="submit">
                        Изпрати
                    </button>
                </form>
            </div>
        </div>
    </div>
    <ul class="breadcrumb-wrapper clearfix">
        <li><a href="/"><span class="home-breadcrumb"></span></a></li>
        <li><a href="#">Възстановяване на парола</a></li>
    </ul>
</div>
@endsection
