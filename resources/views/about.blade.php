@extends('layouts.app')

@section('content')
<div class="container tabs-page">
    <div class="col-md-12">
        <img src="/{{$coverPhoto->file}}" class="main-image img-responsive" alt="">
    </div>
    <div class="clearfix">
        <div class="col-md-3">
            <div class="sidebar-wrapper">
                <h3 class="sidebar-title">ЗА НАС</h3>
                <ul class="sidebar-menu">
                    @foreach ($pages as $k => $page)
                    <li>
                        <p data-toggle="tab" href="#tab-{{$k}}">{{ $page->name }}</p>
                    </li>
                    @endforeach
                    <li>
                        <p data-toggle="tab" href="#tab-world">Айнхел по света</p>
                    </li>
                    <li>
                        <p data-toggle="tab" href="#tab-partner">Партньори в България</p>
                    </li>
                    <li>
                        <p data-toggle="tab" class="materials" href="#tab-materials">Рекламни материали</p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-9">
            <div class="tab-content bg-border">
                @foreach ($pages as $k => $page)
                <div id="tab-{{$k}}" class="tab-pane fade {{$k == 0 ? ' active in' : '' }}">

                    <div class="text-content">
                        {!! $page->description !!}
                    </div>
                </div>
                @endforeach
                <div id="tab-world" class="tab-pane fade">
                    <h3 class="title">Айнхел по света</h3>
                    <div class="text-content">
                        <div class="text-content">
                            {!! app()->getLocale() == 'bg' ? @$mapPage->text : @$mapPage->translated->text !!}
                        </div>
                        <div id="vmap"></div>
                    </div>
                </div>
                <div id="tab-partner" class="tab-pane fade">
                    <h3 class="title">Партньори в България</h3>
                    <div class="text-content">
                        <img src="\img\partners-logos.png" alt="">
                    </div>
                </div>
                <div id="tab-materials" class="tab-pane fade">
                    <h3 class="title">Рекламни материали</h3>
                    <div class="text-content">
                        <ol>
                        @foreach ($files as $file)
                           <li><a target="_blank" href="/{{ $file->file }}"> {{ $file->name }}</a></li>
                        @endforeach
                        <ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-12">
        <ul class="breadcrumb-wrapper clearfix">
            <li><a href="{{ route('home') }}"><span class="home-breadcrumb"></span></a></li>
            <li class="initial">ЗА КОМПАНИЯТА</li>
        </ul>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
@if(!empty($map))
    worldMap = <?php echo json_encode($map); ?>;
@endif
$(document).ready( function() {

    initActiveTab();

    $('ul.sidebar-menu li').click( function() {
        var text = $(this).find('p').text();
        $('ul.breadcrumb-wrapper li.added').remove();
        $('ul.breadcrumb-wrapper li.initial').css('margin-right', '2px');
        $('ul.breadcrumb-wrapper').append('<li class="added"> - '+text+'</li>');
    });

    jQuery('#vmap').vectorMap({
        map: 'world_en',
        backgroundColor: '#fff',
        borderColor: '#818181',
        borderOpacity: 0.25,
        borderWidth: 1,
        color: '#d4d4d4',
        enableZoom: true,
        hoverColor: 'blue',
        hoverOpacity: null,
        normalizeFunction: 'linear',
        scaleColors: ['#fff', '#e3370e'],
        selectedColor: '#e3370e',
        selectedRegions: worldMap,
        showTooltip: false,
        regionsSelectable: false,
        multiSelectRegion: false,
        onLoad: function(event, map)
        {
            // jQuery('#vmap').vectorMap('zoomIn');
            event.preventDefault();
        },
        onLabelShow: function(event, label, code)
        {
            event.preventDefault();
        },
        onRegionOver: function(event, code, region)
        {
            event.preventDefault();
        },
        onRegionOut: function(event, code, region)
        {
            event.preventDefault();
        },
        onRegionClick: function(event, code, region)
        {
            event.preventDefault();
        },
        onResize: function(event, width, height)
        {
            event.preventDefault();
        }
    });

});

function initActiveTab()
{
    var url = "{{ route('about.index') }}";
    var currentUrl = window.location.href;

    var active = currentUrl.replace(url+'?', '');
    // console.log(active);
    $('.sidebar-menu p[href="'+active+'"]' ).click();
}
</script>
@endpush

@section('page-scripts')

@endsection
