<div class="sidebar-wrapper">
    <h3 class="sidebar-title">Търсене</h3>
    <h4 class="category-title">По категория <span></span></h4>
    <div class="sidebar-menu"  id="sidebar-accordion">
        <div class="panel panel-default">
            @if ($categories)
                @foreach ($categories as $k => $base)
                    @if (count($base->category) < 1)
                        <a class="singleBase" href="{{ route('product.base.category', [$base->slug]) }}">{{ $base->name }}</a>
                    @else
                        <p data-toggle="collapse" class="{{ ( ( $baseCategory && $baseCategory->slug == $base->slug ) || ($category && $category->category->slug == $base->slug) ) ? 'active' : 'collapsed' }}" data-parent="#sidebar-accordion" href="#collapse{{$k}}">{{ $base->name }}
                            <span class="img"></span>
                        </p>
                    @endif
                    @if (count($base->category))
                        <ul id="collapse{{$k}}" class="panel-collapse collapse sidebar-dropdown {{ ( ( $baseCategory && $baseCategory->slug == $base->slug ) || ($category && $category->category->slug == $base->slug) ) ? 'in' : '' }}">
                            @foreach ($base->category as $cat)
                                <li class="{{ ($category && $category->slug == $cat->slug) ? 'active' : '' }}"><a class="{{ ($category && $category->slug == $cat->slug) ? 'active' : '' }}" href="{{ route('product.category', [$cat->slug]) }}">{{ $cat->name }}</a></li>
                            @endforeach
                        </ul>
                    @endif
                @endforeach
            @endif
            <a class="singleBase" href="{{ route('product.promotions') }}">Промоции</a>
        </div>
    </div>
    @if (Route::current()->getName() !== 'product.inner')
        @if ($series)
            <h4 class="category-title">По серия <span></span></h4>
            <div class="aditional-options">
                @foreach ($series as $k => $serie)
                <div class="form-group">
                    <div class="checkbox">
                        <input <?= (isset(session('filters')['serie']) && in_array($serie->id, session('filters')['serie'])) ? 'checked' : '' ?> id="{{str_replace(' ', '', strtolower($serie->name))}}" type="checkbox" class="serieFilter" value="{{$serie->id}}" name="{{str_replace(' ', '', strtolower($serie->name))}}">
                        <label for="{{str_replace(' ', '', strtolower($serie->name))}}"> {{$serie->name}} </label>
                    </div>
                </div>
                @endforeach
                <div class="form-group">
                    <div class="checkbox">
                        <input <?= (isset(session('filters')['xchange'])) ? 'checked' : '' ?> id="xchange" type="checkbox" class="xChangeFilter" value="1" name="xchange">
                        <label for="xchange"> Power X-Change </label>
                    </div>
                </div>
            </div>
        @endif
        <h4 class="category-title">По цена <span></span></h4>
        <div class="slider-range-wrapper">
            <div id="slider-range"></div>
            <input type="text" id="amount" readonly>
        </div>
        <div id="querySet" data-url="{{ URL::route( 'query.filters.set' ) }}"></div>
        <div id="queryUnset" data-url="{{ URL::route( 'query.filters.unset' ) }}"></div>
        <div id="loadQuery" data-url="{{ URL::route( 'query.filters.load' ) }}"></div>
        <div id="clearQuery" data-url="{{ URL::route( 'query.filters.clear' ) }}"></div>
    @endif
</div>

@section('page-scripts')
@if(Route::current()->getName() !== 'home' ||  Route::current()->getName() !== 'product.inner')
    <script type="text/javascript" src="{!! asset('js/home-filter.js') !!}" charset="utf-8"></script>
    <script type="text/javascript">
        $( function() {

            $( "#slider-range" ).slider({
                range: true,
                min: 0,
                max: "{{ $maxPrice }}",
                values: [ "{{ @session('filters')['price']['from'] }}" , "{{ @session('filters')['price']['to'] }}" ],
                slide: function(event, ui) {
                    $("#amount").val("Цена: "+ ui.values[0] + " - " + ui.values[1] + "лв.");
                },
                stop: function( event, ui ) {

                    obj = {};
                    obj.from = ui.values[0];
                    obj.to = ui.values[1];

                    setFilter(obj, 'price');
                }
            });

            $("#amount").val("Цена: "+ + $("#slider-range" ).slider("values", 0) + " - " + $("#slider-range").slider( "values", 1 )+ "лв." );

        });
    </script>
@endif
@append
