@extends('layouts.app_admin')

@section('content')
<section class="content-header">
     <h1>
        <h3 class="box-title">Редактирай Продуктова Категория</h3>
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                @if(Session::has('success'))
                    <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                @endif
                <div class="box-body">
                    <div id="tabs">
                        <ul>
                        @foreach ($supportedLocales as $k => $locale)
                            <li><a href="#tabs-{{$locale}}">{{$locale}}</a></li>
                        @endforeach
                        </ul>
                        {!! Form::open( array('action' => array('Admin\ProductCategoriesController@update', $category->id), 'method' => 'PUT', 'files' => 'true', 'class' => '' ) ) !!}
                            @foreach ($supportedLocales as $lang => $locale)
                                <div id="tabs-{{$locale}}">
                                    <div class="form-group">
                                        {!! Form::label('Име:') !!}
                                        {!! Form::text("translations[{$lang}][name]", isset($category->translations[$locale]) ? $category->translations[$locale]->name : $category->name, array('required', 'class'=>'form-control', 'placeholder'=>'Име')) !!}
                                    </div>
                                </div>
                            @endforeach
                            <div class="form-group">
                                {!! Form::label('Закачи към базова:') !!}
                                {!! Form::select('base_categories_id', array('0'=>'Избери Базова Категория')+$baseCategories, $category->base_categories_id, ['class' => 'form-control baseCatSelect']) !!}
                            </div>
                            <div class="form-group">
                                <div class="checkbox">
                                    <input id="xchange" {{ $category->xchange == 1 ? 'checked' : '' }} type="checkbox" name="xchange" value="1">
                                    <label for="xchange"> Добави към Power-X-Change: </label>
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('178х119 /снимки/:') !!}
                                {!! Form::file('picture') !!}
                                <img style="width:150px;height:150px;margin-top:10px" src="/{{$category->picture}}"/>
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Промяна', array('class'=>'btn btn-primary')) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@push('scripts')
<script type="text/javascript">
$(function() {
    $("#tabs").tabs();
});
</script>
@endpush
