@extends('layouts.app_admin')

@section('content')
<section class="content-header">
     <h1>
        Страница XChange
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box">
				<div class="box-header">
                    @if(Session::has('success'))
                        <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
                    @endif
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
				<div class="box-body">
                    <div id="tabs">
                        <ul>
                        @foreach ($supportedLocales as $k => $locale)
                            <li><a href="#tabs-{{$locale}}">{{$locale}}</a></li>
                        @endforeach
                        </ul>
                        {!! Form::open( array('action' => array('Admin\XChangePageController@store'), 'method' => 'POST', 'files' => 'true', 'class' => '' ) ) !!}
                            @foreach ($supportedLocales as $lang => $locale)
                                <div id="tabs-{{$locale}}">
                                    <div class="form-group">
                                        {!! Form::label('Заглавие:') !!}
                                        {!! Form::text("translations[{$lang}][name]", null, array('required', 'class'=>'form-control', 'placeholder'=>'Име')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('Текст:') !!}
                                        {!! Form::textarea("translations[{$lang}][text]", null, array('required', 'class'=>'ckeditor form-control', 'placeholder'=>'Текст')) !!}
                                    </div>
                                </div>
                            @endforeach
                            <div class="form-group">
                                {!! Form::label('Снимка (471x627px):') !!}
                                {!! Form::file('picture') !!}
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Запази', array('class'=>'btn btn-primary')) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
    		</div>
    	</div>
    </div>
</section>
@endsection
@push('scripts')
<script type="text/javascript">
$(function() {
    $("#tabs").tabs();
    CKEDITOR.replace( 'ckeditor' );
});
</script>
@endpush
