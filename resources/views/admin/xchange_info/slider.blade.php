@extends('layouts.app_admin')

@section('content')
<section class="content-header">
     <h1>
        Информация за Слайдера
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    @if (count($errors) > 0)
                       <div class="alert alert-danger">
                           <ul>
                           @foreach ($errors->all() as $error)
                               <li>{{ $error }}</li>
                           @endforeach
                           </ul>
                       </div>
                   @endif
                    @if(Session::has('success'))
                        <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
                    @endif
                </div>
                <div class="box-body">
                    {!! Form::open( array('action' => array('Admin\XChangeInfoController@postSliderInfo'), 'method' => 'POST', 'files' => 'true', 'class' => '' ) ) !!}
                        <div class="form-group">
                            {!! Form::label('Заглавие Слайдер:') !!}
                            {!! Form::text('slider_title', @$data->slider_title, array('required', 'class'=>'form-control', 'placeholder'=>'Заглавие Слайдер')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Текст Слайдер:') !!}
                            {!! Form::textarea('slider_text', @$data->slider_text, array('required', 'class'=>'form-control ckeditor', 'placeholder'=>'Текст Слайдер')) !!}
                        </div>
                        <div class="form-group">
                           {!! Form::label('Снимка Слайдер:') !!}
                           {!! Form::file('slider_picture') !!}
                           @if ($data->slider_picture)
                            <img src="/{{ $data->slider_picture }}" width="150px" height="150px">
                           @endif
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Запази', array('class'=>'btn btn-primary')) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@push('scripts')
<script type="text/javascript">
$(function() {
    CKEDITOR.replace( 'ckeditor' );
});
</script>
@endpush
