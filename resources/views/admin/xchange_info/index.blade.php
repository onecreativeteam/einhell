@extends('layouts.app_admin')

@section('content')
<section class="content-header">
     <h1>
        Информация за Страницата
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    @if (count($errors) > 0)
                       <div class="alert alert-danger">
                           <ul>
                           @foreach ($errors->all() as $error)
                               <li>{{ $error }}</li>
                           @endforeach
                           </ul>
                       </div>
                   @endif
                    @if(Session::has('success'))
                        <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
                    @endif
                </div>
                <div class="box-body">
                    {!! Form::open( array('action' => array('Admin\XChangeInfoController@store'), 'method' => 'POST',  'class' => '' ) ) !!}
                        <div class="form-group">
                            {!! Form::label('Секция 1 заглавие:') !!}
                            {!! Form::text('section_one_title', @$data->section_one_title, array('required', 'class'=>'form-control', 'placeholder'=>'Секция 1 заглавие')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Секция 1 текст:') !!}
                            {!! Form::textarea('section_one_text', @$data->section_one_text, array('required', 'class'=>'form-control ckeditor', 'placeholder'=>'Секция 1 текст')) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('Секция 2 заглавие:') !!}
                            {!! Form::text('section_two_title', @$data->section_two_title, array('required', 'class'=>'form-control', 'placeholder'=>'Секция 2 заглавие')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Секция 2 текст:') !!}
                            {!! Form::textarea('section_two_text', @$data->section_two_text, array('required', 'class'=>'form-control ckeditor', 'placeholder'=>'Секция 2 текст')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Секция 2 youtube линк:') !!}
                            {!! Form::text('section_two_link', @$data->section_two_link, array('required', 'class'=>'form-control', 'placeholder'=>'Секция 2 линк')) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('Секция 3 заглавие:') !!}
                            {!! Form::text('section_three_title', @$data->section_three_title, array('required', 'class'=>'form-control', 'placeholder'=>'Секция 3 заглавие')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Секция 3 текст:') !!}
                            {!! Form::textarea('section_three_text', @$data->section_three_text, array('required', 'class'=>'form-control ckeditor', 'placeholder'=>'Секция 3 текст')) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('Секция 4 заглавие:') !!}
                            {!! Form::text('section_four_title', @$data->section_four_title, array('required', 'class'=>'form-control', 'placeholder'=>'Секция 4 заглавие')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Секция 4 текст (orange box):') !!}
                            {!! Form::textarea('section_four_text', @$data->section_four_text, array('required', 'class'=>'form-control ckeditor', 'placeholder'=>'Секция 4 текст')) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::submit('Запази', array('class'=>'btn btn-primary')) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@push('scripts')
<script type="text/javascript">
$(function() {
    $("#tabs").tabs();
    CKEDITOR.replace( 'ckeditor' );
});
</script>
@endpush
