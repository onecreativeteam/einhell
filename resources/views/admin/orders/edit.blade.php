@extends('layouts.app_admin')

@section('content')
<section class="content-header">
     <h1>
        Редактирай Поръчка
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-10">
            <div class="box">
				<div class="box-header">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(Session::has('success'))
                        <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
                    @endif
                </div>
                {{ Form::hidden('order_id', $order->id, ['class'=>'orderId']) }}
                <div class="box-body credential-page">
                    {!! Form::open( array('action' => array('Admin\OrdersController@update', $order->id), 'method' => 'PUT', 'class' => '' ) ) !!}
                        <div class="row">
                            <div class="col-md-4 col-md-offset-2">
                                <div class="form-group">
                                    <label for="#first_name"><span class="required">* </span>Име</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="formicon formicon-user"></i></span>
                                        {!! Form::text('first_name', @$order->first_name, array('pattern'=>"^[a-zA-Z\u0400-\u04ff\s]{2,}$", 'title'=>"Полето трябва да съдържа минимум 2 символа. Позволени са само букви.", 'required', 'class'=>'form-control','id'=>'first_name', 'placeholder'=>'Име')) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="#last_name"><span class="required">* </span>Фамилия</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="formicon formicon-user"></i></span>
                                        {!! Form::text('last_name', @$order->last_name, array('pattern'=>"^[a-zA-Z\u0400-\u04ff\s]{2,}$", 'title'=>"Полето трябва да съдържа минимум 2 символа. Позволени са само букви.", 'required', 'class'=>'form-control','id'=>'last_name' , 'placeholder'=>'Фамилия')) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="#phone"><span class="required">* </span>Телефон</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="formicon formicon-moblie"></i></span>
                                        {!! Form::text('phone', @$order->phone, array('required', 'class'=>'form-control', 'pattern'=>'^[0-9\-\+\s\(\)]{10,15}$', 'title'=>'Полето трябва да съдържа между 10 и 15 символа. Позволени са +-() и интервал.', 'id'=>'phone', 'placeholder'=>'+359')) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="#adress"><span class="required">* </span>Адрес за доставка</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="formicon formicon-adress"></i></span>
                                        {!! Form::text('adress', @$order->adress, array('pattern'=>"^[0-9\-\+\a-zA-Z\u0400-\u04ff\s]{3,}$", 'title'=>"Полето трябва да съдържа минимум 3 символа.", 'required', 'class'=>'form-control','id'=>'adress',  'placeholder'=>'Град, квартал, улица №, бл., вх., ет., ап.')) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="#postcode"><span class="required">* </span>Пощенски код:</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="formicon formicon-adress"></i></span>
                                        {!! Form::text('postcode', @$order->postcode, array('required', 'class'=>'form-control', 'pattern'=>'^[a-zA-Z\u0400-\u04ff\0-9\-\+\s\(\)]{3,15}$', 'title'=>'Полето трябва да съдържа минимум 3 символа.' ,'required', 'class'=>'form-control','id'=>'postcode',  'placeholder'=>'Пощенски код:')) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="comments">Коментар:</label>
                                    {!! Form::textarea('order_comment', @$order->order_comment, array('class'=>'form-control','id'=>'comments','rows'=>'5', 'placeholder'=>'Коментар')) !!}
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="radio">
                                                <input id="person" {{ ($order->is_company == '0' || Request::old('is_company') == '0') ? 'checked' : '' }} value="0" type="radio" name="is_company">
                                                <label for="person"> Физическо лице </label>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="radio">
                                                <input id="legal" value="1" {{ ($order->is_company == '1' || Request::old('is_company') == '1') ? 'checked' : '' }} type="radio" name="is_company">
                                                <label for="legal"> Юридическо лице </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <span class="required">* </span>  Задължителни полета
                            </div>
                        </div>
                        <div class="company-invoice">
                            <div class="row">
                                <div class="col-md-4 col-md-offset-2">
                                    <div class="form-group">
                                        <label for="#company"><span class="required">** </span>Име на фирма</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="formicon formicon-company"></i></span>
                                            {!! Form::text('company_name', @$order->company_name, array('pattern'=>"^[a-zA-Z\u0400-\u04ff\0-9\-\+\s\(\)]{3,}$", 'title'=>"Полето трябва да съдържа минимум 3 символа.", 'class'=>'form-control', 'id'=>'company', 'placeholder'=>'Име на фирма')) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="#mol"><span class="required">** </span>МОЛ</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="formicon formicon-user"></i></span>
                                            {!! Form::text('company_mol', @$order->company_mol, array('pattern'=>"^[a-zA-Z\u0400-\u04ff\s]{2,}$", 'title'=>"Полето трябва да съдържа минимум 2 символа. Позволени са само букви.", 'class'=>'form-control', 'id'=>'mol', 'placeholder'=>'МОЛ')) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="#bustat"><span class="required">** </span>ЕИК /Булстат/</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="formicon formicon-bustat"></i></span>
                                            {!! Form::text('company_bulstat', @$order->company_bulstat, array('class'=>'form-control', 'id'=>'bustat', 'placeholder'=>'Булстат')) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="#company-adres"><span class="required">** </span>Адрес на фирма:</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="formicon formicon-adress"></i></span>
                                            {!! Form::text('company_adress', @$order->company_adress, array('pattern'=>"^[a-zA-Z\u0400-\u04ff\0-9\-\+\s]{3,}$", 'title'=>"Полето трябва да съдържа минимум 3 символа.", 'class'=>'form-control', 'id'=>'company-adres', 'placeholder'=>'Град, квартал, улица №, бл., вх., ет., ап.')) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row form-status">
                                <div class="col-md-4 col-md-offset-2">
                                    <span class="required">** </span>  Попълва се само за юридически лица
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <input id="zdds" {{ $order->add_dds == '1' ? 'checked' : '' }} type="checkbox" value="1" name="add_dds">
                                            <label for="zdds"> По ЗДДС </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                {!! Form::submit('Потвърди', array('class'=>'submit')) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
                <br>
                
                <div class="bg-border card-product-list-wrapper">
                    <table class="card-product-list">
                        <tr>
                            <th>Продукт:</th>
                            <th></th>
                            <th>Брой:</th>
                            <th>Арт. №: </th>
                            <th>Eд. цена:</th>
                            <th>Крайна цена:</th>
                            <th>Операция:</th>
                        </tr>
                        @foreach ($order->orderProducts as $orderProduct)
                        <tr>
                            <td data-th="Продукт:">
                                <div class="">
                                    <span class="product-image" style="background-image:url(/{{ ( !is_null($orderProduct->product->picture) || $orderProduct->product->picture != '' ) ? $orderProduct->product->picture : ( count($orderProduct->product->pictures) ? $orderProduct->product->pictures[0]->resource : '' )  }})">
                                    </span>
                                </div>
                           </td>
                           <td>
                               <div class="title">
                                   {{ $orderProduct->product->name }}
                               </div>
                           </td>
                           <td data-th="Брой:">
                               <div class="quantity">
                                   {{ $orderProduct->product_quantity }}
                               </div>
                           </td>
                            <td data-th="Арт. №:">
                                <div>
                                    {{ $orderProduct->product->product_number }}
                                </div>
                            </td>
                            <td data-th="Eд. цена:">
                                <div>
                                    <strong>{{ $orderProduct->p_single_price }} лв.</strong>
                                </div>
                            </td>
                            <td data-th="Крайна цена:">
                                <div>
                                    <strong> {{ $orderProduct->p_total_price }} лв.</strong>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <a class="delete" onClick="return confirm('Сигурни ли сте, че искате да изтриете този продукт от поръчката');" href="{{ URL::route( 'admin.order.remove.product', [$orderProduct->id] ) }}"><i class="glyphicon glyphicon-trash"></i></a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @if ($order->has_user_discount)
                            <tr>
                                <th></th>
                                <th>ПОТРЕБИТЕЛСКА ОТСТЪПКА</th>
                                <th>Процент : {{ $order->user_discount_value }}%</th>
                                <th></th>
                                <th></th>
                                <th>{{ $order->order_discount }}</th>
                                <th>Тотал: <strong>{{$order->order_discount_total}}</strong> лв.</th>
                            </tr>
                        @else
                            <tr>
                                <th></th>
                                <th>ПОТРЕБИТЕЛСКА ОТСТЪПКА</th>
                                <th>Процент : 20%</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>Тотал: <strong>{{$order->order_total}}</strong> лв.</th>
                            </tr>
                        @endif
                    </table>
                    <br>
                </div>
                <div class="box-body table-responsive">
                    <h1 style="text-align:center">
                        Добави продукти към поръчката
                    </h1>
                    <hr>
                    <table class="table table-bordered" id="products-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Име</th>
                                <th>Наличност</th>
                                <th>Цена</th>
                                <th>Промо-продукт</th>
                                <th>Промо цена</th>
                                <th>Създаден на:</th>
                                <th>Последна промяна:</th>
                                <th>Операция:</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
		</div>
	</div>
</section>
@endsection
@push('scripts')
<script type="text/javascript">
$(document).ready(function(){

    $('#products-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{!! route('admin.datatables.edit.order.products') !!}",
            type: 'GET',
            data: function ( obj ) {
                obj.order_id = $('.orderId').val();
            },
        },
        order: [[0, 'desc' ]],
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'quantity', name: 'quantity' },
            { data: 'price', name: 'price' },
            { data: 'promo_product', name: 'promo_product' },
            { data: 'promo_price', name: 'promo_price' },
            { data: 'created_at', name: 'created_at' },
            { data: 'updated_at', name: 'updated_at' },
            { data: 'operations', name: 'operations' },
        ]
    });
            // ajax: "{!! route('admin.datatables.edit.order.products') !!}",
        // "data": function ( obj ) {
        //     obj.order_id = $('.orderId').val();
        // },
    var atLeastOneIsChecked = $('input[name="is_company"][value="1"]:checked').length > 0;
    if (atLeastOneIsChecked === true) {
        $('.company-invoice').show();
    }
});
</script>
<script type="text/javascript" src="{!! asset('js/shipping.js') !!}" charset="utf-8"></script>
@endpush