@extends('layouts.app_admin')

@section('content')
<section class="content-header">
      <h1>
        Списък Поръчки
      </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                @endif
                <div class="box-body table-responsive no-padding">
                    <table class="table table-bordered" id="orders-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Име</th>
                                <th>Фамилия</th>
                                <th>Е-мейл</th>
                                <th>Метод на плащане</th>
                                <th>Статус:</th>
                                <th>Създаден на:</th>
                                <th>Последна промяна:</th>
                                <th>Операция:</th>
                                <th>Статус edit:</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@push('scripts')
<script>
$(function() {
    $('#orders-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{!! route('admin.datatables.orders') !!}",
        order: [[0, 'desc' ]],
        columns: [
            { data: 'id', name: 'id' },
            { data: 'first_name', name: 'first_name' },
            { data: 'last_name', name: 'last_name' },
            { data: 'email', name: 'email' },
            { data: 'payment_method', name: 'payment_method' },
            { data: 'status', name: 'status' },
            { data: 'created_at', name: 'created_at' },
            { data: 'updated_at', name: 'updated_at' },
            { data: 'operations', name: 'operations' },
            { data: 'statuses', name: 'statuses' },
        ]
    });

    $('#orders-table').on('click', '.deleteRowBttn', function() {

        if (!confirm('На път сте да изтриете поръчка. Потвърждавате ли?')) {
            return false;
        }

        return true;
    });
});
</script>
@endpush