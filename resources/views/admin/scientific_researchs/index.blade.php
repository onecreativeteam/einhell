@extends('layouts.app_admin')

@section('content')
<section class="content-header">
     <h1>
        Списък Научни изследвания
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box">
				<div class="box-header">

                    @if(Session::has('success'))
                        <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
                    @endif
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
                    <a class="btn btn-success" href="{{ URL::route( 'admin.create.scientific.researchs' ) }}">Създай Изследване</a>
                </div>
            <div class="box-body">
                <table class="table table-hover" id="i-researchs-list-table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Име</th>
                            <th>Операция</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(!empty($researchs))
                    @foreach( $researchs as $k => $research)
                        <tr>
                            <td>
                                <p>{!! $research->id !!}</p>
                            </td>
                            <td>
                                <p>{!! (app()->getLocale() == $default_locale) ? $research->name : $research->translated->name !!}</p>
                            </td>
                            <td>
                                <a class="btn btn-warning btn-md" href="{{ URL::route( 'admin.edit.scientific.researchs', [$research->id] ) }}">Edit</a>

                            </td>
                            <td>{!! Form::open( array('action' => array('Admin\ScientificResearchsController@destroy', $research->id), 'method' => 'DELETE' ) ) !!}
                                    {!! Form::submit('DELETE', array('class'=>'btn btn-danger', 'onClick'=>'return confirm("Сигурни ли сте, че искате да изтриете това научно изследване?")')) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
</section>

@endsection
