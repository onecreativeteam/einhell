@extends('layouts.app_admin')

@section('content')
<section class="content-header">
    <h1>
       Einhell по света
   </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="box">
				<div class="box-header">
                    <!--  -->
                </div>
                <div class="box-body">
                    <div id="vmap" style="width: 600px; height: 400px; margin:0 auto"></div>
                    <br>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            <div class="box">
                <div class="box-header">
                    @if(Session::has('success'))
                        <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
                    @endif
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <div class="box-body">
                    <div id="tabs">
                        <ul>
                        @foreach ($supportedLocales as $k => $locale)
                            <li><a href="#tabs-{{$locale}}">{{$locale}}</a></li>
                        @endforeach
                        </ul>
                        {!! Form::open( array('action' => array('Admin\MapPageController@store'), 'method' => 'POST', 'files' => 'true', 'class' => '' ) ) !!}
                            @foreach ($supportedLocales as $lang => $locale)
                                {!! Form::hidden("translations[{$lang}][slug]", 'map_page_'.$locale) !!}
                                <div id="tabs-{{$locale}}">
                                    <div class="form-group">
                                        {!! Form::label('Текст:') !!}
                                        {!! Form::textarea("translations[{$lang}][text]", isset($mapPage) ? $mapPage->text : '', array('required', 'class'=>'ckeditor form-control', 'placeholder'=>'Текст')) !!}
                                    </div>
                                </div>
                            @endforeach
                            <div class="form-group">
                                {!! Form::submit('Запази', array('class'=>'btn btn-primary')) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection
@push('scripts')
<script type="text/javascript">
@if(!empty($worldMap))
    worldMap = <?php echo json_encode($worldMap); ?>;
@endif
$(document).ready( function() {

    jQuery('#vmap').vectorMap(
    {
        map: 'world_en',
        backgroundColor: '#a5bfdd',
        borderColor: '#818181',
        borderOpacity: 0.25,
        borderWidth: 1,
        color: '#f4f3f0',
        enableZoom: true,
        hoverColor: '#c9dfaf',
        hoverOpacity: null,
        normalizeFunction: 'linear',
        scaleColors: ['#b6d6ff', '#005ace'],
        selectedColor: '#c9dfaf',
        selectedRegions: worldMap,
        showTooltip: true,
        regionsSelectable: true,
        multiSelectRegion: true,
        onRegionSelect: function(element, code, region)
        {
            setCode(code.toUpperCase());
        },
        onRegionDeselect: function(element, code, region)
        {
            unsetCode(code.toUpperCase());
        }
    });

    $("#tabs").tabs();
    CKEDITOR.replace( 'ckeditor' );

});

function setCode(code) {

    var url         = "{{ URL::route( 'admin.home.update.map', ['set'] ) }}";
    var data        = {};
    data.code       = code;
    data._token     = "{{ csrf_token() }}";

    $.ajaxSetup( { headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() } } );
    $.ajax({
        method: "POST",
        url: url,
        dataType: "json",
        data: data,
        success: function (response) {
            console.log(response);
        },
        error: function (error, exception) {
            console.log(error);
        },
    });

}

function unsetCode(code) {

    var url         = "{{ URL::route( 'admin.home.update.map', ['unset'] ) }}";
    var data        = {};
    data.code       = code;
    data._token     = "{{ csrf_token() }}";

    $.ajaxSetup( { headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() } } );
    $.ajax({
        method: "POST",
        url: url,
        dataType: "json",
        data: data,
        success: function (response) {
            console.log(response);
        },
        error: function (error, exception) {
            console.log(error);
        },
    });

}

</script>
@endpush
