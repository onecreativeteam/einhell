@extends('layouts.app_admin')


@section('content')
<section class="content-header">
     <h1>
        Cover-Photo за страницата
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box">
				<div class="box-header">                    
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(Session::has('success'))
                        <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
                    @endif
                </div>
				<div class="box-body">
                    {!! Form::open( array('action' => array('Admin\CoverPhotosController@store'), 'method' => 'POST', 'files' => 'true', 'class' => '' ) ) !!}
                        <div class="form-group">
                            {!! Form::label('Снимка /690х229px/:') !!}
                            {!! Form::file('file') !!}
                            {!! Form::hidden('type', 'about') !!}
                            <img style="width:150px;height:150px;margin-top:10px" src="/{{ @$cover->file }}"/>
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Добави cover-photo', array('class'=>'btn btn-primary')) !!}
                        </div>
                    {!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</section>



@endsection
