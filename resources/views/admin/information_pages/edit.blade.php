@extends('layouts.app_admin')

@section('content')
<section class="content-header">
     <h1>
         Редактирай Страница
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
				<div class="box-header">
                    @if (count($errors) > 0)
                       <div class="alert alert-danger">
                           <ul>
                           @foreach ($errors->all() as $error)
                               <li>{{ $error }}</li>
                           @endforeach
                           </ul>
                       </div>
                   @endif
                    @if(Session::has('success'))
                        <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
                    @endif
                </div>
				<div class="box-body">
                    <div id="tabs">
                        <ul>
                        @foreach ($supportedLocales as $k => $locale)
                            <li><a href="#tabs-{{$locale}}">{{$locale}}</a></li>
                        @endforeach
                        </ul>
                        {!! Form::open( array('action' => array('Admin\InformationPagesController@update', $page->id), 'method' => 'PUT', 'files' => 'true', 'class' => '' ) ) !!}
                            @foreach ($supportedLocales as $lang => $locale)
                                <div id="tabs-{{$locale}}">
                                    <div class="form-group">
                                        {!! Form::label('Име:') !!}
                                        {!! Form::text("translations[{$lang}][name]", isset($page->translations[$locale]) ? $page->translations[$locale]->name : $page->name, array('required', 'class'=>'form-control', 'placeholder'=>'Име')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('Име:') !!}
                                        {!! Form::textarea("translations[{$lang}][description]", isset($page->translations[$locale]) ? $page->translations[$locale]->description : $page->description, array('required', 'class'=>'form-control ckeditor', 'placeholder'=>'Описание')) !!}
                                    </div>
                                </div>
                            @endforeach
                            <div class="form-group">
                                {!! Form::label('Снимка:') !!}
                                {!! Form::file('picture') !!}
                                <img style="width:150px;height:150px;margin-top:10px" src="/{{$page->picture}}"/>
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Създай', array('class'=>'btn btn-primary')) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
			</div>
		</div>
	</div>
</section>


@endsection
@push('scripts')
<script type="text/javascript">
$(function() {
    $("#tabs").tabs();
    CKEDITOR.replace( 'ckeditor' );
});
</script>
@endpush
