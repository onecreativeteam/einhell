@extends('layouts.app_admin')

@section('content')
    <section class="content-header">
     <h1>
        Редактирай Продукт
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
				<div class="box-header">
                    @if(Session::has('success'))
                        <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
                    @endif
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
				<div class="box-body">
                    <div id="tabs">
                        <ul>
                        @foreach ($supportedLocales as $k => $locale)
                            <li><a href="#tabs-{{$locale}}">{{$locale}}</a></li>
                        @endforeach
                        </ul>
                        {!! Form::open( array('action' => array('Admin\ProductsController@update', $product->id), 'method' => 'PUT', 'files' => 'true', 'class' => '' ) ) !!}
                            @foreach ($supportedLocales as $lang => $locale)
                                <div id="tabs-{{$locale}}">
                                    <div class="form-group">
                                        {!! Form::label('Име:') !!}
                                        {!! Form::text("translations[{$lang}][name]", isset($product->translations[$locale]) ? $product->translations[$locale]->name : $product->name, array('required', 'class'=>'form-control', 'placeholder'=>'Име')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('Описание:') !!}
                                        {!! Form::textarea("translations[{$lang}][description]", isset($product->translations[$locale]) ? $product->translations[$locale]->description : $product->description, array('required', 'class'=>'ckeditor form-control', 'placeholder'=>'Описание')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('Tech Описание:') !!}
                                        {!! Form::textarea("translations[{$lang}][technical_description]", isset($product->translations[$locale]) ? $product->translations[$locale]->technical_description : $product->technical_description, array('required', 'class'=>'ckeditor form-control', 'placeholder'=>'Tech Описание')) !!}
                                    </div>
                                </div>
                            @endforeach
                            <div class="form-group">
                                {!! Form::label('Закачи към ГЛАВНА категория:') !!}
                                {!! Form::select('base_categories_id', array('null'=>'Избери ГЛАВНА Категория')+$baseCategorySelect, @$product->base_categories_id, ['class' => 'form-control baseCatSelect']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Закачи към категория:') !!}
                                {!! Form::select('product_categories_id', array('0'=>'Избери Категория')+$categorySelect, $product->product_categories_id, ['class' => 'form-control baseCatSelect']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Закачи към серия:') !!}
                                {!! Form::select('product_series_id', array('0'=>'Избери Серия')+$seriesSelect, $product->product_series_id, ['class' => 'form-control baseCatSelect']) !!}
                            </div>
<!--                             <div class="form-group">
                                {!! Form::label('Избери основна гаранция:') !!}
                                {!! Form::select('base_warranty_id', array('0'=>'Избери Гаранция')+$baseWarrantySelect, $product->base_warranty_id, ['class' => 'form-control baseWarrantySelect']) !!}
                            </div> -->
                            <div class="form-group">
                                {!! Form::label('Цена:') !!}
                                {!! Form::text('price', $product->price ? $product->price : 0, array('required', 'class'=>'form-control', 'placeholder'=>'Цена')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Наличност:') !!}
                                {!! Form::text('quantity', $product->quantity ? $product->quantity : 0, array('required', 'class'=>'form-control', 'placeholder'=>'Наличност')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Артикулен номер:') !!}
                                {!! Form::text('product_number', $product->product_number ? $product->product_number : 0, array('required', 'class'=>'form-control', 'placeholder'=>'Артикулен номер')) !!}
                            </div>
                            <div class="form-group">
                                <div class="checkbox">
                                    <input id="top_product" {{ $product->top_product == 1 ? 'checked' : '' }} type="checkbox" name="top_product" value="1">
                                    <label for="top_product"> Топ-продукт: </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="checkbox">
                                    <input id="promo_product" {{ $product->promo_product == 1 ? 'checked' : '' }} type="checkbox" name="promo_product" value="1">
                                    <label for="promo_product"> Промо продукт: </label>
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('Промо Цена:') !!}
                                {!! Form::text('promo_price', $product->promo_price ? $product->promo_price : '', array('class'=>'form-control', 'placeholder'=>'Промо Цена')) !!}
                            </div>
                            <div class="form-group">
                                <div class="checkbox">
                                    <input id="xchange" {{ $product->xchange == 1 ? 'checked' : '' }} type="checkbox" name="xchange" value="1">
                                    <label for="xchange"> Добави към Power-X-Change: </label>
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('Главна снимка:') !!}
                                {!! Form::file('picture') !!}
                                @if ($product->picture && file_exists($product->picture))
                                    <img style="width:150px;height:150px;margin-top:10px" src="/{{$product->picture}}"/>
                                @else
                                    <p style="color:red;font-weight:bold">Не е качена снимка : {{ str_replace('uploads/product/', '', $product->picture) }}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                {!! Form::label('Добави продуктови файлове:') !!}
                                {!! Form::file('file[]', ['multiple' => 'multiple']) !!}
                            </div>
                            @if ($files)
                            <div class="box-body">
                                @foreach ($product->files as $file)
                                    {{$file->file}}
                                    <a class="btn btn-default" target="_blank" href="/{{ $file->resource }}">Виж</a>
                                @endforeach
                            </div>
                            @endif
                            <div class="form-group">
                                {!! Form::submit('Промяна', array('class'=>'btn btn-primary')) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
@push('scripts')
<script type="text/javascript">
$(function() {
    $("#tabs").tabs();
    CKEDITOR.replace( 'ckeditor' );
});
</script>
@endpush
