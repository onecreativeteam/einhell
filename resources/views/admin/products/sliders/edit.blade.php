@extends('layouts.app_admin')

@section('content')

 <section class="content-header">
      <h1>
          Редактирай Продуктов Слайдер
     </h1>
 </section>
 <section class="content">
     <div class="row">
         <div class="col-md-6">
            <div class="box">
 				<div class="box-header">
                    @if(Session::has('success'))
                        <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
                    @endif
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
 				<div class="box-body">
                    <div id="tabs">
                        <ul>
                        @foreach ($supportedLocales as $k => $locale)
                            <li><a href="#tabs-{{$locale}}">{{$locale}}</a></li>
                        @endforeach
                        </ul>
                        {!! Form::open( array('action' => array('Admin\ProductSlidersController@update', $slider->id), 'method' => 'PUT', 'files' => 'true', 'class' => '' ) ) !!}
                                @foreach ($supportedLocales as $lang => $locale)
                                    <div id="tabs-{{$locale}}">
                                        <div class="form-group">
                                            {!! Form::label('Име:') !!}
                                            {!! Form::text("translations[{$lang}][name]", isset($slider->translations[$locale]) ? $slider->translations[$locale]->name : $slider->name, array('required', 'class'=>'form-control', 'placeholder'=>'Име')) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('Кратко описание:') !!}
                                            {!! Form::text("translations[{$lang}][description]", isset($slider->translations[$locale]) ? $slider->translations[$locale]->description : $slider->description, array('required', 'class'=>'form-control', 'placeholder'=>'Кратко описание')) !!}
                                        </div>
                                    </div>
                                @endforeach
                                <div class="form-group">
                                    {!! Form::label('Избери продукт (Търсете по име):') !!}
                                    {!! Form::select('products_id', array('0'=>'')+$productsSelect, $slider->products_id, ['class' => 'form-control productsSelect']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('Custom линк:') !!}
                                    {!! Form::text('link', $slider->link ? $slider->link : '', array('class'=>'form-control', 'placeholder'=>'Link')) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('Снимка /205х184px/:') !!}
                                    {!! Form::file('picture') !!}
                                    <img style="width:150px;height:150px;margin-top:10px" src="/{{$slider->picture}}"/>
                                </div>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input id="xchange" {{ $slider->xchange == 1 ? 'checked' : '' }} type="checkbox" name="xchange" value="1">
                                        <label for="xchange"> Добави към Power-X-Change: </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::submit('Редактирай', array('class'=>'btn btn-primary')) !!}
                                </div>
                        {!! Form::close() !!}   
 				   </div>
 			    </div>
 		</div>
    </div>
 </div>
 </section>
 <style>
  .ui-combobox {
    display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
  }
  .ui-combobox-input {
    background: #fff !important;
  }
</style>
@endsection
@push('scripts')
<script type="text/javascript">
$(function() {
    $("#tabs").tabs();
    $('.productsSelect').combobox();
});
(function ($) {
    $.widget("ui.combobox", {
        _create: function () {
            var input,
              that = this,
              wasOpen = false,
              select = this.element.hide(),
              selected = select.children(":selected"),
              defaultValue = selected.text() || "",
              wrapper = this.wrapper = $("<span>")
                .addClass("ui-combobox")
                .insertAfter(select);

            function removeIfInvalid(element) {
                var value = $(element).val(),
                  matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(value) + "$", "i"),
                  valid = false;
                select.children("option").each(function () {
                    if ($(this).text().match(matcher)) {
                        this.selected = valid = true;
                        return false;
                    }
                });

                if (!valid) {
                    // remove invalid value, as it didn't match anything
                    $(element).val(defaultValue);
                    select.val(defaultValue);
                    input.data("ui-autocomplete").term = "";
                }
            }

            input = $("<input>")
              .appendTo(wrapper)
              .val(defaultValue)
              .attr("title", "")
              .addClass("ui-state-default ui-combobox-input")
              .width(select.width())
              .autocomplete({
                  delay: 0,
                  minLength: 0,
                  autoFocus: true,
                  source: function (request, response) {
                      var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                      response(select.children("option").map(function () {
                          var text = $(this).text();
                          if (this.value && (!request.term || matcher.test(text)))
                              return {
                                  label: text.replace(
                                    new RegExp(
                                      "(?![^&;]+;)(?!<[^<>]*)(" +
                                      $.ui.autocomplete.escapeRegex(request.term) +
                                      ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                    ), "<strong>$1</strong>"),
                                  value: text,
                                  option: this
                              };
                      }));
                  },
                  select: function (event, ui) {
                      ui.item.option.selected = true;
                      that._trigger("selected", event, {
                          item: ui.item.option
                      });
                  },
                  change: function (event, ui) {
                      if (!ui.item) {
                          removeIfInvalid(this);
                      }
                  }
              })
              .addClass("ui-widget ui-widget-content ui-corner-left");

            input.data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li>")
                  .append("<a>" + item.label + "</a>")
                  .appendTo(ul);
            };

            $("<a>")
              .attr("tabIndex", -1)
              .appendTo(wrapper)
              .button({
                  icons: {
                      primary: "ui-icon-triangle-1-s"
                  },
                  text: false
              })
              .removeClass("ui-corner-all")
              .addClass("ui-corner-right ui-combobox-toggle")
              .mousedown(function () {
                  wasOpen = input.autocomplete("widget").is(":visible");
              })
              .click(function () {
                  input.focus();

                  // close if already visible
                  if (wasOpen) {
                      return;
                  }

                  // pass empty string as value to search for, displaying all results
                  input.autocomplete("search", "");
              });
        },

        _destroy: function () {
            this.wrapper.remove();
            this.element.show();
        }
    });
})(jQuery);
</script>
@endpush
