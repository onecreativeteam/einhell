@extends('layouts.app_admin')

@section('content')
<section class="content-header">
     <h1>
        Слайдери
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box">
				<div class="box-header">
                    @if(Session::has('success'))
                        <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
                    @endif

                    <a class="btn btn-success" href="{{ URL::route( 'admin.create.product.sliders' ) }}">Създай Продуктов Слайдер</a>
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
				<div class="box-body">
                    <table class="table table-hover" id="p-sliders-list-table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Име</th>
                                <th>Операция</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(!empty($sliders))
                        @foreach( $sliders as $k => $slider)
                            <tr>
                                <td>
                                    <p>{!! $slider->id !!}</p>
                                </td>
                                <td>
                                    <p>{!! (app()->getLocale() == $default_locale) ? $slider->name : $slider->translated->name !!}</p>
                                </td>
                                <td>
                                    <a class="btn btn-warning btn-md" href="{{ URL::route( 'admin.edit.product.sliders', [$slider->id] ) }}">Edit</a>
                                </td>
                                <td>
                                    {!! Form::open( array('action' => array('Admin\ProductSlidersController@destroy', $slider->id), 'method' => 'DELETE' ) ) !!}
                                        {!! Form::submit('DELETE', array('class'=>'btn btn-danger', 'onClick'=>'return confirm("Сигурни ли сте, че искате да изтриете този слайдер")')) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        @endif
                        </tbody>
                    </table>

				</div>
			</div>
		</div>
	</div>
</section>




@endsection
