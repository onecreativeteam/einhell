@extends('layouts.app_admin')

@section('content')
<section class="content-header">
     <h1>
        Добави Галерия или Видео
    </h1>
</section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
                @endif
                @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
            </div>
        </div>
        <div class="col-md-7">
            <div class="box">
                <div class="box-body">
                    {!! Form::open( array('action' => array('Admin\ProductsGalleryController@store', $product->id), 'method' => 'POST', 'files' => 'true', 'class' => '' ) ) !!}
                        {!! Form::hidden('type', 'picture') !!}
                        <div class="form-group">
                            {!! Form::label('Добави галерия:') !!}
                            {!! Form::file('resource[]', ['multiple' => 'multiple']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Добави Галерия', array('class'=>'btn btn-success')) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="box">
                <div class="box-body">
                    @if ($pictures)
                        @foreach ($pictures as $picture)
                        <div style="float:left; margin:30px">
                            <img style="width:150px;height:150px" src="/{{$picture->resource}}"/>
                            {!! Form::open( array('action' => array('Admin\ProductsGalleryController@destroy', $product->id, $picture->id), 'method' => 'DELETE' ) ) !!}
                                {!! Form::submit('X', array('class'=>'btn btn-danger', 'onClick'=>'return confirm("Потвърждавате ли изтриване на снимка?");')) !!}
                            {!! Form::close() !!}
                            {!! Form::open( array('action' => array('Admin\ProductsGalleryController@updateOrder', $picture->id), 'method' => 'POST' ) ) !!}
                                {!! Form::text('view_order', @$picture->view_order, ['class' => 'form-control']) !!}
                                {!! Form::submit('Save', array('class'=>'btn btn-danger', 'onClick'=>'return confirm("Потвърждавате ли промяна в поредноста?");')) !!}
                            {!! Form::close() !!}
                        </div>
                        @endforeach
                    @endif

                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="box">
                <div class="box-body">
                    {!! Form::open( array('action' => array('Admin\ProductsGalleryController@store', $product->id), 'method' => 'POST', 'class' => '' ) ) !!}
                        {!! Form::hidden('type', 'video') !!}
                        <div class="form-group">
                            {!! Form::label('видео:') !!}
                            {!! Form::text('resource', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Създай', array('class'=>'btn btn-primary')) !!}
                        </div>
                    {!! Form::close() !!}
                    @if ($videos)
                        @foreach ($videos as $video)
                        {!! Form::open( array('action' => array('Admin\ProductsGalleryController@update', $product->id, $video->id), 'method' => 'PUT', 'class' => '' ) ) !!}
                            {!! Form::hidden('type', 'video') !!}
                            <div class="form-group">
                                {!! Form::label('видео:') !!}
                                {!! Form::text('resource', $video->resource ? $video->resource : '', ['class' => 'form-control']) !!}
                                {!! Form::submit('Промяна', array('class'=>'btn btn-submit')) !!}
                            </div>
                        {!! Form::close() !!}
                        {!! Form::open( array('action' => array('Admin\ProductsGalleryController@destroy', $product->id, $video->id), 'method' => 'DELETE' ) ) !!}
                            {!! Form::submit('Изтрий видео', array('class'=>'btn btn-danger',  'onClick'=>'return confirm("Сигурни ли сте, че искате да изтриете това видео?")')) !!}
                        {!! Form::close() !!}
                        @endforeach
                    @endif
				</div>
            </div>
		</div>
	</div>
</section>





@endsection
@push('scripts')
<script type="text/javascript">
$(function() {
    $("#tabs").tabs();
});
</script>
@endpush
