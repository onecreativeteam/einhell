@extends('layouts.app_admin')

@section('content')
<section class="content-header">
    <h1>
       Създай Продукт
   </h1>
</section>
<section class="content">
    <div class="row"> 
        <div class="col-md-12">
            <div class="box">
				<div class="box-header">
                    @if(Session::has('success'))
                        <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
                    @endif
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
				<div class="box-body">
                    <div id="tabs">
                        <ul>
                        @foreach ($supportedLocales as $k => $locale)
                            <li><a href="#tabs-{{$locale}}">{{$locale}}</a></li>
                        @endforeach
                        </ul>
                        {!! Form::open( array('action' => array('Admin\ProductsController@store'), 'method' => 'POST', 'files' => 'true', 'class' => '' ) ) !!}
                            @foreach ($supportedLocales as $lang => $locale)
                                <div id="tabs-{{$locale}}">
                                    <div class="form-group">
                                        {!! Form::label('Име:') !!}
                                        {!! Form::text("translations[{$lang}][name]", null, array('required', 'class'=>'form-control', 'placeholder'=>'Име')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('Описание:') !!}
                                        {!! Form::textarea("translations[{$lang}][description]", null, array('required', 'class'=>'ckeditor form-control', 'placeholder'=>'Описание')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('Техническа характеристика:') !!}
                                        {!! Form::textarea("translations[{$lang}][technical_description]", null, array('required', 'class'=>'ckeditor form-control', 'placeholder'=>'Tech Описание')) !!}
                                    </div>
                                </div>
                            @endforeach
                            <div class="form-group">
                                {!! Form::label('Закачи към ГЛАВНА категория:') !!}
                                {!! Form::select('base_categories_id', array('null'=>'Избери ГЛАВНА Категория')+$baseCategorySelect, null, ['class' => 'form-control baseCatSelect']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Закачи към ПОД категория:') !!}
                                {!! Form::select('product_categories_id', array('0'=>'Избери ПОД Категория')+$categorySelect, null, ['class' => 'form-control baseCatSelect']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Закачи към серия:') !!}
                                {!! Form::select('product_series_id', array('0'=>'Избери Серия')+$seriesSelect, null, ['class' => 'form-control baseSerieSelect']) !!}
                            </div>
<!--                             <div class="form-group">
                                {!! Form::label('Избери основна гаранция:') !!}
                                {!! Form::select('base_warranty_id', array('0'=>'Избери Гаранция')+$baseWarrantySelect, null, ['class' => 'form-control baseWarrantySelect']) !!}
                            </div> -->
                            <div class="form-group">
                                {!! Form::label('Цена:') !!}
                                {!! Form::text('price', null, array('required', 'class'=>'form-control', 'placeholder'=>'Цена')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Наличност:') !!}
                                {!! Form::text('quantity', null, array('required', 'class'=>'form-control', 'placeholder'=>'Наличност')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Артикулен номер:') !!}
                                {!! Form::text('product_number', null, array('required', 'class'=>'form-control', 'placeholder'=>'Артикулен номер')) !!}
                            </div>
                            <div class="form-group">
                                <div class="checkbox">
                                    <input id="top_product" type="checkbox" name="top_product" value="1">
                                    <label for="top_product"> Топ-продукт: </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="checkbox">
                                    <input id="promo_product" type="checkbox" name="promo_product" value="1">
                                    <label for="promo_product"> Промо продукт: </label>
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('Промо Цена:') !!}
                                {!! Form::text('promo_price', null, array('class'=>'form-control', 'placeholder'=>'Промо Цена')) !!}
                            </div>
                            <div class="form-group">
                                <div class="checkbox">
                                    <input id="xchange" type="checkbox" name="xchange" value="1">
                                    <label for="xchange"> Добави към Power-X-Change: </label>
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('Главна снимка:') !!}
                                {!! Form::file('picture') !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Добави продуктови файлове:') !!}
                                {!! Form::file('file[]', ['multiple' => 'multiple']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Създай', array('class'=>'btn btn-primary')) !!}
                            </div>
                        {!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</section>

@endsection
@push('scripts')
<script type="text/javascript">
$(function() {
    $("#tabs").tabs();
    CKEDITOR.replace( 'ckeditor' );
});
</script>
@endpush
