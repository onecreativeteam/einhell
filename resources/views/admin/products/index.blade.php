@extends('layouts.app_admin')

@section('content')
<section class="content-header">
     <h1>
        Продукти
    </h1>
</section>
<section class="content">
    <div class="row"> 
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    @if(Session::has('success'))
                        <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
                    @endif
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif

                    <!-- // COMMENT THIS ROW AFTER SERIES DONE -->
                    <!--                     
                    <div class="col-md-12">
                        {!! Form::open( array('action' => array('Admin\ProductsController@manualChangeSeries'), 'method' => 'POST', 'files' => 'true', 'class' => '' ) ) !!}
                            <div class="form-group">
                                {!! Form::label('Промени Серии:') !!}
                                {!! Form::file('series_excell') !!}
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Промяна', array('class'=>'btn btn-success')) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                    -->

                    <a class="btn btn-success" href="{{ URL::route( 'admin.create.product' ) }}">Създай Продукт</a>
                    <div class="row" style="margin-left:10px;">
                        <h2>Обработка на продукти с ексел и качване на файлове към тях.</h2>
                        <h3 style="margin-right:10px">Максималният допустим брой снимки за качване на един път е 20!</h3>
                        <div class="col-md-2">
                            <a class="btn btn-success" href="{{ URL::route( 'admin.export.product' ) }}">Свали Ексел</a>
                        </div>
                        <div class="productImport col-md-2">
                            {!! Form::open( array('action' => array('Admin\ProductsController@import'), 'method' => 'POST', 'files' => 'true', 'class' => '' ) ) !!}
                                <div class="form-group">
                                    {!! Form::label('Качи Excel файл:') !!}
                                    {!! Form::file('products_excel') !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::submit('Качи продукти', array('class'=>'btn btn-success')) !!}
                                </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="productPictureImport col-md-2">
                            {!! Form::open( array('action' => array('Admin\ProductsController@uploadExcellPictures'), 'method' => 'POST', 'files' => 'true', 'class' => '' ) ) !!}
                                <div class="form-group">
                                    {!! Form::label('Избери снимки за excel:') !!}
                                    {!! Form::file('excel_pictures[]', ['multiple'=>'multiple']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::submit('Качи снимки', array('class'=>'btn btn-success')) !!}
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <br/>
                    <div class="row" style="margin-left:10px;">
                        <h2>Обработка на галерия с ексел и качване на файлове към нея.</h2>
                        <div class="col-md-2">
                            <a class="btn btn-success" href="{{ URL::route( 'admin.export.product.gallery' ) }}">Свали Галерия Ексел</a>
                        </div>
                        <div class="productImport col-md-2">
                            {!! Form::open( array('action' => array('Admin\ProductsController@importGallery'), 'method' => 'POST', 'files' => 'true', 'class' => '' ) ) !!}
                                <div class="form-group">
                                    {!! Form::label('Качи Gallery-Excel файл:') !!}
                                    {!! Form::file('products_gallery_excel') !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::submit('Качи галерия', array('class'=>'btn btn-success')) !!}
                                </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="productPictureImport col-md-2">
                            {!! Form::open( array('action' => array('Admin\ProductsController@uploadExcellGalleryPictures'), 'method' => 'POST', 'files' => 'true', 'class' => '' ) ) !!}
                                <div class="form-group">
                                    {!! Form::label('Избери снимки за excel:') !!}
                                    {!! Form::file('excel_gallery_pictures[]', ['multiple'=>'multiple']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::submit('Качи снимки в галерия', array('class'=>'btn btn-success')) !!}
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
				<div class="box-body">
                    <table class="table table-bordered" id="products-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Име</th>
                                <th>Категория</th>
                                <th>Серия</th>
                                <th>Арт №</th>
                                <th>Топ Продукт</th>
                                <th>Промо Продукт</th>
                                <th>X-change</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                <th>Операция</th>
                            </tr>
                        </thead>
                    </table>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
@push('scripts')
<script>
$(function() {
    $('#products-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{!! route('admin.datatables.products') !!}",
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'category.name', name: 'category.name' },
            { data: 'serie.name', name: 'serie.name' },
            { data: 'product_number', name: 'product_number' },
            { data: 'top_product', name: 'top_product' },
            { data: 'promo_product', name: 'promo_product' },
            { data: 'xchange', name: 'xchange' },
            { data: 'created_at', name: 'created_at' },
            { data: 'updated_at', name: 'updated_at' },
            { data: 'operations', name: 'operations' },
        ]
    });

    $('#products-table').on('click', '.deleteRowBttn', function() {

        if (!confirm('На път сте да изтриете продукт. Потвърждавате ли?')) {
            return false;
        }

        return true;
    });
});
</script>
@endpush