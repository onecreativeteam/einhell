@extends('layouts.app_admin')

@section('content')
<section class="content-header">
     <h1>
        Добави Гаранция
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if(Session::has('success'))
                <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
            @endif
            @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box">
	             <div class="box-body">
                    <table class="table table-hover" id="product-warrantys-table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Месеци</th>
                                <th>Цена</th>
                                <th>Операция</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(!empty($product->warranty))
                        @foreach($product->warranty as $k => $warranty)
                            <tr>
                                {!! Form::open( array('action' => array('Admin\ProductsWarrantyController@update', $product->id, $warranty->id), 'method' => 'PUT' ) ) !!}
                                    <td width="2%">
                                        <p>{!! $warranty->id !!}</p>
                                    </td>
                                    <td width="40%">
                                        {!! Form::text('warranty_months', $warranty->warranty_months, ['class' => 'form-control']) !!}
                                    </td>
                                    <td width="40%">
                                        {!! Form::text('warranty_price', $warranty->warranty_price, ['class' => 'form-control']) !!}
                                    </td>
                                    <td width="8%">
                                        {!! Form::submit('Промяна', array('class'=>'btn btn-warning')) !!}
                                    </td>
                                {!! Form::close() !!}
                                <td width="10%">
                                    {!! Form::open( array('action' => array('Admin\ProductsWarrantyController@destroy', $product->id, $warranty->id), 'method' => 'DELETE' ) ) !!}
                                        {!! Form::submit('Изтрии', array('class'=>'btn btn-danger')) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        @endif
                        </tbody>
                    </table>
				</div>
			</div>
		</div>
        <div class="col-md-6">
            <div class="box">
	             <div class="box-body">
                    {!! Form::open( array('action' => array('Admin\ProductsWarrantyController@store', $product->id), 'method' => 'POST', 'class' => '' ) ) !!}
                        <div class="col-md-5">
                            {!! Form::label('Месеци:') !!}
                            {!! Form::text('warranty_months', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="col-md-5">
                            {!! Form::label('Цена:') !!}
                            {!! Form::text('warranty_price', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="col-md-1">
                            {!! Form::label('Операция:') !!}
                            {!! Form::submit('Добави', array('class'=>'btn btn-success')) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        
	</div>
</section>
@endsection
@push('scripts')
<script type="text/javascript">

</script>
@endpush
