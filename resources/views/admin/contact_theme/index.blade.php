@extends('layouts.app_admin')

@section('content')
<section class="content-header">
     <h1> Теми на контакти    
    </h1>
</section>
<section class="content">
   <div class="row">
       <div class="col-md-6">
           <div class="box">
				<div class="box-header">
                    @if(Session::has('success'))
                        <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
                    @endif

                    <a class="btn btn-success" href="{{ URL::route( 'admin.create.contact.theme' ) }}">Създай Тема</a>
                </div>
                <div class="box-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
                    <table class="table table-hover" id="theme-list-table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Име</th>
                                <th>Операция</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(!empty($themes))
                        @foreach( $themes as $k => $theme)
                            <tr>
                                <td>
                                    <p>{!! $theme->id !!}</p>
                                </td>
                                <td>
                                    <p>{!! (app()->getLocale() == $default_locale) ? $theme->name : $theme->translated->name !!}</p>
                                </td>
                                <td>
                                    <a class="btn btn-warning btn-md" href="{{ URL::route( 'admin.edit.contact.theme', [$theme->id] ) }}">Edit</a>

                                </td>
                                <td>
                                     {!! Form::open( array('action' => array('Admin\ContactThemesController@destroy', $theme->id), 'method' => 'DELETE' ) ) !!}
                                        {!! Form::submit('DELETE', array('class'=>'btn btn-danger')) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
			</div>
		</div>
	</div>
</section>
@endsection
