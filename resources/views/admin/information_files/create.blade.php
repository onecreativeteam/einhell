@extends('layouts.app_admin')

@section('content')
<section class="content-header">
     <h1>
         Добави Файлове
    </h1>
</section>
<section class="content">
  <div class="row">
       @if (count($errors) > 0)
           <div class="alert alert-danger">
               <ul>
               @foreach ($errors->all() as $error)
                   <li>{{ $error }}</li>
               @endforeach
               </ul>
           </div>
       @endif
       @if(Session::has('success'))
           <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
       @endif
       <div class="col-md-8">
          <div id="tabs">
                <ul>
                @foreach ($supportedLocales as $k => $locale)
                    <li><a href="#tabs-{{$locale}}">{{$locale}}</a></li>
                @endforeach
                </ul>
                {!! Form::open( array('action' => array('Admin\InformationFilesController@store'), 'method' => 'POST', 'files' => 'true', 'class' => '' ) ) !!}
                    @foreach ($supportedLocales as $lang => $locale)
                        <div id="tabs-{{$locale}}">
                            <div class="form-group">
                                {!! Form::label('Име на файл:') !!}
                                {!! Form::text("translations[{$lang}][name]", null, array('required', 'class'=>'form-control', 'placeholder'=>'Име')) !!}
                            </div>
                        </div>
                    @endforeach
                    <div class="form-group">
                       {!! Form::label('Добави файлове:') !!}
                       {!! Form::file('file') !!}
                    </div>
                    <div class="form-group">
                        {!! Form::submit('Добави Файлове', array('class'=>'btn btn-success')) !!}
                    </div>
                {!! Form::close() !!}
          </div>
      </div>
      <div class="col-md-4">
          <div class="box">
               @if ($files)
                 <div class="box-body">
                     @foreach ($files as $file)
                         <img class="img-responsive img-thumbnail"src="/{{$file->file}}" alt="">
                         <br>
                         {!! Form::open( array('action' => array('Admin\InformationFilesController@destroy', $file->id), 'method' => 'DELETE' ) ) !!}
                             <a class="btn btn-success" href="/{{app()->getLocale()}}/admin/download/{{ str_replace($file->filePathAttribute, '', $file->file) }}">Свали</a>
                             <a class="btn btn-default" target="_blank" href="/{{ $file->file }}">Виж</a>
                             {!! Form::submit('X', array('class'=>'btn btn-danger')) !!}
                         {!! Form::close() !!}
                          <br>
                     @endforeach
                 </div>
               @endif
          </div>
      </div>
  </div>
</section>




@endsection
@push('scripts')
<script type="text/javascript">
$(function() {
    $("#tabs").tabs();
});
</script>
@endpush
