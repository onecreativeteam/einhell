@extends('layouts.app_admin')

@section('content')
<section class="content-header">
    <h1>
       Създай Базова Категория
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box">
				<div class="box-header">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(Session::has('success'))
                        <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
                    @endif
                </div>
                <div class="box-body">
                    <div id="tabs">
                        <ul>
                        @foreach ($supportedLocales as $k => $locale)
                            <li><a href="#tabs-{{$locale}}">{{$locale}}</a></li>
                        @endforeach
                        </ul>
                        {!! Form::open( array('action' => array('Admin\BaseCategoriesController@store'), 'method' => 'POST', 'files' => 'true', 'class' => '' ) ) !!}
                            @foreach ($supportedLocales as $lang => $locale)
                                <div id="tabs-{{$locale}}">
                                    <div class="form-group">
                                        {!! Form::label('Име:') !!}
                                        {!! Form::text("translations[{$lang}][name]", null, array('required', 'class'=>'form-control', 'placeholder'=>'Име')) !!}
                                    </div>
                                </div>
                            @endforeach
                            <div class="form-group">
                                <div class="checkbox">
                                    <input id="show_in_menu" type="checkbox" name="show_in_menu" value="1">
                                    <label for="show_in_menu"> Добави в меню: </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="checkbox">
                                    <input id="show_in_homepage" type="checkbox" name="show_in_homepage" value="1">
                                    <label for="show_in_homepage"> Добави в homepage: </label>
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('137х119 /снимки/:') !!}
                                {!! Form::file('picture') !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('114х99px /пиктограма/:') !!}
                                {!! Form::file('pictogram') !!}
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Създай', array('class'=>'btn btn-primary')) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@push('scripts')
<script type="text/javascript">
$(function() {
    $("#tabs").tabs();
});
</script>
@endpush
