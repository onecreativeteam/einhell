@extends('layouts.app_admin')

@section('content')
<section class="content-header">
     <h1>
        Създай Представител
    </h1>
</section>
<section class="content">
   <div class="row">
       <div class="col-md-6">
           <div class="box">
               <div class="box-header">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(Session::has('success'))
                        <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
                    @endif
                </div>
                <div class="box-body">
                    <div id="tabs">
                        <ul>
                        @foreach ($supportedLocales as $k => $locale)
                            <li><a href="#tabs-{{$locale}}">{{$locale}}</a></li>
                        @endforeach
                        </ul>
                        {!! Form::open( array('action' => array('Admin\DistributorsController@store'), 'method' => 'POST', 'class' => '' ) ) !!}
                            @foreach ($supportedLocales as $lang => $locale)
                                <div id="tabs-{{$locale}}">
                                    <div class="form-group">
                                        {!! Form::label('Име:') !!}
                                        {!! Form::text("translations[{$lang}][name]", null, array('required', 'class'=>'form-control', 'placeholder'=>'Име')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('Адрес:') !!}
                                        {!! Form::text("translations[{$lang}][adress]", null, array('class'=>'form-control', 'placeholder'=>'Адрес')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('Телефон:') !!}
                                        {!! Form::text("translations[{$lang}][phone]", null, array('class'=>'form-control', 'placeholder'=>'Телефон')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('Мобилен Тел:') !!}
                                        {!! Form::text("translations[{$lang}][mobile_phone]", null, array('class'=>'form-control', 'placeholder'=>'Мобилен Тел.')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('Факс:') !!}
                                        {!! Form::text("translations[{$lang}][fax]", null, array('class'=>'form-control', 'placeholder'=>'Факс')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('Управител:') !!}
                                        {!! Form::text("translations[{$lang}][owner]", null, array('class'=>'form-control', 'placeholder'=>'Управител')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('E-mail:') !!}
                                        {!! Form::text("translations[{$lang}][email]", null, array('class'=>'form-control', 'placeholder'=>'E-mail')) !!}
                                    </div>
                                </div>
                            @endforeach
                            <div class="form-group">
                                {!! Form::label('Закачи град:') !!}
                                {!! Form::select('cities_id', array('0'=>'Избери Град')+$citySelect, null, ['class' => 'form-control citySelect']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Тип:') !!}
                                {!! Form::select('type', array('0'=>'Избери Тип')+$typeSelect, null, ['class' => 'form-control typeSelect']) !!}
                            </div>
                            <div class="form-group">
                                <div class="checkbox">
                                    <input id="show_in_contact" type="checkbox" name="show_in_contact" value="1">
                                    <label for="show_in_contact"> Отбележи като официален представител: </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="checkbox">
                                    <input id="show_in_contact_only" type="checkbox" name="show_in_contact_only" value="1">
                                    <label for="show_in_contact_only"> Покажи в контакти: </label>
                                </div>
                            </div>
                            <fieldset class="gllpLatlonPicker">
                                <br/>
                                <div id="gllpMap" class="gllpMap">Google Maps</div>
                                lat:
                                <input type="text" name="gps_lat" class="gllpLatitude form-control" value=""/>
                                lon:
                                <input type="text" name="gps_lon" class="gllpLongitude form-control" value=""/>
                                zoom: <input type="text" name="gps_zoom" class="gllpZoom form-control" value=""/>
                                <br>
                                <input type="button" class="gllpUpdateButton btn btn-warning" value="update map">
                            </fieldset>
                            <br>
                            <div class="form-group">
                                {!! Form::submit('Създай', array('class'=>'btn btn-primary')) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@push('scripts')
<script defer src="https://maps.googleapis.com/maps/api/js?sensor=false?key=AIzaSyAm0Cfq02K2eqq4JSk1AZp_ot6MNHIy54Y" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#tabs").tabs();
    // Copy the init code from "jquery-gmaps-latlon-picker.js" and extend it here
    $(".gllpLatlonPicker").each(function() {

      $obj = $(document).gMapsLatLonPicker();

      $obj.params.strings.markerText = "Drag this Marker (example edit)";

      $obj.params.displayError = function(message) {
        console.log("MAPS ERROR: " + message); // instead of alert()
      };

      $obj.init( $(this) );
    });
  });
</script>
@endpush
