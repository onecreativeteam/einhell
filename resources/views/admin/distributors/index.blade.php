@extends('layouts.app_admin')

@section('content')
<section class="content-header">
     <h1>
        Търговски представители
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
				<div class="box-header">
                    @if(Session::has('success'))
                        <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
                    @endif
                    <a class="btn btn-success" href="{{ URL::route( 'admin.create.distributors' ) }}">Създай Представител</a>
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            	<div class="box-body">
                    <table class="table table-bordered" id="distributors-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Име</th>
                                <th>Тип</th>
                                <th>Адрес</th>
                                <th>Официален 1/0</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                <th>Операция</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@push('scripts')
<script>
$(function() {
    $('#distributors-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{!! route('admin.datatables.distributors') !!}",
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'type', name: 'type' },
            { data: 'adress', name: 'adress' },
            { data: 'show_in_contact', name: 'show_in_contact' },
            { data: 'created_at', name: 'created_at' },
            { data: 'updated_at', name: 'updated_at' },
            { data: 'operations', name: 'operations' },
        ]
    });

    $('#distributors-table').on('click', '.deleteRowBttn', function() {

        if (!confirm('На път сте да изтриете представител. Потвърждавате ли?')) {
            return false;
        }

        return true;
    });
});
</script>
@endpush