@extends('layouts.app_admin')

@section('content')
<section class="content-header">
    <h1>
       Създай Базова Гаранция
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box">
				<div class="box-header">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(Session::has('success'))
                        <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
                    @endif
                </div>
                <div class="box-body">
                    {!! Form::open( array('action' => array('Admin\BaseWarrantyController@store'), 'method' => 'POST', 'class' => '' ) ) !!}
                        <div class="form-group">
                            {!! Form::label('Срок:') !!}
                            {!! Form::text('range', null, array('required', 'class'=>'form-control', 'placeholder'=>'Срок на гаранция')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Избери тип:') !!}
                            {!! Form::select('type', array('0'=>'Избери Тип')+$typeSelect, null, ['class' => 'form-control typeSelect']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Създай', array('class'=>'btn btn-primary')) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
