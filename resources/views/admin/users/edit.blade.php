@extends('layouts.app_admin')

@section('content')
<section class="content-header">
    <h1>
        Редактирай Потребител
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-body">
                    {!! Form::open( array('action' => array('Admin\UsersController@update', $user->id), 'method' => 'PUT', 'class' => 'form-horizontal' ) ) !!}

                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label for="first_name" class="col-md-4 control-label">Име</label>
                            <div class="col-md-6">
                                <input id="first_name" type="text" class="form-control" name="first_name" value="{{ $user->first_name }}" required autofocus>
                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label for="last_name" class="col-md-4 control-label">Фамилия</label>
                            <div class="col-md-6">
                                <input id="last_name" type="text" class="form-control" name="last_name" value="{{ $user->last_name }}" required autofocus>
                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-4 control-label">Телефон</label>
                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ $user->phone }}" required autofocus>
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('adress') ? ' has-error' : '' }}">
                            <label for="adress" class="col-md-4 control-label">Адрес</label>
                            <div class="col-md-6">
                                <input id="adress" type="text" class="form-control" name="adress" value="{{ $user->adress }}" required autofocus>
                                @if ($errors->has('adress'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('adress') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="checkbox col-md-6 col-md-offset-4">
                                <input id="has_user_discount" type="checkbox" name="has_user_discount" {{ $user->has_user_discount == 1 ? 'checked' : '' }} value="1">
                                <label for="has_user_discount"> Отстъпка за потребителя: </label>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('user_discount_value') ? ' has-error' : '' }}">
                            <label for="user_discount_value" class="col-md-4 control-label">Отстъпка %</label>
                            <div class="col-md-6">
                                <input id="user_discount_value" type="text" class="form-control" name="user_discount_value" value="{{ @$user->user_discount_value }}" autofocus>
                                @if ($errors->has('user_discount_value'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('user_discount_value') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- EMAIL FIELD REMOVED FROM HERE -->

                        <!-- PASSWORD FIELD REMOVED FROM HERE -->

                        <!-- TC FIELD REMOVED FROM HERE -->

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Запиши
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
