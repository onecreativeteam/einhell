@extends('layouts.app_admin')

@section('content')
<section class="content-header">
      <h1>
        Списък потребители

      </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                @endif
                <div class="box-header">
                    <a class="btn btn-success" href="{{ URL::route( 'admin.create.users' ) }}">Създай Потребител</a>
                </div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-bordered" id="users-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Име</th>
                                <th>Фамилия</th>
                                <th>Е-мейл</th>
                                <th>Създаден на:</th>
                                <th>Последна промяна:</th>
                                <th>Операция:</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@push('scripts')
<script>
$(function() {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{!! route('admin.datatables.users') !!}",
        columns: [
            { data: 'id', name: 'id' },
            { data: 'first_name', name: 'first_name' },
            { data: 'last_name', name: 'last_name' },
            { data: 'email', name: 'email' },
            { data: 'created_at', name: 'created_at' },
            { data: 'updated_at', name: 'updated_at' },
            { data: 'operations', name: 'operations' },
        ]
    });
});
</script>
@endpush