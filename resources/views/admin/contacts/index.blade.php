@extends('layouts.app_admin')

@section('content')
<section class="content-header">
     <h1>Контакти</h1>
 </section>
 <section class="content">
 <div class="row">
     <div class="col-md-6">
         <div class="box">
             <div class="box-header">
                @if(Session::has('success'))
                    <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
                @endif
                <a class="btn btn-success" href="{{ URL::route( 'admin.create.contacts' ) }}">Създай Контакт</a>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="box-body">
                <table class="table table-hover" id="contact-list-table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Име</th>
                            <th>Операция</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(!empty($contacts))
                    @foreach( $contacts as $k => $contact)
                        <tr>
                            <td>
                                <p>{!! $contact->id !!}</p>
                            </td>
                            <td>
                                <p>{!! (app()->getLocale() == $default_locale) ? $contact->name : $contact->translated->name !!}</p>
                            </td>
                            <td>
                                <a class="btn btn-warning btn-md" href="{{ URL::route( 'admin.edit.contacts', [$contact->id] ) }}">Edit</a>

                            </td>
                            <td>{!! Form::open( array('action' => array('Admin\ContactsController@destroy', $contact->id), 'method' => 'DELETE' ) ) !!}
                                {!! Form::submit('DELETE', array('class'=>'btn btn-danger')) !!}
                            {!! Form::close() !!}</td>
                        </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
		</div>
	</div>
</section>
@endsection
