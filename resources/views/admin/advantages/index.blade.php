@extends('layouts.app_admin')

@section('content')
<section class="content-header">
     <h1>  Предимства
     </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box">
				<div class="box-header">
                    @if(Session::has('success'))
                        <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
                    @endif
                    <a class="btn btn-success" href="{{ URL::route( 'admin.create.advantages' ) }}">Създай Предимство</a>
                </div>
            <div class="box-body">

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                @endif

                <table class="table table-hover" id="advantages-list-table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Име</th>
                            <th>Операция</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($advantages))
                        @foreach( $advantages as $k => $advantage)
                            <tr>
                                <td>
                                    <p>{!! $advantage->id !!}</p>
                                </td>
                                <td>
                                    <p>{!! (app()->getLocale() == $default_locale) ? $advantage->name : $advantage->translated->name !!}</p>
                                </td>
                                <td>
                                    <a class="btn btn-warning btn-md" href="{{ URL::route( 'admin.edit.advantages', [$advantage->id] ) }}">Edit</a>

                                </td>
                                <td>{!! Form::open( array('action' => array('Admin\AdvantagesController@destroy', $advantage->id), 'method' => 'DELETE' ) ) !!}
                                    {!! Form::submit('DELETE', array('class'=>'btn btn-danger', 'onClick'=>'return confirm("Сигурни ли сте, че искате да изтриете това предимство?")')) !!}
                                {!! Form::close() !!}</td>
                            </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
		</div>
	</div>
</section>
@endsection
