@extends('layouts.app_admin')

@section('content')
<section class="content-header">
     <h1>
        Редактирай Предимство
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box">
				<div class="box-header">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(Session::has('success'))
                        <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
                    @endif
                </div>
                <div class="box-body">
                    <div id="tabs">
                        <ul>
                        @foreach ($supportedLocales as $k => $locale)
                            <li><a href="#tabs-{{$locale}}">{{$locale}}</a></li>
                        @endforeach
                        </ul>
                        {!! Form::open( array('action' => array('Admin\AdvantagesController@update', $advantage->id), 'method' => 'PUT', 'files' => 'true', 'class' => '' ) ) !!}
                            @foreach ($supportedLocales as $lang => $locale)
                                <div id="tabs-{{$locale}}">
                                    <div class="form-group">
                                        {!! Form::label('Име:') !!}
                                        {!! Form::text("translations[{$lang}][name]", isset($advantage->translations[$locale]) ? $advantage->translations[$locale]->name : $advantage->name, array('required', 'class'=>'form-control', 'placeholder'=>'Име')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('Текст:') !!}
                                        {!! Form::text("translations[{$lang}][text]", isset($advantage->translations[$locale]) ? $advantage->translations[$locale]->text : $advantage->text, array('required', 'class'=>'form-control', 'placeholder'=>'Текст')) !!}
                                    </div>
                                </div>
                            @endforeach 
                            <div class="form-group">
                                {!! Form::label('Снимка /98х76/:') !!}
                                {!! Form::file('picture') !!}
                                <img style="width:150px;height:150px;margin-top:10px" src="/{{$advantage->picture}}"/>
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Редактирай', array('class'=>'btn btn-primary')) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
		</div>
	</div>
</section>
@endsection
@push('scripts')
<script type="text/javascript">
$(function() {
    $("#tabs").tabs();
});
</script>
@endpush
