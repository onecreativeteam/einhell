@extends('layouts.app_admin')

@section('content')
<section class="content-header">
     <h1>
         Добави Файлове
    </h1>
</section>
    @if(Session::has('success'))
        <div class="alert alert-success"><em> {!! session('success') !!}</em></div>
    @endif
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
    @endif
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-body">
                        {!! Form::open( array('action' => array('Admin\SystemFilesController@store'), 'method' => 'POST', 'files' => 'true', 'class' => '' ) ) !!}

                                <div class="form-group">
                                    {!! Form::label('Добави Faq:') !!}
                                    {!! Form::file('file') !!}
                                    {!! Form::hidden('type', 'faq') !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::submit('Добави Faq', array('class'=>'btn btn-success')) !!}
                                </div>
                                @if (isset($files['faq']))
                                    <a class="btn btn-default btn-md" target="_blank" href="/{{$files['faq']->file}}">Faq Преглед</a>
                                @endif
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-body">
                        {!! Form::open( array('action' => array('Admin\SystemFilesController@store'), 'method' => 'POST', 'files' => 'true', 'class' => '' ) ) !!}

                                <div class="form-group">
                                    {!! Form::label('Добави Terms&Conditions:') !!}
                                    {!! Form::file('file') !!}
                                    {!! Form::hidden('type', 'tc') !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::submit('Добави Terms&Conditions', array('class'=>'btn btn-success')) !!}
                                </div>
                                @if (isset($files['tc']))
                                    <a class="btn btn-default btn-md" target="_blank" href="/{{$files['tc']->file}}">Terms&Conditions Преглед</a>
                                @endif
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('scripts')
<script type="text/javascript">
$(function() {
    $("#tabs").tabs();
});
</script>
@endpush
