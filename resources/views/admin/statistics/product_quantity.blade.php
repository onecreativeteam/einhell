@extends('layouts.app_admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>
                        <a class="btn btn-default" href="{{ route('admin.home') }}">Назад</a>
                        Продукти с изчерпващи се наличности
                    </h2>
                </div>
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                          <div class="box">
                            <div class="box-body table-responsive no-padding">
                              <table class="table table-hover">
                                <tbody>
                                    <tr>
                                      <th>ID</th>
                                      <th>Име</th>
                                      <th>Наличност</th>
                                      <th>Номер</th>
                                      <th>Операция</th>
                                    </tr>
                                    @foreach ($products as $product)
                                    <tr>
                                      <td>{{ $product->id }}</td>
                                      <td>{{ $product->name }}</td>
                                      <td>{{ $product->quantity }}</td>
                                      <td>{{ $product->product_number }}</td>
                                      <td><a class="btn btn-warning btn-md" href="{{ URL::route( 'admin.edit.product', [$product->id] ) }}">Edit</a></td>
                                    </tr>
                                    @endforeach
                              </tbody>
                            </table>
                            </div>
                          </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')

@endpush
