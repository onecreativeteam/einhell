@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
                <h3 class="page-title"><span></span>Данни за поръчка</h3>
        </div>
    </div>
    <div class="credential-page">
        <div class="bg-border">
            <form role="form" method="POST" action="">
                <div class="row">
                    <div class="col-md-4 col-md-offset-2">
                        <div class="form-group">
                            <label for="#first_name"><span class="required">* </span>Име</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="formicon formicon-user"></i></span>
                                <input autocomplete="off" placeholder="Име" id="first_name" type="text" class="form-control" name="first_name" value="" required autofocus>

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="#last_name"><span class="required">* </span>Фамилия</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="formicon formicon-user"></i></span>
                                <input autocomplete="off" placeholder="Фамилия" id="last_name" type="text" class="form-control" name="last_name" value="" required autofocus>

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email"><span class="required">* </span>Еmail</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="formicon formicon-mail"></i></span>
                                <input autocomplete="off" id="email" placeholder="Еmail" class="form-control" value="" required autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="#phone"><span class="required">* </span>Телефон</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="formicon formicon-moblie"></i></span>
                                <input autocomplete="off" placeholder="+359" id="phone" type="text" class="form-control" name="phone" value="" required autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="#adress"><span class="required">* </span>Адрес</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="formicon formicon-adress"></i></span>
                                <input autocomplete="off" placeholder="Град, квартал, улица №, бл., вх., ет., ап." id="adress" type="text" class="form-control" name="adress" value="" required autofocus>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="#postcode"><span class="required">* </span>Пощенски код:</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="formicon formicon-adress"></i></span>
                                <input autocomplete="off" placeholder="Пощенски код:" id="postcode" type="password" class="form-control" name="postcode" required autofocus>
                            </div>
                        </div>
                        <div class="form-group custom-select">
                            <label for="#postcode"><span class="required">* </span>Метод на плащане:</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="formicon formicon-payment"></i></span>
                                <select class="selectpicker">
                                    <option>Mustard</option>
                                    <option>Ketchup</option>
                                    <option>Relish</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="comments">Коментар:</label>
                            <textarea placeholder="Коментар" class="form-control" id="comments" rows="8"></textarea>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="radio">
                                        <input id="person" checked value="person" type="radio" name="ivoice">
                                        <label for="person"> Физическо лице </label>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="radio">
                                        <input id="legal" value="legal"  type="radio" name="ivoice">
                                        <label for="legal"> Юридическо лице </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <span class="required">* </span>  Задължителни полета
                    </div>
                </div>
                <div class="company-invoice">
                    <div class="row">
                        <div class="col-md-4 col-md-offset-2">
                            <div class="form-group">
                                <label for="#company"><span class="required">** </span>Име на фирма</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="formicon formicon-company"></i></span>
                                    <input autocomplete="off" placeholder="Име на фирма" id="company" type="text" class="form-control" name="company" value="" required autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="#mol"><span class="required">** </span>МОЛ</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="formicon formicon-user"></i></span>
                                    <input autocomplete="off" placeholder="МОЛ" id="mol" type="text" class="form-control" name="mol" value="" required autofocus>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="#bustat"><span class="required">** </span>Булстат</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="formicon formicon-bustat"></i></span>
                                    <input autocomplete="off" placeholder="Булстат" id="bustat" type="text" class="form-control" name="bustat" value="" required autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="#company-adres"><span class="required">** </span>Адрес на фирма:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="formicon formicon-adress"></i></span>
                                    <input autocomplete="off" placeholder="Град, квартал, улица №, бл., вх., ет., ап. " id="company-adres" type="text" class="form-control" name="company-adres" value="" required autofocus>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-status">
                        <div class="col-md-4 col-md-offset-2">
                            <span class="required">** </span>  Попълва се само за юридически лица
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="checkbox">
                                    <input id="remember-me" type="checkbox"checked name="remember">
                                    <label for="remember-me"> По ЗДДС </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="submit">
                            Потвърди
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <ul class="breadcrumb-wrapper clearfix">
        <li><a href="/"><span class="home-breadcrumb"></span></a></li>
        <li><a href="#">Моята количка</a></li>
        <li><a href="#">Данни за поръчка</a></li>
    </ul>
</div>
@endsection
@section('page-scripts')
    <script type="text/javascript" src="{!! asset('js/shipping.js') !!}" charset="utf-8"></script>
@endsection
