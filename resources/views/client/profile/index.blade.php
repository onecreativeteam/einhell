@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                </div>
            @endif
            @if(Session::has('flash_success'))
                <div class="alert alert-success"><em> {!! session('flash_success') !!}</em></div>
            @endif
                {{-- <h2>Hello in Einhell {{$user->first_name .' '.$user->last_name}}.</h2> --}}
                <h3 class="page-title"><span></span>Моят профил</h3>
        </div>
    </div>
    <div class="credential-page">
        <div class="bg-border">
            <form role="form" method="POST" action="{{ route('profile.update') }}">
                <div class="row">
                    <div class="col-md-4 col-md-offset-2">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="formicon formicon-user"></i></span>
                                <input placeholder="Име" pattern="^[a-zA-Z\u0400-\u04ff\s]{2,}$" title="Полето трябва да съдържа минимум 2 символа. Позволени са само букви." id="first_name"  type="text" class="form-control" name="first_name" value="{{ $user->first_name ? $user->first_name : old('first_name') }}" required autofocus>
                            </div>
                            @if ($errors->has('first_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="formicon formicon-user"></i></span>
                                <input placeholder="Фамилия" pattern="^[a-zA-Z\u0400-\u04ff\s]{2,}$" title="Полето трябва да съдържа минимум 2 символа. Позволени са само букви." id="last_name" type="text" class="form-control" name="last_name" value="{{ $user->last_name ? $user->last_name : old('last_name') }}" required autofocus>

                            </div>
                            @if ($errors->has('last_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="formicon formicon-moblie"></i></span>
                                <input placeholder="Телефон" title="Полето трябва да съдържа между 10 и 15 символа. Позволени са +-() и интервал."  pattern="^[0-9\-\+\s\(\)]{10,15}$" id="phone" type="tel" class="form-control" name="phone" value="{{ $user->phone ? $user->phone : old('phone') }}" required autofocus>

                            </div>
                            @if ($errors->has('phone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('adress') ? ' has-error' : '' }}">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="formicon formicon-adress"></i></span>
                            <input  placeholder="Адрес" id="adress" type="text" class="form-control" name="adress" value="{{ $user->adress ? $user->adress : old('adress') }}" required autofocus>
                            </div>
                            @if ($errors->has('adress'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('adress') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="formicon formicon-mail"></i></span>
                                <input placeholder="Е-мейл Адрес" class="form-control" value="{{ $user->email ? $user->email : old('email') }}" disabled>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="formicon formicon-pass"></i></span>
                                <input pattern=".{6,}" title="Полето трябва да съдържа минимум 6 символа." placeholder="Стара Парола"  id="old_password" type="password" class="form-control" name="old_password">
                                @if ($errors->has('old_password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('old_password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="formicon formicon-pass"></i></span>
                                <input pattern=".{6,}" title="Полето трябва да съдържа минимум 6 символа." placeholder="Нова парола" id="password" type="password" class="form-control" name="password">
                            </div>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="formicon formicon-pass"></i></span>
                                <input pattern=".{6,}" title="Полето трябва да съдържа минимум 6 символа." placeholder="Повтори нова парола" id="password-confirm" type="password" class="form-control" name="password_confirmation">
                            </div>
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>

                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="submit">
                            Запази
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @if (count($orderProducts))
    <div class="row">
        <div class="col-md-12 text-center">
            <h3 class="page-title margin"><span></span>История на покупки</h3>
        </div>
    </div>
    <div class="bg-border card-product-list-wrapper">
        <table class="card-product-list">
            <tr>
                <th>Продукт:</th>
                <th></th>
                <th>Брой:</th>
                <th>Арт. №: </th>
                <th>Eд. цена:</th>
                <th>Крайна цена:</th>
                <th></th>
            </tr>
            @foreach ($orderProducts as $product)
            <tr>
                <td data-th="Продукт:">
                    <div class="">
                        <span class="product-image" style="background-image: url(/{{ ( !is_null($product->picture) || $product->picture != '' ) ? $product->picture : ( count($product->pictures) ? $product->pictures[0]->resource : '' )  }})">
                        </span>
                    </div>
               </td>
               <td>
                   <div class="title">
                       {{ (strlen($product->name) > 30) ? mb_substr($product->name,0,31).'..' : $product->name }}
                   </div>
               </td>
               <td data-th="Брой:">
                   <div class="quantity">
                       {{ $product->product_order_quantity }}
                   </div>
               </td>
                <td data-th="Арт. №:">
                    <div>
                        {{ $product->product_number }}
                    </div>
                </td>
                <td data-th="Eд. цена:">
                    <div>
                        <strong>{{ $product->product_order_s_price }}лв.</strong>
                    </div>
                </td>
                <td data-th="Крайна цена:">
                    <div>
                        <strong>{{ $product->product_order_t_price }}лв.</strong>
                    </div>
                </td>
                <td>
                    <div>
                        <a class="submit-small" href="{{ route('profile.view.order', [$product->order_id]) }}">Виж поръчката</a>
                    </div>
                </td>
            </tr>
            @endforeach
        </table>
    </div>
    @endif
    <ul class="breadcrumb-wrapper clearfix">
        <li><a href="{{ route('home') }}"><span class="home-breadcrumb"></span></a></li>
        <li><a href="{{ route('profile') }}">Моят профил</a></li>
    </ul>
</div>
@endsection
