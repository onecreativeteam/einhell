@extends('layouts.app')

@section('content')
<div class="home-header-wrapper">
    <div class="background">
        <div class="container ">
            <div class="row">
                <div class="col-md-12">
                    <div class="header-slider-wrapper">
                        <div class="swiper-container home-header-slider">
                           <div class="swiper-wrapper">
                              @if ($sliders)
                                @foreach ($sliders as $slider)
                                  <div class="swiper-slide">
                                     <div class="row">
                                         <div class="col-sm-4 col-sm-push-6">
                                             <img class="product-pic" src="/{{$slider->picture}}" alt="">
                                         </div>
                                         <div class="col-sm-6 col-sm-pull-4 ">
                                             <h3 class="product-summary">
                                                 {{ $slider->name }}
                                             </h3>
                                             <h5 class="product-title">
                                                 {{ $slider->product->name }}
                                             </h5>
                                             <a href="{{ route('product.inner', [$slider->product->slug]) }}" class="product-price">купи сега</a>
                                             <p class="additional">{{ $slider->description }}</p>
                                         </div>
                                     </div>
                                  </div>
                                @endforeach
                              @else
                               <div class="swiper-slide">
                                   <div class="row">
                                       <div class="col-sm-4 col-sm-push-6 col-sm-offset-1">
                                           <img class="product-pic" src="/img/product.png" alt="">
                                       </div>
                                       <div class="col-sm-6 col-sm-pull-4 ">
                                           <h3 class="product-summary">
                                               Снабден с мощност, отлично проектиран
                                           </h3>
                                           <h5 class="product-title">
                                               Перфоратор TH-RH 1600
                                           </h5>
                                           <a href="#" class="product-price">купи сега</a>
                                           <p class="additional">+ метален ограничител за дълбочината на пробиване и куфар</p>
                                       </div>
                                   </div>
                               </div>
                               @endif
                           </div>
                           <!-- Add Arrows -->
                           <div class="swiper-button-next swiper-button-next-home"></div>
                           <div class="swiper-button-prev swiper-button-prev-home"></div>
                       </div>
                   </div>
               </div>
            </div>
        </div>
    </div>
</div>
<div class="container home-categories">
    @if(Session::has('success'))
      <div class="alert alert-success" style="text-align:center"><em> {!! session('success') !!}</em></div>
    @endif
    <div class="row">
        <div class="col-xs-3">
            <a href="{{ route('product.index') }}" class="caregory-wrapper">
                <h3>Онлайн <br> магазин</h3>
                <img class="category-icon" src="/img/home-categories/online-shop.png" alt="">
            </a>
        </div>
        @if ($baseCategories)
          @foreach ($baseCategories as $baseCat)
          <div class="col-xs-3">
              <a href="{{ route('product.base.category', [$baseCat->slug]) }}" class="caregory-wrapper">
                  <h3>{{ $baseCat->name }}</h3>
                  <img class="category-icon" src="/{{ $baseCat->pictogram_picture }}" alt="">
              </a>
          </div>
          @endforeach
        @endif
        <div class="col-xs-3">
            <a href="{{ route('partners.index') }}" class="caregory-wrapper">
                <h3>Магазини и  <br> сервизи</h3>
                <img class="category-icon" src="/img/home-categories/dealers-recruiting.png" alt="">
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-3">
            <a href="{{ route('about.index', ['#tab-materials']) }}" class="caregory-wrapper">
                <h3>Рекламни  <br> материали</h3>
                <img class="category-icon" src="/img/home-categories/promotion-materials.png" alt="">
            </a>
        </div>
        <div class="col-xs-3">
            <a href="https://www.youtube.com/user/Einhellgermany?&ab_channel=Einhell" target="_blank" class="caregory-wrapper">
                <h3>YouТube канал <br> на Einhell</h3>
                <img class="category-icon" src="/img/home-categories/youtube.png" alt="">
            </a>
        </div>
        <div class="col-xs-3">
            <a href="{{ route('research.index') }}" class="caregory-wrapper">
                <h3>Сертификати <br> за качество</h3>
                <img class="category-icon" src="/img/home-categories/quality-certificate.png" alt="">
            </a>
        </div>
        <div class="col-xs-3">
            <a href="{{ route('xchange.index') }}" class="caregory-wrapper">
                {{--  <h3>Инициативата <br> Power X-Change</h3>  --}}
                <h3 style="color: #e3370e">Семейство <br> Power X-Change</h3>

                <img class="category-icon" src="/img/home-categories/power-exchange.png" alt="">
            </a>
        </div>
    </div>
</div>
<div class="container home-promotional-products">
    <div class="row">
        <div class="col-md-12">
            <h2>Промоционални продукти <span></span></h2>
        </div>
    </div>
    <div class="row">
        <div class="swiper-container promotions-slider">
           <div class="swiper-wrapper">
              @if ($promoProducts)
               @foreach ($promoProducts as $product)
               <div class="swiper-slide">
                   <a class="single-product-grid" href="{{ route('product.inner', [$product->slug]) }}">
                       <div class="text-center">
                            <div class="promo-label">Промо</div>
                            <div class="product-pic" style="background-image: url(/{{ ( !is_null($product->picture) || $product->picture != '' ) ? $product->picture : ( count($product->pictures) ? $product->pictures[0]->resource : '' )  }})"></div>
                            <h3> {{ mb_substr($product->name,0,34) }} </h3>
                            <h5>Арт.№: {{ $product->product_number }} </h5>
                            <div class="price-wrapper">
                                <p>цена: <strong>{{ $product->promo_price }} лв. </strong></p>
                                <p class="more">виж повече</p>
                            </div>
                        </div>
                    </a>
               </div>
               @endforeach
              @endif
           </div>
           <!-- Add Arrows -->
           {{-- <div class="swiper-button-next next-home-promotion"></div>
           <div class="swiper-button-prev prev-home-promotion"></div> --}}
       </div>

    </div>
</div>
<div class="container home-promotional-products">
    <div class="row">
        <div class="col-md-12">
            <h2>Най-продавани продукти <span></span></h2>
        </div>

    </div>
    <div class="row">
        <div class="swiper-container bestsellers">
           <div class="swiper-wrapper">
              @if ($topProducts)
               @foreach ($topProducts as $product)
               <div class="swiper-slide">
                   <a class="single-product-grid" href="{{ route('product.inner', [$product->slug]) }}">
                       <div class="text-center">
                            @if ($product->promo_product == '1')
                              <div class="promo-label">Промо</div>
                            @endif
                            <div class="product-pic" style="background-image: url(/{{ ( !is_null($product->picture) || $product->picture != '' ) ? $product->picture : ( count($product->pictures) ? $product->pictures[0]->resource : '' )  }})"></div>
                            <h3>{{ mb_substr($product->name,0,34) }}</h3>
                            <h5>Арт.№: {{ $product->product_number }} </h5>
                            <div class="price-wrapper">
                                <p>цена: <strong>{{ $product->promo_product == '1' ? $product->promo_price : $product->price }} лв. </strong></p>
                                <p class="more">виж повече</p>
                            </div>
                        </div>
                    </a>
               </div>
               @endforeach
              @endif
            </div>
           <!-- Add Arrows -->
           {{-- <div class="swiper-button-next next-bestsellers"></div>
           <div class="swiper-button-prev prev-bestsellers"></div> --}}
       </div>

    </div>
</div>
@endsection
@section('page-scripts')
    <script type="text/javascript" src="{!! asset('js/sliders.js') !!}" charset="utf-8"></script>
@endsection
