@extends('layouts.app')

@section('content')

<div class="container contact-page">
    <div class="clearfix">
        <div class="col-md-12">
            <h3 class="page-title-stroke">Контакти<span></span></h3>
        </div>
    </div>
    <br>
    <div class="clearfix">
        <div class="col-md-12">

            <div class="bg-border">
                <div class="text-center">
                    <h3 class="page-title"><span></span>Официални магазини за Айнхел България ООД:</h3>
                </div>
                @if ($mainShops)
                @foreach ($mainShops->chunk(3) as $chunk)
                <div class="row sigle-row">
                    <div class="col-md-10 col-md-offset-1">
                        @foreach ($chunk as $shop)
                        <div class="col-md-4">
                            <div class="single-contact">
                                <div class="icon contact-adress"></div>
                                <div class="text-wrapper">
                                    {{ @$shop->city->name . ' ' . @$shop->city->postcode }}, </br>
                                    {{$shop->adress}}
                                </div>
                            </div>
                            <div class="single-contact">
                                <div class="icon contact-phone"></div>
                                <div class="text-wrapper">
                                    Тел.: {{ $shop->phone }}<br>
                                    Факс: {{ $shop->fax }}
                                </div>
                            </div>
                            <div class="single-contact mail">
                                <div class="icon contact-mail"></div>
                                <div class="text-wrapper">
                                    Еmail: {{ $shop->email }}
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                @endforeach
                @endif
                <div class="text-center">
                    <h3 class="page-title"><span></span>Официални сервизи за Айнхел България ООД:</h3>
                </div>
                @if ($mainServices)
                @foreach ($mainServices->chunk(3) as $chunk)
                <div class="row sigle-row">
                    <div class="col-md-10 col-md-offset-1">
                        @foreach ($chunk as $service)
                        <div class="col-md-4">
                            <div class="single-contact">
                                <div class="icon contact-adress"></div>
                                <div class="text-wrapper">
                                    {{ @$service->city->name . ' ' . @$service->city->postcode }}, </br>
                                    {{$service->adress}}
                                </div>
                            </div>
                            <div class="single-contact">
                                <div class="icon contact-phone"></div>
                                <div class="text-wrapper">
                                    Тел.: {{ $service->phone }}<br>
                                    Факс: {{ $service->fax }}
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                @endforeach
                @endif
                <div class="text-center" id="contact">
                    <h3 class="page-title"><span></span>Имате въпрос? Можете да ни попитате тук:</h3>
                     @if(Session::has('success'))
                        <script type="text/javascript">
                            $( document ).ready(function() {
                                $(window).scrollTop($('#contact').offset().top);
                            });
                        </script>
                        <div class="alert-bg">
                            <div class="alert-wrapper">
                                <p>{!! session('success') !!}</p> 
                                <div class="submit">
                                    Ok
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                    {!! Form::open( array('action' => array('Home\ContactController@store'), 'method' => 'POST', 'class' => '' ) ) !!}
                    <div  class="row credential-page">
                        <div class="col-md-5 col-md-offset-1">

                            <div class="form-group custom-select">
                                <div class="input-group">
                                {!! Form::select('theme_id', $themeSelect, null, ['required', 'placeholder'=>'Тема *', 'class' => 'selectpicker themeSelect']) !!}
                                @if ($errors->has('theme_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('theme_id') }}</strong>
                                    </span>
                                @endif
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::text('first_name', null, array('required', 'class'=>'form-control', 'placeholder'=>'Име *')) !!}
                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                {!! Form::text('last_name', null, array('class'=>'form-control', 'placeholder'=>'Фамилия')) !!}
                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                {!! Form::text('adress', null, array('class'=>'form-control', 'placeholder'=>'Адрес')) !!}
                                @if ($errors->has('adress'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('adress') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                {!! Form::text('phone', null, array('class'=>'form-control', 'placeholder'=>'Телефон')) !!}
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                {!! Form::email('email', null, array('required', 'class'=>'form-control', 'placeholder'=>'Email *')) !!}
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                {!! Form::text('product_number', null, array('class'=>'form-control', 'placeholder'=>'Номер на продукт')) !!}
                                @if ($errors->has('product_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('product_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                {!! Form::textarea('message', null, array( 'required', 'class'=>'form-control', 'placeholder'=>'Съобщение *')) !!}
                                @if ($errors->has('message'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group text-right">
                                {!! Form::submit('Изпрати', array('class'=>'submit')) !!}
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
        </div>
    </div>
</div>
<div id="map_canvas_contacts"></div>
<div class="container contact-page">
    <div class="clearfix">
        <div class="col-md-12">
            <ul class="breadcrumb-wrapper clearfix">
                <li><a href="{{ route('home') }}"><span class="home-breadcrumb"></span></a></li>
                <li>КОНТАКТИ</li>
             </ul>
         </div>
    </div>
</div>
@endsection
@section('page-scripts')
    <script src="https://maps.googleapis.com/maps/api/js?libraries=geometry,places&ext=.js" type="text/javascript"></script>

    <script type="text/javascript">

    $('.alert-bg .submit').click(function(){
        $('.alert-bg').hide();
    })
    // function createIcon(color) {
    //     return {
    //         path: 'M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z',
    //         fillColor: color,
    //         fillOpacity: 0.95,
    //         scale: 2,
    //         strokeColor: '#fff',
    //         strokeWeight: 3,
    //         anchor: new google.maps.Point(12, 24)
    //     };
    // }
    function returnZoom () {
        if($(window).width()>991){
            return 8
        }else if($(window).width()>550){
            return 7
        }else if($(window).width()>319){
            return 6
        }
    }
    function initialize() {
        var isDraggable = $(document).width() > 480 ? true : false;


        var map = new google.maps.Map(
            document.getElementById("map_canvas_contacts"), {
                center: new google.maps.LatLng(42.65, 25.55),
                zoom: returnZoom(),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                draggable: isDraggable,
                scrollwheel: false,
                navigationControl: false,
                mapTypeControl: false,
                scaleControl: false,
            }
        );

        var marker1 = new google.maps.Marker({
            map: map,
            position: {
                lat: 43.235081,
                lng: 27.856206
            },
             icon: '/img/google-pin.png'
        })
        var marker2 = new google.maps.Marker({
            map: map,
            position: {
                lat: 43.145093,
                lng: 27.855834
            },
             icon: '/img/google-pin-service.png'
        });

    }
    google.maps.event.addDomListener(window, "resize", initialize);
    google.maps.event.addDomListener(window, "load", initialize);
</script>
@append
