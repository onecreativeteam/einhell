@extends('layouts.app')

@section('content')
<div class="power-xchange-page">
    <div class="home-header-wrapper power-xchange">
        <div class="container">
            <div class="row">
                @if (count($sliders))
                <div class="col-md-12">
                    <div class="header-slider-wrapper">
                        <div class="swiper-container home-header-slider">
                           <div class="swiper-wrapper">
                              @foreach ($sliders as $slider)
                                <div class="swiper-slide">
                                 <div class="row">
                                     <div class="col-sm-6 col-sm-push-5 col-sm-offset-1">
                                         <img class="product-pic" src="/{{$slider->picture}}" alt="">
                                     </div>
                                     <div class="col-sm-5 col-sm-pull-6 col-sm-offset-1">
                                         <h3 class="product-summary">
                                             {{ $slider->name }}
                                         </h3>
                                         <h5 class="product-desc">
                                             {{ $slider->product->name }}. {{ $slider->description }}
                                         </h5>
                                         <a href="{{ route('product.inner', [$slider->product->slug]) }}" class="product-price">виж продукти</a>
                                     </div>
                                 </div>
                                </div>
                              @endforeach
                            </div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next swiper-button-next-home"></div>
                            <div class="swiper-button-prev swiper-button-prev-home"></div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h3 class="page-title margin"><span></span>Power X-Change Приложение</h3>
            </div>
            @if ($categories)
            <div class="custom-colum-wrapper">
                @foreach ($categories->chunk(5) as $k => $chunk)
                <div class="clearfix">
                    @foreach ($chunk as $cat)
                    <div class="custom-colum">
                        <a class="inner" href="{{ route('product.xchange.category', [$cat->slug]) }}">
                            <div class="img" style="background-image: url(/{{$cat->picture}})"></div>
                            <h3 class="title">{{$cat->name}}</h3>
                        </a>
                    </div>
                    @endforeach
                </div>
                @endforeach
            @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <h3 class="page-title margin"><span></span>Предимства</h3>
            </div>
        </div>
        <div class="row advantages">

            @if ($advantages)
            @foreach ($advantages as $advantage)

            <div class="col-md-4 text-center">
                <div class="icon icon-1" style="background-image: url(/{{ $advantage->picture }})"></div>
                <h5>{{$advantage->name}}</h5>
                <div class="text-wrapper">
                    {{$advantage->text}}
                </div>
            </div>
            @endforeach
            @endif
        </div>
    </div>
    <div class="container power-xchange-main-description">
        <div class="bg-border">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <img class="main-image" src="/{{$xchange->first()->picture}}" alt="">
                    <h3 class="title" style="margin-bottom:35px"> {{$xchange->first()->name}} </h3>
                    <div class="text-wrapper">
                        {!! $xchange->first()->text !!}
                    </div>
                </div>
            </div>
        </div>
        <ul class="breadcrumb-wrapper clearfix">
            <li><a href="{{ route('home') }}"><span class="home-breadcrumb"></span></a></li>
            <li>Power X-Change</li>
        </ul>
    </div>
</div>
@endsection
@section('page-scripts')
    <script type="text/javascript" src="{!! asset('js/sliders.js') !!}" charset="utf-8"></script>
@endsection
