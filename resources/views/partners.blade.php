@extends('layouts.app')

@section('content')
<div class="container contact-page partners-page">

    <div class="clearfix">
        <div class="col-md-10 col-md-offset-1 text-center">
            <h3 class="page-title"><span></span>Намери всички оторизирани дилъри, сервизи и магазини на Айнхел България</h3>
        </div>
    </div>
    <div class="clearfix map-search">
        <div class="col-md-6 col-md-offset-1 no-padding credential-page">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="formicon formicon-adress"></i></span>
                    <input autocomplete="off" id="autocomplete"  placeholder="Търси дилъри, сервизи и магазини" id="email" type="email" class="form-control" name="email" value="" required="">
                </div>
            </div>
        </div>
        <div class="col-md-3 no-padding">
            <div class="form-group custom-select">
                <div class="input-group">
                    <select class="selectpicker" title="всички"  multiple id="map-selector">

                        <option value="diller">дилъри</option>
                        <option value="service">сервизи</option>
                        <option value="shop">магазини</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-1 no-padding">
            <div id="search">
                <img src="/img/search-icon.png" alt="">
            </div>
        </div>
    </div>
    <div class="clearfix">
        <div class="col-md-5 col-md-offset-1 no-padding">
            <div class="legent-wrapper">
                <ul class="clearfix">
                    <li><strong>Легенда:</strong> </li>
                    <li><span class="legend"></span>Магазини</li>
                    <li><span class="shop"></span>Сервизи</li>
                    <li><span class="dilar"></span>Дилъри</li>
                </ul>
            </div>
        </div>
        <!-- <div class="col-md-4 col-md-offset-1 no-padding">
            <div id="find-nearest">
                Намери най-близкия магазин до мен
            </div>
        </div> -->
    </div>
</div>
    <div id="map_canvas_contacts"></div>
<div class="container contact-page partners-page">
    <div class="clearfix">
        <div class="col-md-12">
            <h3 class="page-title-stroke">магазини и сервизи<span></span></h3>
        </div>
    </div>

    <br>
    <div class="clearfix">
        <div class="col-md-12">
            <div class="bg-border">
                <div class="text-center">
                    <h3 class="page-title"><span></span>Официални магазини за Айнхел България ООД:</h3>
                </div>
                @if ($mainShops)
                @foreach ($mainShops->chunk(3) as $chunk)
                <div class="row sigle-row full-row-filtered" id="full-shop">
                    <div class="col-md-10 col-md-offset-1">
                        @foreach ($chunk as $shop)
                        <div class="col-md-4">
                            <div class="single-contact">
                                <div class="icon contact-adress"></div>
                                <div class="text-wrapper">
                                    {{ $shop->city ? @$shop->city->name : '' . ' ' . @$shop->city->postcode }}, </br>
                                    {{$shop->adress}}
                                </div>
                            </div>
                            <div class="single-contact">
                                <div class="icon contact-phone"></div>
                                <div class="text-wrapper">
                                    Тел.: {{ $shop->phone }}<br>
                                    Факс: {{ $shop->fax }}
                                </div>
                            </div>
                            <div class="single-contact mail">
                                <div class="icon contact-mail"></div>
                                <div class="text-wrapper">
                                    Еmail: {{ $shop->email }}
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                @endforeach
                @endif
                <div class="col-md-10 col-md-offset-1">
                    <div class="separator"></div>
                </div>
                <div class="text-center">
                    <h3 class="page-title"><span></span>Официални сервизи за Айнхел България ООД:</h3>
                </div>
                @if ($mainServices)
                @foreach ($mainServices->chunk(3) as $chunk)
                <div class="row sigle-row full-row-filtered" id="full-service">
                    <div class="col-md-10 col-md-offset-1">
                        @foreach ($chunk as $service)
                        <div class="col-md-4">
                            <div class="single-contact">
                                <div class="icon contact-adress"></div>
                                <div class="text-wrapper">
                                    {{ $service->city ? @$service->city->name : '' . ' ' . @$service->city->postocode }}, </br>
                                    {{$service->adress}}
                                </div>
                            </div>
                            <div class="single-contact">
                                <div class="icon contact-phone"></div>
                                <div class="text-wrapper">
                                    Тел.: {{ $service->phone }}<br>
                                    Мобилен: {{ $service->mobile_phone }}
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                @endforeach
                @endif
                <div class="col-md-10 col-md-offset-1">
                    <div class="separator"></div>
                </div>
                <div class="text-center dealers">
                    <h3 class="page-title"><span></span>ОТОРИЗИРАНИ ДИЛЪРИ НА АЙНХЕЛ БЪЛГАРИЯ ООД :</h3>
                    <p>Набираме дилъри за страната!<br>
                    Ако желаете да станете наш дилър, моля свържете се с нас:</p>
                </div>
                @if ($mainDillers)
                @foreach ($mainDillers->chunk(3) as $chunk)
                <div class="row sigle-row full-row-filtered" id="full-diller">
                    <div class="col-md-10 col-md-offset-1">
                        @foreach ($chunk as $diller)
                        <div class="col-md-4">
                            <div class="single-contact mail">
                                <div class="icon contact-adress"></div>
                                <div class="text-wrapper">
                                    <strong>{{$diller->name}}</strong>
                                </div>
                            </div>
                            <div class="single-contact">
                                <div class="icon contact-user"></div>
                                <div class="text-wrapper">
                                    {{$diller->owner}}
                                </div>
                            </div>
                            <div class="single-contact">
                                <div class="icon contact-phone"></div>
                                <div class="text-wrapper">
                                    Моб.: {{ $diller->mobile_phone }}
                                </div>
                            </div>
                            <div class="single-contact mail">
                                <div class="icon contact-mail"></div>
                                <div class="text-wrapper">
                                    Еmail: {{ $diller->email }}
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                @endforeach
                @endif
                <div class="col-md-10 col-md-offset-1">
                    <div class="separator"></div>
                </div>
                <div class="other-contacts">
                        @if ($otherDistributors)
                        @foreach ($otherDistributors->chunk(3) as $chunk)
                        <div class="row sigle-row">
                            <div class="col-md-10 col-md-offset-1">
                                @foreach ($chunk as $distributor)
                                <div class="col-md-4">
                                    <ul class="others">
                                        <li class="upper">{{ $distributor->city ? $distributor->city->name : '' }}</li>
                                        <li class="upper">{{$distributor->name}}</li>
                                        <li>{{$distributor->adress}}</li>
                                        <li>{{ $distributor->phone .'; '. $distributor->mobile_phone }}</li>
                                    </ul>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        @endforeach
                        @endif
                </div>

            </div>
        </div>
    </div>
    <div class="clearfix">
        <div class="col-md-12">
            <ul class="breadcrumb-wrapper clearfix">
                <li><a href="{{ route('home') }}"><span class="home-breadcrumb"></span></a></li>
                <li> Магазини и сервизи</li>
             </ul>
         </div>
    </div>
</div>
@endsection
@section('page-scripts')

<script type="text/javascript">

    var total, marker, cpos, pos, map, infoWindow, coordinates, nearestPin, closestDistance, closestTo;
    var markers = new Array();
    var newCoordinates = new Array();
    var sortCoord = new Array();
    var allDistances=new Array();
    var isDraggable = $(document).width() > 480 ? true : false;
    function returnZoom () {
        if($(window).width()>991){
            return 8
        }else if($(window).width()>550){
            return 7
        }else if($(window).width()>319){
            return 6
        }
    }
    function initMap() {
        map = new google.maps.Map(document.getElementById('map_canvas_contacts'), {
            center: {lat: 42.7, lng: 25.55},
            zoom: returnZoom(),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            draggable: isDraggable,
            scrollwheel: false,
            navigationControl: false,
            mapTypeControl: false,
            scaleControl: false
        });
        coordinates = @php echo json_encode($mapData); @endphp;


        // check type of marker
        function returnMarker (obj) {
            if(obj=='shop') {
                return '/img/google-pin-shop.png'
            }else if(obj=='service') {
                return '/img/google-pin-service.png'
            }else if(obj=='diller') {
                return '/img/google-pin-diller.png'
            }
        }


        function populateAllMarkers(type){
            deleteMarkers()
            if (type) {
                for (j=0; j <type.length ; j++) {
                    for (i = 0; i < coordinates.length; i++) {
                        var content ="<h5 class='text-center'>"+ coordinates[i].name + "</h5><br><p class='text-center'>"+ coordinates[i].adress+ "</p>"
                        if (coordinates[i].type==type[j]) {
                            console.log(coordinates[i].type);
                            newCoordinates.push(new Array(new google.maps.LatLng(coordinates[i].gps_lat, coordinates[i].gps_lon), coordinates[i].type, content));
                            addMarker(new google.maps.LatLng(coordinates[i].gps_lat, coordinates[i].gps_lon), coordinates[i].type, content)
                        }
                     }
                }
            }else{
                for (i = 0; i < coordinates.length; i++) {
                    var content ="<h5 class='text-center'>"+ coordinates[i].name + "</h5><br><p class='text-center'>"+ coordinates[i].adress+ "</p>"
                    newCoordinates.push(new Array(new google.maps.LatLng(coordinates[i].gps_lat, coordinates[i].gps_lon), coordinates[i].type, content));
                    addMarker(new google.maps.LatLng(coordinates[i].gps_lat, coordinates[i].gps_lon), coordinates[i].type, content)
                 }
            }

        }
        populateAllMarkers();

         function addMarker(location, pin, content) {
            marker = new google.maps.Marker({
                position: location,
                map: map,
                icon:returnMarker(pin)
            });
            var infowindow = new google.maps.InfoWindow();
            google.maps.event.addListener(marker, 'click', (function(marker) {
               return function() {

                   infowindow.setContent(content);
                   infowindow.open(map, marker);
               }
           })(marker));

            markers.push(marker);
         }

          function setMapOnAll(map) {
              for (var i = 0; i < markers.length; i++) {
                  markers[i].setMap(map);


              }
          }
          function clearMarkers() {
              setMapOnAll(null);
          }
          function deleteMarkers() {
              clearMarkers();
              markers = [];
          }


        // geolocation.
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude)
            }, function() {
                handleLocationError(true, infoWindow, map.getCenter());
            });
        } else {
            handleLocationError(false, infoWindow, map.getCenter());
        }



        // compare funct
        function compare(location, type){
            sortCoord = [];
            closestDistance=[];
            nearestPin =[];
            allDistances=[];
            console.log(location);
            if(type!=undefined){
                console.log(newCoordinates);
                if (type.length>1 ) {
                    for (j=0; j < type.length; j++) {
                        for (i=0; i <newCoordinates.length ; i++) {
                            if (type==undefined || newCoordinates[i][1]==type[j]) {

                                cpos = google.maps.geometry.spherical.computeDistanceBetween(newCoordinates[i][0], location)
                                sortCoord.push([cpos, newCoordinates[i]])
                            }
                        }
                    }
                }else {
                    for (i=0; i <newCoordinates.length ; i++) {
                        if (newCoordinates[i][1]==type) {
                            cpos = google.maps.geometry.spherical.computeDistanceBetween(newCoordinates[i][0], location)
                            sortCoord.push([cpos, newCoordinates[i]])
                        }
                    }
                }
            }else{
                for (i=0; i <newCoordinates.length ; i++) {
                    cpos = google.maps.geometry.spherical.computeDistanceBetween(newCoordinates[i][0], location)
                    sortCoord.push([cpos, newCoordinates[i]])
                }
            }


            // find nearest distance
            sortCoord.filter(function(arr){
               allDistances.push(arr[0])
            });
            closestDistance = allDistances.sort(function(a, b) {
              return a - b;
            });
            nearestPin = sortCoord.filter(function (obj){
                return obj[0]==closestDistance[0]
            });
            addMarker(nearestPin[0][1][0], nearestPin[0][1][1], nearestPin[0][1][2])
        }


        $('#find-nearest').on('click', function(){
            deleteMarkers()
            compare(pos)
        })


        // enable autocoplete
        var searchInput = document.getElementById('autocomplete');
        autocomplete = new google.maps.places.Autocomplete((searchInput),{types: ['geocode']});



        function GetLatlong(type){
            if($('#autocomplete').val().length>0){

                deleteMarkers()
                var geocoder = new google.maps.Geocoder();
                var address = document.getElementById('autocomplete').value;

                geocoder.geocode({ 'address': address }, function (results, status) {

                    if (status == google.maps.GeocoderStatus.OK) {
                        // return results[0].geometry.location
                        closestTo = results[0].geometry.location;
                        compare(closestTo, type)
                    }
                });
            }else{

                populateAllMarkers(type);
            }
        }

        var selected=[];
        $('#map-selector').on('changed.bs.select', function (e) {
             selected = $(e.target).val();

            // if (selected!=null && selected.length>2) {
            //     $('.selectpicker').selectpicker('deselectAll');
            //     selected=[];
            // }

            if (selected==null) {
                $('.selectpicker').selectpicker('deselectAll');
                selected=[];
            }

            GetLatlong(selected);
            filterShowResultsByType(selected);
        })

        function filterShowResultsByType(selected) {

            if (selected instanceof Array) {
                
                $('.full-row-filtered').hide();

                $.each(selected, function( index, value ) {
                    $('#full-'+value).show();
                });

            } else {

                $('.full-row-filtered').show();

            }
        }


        $('#search').on('click', function(){

            if (selected!=undefined) {
                if (selected.length==0) {
                    selected=undefined
                }
            }

            GetLatlong(selected);
            filterShowResultsByType(selected);

            // console.log(type[0]);
            // console.log(type);
            // GetLatlong(type)
            // console.log($('.selectpicker').val());

        })
    }

    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
              'Error: The Geolocation service failed.' :
              'Error: Your browser doesn\'t support geolocation.');
          infoWindow.open(map);
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDxte_5hpwHR7TRg_9KNmVOgiSXGE3gwSY&callback=initMap&libraries=geometry,places" type="text/javascript"></script>


@append
