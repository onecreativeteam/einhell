@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
                <h3 class="page-title"><span></span>Данни за поръчка</h3>
        </div>
    </div>
    <div class="credential-page">
        <div class="bg-border">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                </div>
            @endif
            @if(Session::has('flash_success'))
                <div class="alert alert-success"><em> {!! session('flash_success') !!}</em></div>
            @endif
            {!! Form::open( array('action' => array('Home\CartController@postOrderInfo'), 'method' => 'POST', 'class' => '' ) ) !!}
                <div class="row">
                    <div class="col-md-4 col-md-offset-2">
                        <div class="form-group">
                            <label for="#first_name"><span class="required">* </span>Име</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="formicon formicon-user"></i></span>
                                {!! Form::text('first_name', @$orderInfo['first_name'] ? : '', array('pattern'=>"^[a-zA-Z\u0400-\u04ff\s]{2,}$", 'title'=>"Полето трябва да съдържа минимум 2 символа. Позволени са само букви.", 'required', 'class'=>'form-control','id'=>'first_name', 'placeholder'=>'Име')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="#last_name"><span class="required">* </span>Фамилия</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="formicon formicon-user"></i></span>
                                {!! Form::text('last_name', @$orderInfo['last_name'] ? : '', array('pattern'=>"^[a-zA-Z\u0400-\u04ff\s]{2,}$", 'title'=>"Полето трябва да съдържа минимум 2 символа. Позволени са само букви.", 'required', 'class'=>'form-control','id'=>'last_name' , 'placeholder'=>'Фамилия')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email"><span class="required">* </span>Email</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="formicon formicon-user"></i></span>
                                {!! Form::email('email', @$orderInfo['email'] ? : '', array('required', 'type'=>'email', 'class'=>'form-control','id'=>'email' , 'placeholder'=>'Еmail')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="#phone"><span class="required">* </span>Телефон</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="formicon formicon-moblie"></i></span>
                                {!! Form::text('phone', @$orderInfo['phone'] ? : '', array('required', 'class'=>'form-control', 'pattern'=>'^[0-9\-\+\s\(\)]{10,15}$', 'title'=>'Полето трябва да съдържа между 10 и 15 символа. Позволени са +-() и интервал.', 'id'=>'phone', 'placeholder'=>'+359')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="#adress"><span class="required">* </span>Адрес за доставка</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="formicon formicon-adress"></i></span>
                                <!-- 'pattern'=>"^[0-9\-\+\a-zA-Z\u0400-\u04ff\s]{3,}$", -->
                                {!! Form::text('adress', @$orderInfo['adress'] ? : '', array('title'=>"Полето трябва да съдържа минимум 3 символа.", 'required', 'class'=>'form-control','id'=>'adress',  'placeholder'=>'Град, квартал, улица №, бл., вх., ет., ап.')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="#postcode"><span class="required">* </span>Пощенски код:</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="formicon formicon-adress"></i></span>
                                {!! Form::text('postcode', @$orderInfo['postcode'] ? : '', array('required', 'class'=>'form-control', 'pattern'=>'^[a-zA-Z\u0400-\u04ff\0-9\-\+\s\(\)]{3,15}$', 'title'=>'Полето трябва да съдържа минимум 3 символа.' ,'required', 'class'=>'form-control','id'=>'postcode',  'placeholder'=>'Пощенски код:')) !!}
                            </div>
                        </div>
                        <div class="form-group custom-select">
                            <label><span class="required">* </span>Метод на плащане:</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="formicon formicon-payment"></i></span>
                                <!-- {!! Form::select('payment_method', array('null'=>'Метод на плащане', 'cash'=>'Наложен платеж', 'vpos'=>'Vpos Плащане'), @$orderInfo['payment_method'], ['class' => 'selectpicker']) !!} -->
                                {!! Form::select('payment_method', array('cash'=>'Наложен платеж'), @$orderInfo['payment_method'], ['class' => 'selectpicker']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="comments">Коментар:</label>
                            {!! Form::textarea('order_comment', @$orderInfo['order_comment'] ? : '', array('class'=>'form-control','id'=>'comments','rows'=>'5', 'placeholder'=>'Коментар')) !!}
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="radio">
                                        <input id="person" checked {{ ( @$orderInfo['is_company'] == '0' || Request::old('is_company') == '0' ) ? 'checked' : '' }} value="0" type="radio" name="is_company">
                                        <label for="person"> Физическо лице </label>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="radio">
                                        <input id="legal" {{ ( @$orderInfo['is_company'] == '1' || \Request::old('is_company') == '1' ) ? 'checked' : '' }} value="1" type="radio" name="is_company">
                                        <label for="legal"> Юридическо лице </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <span class="required">* </span>  Задължителни полета
                    </div>
                </div>
                <div class="company-invoice">
                    <div class="row">
                        <div class="col-md-4 col-md-offset-2">
                            <div class="form-group">
                                <label for="#company"><span class="required">** </span>Име на фирма</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="formicon formicon-company"></i></span>
                                    {!! Form::text('company_name', @$orderInfo['company_name'] ? : '', array('pattern'=>"^[a-zA-Z\u0400-\u04ff\0-9\-\+\s\(\)]{3,}$", 'title'=>"Полето трябва да съдържа минимум 3 символа.", 'class'=>'form-control', 'id'=>'company', 'placeholder'=>'Име на фирма')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="#mol"><span class="required">** </span>МОЛ</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="formicon formicon-user"></i></span>
                                    {!! Form::text('company_mol', @$orderInfo['company_mol'] ? : '', array('pattern'=>"^[a-zA-Z\u0400-\u04ff\s]{2,}$", 'title'=>"Полето трябва да съдържа минимум 2 символа. Позволени са само букви.", 'class'=>'form-control', 'id'=>'mol', 'placeholder'=>'МОЛ')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="#bustat"><span class="required">** </span>ЕИК /Булстат/</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="formicon formicon-bustat"></i></span>
                                    {!! Form::text('company_bulstat', @$orderInfo['company_bulstat'] ? : '', array('class'=>'form-control', 'id'=>'bustat', 'placeholder'=>'Булстат')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="#company-adres"><span class="required">** </span>Адрес на фирма:</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="formicon formicon-adress"></i></span>
                                    <!-- 'pattern'=>"^[a-zA-Z\u0400-\u04ff\0-9\-\+\s]{3,}$", -->
                                    {!! Form::text('company_adress', @$orderInfo['company_adress'] ? : '', array('title'=>"Полето трябва да съдържа минимум 3 символа.", 'class'=>'form-control', 'id'=>'company-adres', 'placeholder'=>'Град, квартал, улица №, бл., вх., ет., ап.')) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-status">
                        <div class="col-md-4 col-md-offset-2">
                            <span class="required">** </span>  Попълва се само за юридически лица
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="checkbox">
                                    <input id="zdds" {{ (isset($orderInfo['add_dds']) && $orderInfo['add_dds'] == '1') ? 'checked' : '' }} type="checkbox" value="1" name="add_dds">
                                    <label for="zdds"> По ЗДДС </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <br>
                <div class="row">
                    <div class="col-md-12 text-center">
                        {!! Form::submit('Потвърди', array('class'=>'submit')) !!}
                    </div>
                </div>

                {!! Form::close() !!}
        </div>
    </div>
    <ul class="breadcrumb-wrapper clearfix">
        <li><a href="{{ route('home') }}"><span class="home-breadcrumb"></span></a></li>
        <li><a href="{{ route('cart.index') }}">Моята количка</a></li>
        <li>Данни за поръчка</li>
    </ul>
</div>
@endsection
@section('page-scripts')
<script type="text/javascript">
$(document).ready(function(){
    var atLeastOneIsChecked = $('input[name="is_company"][value="1"]:checked').length > 0;
    if (atLeastOneIsChecked === true) {
        $('.company-invoice').show();
    }
});
</script>
    <script type="text/javascript" src="{!! asset('js/shipping.js') !!}" charset="utf-8"></script>
@endsection
