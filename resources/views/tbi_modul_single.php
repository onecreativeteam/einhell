            /* Начало на PHP кода за Кредитен Калкулатор TBI Bank */
            $tbi_mod_version = '2.1.10';
            $product_id = '100'; //задайте променливата на PHP, която определя продуктовия id 
            $product_price = '1000'; //задайте променливата на PHP, която определя продуктовата цена
            $product_quantity = '1'; //задайте променливата на PHP, която определя продуктовата бройка
            $product_name = 'Продукт 1'; //задайте променливата на PHP, която определя продуктовото име
            $prod_categories = null; //тази променлива е масив от категориите в които влиза Вашия продукт (незадължителна)
            $manufacturer_id = null; //тази променлива е идентификатор номерът на производителя на Вашия продукт (незадължителна)
            ///////////////////////////////////////////////////////////////////////////////////
$unicid = '99ec799c-2434-4279-b8c0-1f7e20677f74';            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL, 'https://tbibank.support/function/getparameters.php?cid='.$unicid);
            $paramstbi = json_decode(curl_exec($ch), true);
            curl_close($ch);
            
            function isProductInCategories($categories_id=array(), $prod_categories_id=array()) {
                if ($categories_id[0] != ""){
                    if ($prod_categories_id[0] != 0){
                        foreach ($prod_categories_id as $prod_category_id){
                            if (in_array($prod_category_id,$categories_id)){
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
            
            $minprice_tbi = $paramstbi['tbi_minstojnost'];
            $tbi_theme = $paramstbi['tbi_theme'];
            $tbi_zastrahovka_select = $paramstbi['tbi_zastrahovka_select'];
            $tbi_btn_color = '#e55a00;';
            if ($paramstbi['tbi_btn_theme'] == 'tbi'){
                $tbi_btn_color = '#e55a00;';
            }
            if ($paramstbi['tbi_btn_theme'] == 'tbi2'){
                $tbi_btn_color = '#00368a;';
            }
            if ($paramstbi['tbi_btn_theme'] == 'tbi3'){
                $tbi_btn_color = '#2b7953;';
            }
            if ($paramstbi['tbi_btn_theme'] == 'tbi4'){
                $tbi_btn_color = '#848789;';
            }
            $tbi_btn_theme = $paramstbi['tbi_btn_theme'];
            $tbi_btn_color = $tbi_btn_color;
            $tbi_custom_button_status = $paramstbi['tbi_custom_button_status'];
            $tbi_button_position = $paramstbi['tbi_button_position'];
            
        $tpurcent = 12;
        if ($paramstbi['tbi_purcent_default'] == 1){
            $tpurcent = 3;
        }
        if ($paramstbi['tbi_purcent_default'] == 2){
            $tpurcent = 4;
        }
        if ($paramstbi['tbi_purcent_default'] == 3){
            $tpurcent = 6;
        }
        if ($paramstbi['tbi_purcent_default'] == 4){
            $tpurcent = 9;
        }
        if ($paramstbi['tbi_purcent_default'] == 5){
            $tpurcent = 12;
        }
        if ($paramstbi['tbi_purcent_default'] == 6){
            $tpurcent = 15;
        }
        if ($paramstbi['tbi_purcent_default'] == 7){
            $tpurcent = 18;
        }
        if ($paramstbi['tbi_purcent_default'] == 8){
            $tpurcent = 24;
        }
        if ($paramstbi['tbi_purcent_default'] == 9){
            $tpurcent = 30;
        }
        if ($paramstbi['tbi_purcent_default'] == 10){
            $tpurcent = 36;
        }
        // схема 10+1
        if ($paramstbi['tbi_purcent_default'] == 11){
            $tpurcent = 10;
        }
        // схема 10+1
        // схема 8-1
        if ($paramstbi['tbi_purcent_default'] == 12){
            $tpurcent = 7;
        }
        // схема 8-1
        // схема 13-2
        if ($paramstbi['tbi_purcent_default'] == 13){
            $tpurcent = 11;
        }
        // схема 13-2
        // схема 12-5
        if ($paramstbi['tbi_purcent_default'] == 14){
            $tpurcent = 14;
        }
        // схема 12-5
        // схема 10+1
        if (($paramstbi['tbi_purcent_default'] == 11) || ((($paramstbi['tbi_purcent_default'] == 2) && (($paramstbi['tbi_4m'] == "Yes") || ($paramstbi['tbi_4m_pv'] == "Yes"))) || (($paramstbi['tbi_purcent_default'] == 3) && (($paramstbi['tbi_6m'] == "Yes") || ($paramstbi['tbi_6m_pv'] == "Yes"))) )){
            $oskapiavane_12 = 0.015;        
            $vnoska = $product_price / $tpurcent;
        }else{
            if ($paramstbi['tbi_purcent_default'] == 14){
                $oskapiavane_12 = 0.015;        
                $vnoska = (($product_price - ($product_price * 0.05)) * (1 + $oskapiavane_12 * 12)) / 12;                   
            }else{
                $meseci = "tbi_" . $tpurcent . "m_purcent";
                $oskapiavane_12 = 0.015;        
                if ($paramstbi["$meseci"]){         
                    if (is_numeric($paramstbi["$meseci"])){             
                        $oskapiavane_12 = $paramstbi["$meseci"] / 100;          
                    }
                }
                $vnoska = ($product_price * (1 + $oskapiavane_12 * $tpurcent)) / $tpurcent;
            }
        }
        // схема 10+1
            
            if ($paramstbi['tbi_4m'] == "Yes"){
                if (is_numeric($product_id)){
                    $categories = explode('_', $paramstbi['tbi_4m_categories']);
                    if (isProductInCategories($categories, $prod_categories)){
                        $is4m = 'Yes';
                        }else{
                        $manufacturers = explode('_', $paramstbi['tbi_4m_manufacturers']);
                        if (($manufacturer_id != null) && in_array($manufacturer_id, $manufacturers)){
                            $is4m = 'Yes';
                            }else{
                            if ((doubleval($paramstbi['tbi_4m_min']) <= $product_price) && ($product_price <= doubleval($paramstbi['tbi_4m_max']))){
                                $is4m = 'Yes';
                                }else{
                                $is4m = 'No';
                            }
                        }
                    }
                    }else{
                    $is4m = 'No';
                }       
                }else{
                $is4m = 'No';
            }
            
            if ($paramstbi['tbi_4m_pv'] == "Yes"){
                if (is_numeric($product_id)){
                    $categories = explode('_', $paramstbi['tbi_4m_categories']);
                    if (isProductInCategories($categories, $prod_categories)){
                        $is4m_pv = 'Yes';
                        }else{
                        $manufacturers = explode('_', $paramstbi['tbi_4m_manufacturers']);
                        if (($manufacturer_id != null) && in_array($manufacturer_id, $manufacturers)){
                            $is4m_pv = 'Yes';
                            }else{
                            if ((doubleval($paramstbi['tbi_4m_min']) <= $product_price) && ($product_price <= doubleval($paramstbi['tbi_4m_max']))){
                                $is4m_pv = 'Yes';
                                }else{
                                $is4m_pv = 'No';
                            }
                        }
                    }
                    }else{
                    $is4m_pv = 'No';
                }       
                }else{
                $is4m_pv = 'No';
            }
            
            if ($paramstbi['tbi_6m'] == "Yes"){
                if (is_numeric($product_id)){
                    $categories = explode('_', $paramstbi['tbi_6m_categories']);
                    if (isProductInCategories($categories, $prod_categories)){
                        $is6m = 'Yes';
                        }else{
                        $manufacturers = explode('_', $paramstbi['tbi_6m_manufacturers']);
                        if (($manufacturer_id != null) && in_array($manufacturer_id, $manufacturers)){
                            $is6m = 'Yes';
                            }else{
                            if ((doubleval($paramstbi['tbi_6m_min']) <= $product_price) && ($product_price <= doubleval($paramstbi['tbi_6m_max']))){
                                $is6m = 'Yes';
                                }else{
                                $is6m = 'No';
                            }
                        }
                    }
                    }else{
                    $is6m = 'No';
                }       
                }else{
                $is6m = 'No';
            }
            
            if ($paramstbi['tbi_6m_pv'] == "Yes"){
                if (is_numeric($product_id)){
                    $categories = explode('_', $paramstbi['tbi_6m_categories']);
                    if (isProductInCategories($categories, $prod_categories)){
                        $is6m_pv = 'Yes';
                        }else{
                        $manufacturers = explode('_', $paramstbi['tbi_6m_manufacturers']);
                        if (($manufacturer_id != null) && in_array($manufacturer_id, $manufacturers)){
                            $is6m_pv = 'Yes';
                            }else{
                            if ((doubleval($paramstbi['tbi_6m_min']) <= $product_price) && ($product_price <= doubleval($paramstbi['tbi_6m_max']))){
                                $is6m_pv = 'Yes';
                                }else{
                                $is6m_pv = 'No';
                            }
                        }
                    }
                    }else{
                    $is6m_pv = 'No';
                }       
                }else{
                $is6m_pv = 'No';
            }

        // shema 101
        if ($paramstbi['tbi_pokazi']  & 1024){
            $is101 = 'Yes';
            }else{
            $is101 = 'No';
        }
        // shema 101
        // shema 8-1
        if ($paramstbi['tbi_pokazi']  & 2048){
            $is81 = 'Yes';
            }else{
            $is81 = 'No';
        }
        // shema 8-1
        // shema 13-2
        if ($paramstbi['tbi_pokazi']  & 4096){
            $is112 = 'Yes';
            }else{
            $is112 = 'No';
        }
        // shema 13-2
        // shema 12-5
        if ($paramstbi['tbi_pokazi']  & 8192){
            $is13 = 'Yes';
            }else{
            $is13 = 'No';
        }
        // shema 12-5

            if ($paramstbi['tbi_taksa_categories'] != ""){
                $cats = explode('_', $paramstbi['tbi_taksa_categories']);
                if (isProductInCategories($cats, $prod_categories)){
                    $isTaksa = 'Yes';
                }else{
                    $isTaksa = 'No';
                }       
            }else{
                $isTaksa = 'Yes';
            }
                
            if (($paramstbi['tbi_status'] == 'Yes') && ($product_price  > $minprice_tbi)){
                if($paramstbi['tbi_vnoska'] == 'Yes'){
                ?>
                <?php if(($paramstbi['tbi_zaglavie'] != '') || ($paramstbi['tbi_opisanie'] != '') || ($paramstbi['tbi_product'] != '')) { ?>
                <span style="font-size:22px;font-weight:bold;"><?php echo $paramstbi['tbi_zaglavie']; ?></span> <span style="font-size:18px;"> <?php echo $paramstbi['tbi_opisanie']; ?></span> <?php echo $paramstbi['tbi_product']; ?>
                <?php } ?>
                <table border="0">
                    <tr>
                    <?php if ($tbi_button_position == 1) { ?>
                        <td style="vertical-align:middle;padding-right:5px;padding-bottom:5px;">
                            <p style="color:<?php echo $tbi_btn_color; ?>font-size:11pt;font-weight:bold;">Само за <?php echo number_format($vnoska, 2, '.', ''); ?> лв. на месец</p>
                        </td>
                        <td style="padding-right:5px;padding-bottom:5px;">
                            <?php if ($tbi_custom_button_status == 'Yes') { ?>
                            <img id="btn_tbi" style="padding-bottom: 5px;cursor:pointer;" src="https://tbibank.support/calculators/assets/img/custom_buttons/<?php echo $unicid; ?>.png" alt="Кредитен модул TBI Bank 19.68" onmouseover="this.src='https://tbibank.support/calculators/assets/img/custom_buttons/<?php echo $unicid; ?>_hover.png'" onmouseout="this.src='https://tbibank.support/calculators/assets/img/custom_buttons/<?php echo $unicid; ?>.png'">
                            <?php }else{ ?>
                            <img id="btn_tbi" style="padding-bottom: 5px;cursor:pointer;" src="https://tbibank.support/calculators/assets/img/buttons/<?php echo $tbi_btn_theme; ?>.png" alt="Кредитен модул TBI Bank 19.68" onmouseover="this.src='https://tbibank.support/calculators/assets/img/buttons/<?php echo $tbi_btn_theme; ?>-hover.png'" onmouseout="this.src='https://tbibank.support/calculators/assets/img/buttons/<?php echo $tbi_btn_theme; ?>.png'">                          
                            <?php } ?>
                        </td>
                    <?php } ?>
                    <?php if ($tbi_button_position == 2) { ?>
                        <td style="padding-right:5px;padding-bottom:5px;">
                            <?php if ($tbi_custom_button_status == 'Yes') { ?>
                            <img id="btn_tbi" style="padding-bottom: 5px;cursor:pointer;" src="https://tbibank.support/calculators/assets/img/custom_buttons/<?php echo $unicid; ?>.png" alt="Кредитен модул TBI Bank 19.68" onmouseover="this.src='https://tbibank.support/calculators/assets/img/custom_buttons/<?php echo $unicid; ?>_hover.png'" onmouseout="this.src='https://tbibank.support/calculators/assets/img/custom_buttons/<?php echo $unicid; ?>.png'">
                            <?php }else{ ?>
                            <img id="btn_tbi" style="padding-bottom: 5px;cursor:pointer;" src="https://tbibank.support/calculators/assets/img/buttons/<?php echo $tbi_btn_theme; ?>.png" alt="Кредитен модул TBI Bank 19.68" onmouseover="this.src='https://tbibank.support/calculators/assets/img/buttons/<?php echo $tbi_btn_theme; ?>-hover.png'" onmouseout="this.src='https://tbibank.support/calculators/assets/img/buttons/<?php echo $tbi_btn_theme; ?>.png'">                          
                            <?php } ?>
                        </td>
                        <td style="vertical-align:middle;padding-right:5px;padding-bottom:5px;">
                            <p style="color:<?php echo $tbi_btn_color; ?>font-size:11pt;font-weight:bold;">Само за <?php echo number_format($vnoska, 2, '.', ''); ?> лв. на месец</p>
                        </td>
                    <?php } ?>
                    <?php if ($tbi_button_position == 3) { ?>
                        <td style="padding-right:5px;padding-bottom:5px;">
                            <?php if ($tbi_custom_button_status == 'Yes') { ?>
                            <img id="btn_tbi" style="padding-bottom: 5px;cursor:pointer;" src="https://tbibank.support/calculators/assets/img/custom_buttons/<?php echo $unicid; ?>.png" alt="Кредитен модул TBI Bank 19.68" onmouseover="this.src='https://tbibank.support/calculators/assets/img/custom_buttons/<?php echo $unicid; ?>_hover.png'" onmouseout="this.src='https://tbibank.support/calculators/assets/img/custom_buttons/<?php echo $unicid; ?>.png'">
                            <?php }else{ ?>
                            <img id="btn_tbi" style="padding-bottom: 5px;cursor:pointer;" src="https://tbibank.support/calculators/assets/img/buttons/<?php echo $tbi_btn_theme; ?>.png" alt="Кредитен модул TBI Bank 19.68" onmouseover="this.src='https://tbibank.support/calculators/assets/img/buttons/<?php echo $tbi_btn_theme; ?>-hover.png'" onmouseout="this.src='https://tbibank.support/calculators/assets/img/buttons/<?php echo $tbi_btn_theme; ?>.png'">                          
                            <?php } ?>
                            <p style="color:<?php echo $tbi_btn_color; ?>font-size:11pt;font-weight:bold;">Само за <?php echo number_format($vnoska, 2, '.', ''); ?> лв. на месец</p>
                        </td>
                    <?php } ?>
                    </tr>
                </table>
                <?php
                    }else{
                ?>
                <?php if(($paramstbi['tbi_zaglavie'] != '') || ($paramstbi['tbi_opisanie'] != '') || ($paramstbi['tbi_product'] != '')) { ?>
                <?php echo $paramstbi['tbi_zaglavie'] . ' ' . $paramstbi['tbi_opisanie'] . ' ' . $paramstbi['tbi_product']; ?>
                <?php } ?>
                <img id="btn_tbi" style="padding-bottom: 5px;cursor:pointer;" src="https://tbibank.support/calculators/assets/img/buttons/<?php echo $tbi_btn_theme; ?>.png" alt="Кредитен модул TBI Bank 19.68" onmouseover="this.src='https://tbibank.support/calculators/assets/img/buttons/<?php echo $tbi_btn_theme; ?>-hover.png'" onmouseout="this.src='https://tbibank.support/calculators/assets/img/buttons/<?php echo $tbi_btn_theme; ?>.png'">
                <?php
                }
            ?>
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            <link rel="stylesheet" href="https://tbibank.support/calculators/assets/css/<?php echo $tbi_theme; ?>">
            <div id="tbi_box" class="modal">
                <div class="modal-content">
                    <div id="tbi_body" class="modal-body">
                    </div>
                </div>                        
            </div>
            <script>
                var cid = '<?php echo $unicid; ?>';
                var tbi_box = document.getElementById('tbi_box');
                var tbi_btn_open = document.getElementById("btn_tbi");
                tbi_btn_open.onclick = function() {
                    showTbiBoxHtml(cid, <?php echo $tpurcent; ?>, '<?php echo $tbi_zastrahovka_select; ?>', 0);
                    tbi_box.style.display = "block";
                }
                function tbibuy(_comment, _pogasitelni_vnoski_input, _zastrahovka_input, _parva_input, _mesecna_vnoska_input, _gpr_input, _tbi_uslovia){
                    if (typeof(_tbi_uslovia) != 'undefined' && _tbi_uslovia != null && _tbi_uslovia.checked){
                        showTbibuyHtml(cid, _comment.value, _pogasitelni_vnoski_input.value, _zastrahovka_input.value, _parva_input.value, _mesecna_vnoska_input.value, _gpr_input.value);
                    }else{
                        alert('Моля съгласете се с обработката на личните Ви данни за да преминете към попълване на данни за клиента!');
                    }
                }
                function tbisend(_name, _egn, _phone, _email, _address, _address2, _comment, _pogasitelni_vnoski_input, _zastrahovka_input, _parva_input, _mesecna_vnoska_input, _gpr_input){
                    showTbisendHtml(cid, _name, _egn, _phone, _email, _address, _address2, _comment, _pogasitelni_vnoski_input, _zastrahovka_input, _parva_input, _mesecna_vnoska_input, _gpr_input);
                }
                function tbibuy_back(){
                    showTbiBoxHtml(cid, <?php echo $tpurcent; ?>, '<?php echo $tbi_zastrahovka_select; ?>', 0);
                }
                function showTbiBoxHtml(param, _vnoski, _zastrahovka, _parva) {
                    if (param.length == 0) { 
                        document.getElementById("tbi_body").innerHTML = "";
                        return;
                        } else {
                        var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                                document.getElementById("tbi_body").innerHTML = this.responseText;
                                document.getElementById("tbi_product_name").innerHTML = '<?php echo str_replace('"', '', str_replace("'", "", $product_name)); ?>';
                            }
                        };
                        var q = parseFloat('<?php echo $product_quantity; ?>');
                        if (isNaN(q) || (q == 0) || (q > 5)){
                            q = 1;
                        }
                        var priceall = parseFloat('<?php echo $product_price; ?>') * q;
                        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                        <?php if ($tbi_theme == 'style3.css') { ?> 
                        var urlto = 'tbi_css.php';              
                        <?php }else{ ?>
                        <?php if ($tbi_theme == 'style4.css'){ ?>
                        var urlto = 'tbi_short.php';
                        <?php }else{ ?>
                        if (x <= 1024){
                            var urlto = 'tbi_m.php';
                        }else{
                            var urlto = 'tbi_tab.php';              
                        }
                        <?php } ?>
                        <?php } ?>
                        xmlhttp.open("GET", "https://tbibank.support/calculators/"+urlto+"?cid=" + param + "&is4m=<?php echo $is4m; ?>&is4m_pv=<?php echo $is4m_pv; ?>&is6m=<?php echo $is6m; ?>&is6m_pv=<?php echo $is6m_pv; ?>&is101=<?php echo $is101; ?>&is81=<?php echo $is81; ?>&is112=<?php echo $is112; ?>&is13=<?php echo $is13; ?>&isTaksa=<?php echo $isTaksa; ?>&price_input="+priceall+"&pogasitelni_vnoski_input="+_vnoski+"&zastrahovka_input="+_zastrahovka+"&parva_input="+_parva+"&tbi_mod_version=<?php echo $tbi_mod_version; ?>", true);
                        xmlhttp.send();
                    }
                }
                function showTbibuyHtml(param, _comment, _pogasitelni_vnoski_input, _zastrahovka_input, _parva_input, _mesecna_vnoska_input, _gpr_input) {
                    if (param.length == 0) { 
                        document.getElementById("tbi_body").innerHTML = "";
                        return;
                        } else {
                        document.getElementById("tbi_body").innerHTML = "";
                        var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                                document.getElementById("tbi_body").innerHTML = this.responseText;
                                document.getElementById("tbi_product_name").innerHTML = 'Необходими данни за искане на стоков кредит';
                            }
                        };
                        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                        <?php if ($tbi_theme == 'style3.css') { ?> 
                        var urlto = 'tbibuy_css.php';               
                        <?php }else{ ?>
                        <?php if ($tbi_theme == 'style4.css'){ ?>
                        var urlto = 'tbibuy_short.php';
                        <?php }else{ ?>
                        if (x <= 1024){
                            var urlto = 'tbibuy_m.php';
                        }else{
                            var urlto = 'tbibuy_tab.php';               
                        }
                        <?php } ?>
                        <?php } ?>
                        xmlhttp.open("GET", "https://tbibank.support/calculators/"+urlto+"?cid="+param+"&comment="+_comment+"&pogasitelni_vnoski_input="+_pogasitelni_vnoski_input+"&zastrahovka_input="+_zastrahovka_input+"&parva_input="+_parva_input+"&mesecna_vnoska_input="+_mesecna_vnoska_input+"&gpr_input="+_gpr_input, true);
                        xmlhttp.send();
                    }
                }
                function showTbisendHtml(param, _name, _egn, _phone, _email, _address, _address2, _comment, _pogasitelni_vnoski_input, _zastrahovka_input, _parva_input, _mesecna_vnoska_input, _gpr_input) {
                    if (param.length == 0) { 
                        document.getElementById("tbi_body").innerHTML = "";
                        return;
                        } else {
                        document.getElementById("tbi_body").innerHTML = "";
                        var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                                document.getElementById("tbi_body").innerHTML = this.responseText;
                                document.getElementById("tbi_product_name").innerHTML = 'Изпращане на заявка за стоков кредит';
                            }
                        };
                        var _pq = parseFloat(<?php echo $product_quantity; ?>);
                        if (isNaN(_pq) || (_pq == 0) || (_pq > 5)){
                            _pq = 1;
                        }
                        var price1 = parseFloat('<?php echo $product_price; ?>');
                        var priceall = price1 * _pq;
                        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                        <?php if ($tbi_theme == 'style3.css') { ?> 
                        var urlto = 'tbisend_css.php';              
                        <?php }else{ ?>
                        <?php if ($tbi_theme == 'style4.css'){ ?>
                        var urlto = 'tbisend_short.php';
                        <?php }else{ ?>
                        if (x <= 1024){
                            var urlto = 'tbisend_m.php';
                        }else{
                            var urlto = 'tbisend.php';              
                        }
                        <?php } ?>
                        <?php } ?>
                        xmlhttp.open("GET", "https://tbibank.support/calculators/"+urlto+"?cid="+param+"&name="+_name+"&egn="+_egn+"&phone="+_phone+"&email="+_email+"&address="+_address+"&address2="+_address2+"&comment="+_comment+"&product_id="+<?php echo $product_id; ?>+"&product_q="+_pq+"&products_name=<?php echo $product_name; ?>&isTaksa=<?php echo $isTaksa; ?>&price_input="+priceall+"&pogasitelni_vnoski_input="+_pogasitelni_vnoski_input+"&zastrahovka_input="+_zastrahovka_input+"&parva_input="+_parva_input+"&mesecna_vnoska_input="+_mesecna_vnoska_input+"&gpr_input="+_gpr_input, true);
                        xmlhttp.send();
                    }
                }
                function close_tbi_box(){
                    document.getElementById("tbi_body").innerHTML = "";
                    document.getElementById('tbi_box').style.display = "none";
                }
                function change_btn_tbicredit(){
                    var uslovia = document.getElementById('uslovia');
                    var buy_tbicredit = document.getElementById('buy_tbicredit');
                    if (uslovia.checked){
                        buy_tbicredit.disabled = false;
                        }else{
                        buy_tbicredit.disabled = true;
                    }
                }
                function preizcisli_tbi(select_vnoski, select_zastrahovka, input_parva){
                    showTbiBoxHtml(cid, select_vnoski.options[select_vnoski.selectedIndex].value, select_zastrahovka.options[select_zastrahovka.selectedIndex].value, input_parva.value);
                }
                function isNumberKey(evt){
                    var charCode = (evt.which) ? evt.which : event.keyCode
                    if (charCode > 31 && (charCode < 48 || charCode > 57)){
                        return false;
                    }
                    return true;
                }
                function ispogoliamo(_price, _parva){
                    if (parseInt(_price.value) < parseInt(_parva.value)){
                        _parva.value = _parva.value.slice(0,-1);
                        return false;
                    }
                    return true;
                }
                function cyrKey(_e){
                    _e.value = _e.value.replace(/[a-zA-Z]*/, "");
                }
                function elementOnFocus(_e){
                    _e.style.border="3px solid #e55a00";
                }
                function checkForm(_name, _egn, _phone, _email, _address, _address2, _comment, _pogasitelni_vnoski_input, _zastrahovka_input, _parva_input, _mesecna_vnoska_input, _gpr_input) {
                    var _test = true;
                    if(_name.value == '') {
                        _name.style.border="3px solid red";
                        _test = false;
                        }else{
                        var re = /^[\w ]+$/;
                        if(!re.test(_egn.value)) {
                            _egn.style.border="3px solid red";
                            _test = false;
                            }else{
                            if(_phone.value == '') {
                                _phone.style.border="3px solid red";
                                _test = false;
                                }else{
                                if(_email.value == '') {
                                    _email.style.border="3px solid red";
                                    _test = false;
                                    }else{
                                    if(_address.value == '') {
                                        _address.style.border="3px solid red";
                                        _test = false;
                                        }else{
                                        if(_address2.value == '') {
                                            _address2.style.border="3px solid red";
                                            _test = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (_test){
                        tbisend(_name.value, _egn.value, _phone.value, _email.value, _address.value, _address2.value, _comment.value, _pogasitelni_vnoski_input, _zastrahovka_input, _parva_input, _mesecna_vnoska_input, _gpr_input);
                    }
                }
            </script>
            <style>
*{padding:0;margin:0;}

/* fontawesom */
@font-face {
    font-family: 'FontAwesome';
    src: url('../fonts/fontawesome-webfont.eot?v=4.4.0');
    src: url('../fonts/fontawesome-webfont.eot?#iefix&v=4.4.0') format('embedded-opentype'), url('../fonts/fontawesome-webfont.woff2?v=4.4.0') format('woff2'), url('../fonts/fontawesome-webfont.woff?v=4.4.0') format('woff'), url('../fonts/fontawesome-webfont.ttf?v=4.4.0') format('truetype'), url('../fonts/fontawesome-webfont.svg?v=4.4.0#fontawesomeregular') format('svg');
    font-weight: normal;
    font-style: normal;
}
.fa {
    display: inline-block;
    font: normal normal normal 14px/1 FontAwesome;
    font-size: inherit;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}
.fa-square:before {
  content: "\f0c8";
}
.fa-play:before {
  content: "\f04b";
}
.fa-rotate-180 {
  filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=2);
  -webkit-transform: rotate(180deg);
  -ms-transform: rotate(180deg);
  transform: rotate(180deg);
}
:root .fa-rotate-180 {
  filter: none;
}
/* fontawesom */

/* tbi float */
.tbi-label-container{
    z-index:999;
    position:fixed;
    top:calc(100% / 2 - 130px);
    left:67px;
    display:table;
    visibility: hidden;
}
.tbi-label-text{
    width:410px;
    height:260px;
    color:#696969;
    background:#f5f5f5;
    display:table-cell;
    vertical-align:top;
    padding-left:5px;
    border:1px solid #f18900;
    border-radius:3px;
}
.tbi-label-text-a{
    text-align:center;
}
.tbi-label-text-a a{
    color:#b73607;
}
.tbi-label-text-a a:hover{
    color:#672207;
    text-decoration:underline;
}
.tbi-label-arrow{
    display:table-cell;
    vertical-align:middle;
    color:#f5f5f5;
    opacity:1;
}
.tbi_float{
    z-index:999;
    position:fixed;
    width:60px;
    height:60px;
    top:calc(100% / 2 - 30px);
    left:0px;
    background-color:#ffffff;
    border-top:1px solid #f18900;
    border-right:1px solid #f18900;
    border-bottom:1px solid #f18900;
    color:#FFF;
    border-top-right-radius:8px;
    border-bottom-right-radius:8px;
    text-align:center;
    box-shadow: 2px 2px 3px #999;
    cursor:pointer;
}
.tbi-my-float{
    margin-top:12px;
}   
</style>
<?php
$ch = curl_init();
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_URL, 'https://tbibank.support/function/getparameters.php?cid='.$unicid);
$paramstbi=json_decode(curl_exec($ch), true);
curl_close($ch);
?>
<?php if ($paramstbi['tbi_container_status'] == 'Yes'){ ?>
<div class="tbi_float" onclick="tbiChangeContainer();">
    <img src="https://tbibank.support/dist/img/tbi_logo.png" class="tbi-my-float">
</div>
<div class="tbi-label-container">
    <i class="fa fa-play fa-rotate-180 tbi-label-arrow"></i>
    <div class="tbi-label-text">
        <div style="padding-bottom:5px;"></div>
        <img src="https://tbibank.support/calculators/assets/img/tbim<?php echo $paramstbi['tbi_container_reklama']; ?>.png">
        <div style="font-size:16px;padding-top:3px;"><?php echo $paramstbi['tbi_container_txt1']; ?></div>
        <p style="font-size:14px;"><?php echo $paramstbi['tbi_container_txt2']; ?></p>
        <div class="tbi-label-text-a"><a href="https://tbibank.support/calculators/assets/img/Procedura%20Online%20TBI%20Bank-2017.pdf" target="_blank" alt="ИНФОРМАЦИЯ ЗА ОНЛАЙН ПАЗАРУВАНЕ НА КРЕДИТ С TBI BANK">ИНФОРМАЦИЯ ЗА ОНЛАЙН ПАЗАРУВАНЕ НА КРЕДИТ!</a></div>
    </div>
</div>
<script type="application/javascript">
    function tbiChangeContainer(){
        var tbi_label_container = document.getElementsByClassName("tbi-label-container")[0];
        if (tbi_label_container.style.visibility == 'visible'){
            tbi_label_container.style.visibility = 'hidden';
            tbi_label_container.style.opacity = 0;
            tbi_label_container.style.transition = 'visibility 0s, opacity 0.5s ease';              
        }else{
            tbi_label_container.style.visibility = 'visible';
            tbi_label_container.style.opacity = 1;          
        }
    }
</script>
<?php } ?>
<?php
}
/* Край на PHP кода за Кредитен Калкулатор TBI Bank */