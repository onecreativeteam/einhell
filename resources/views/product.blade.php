@extends('layouts.app')

@section('content')
<div class="container">
    <div class="clearfix">
        <div class="col-md-3 sidebar-eq-product">
            <div class="sidebar-sticky">
                @include(
                    'partials.left-sidebar',
                    [
                        'maxPrice' => $maxPrice,
                        'categories' => $baseCategories,
                        'series' => $series,
                        'category' => $category,
                        'baseCategory' => $baseCategory
                    ]
                )
            </div> <!--  .sidebar-sticky -->
       </div>
       <div class="col-md-9">
           <h3 class="page-title-stroke">всички продукти<span></span></h3>
           <div class="products-grid-wrapper">
               <div class="row">
                  @if ($products)
                    @foreach ($products as $product)
                     <div class="col-md-4">
                         <a class="single-product-grid" href="{{ route('product.inner', [$product->slug]) }}">
                             <div class="text-center"> 
                                  @if ($product->promo_product == '1')
                                    <div class="promo-label">Промо</div>
                                  @endif
                                  <div class="product-pic" style="background-image: url(/{{ ( !is_null($product->picture) || $product->picture != '' ) ? $product->picture : ( count($product->pictures) ? $product->pictures[0]->resource : 'img/no-image.png' ) }})"></div>
                                  <h3> {{ (strlen($product->name) > 30) ? mb_substr($product->name,0,31).'..' : $product->name }} </h3>
                                  <h5>Арт.№: {{@$product->product_number}} </h5>
                                  <div class="price-wrapper">
                                      <p>цена:
                                        <strong>
                                          {{ ($product->promo_product == '1') ? @$product->promo_price : @$product->price }} лв.
                                        </strong>
                                      </p>
                                      <p class="more">виж повече</p>
                                  </div>
                              </div>
                          </a>
                     </div>
                     @endforeach
                   @endif
               </div>
           </div>
           <div class="row">
            {{ $products->links('partials.product-paginate') }}
           </div>
       </div>
    </div>
   <div class="clearfix">
       <div class="col-md-12">

       </div>
       <ul class="breadcrumb-wrapper clearfix">
           <li><a href="{{ route('home') }}"><span class="home-breadcrumb"></span></a></li>
           @if (Route::current()->getName() == 'product.index')
           <li>Продукти</li>
           @elseif (Route::current()->getName() == 'product.base.category')
            <li><a href="{{ route('product.index') }}"> Продукти </a></li>
            <li> {{ $baseCategory->name }} </li>
           @elseif (Route::current()->getName() == 'product.category')
            <li><a href="{{ route('product.index') }}"> Продукти </a></li>
            <li><a href="{{ route('product.base.category', [$baseCategory->slug]) }}"> {{ $baseCategory->name }} </a></li>
            <li>{{ $category->name }}</li>
           @elseif (Route::current()->getName() == 'product.xchange.category')
            <li><a href="{{ route('xchange.index') }}">POWER X-CHANGE ПРИЛОЖЕНИЕ</a></li>
            <li>{{ $category->name }}</li>
           @endif
        </ul>
   </div> 
</div>
@endsection
@section('page-scripts')

@append 
