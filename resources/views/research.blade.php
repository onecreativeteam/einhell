@extends('layouts.app')

@section('content')
<div class="container tabs-page">
    <div class="col-md-12">
        <img src="/{{$coverPhoto->file}}" class="main-image img-responsive" alt="">
    </div>
    <div class="clearfix">
        <div class="col-md-3">
            <div class="sidebar-wrapper">
                <h3 class="sidebar-title">Научни изследвания</h3>
                <ul class="sidebar-menu">
                    @foreach ($research as $k => $res)
                    <li>
                        <p data-toggle="tab" href="#tab-{{$k}}">{{ $res->name }}</p>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="col-md-9">
            <div class="tab-content bg-border">
                @foreach ($research as $k => $res)
                <div id="tab-{{$k}}" class="tab-pane fade {{ $k == 0 ? 'in active' : '' }}">
                    <!-- <h3 class="title">{{ $res->name }}</h3> -->
                    <div class="text-content">
                        {!! $res->description !!}
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="col-md-12"> 
        <ul class="breadcrumb-wrapper clearfix">
            <li><a href="{{ route('home') }}"><span class="home-breadcrumb"></span></a></li>
            <li class="initial">Научни изследвания</li>
        </ul>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
$(document).ready( function() {
    $('ul.sidebar-menu li').click( function() {
        var text = $(this).find('p').text();
        $('ul.breadcrumb-wrapper li.added').remove();
        $('ul.breadcrumb-wrapper li.initial').css('margin-right', '2px');
        $('ul.breadcrumb-wrapper').append('<li class="added"> - '+text+'</li>');
    });
});
</script>
@endpush
@section('page-scripts')

@endsection
