<p>Здравейте Администратор, <br>
Беше направена поръчка в електронния магазин на Einhell България.</p>

<p>Номерът на поръчката е №<strong>{{@$order->id}}</strong>.</p>

Артулите, които поръчаха са:

@if ($order->orderProducts)
	@foreach ($order->orderProducts as $orderProduct)
		<p><strong>{{ $orderProduct->product_quantity }} x {{ @$orderProduct->product->name }}({{ $orderProduct->p_single_price }}лв.) - цена {{ $orderProduct->p_total_price }}лв.</strong></p>
	@endforeach
@endif

Методът на плащане, който избраха е <strong>{{ @$order->payment_method == 'cash' ? 'Наложен платеж' : 'Vpos плащане' }}</strong>.<br>

С уважение,<br>
Екипът на <strong>Еinhell</strong>