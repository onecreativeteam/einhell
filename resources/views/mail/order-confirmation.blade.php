<p>Здравейте, <br>
Вие направихте поръчка в електронния магазин на Einhell България.</p>

<p>Номерът на вашата поръчка е №<strong>{{@$order->id}}</strong>.</p>

Артулите, които поръчахте са:

@if ($order->orderProducts)
	@foreach ($order->orderProducts as $orderProduct)
		<p><strong>{{ $orderProduct->product_quantity }} x {{ @$orderProduct->product->name }}({{ $orderProduct->p_single_price }}лв.) - цена {{ $orderProduct->p_total_price }}лв.</strong></p>
	@endforeach
@endif

Методът на плащане, който избрахте е <strong>{{ @$order->payment_method == 'cash' ? 'Наложен платеж' : 'Vpos плащане' }}</strong>.<br>

Наш служител ще се свърже с Вас на посочения телефонен номер, за да Ви уведоми относно датата на изпълнение на Вашата поръчка.<br>

С уважение,<br>
Екипът на <strong>Еinhell</strong>