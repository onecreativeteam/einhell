
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Einhell Admin Panel</title>
    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    <!-- Styles -->

    <link rel="stylesheet" href="{!! asset('css/bootstrap.min.css') !!}">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    
    <link rel="stylesheet" href="{!! asset('css/AdminLTE.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/_all-skins.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/jquery-gmaps-latlon-picker.css') !!}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet"  href="{!! asset('css/admin-custom.css') !!}">

    <link href="{!! asset('css/jqv/jqvmap.css') !!}" media="screen" rel="stylesheet" type="text/css">

    <script type="text/javascript">
         public_path = "{{ URL::to('/') }}";
    </script>
    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script src="{!! asset('js/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('js/app.min.js') !!}"></script>
    <script src="{!! asset('js/jquery-lat-lon-picker.js') !!}"></script>

    <script type="text/javascript" src="{!! asset('js/jqv/jquery.vmap.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/jqv/maps/jquery.vmap.world.js') !!}" charset="utf-8"></script>

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">

    <script src="{!! asset('js/templateEditor/ckeditor/ckeditor.js') !!}"></script>
    <style>
        /*body {
            font-family: 'Lato';
        }*/
        .fa-btn {
            margin-right: 6px;
        }
    </style>
 <body class="skin-red sidebar-mini">
    <div class="wrapper">
        <header class="main-header">
            <a href="{!! route('home') !!}" class="logo">
                <span class="logo-mini"><b>Einhell</b></span>
                <span class="logo-lg">Einhell</span>
            </a>
            <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
            </nav>
        </header>
        @include('layouts.parts.nav_admin')
        <div class="content-wrapper">

                    @yield('content')

        </div>
        <footer class="main-footer">
            <div class="pull-right hidden-xs">  Einhell Administration Panel</div>
            <strong>Copyright &copy; {{ date('Y') }} <b>Einhell</b>.</strong> All rights reserved.
        </footer>
    </div>

    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    
<script type="text/javascript">
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
</script>
@stack('scripts')
</body>
</html>
