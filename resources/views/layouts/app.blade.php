<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="/img/favicon.ico"/>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700" rel="stylesheet">
    <!-- Styles -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{!! asset('css/bootstrap.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/swiper.min.css') !!}">

    {{-- <link href="{!! asset('css/app.css') !!}" rel="stylesheet"> --}}
    @yield('page-css')

    <link rel="stylesheet"  href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link href="{!! asset('css/jqv/jqvmap.css') !!}" media="screen" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.css" media="screen" rel="stylesheet" type="text/css">

    <link href="{!! asset('css/custom.css') !!}" rel="stylesheet">
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <script type="text/javascript">
         public_path = "{{ URL::to('/') }}";
    </script>
    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <script src="{!! asset('js/bootstrap.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/jqv/jquery.vmap.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/jqv/maps/jquery.vmap.world.js') !!}" charset="utf-8"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.js" charset="utf-8"></script>

<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
<script>
// window.addEventListener("load", function(){
// window.cookieconsent.initialise({
//   "palette": {
//     "popup": {
//       "background": "#e3370e"
//     },
//     "button": {
//       "background": "#ffffff",
//       "text": "#e3370e"
//     }
//   },
//   "content": {
//     "message": "Всички поръчки влезли след 15:00 ч. на 22.12.2017 ще бъдат обработени на 02.01.2018.  Извиняваме се за причиненото неудобство.\n",
//     "dismiss": "Ok"
//   },
//   "expiryDays": 1
// })});
</script>
</head>
<body>
@yield('facebook-share')

 @if(Route::current()->getName() == 'home' ||  Route::current()->getName() == 'xchange.index')
     <header>
@else
    <header class="header-bg">
@endif
        <div class="container">
            <div class="col-md-12 no-padding">
                <div class="menu-background">
                    <nav class="navbar navbar-default">
                        <ul class="login-wrapper clearfix">
                            @if (Auth::guest())
                                <li><a href="{{ route('login') }}">вход I регистрация</a></li>
                            @else
                                <li><a href="{{ route('logout') }}">Изход</a></li>
                                <li>
                                    <a href="{{route('profile')}}">
                                        @if (Auth::user()->first_name != '')
                                            {{ Auth::user()->first_name .' '. @Auth::user()->last_name }}
                                        @else
                                            Моят Профил
                                        @endif
                                    </a>
                                </li>
                            @endif
                            <li><a href="{{route('cart.index')}}">Моята количка</a></li>
                        </ul>

                        <div class="navbar-header">
                            <!-- Collapsed Hamburger -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-main">
                                <span class="sr-only">Toggle Navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <!-- Branding Image -->
                            <a class="navbar-brand" href="{{ route('home') }}">
                                <img src="/img/logo.png" alt="">
                            </a>


                        </div>
                        <ul class="search-form clearfix">
                            <li id="search-form-wrapper">
                                <form class="form-inline" id="user_search_form" method="POST" action="{{ route('home.search.user') }}">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><img src="/img/search-icon.png" alt=""></div>
                                            <input type="text" name="user_search_string" autocomplete="off" class="form-control" id="exampleInputAmount" placeholder="Search">
                                        </div>
                                    </div>
                                </form>
                                <div id="results-header-search"></div>
                            </li>
                            <li>
                                <div class="card clearfix">
                                    <div class="card-wrapper clearfix">
                                        <div class="icon-wrapper">
                                            <img src="/img/card-icon.png" alt="">
                                        </div>
                                        <div class="total">
                                            {{ \Cart::count() }} продуктa - <span class="sum">{{ \Cart::subtotal() }} лв.</span>
                                        </div>
                                        <div class="icon-wrapper white">
                                            <img src="/img/card-icon-arrow.png" alt="">
                                        </div>
                                    </div>
                                    @if (count(\Cart::content()))
                                    <div class="card-products-wrapper">
                                        <ul>
                                            @foreach (\Cart::content() as $row)
                                            <li>
                                                <div class="product-img" style="background-image:url(/{{$row->options->picture}})"></div>
                                                <div class="product-summary">
                                                    <p>{{$row->name}}</p>
                                                    <p><span>{{$row->price}} лв.</span></p>
                                                </div>


                                            </li>
                                            @endforeach
                                        </ul>
                                        <div class="text-center">
                                            <a href="{{route('cart.index')}}" class="to-the-card">към количката</a>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </li>
                        </ul>
                    </nav>
                    <div class="collapse navbar-collapse" id="navbar-main">
                        <ul class="nav navbar-nav">
                            <li class="{{ Route::current()->getName() == 'home' ? 'active' : '' }}"><a href="{{ route('home') }}">начало</a></li>
                            <li class="{{ Route::current()->getName() == 'about.index' ? 'active' : '' }}"><a href="{{ route('about.index') }}">За компанията</a></li>
                            <li class="dropdown mega-dropdown {{ (Route::current()->getName() == 'product.index' || Route::current()->getName() == 'product.base.category') ? 'active' : '' }}">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="{{ route('product.index') }}">Продукти <span class="caret"></span></a>
                                @if ($baseCategories)
                                <ul class="dropdown-menu mega-dropdown-menu clearfix">
                                    @foreach ($baseCategories as $baseCat)
                                    <li>
                                        <a href="{{ route('product.base.category', [$baseCat->slug]) }}">
                                            <div class="dropdown-cat" style="background-image:url(/{{$baseCat->picture}})"></div>
                                            <p>{{$baseCat->name}}</p>
                                        </a>
                                    </li>
                                    @endforeach
                                    <li class="{{ Route::current()->getName() == 'product.promotions' ? 'active' : '' }}">
                                        <a href="{{ route('product.promotions') }}">
                                            <div class="dropdown-cat" style="background-image:url(/img/temp/menu_promo.png)"></div>
                                            <p>Промоции</p>
                                        </a>
                                    </li>
                                    <li class="{{ Route::current()->getName() == 'product.series' ? 'active' : '' }}">
                                        <a href="{{ route('product.series') }}">
                                            <div class="dropdown-cat" style="background-image:url(/img/temp/menu4.png)"></div>
                                            <p>Серии</p>
                                        </a>
                                    </li>
                                </ul>
                                @endif
                            </li>
                            <li class="{{ Route::current()->getName() == 'research.index' ? 'active' : '' }}"><a href="{{ route('research.index') }}">Научни изследвания</a></li>
                            <li class="{{ Route::current()->getName() == 'partners.index' ? 'active' : '' }}"><a href="{{ route('partners.index') }}">магазини и сервизи</a></li>
                            <li class="{{ Route::current()->getName() == 'contact.index' ? 'active' : '' }}"><a href="{{ route('contact.index') }}">Контакти</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="sticky-menu">
            <div class="container">

            </div>
        </div>
    </header>
@if(Route::current()->getName() !== 'home' && Route::current()->getName() !== 'xchange.index')
    <div class="main-wrapper">
@endif
        @yield('content')
@if(Route::current()->getName() !== 'home' && Route::current()->getName() !== 'xchange.index')
    </div>
@endif
    <footer>
        <div class="container footer-navigation">
            <div class="row">
                <div class="col-md-3">
                    <h5>Продуктов асортимент</h5>
                    <ul>
                        @if ($baseCategories)
                          @foreach ($baseCategories as $baseCat)
                            <li><a href="{{ route('product.base.category', [$baseCat->slug]) }}">{{ $baseCat->name }}</a></li>
                          @endforeach
                        @endif
                        <li><a href="{{ route('product.series') }}">Серии</a></li>
                    </ul>
                </div>
                <div class="col-md-2 col-md-offset-1 no-padding">
                    <h5>Моят Еinhell</h5>
                    <ul>
                        <li><a href="{{route('profile')}}">Моят профил</a></li>
                        <li><a href="{{route('cart.index')}}">Моята количка</a></li>
                        @if (isset($filesFAQ))
                            <li><a target="_blank" href="/{{$filesFAQ->file}}">Поръчки и доставки</a></li>
                        @endif
                        @if (isset($filesTC))
                            <li><a target="_blank" href="/{{$filesTC->file}}">Общи условия</a></li>
                        @endif
                    </ul>
                </div>
                <div class="col-md-2 col-md-offset-1 no-padding">
                    <h5>За компанията</h5>
                    <ul>
                        <li><a href="{{ route('about.index') }}">Повече за нас</a></li>
                        <li><a href="{{ route('about.index', ['#tab-materials']) }}">Рекламни материали</a></li>
                        <li><a href="{{ route('partners.index') }}">Набиране на дилъри</a></li>
                        <li><a href="https://www.youtube.com/user/Einhellgermany?&ab_channel=Einhell" target="_blank">YouТube канал на Einhell</a></li>
                    </ul>
                </div>
                <div class="col-md-2 col-md-offset-1 no-padding-left">
                    <h5>Контакти</h5>
                    <p>
                        <strong> Адрес:</strong> гр. Варна 9023,<br>
                        бул. “Цар Освободител” 331
                    </p>
                    <p>
                        <strong>Тел.:</strong> +359 52 739 038 <br>
                        <strong>Факс:</strong> +359 52 739 098
                    </p>
                    <p>
                        <strong>Е-mail:</strong> <a href="mailto:office@einhell.bg">office@einhell.bg</a>
                    </p>
                </div>
            </div>
        </div>

    </footer>
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <p>© 2017 Einhell. Всички права запазени. </p>
                </div>
                <div class="col-md-2 col-md-offset-2">
                    <ul class="social-navigation clearfix text-center">
                        <!-- <li><a href="#" target="_blank"><img src="/img/footer_social/fb.png" alt=""></a></li> -->
                        <!-- <li><a href="#" target="_blank"><img src="/img/footer_social/twitter.png" alt=""></a></li> -->
                        <li><a href="https://www.youtube.com/user/Einhellgermany?&ab_channel=Einhell" target="_blank"><img src="/img/footer_social/youtube.png" alt=""></a></li>
                        <!-- <li><a href="#" target="_blank"><img src="/img/footer_social/google.png" alt=""></a></li> -->
                    </ul>
                </div>
                <div class="col-md-2  col-md-offset-3">
                    <a href="http://onecreative.eu" class="oc-link" target="_blank">Created by <img src="/img/perspective_logo-small.png" alt=""></a>
                </div>
            </div>
        </div>
    </div>
    <div id="querySet" data-url="{{ URL::route( 'query.filters.set' ) }}"></div>
    <div id="queryUnset" data-url="{{ URL::route( 'query.filters.unset' ) }}"></div>
    <div id="loadQuery" data-url="{{ URL::route( 'query.filters.load' ) }}"></div>
    <div id="clearQuery" data-url="{{ URL::route( 'query.filters.clear' ) }}"></div>
    <!-- Scripts -->
    @stack('scripts')
    <script type="text/javascript">

        $('#exampleInputAmount').keypress(function(e) {
            if(e.which == 13) {
                $('form#user_search_form').submit();
            }
        });

        $(function() {
            $('a').mouseover(function() {
                if(!$(this).parents('#results-header-search').length){
                    $('#results-header-search').hide();
                    $("#results-header-search").empty();
                }
            });

            var timer;
            var products='';
            var categories='';
            var series='';
            var productImg;
            var data = {};
            $('#exampleInputAmount').on('keyup', function() {
                clearTimeout(timer);
                timer = setTimeout(function() {

                    data.search = $('#exampleInputAmount').val();
                    if(data.search.length>1){
                        callSearch(data)
                    }else{
                        $('#results-header-search').hide();
                        $("#results-header-search").empty();
                    }
                }, 750);
            });
            $('#exampleInputAmount').mouseover(function() {
                if ($(this).val().length>1) {
                    data.search = $('#exampleInputAmount').val();

                    callSearch(data)
                }

            });

            function callSearch(data){
                $.ajaxSetup( { headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr("content") } } );
                $.ajax({
                    method: "POST",
                    url: "{{ route('home.search') }}",
                    dataType: "json",
                    data: data,
                    success: function (data) {
                        products='';
                        categories='';
                        series='';
                        $("#results-header-search").empty().show();
                        console.log(data);
                        if (data.series.length>0 || data.products.length>0 || data.category.length>0){

                            $.each(data, function(i, obj){
                                if (i=="products") {

                                    if (data.products.length>0) {
                                        products+='<h3>Продукти:</h3>'
                                    }
                                    products+='<ul>'
                                    $.each(obj, function(i, item){
                                        if (item.pictures!==undefined) {
                                            productImg = item.pictures[0].resource
                                        }else{
                                            productImg='';
                                        }

                                        var url = "{{ route('product.index') }}"+'/'+item.slug;

                                        products+='<li>'
                                        products+='<a href='+url+' class="single-result clearfix">' +
                                                    '<div class="img-wrapper" style="background-image:url(/'+productImg+')"></div>'+
                                                    '<h5>'+item.name+'</h5>'+
                                                    // '<p>'+item.technical_description+'</p>'+
                                                '</a>'
                                        products+='</li>'
                                        products+='</ul>'

                                    });

                                }else if (i=="category") {
                                    if (data.category.length>0) {
                                        categories+='<h3>Катеогрии:</h3>'
                                    }
                                    categories+='<ul>'
                                    $.each(obj, function(i,item){

                                        var catUrl = "{{ route('product.index') }}"+'/category/'+item.slug;

                                        categories+='<li>'
                                        categories+='<a href='+catUrl+' class="single-result single-result-cat clearfix">' +
                                                        // '<div class="img-wrapper" style="background-image:url(/'+item.picture+')"></div>'+
                                                        '<h5 class="text-center">'+item.name+'</h5>'+
                                                    '</a>'
                                        categories+='</li>'
                                        categories+='</ul>'
                                    });
                                }else if (i=="series") {
                                    if (data.series.length>0) {
                                        series+='<h3>Серии:</h3>'
                                    }
                                    series+='<ul>';
                                    $.each(obj, function(i,item){

                                        series+='<li>';
                                        series+='<a href="{{ route("product.series") }}" class="single-result single-result-cat clearfix">' +
                                                    '<h5 class="text-center">'+item.name+'</h5>'+
                                                '</a>';
                                        series+='</li>';
                                        series+='</ul>';
                                    });

                                }
                            });
                            $('#results-header-search').append(products + categories + series)
                        }else{
                            $("#results-header-search").empty().show();
                            $('#results-header-search').append('<h5 class="no-results">Няма намерени резултати</h5>')
                        }
                     }
                });
            }



        });
    </script>

    <script type="text/javascript" src="{!! asset('js/swiper.min.js') !!}" charset="utf-8"></script>
    <script type="text/javascript" src="{!! asset('js/scripts.js') !!}" charset="utf-8"></script>

    @yield('page-scripts')
    {{-- <script type="text/javascript" src="{!! asset('js/sliders.js') !!}" charset="utf-8"></script> --}}
</body>
</html>