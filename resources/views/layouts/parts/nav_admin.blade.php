<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section style="height: auto;" class="sidebar">
        <ul class="sidebar-menu">
            <li class="">
                <a href="{{ URL::route( 'admin.home' ) }}">
                    <i class="fa fa-tachometer"></i> 
                    <span>Dashboard</span>
                </a>
            </li>
            @if(count($admin_options))
                @foreach($admin_options as $option)
                    @if(isset($option['sub_nav']))
                    <li class="treeview">
                        <a href="/admin">
                            <i class="{{@$option['icon']}}"></i>
                            <span>{{$option['title']}}</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-down pull-right"></i>
                            </span>
                        </a>

                        <ul class="treeview-menu">
                            @foreach($option['sub_nav'] as $sub_nav_item)
                                <li class="">
                                    <a href="{{ URL::to(app()->getLocale().'/'.$sub_nav_item['url']) }}">
                                        <i class="fa fa-circle-o"></i>{{$sub_nav_item['title']}}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                    @else
                    <li class="">
                        <a href="{{ URL::to(app()->getLocale().'/'.$option['url']) }}">
                            <i class="{{@$option['icon']}}"></i>
                            <span>{{$option['title']}}</span>
                        </a>
                    </li>
                    @endif
                @endforeach
            @endif
            <li class="">
                <a href="{{ route('logout') }}"> <i class="fa fa-power-off"></i><span> Logout</span></a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
