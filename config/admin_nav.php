<?php

return [
    [
        'title' => 'Потребители',
        'url' => 'admin/users',
        'icon' => 'fa fa-user',
    ],
    [
        'title' => 'Поръчки',
        'url' => 'admin/orders/list',
        'icon' => 'fa fa-shopping-cart',
    ],
    [
        'title' => 'Категории',
        'icon' => 'fa fa-list-alt',
        'sub_nav' => [
            [
                'title' => 'Главни категории',
                'url' => 'admin/base-category',
            ],
            [
                'title' => 'Продуктови категории',
                'url' => 'admin/product-category',
            ]
        ]
    ],
    [
        'title' => 'Теми на контакти',
        'icon' => 'fa fa-commenting-o',
        'url' => 'admin/contact-theme',
    ],
    [
        'title' => 'Продуктови серии',
        'url' => 'admin/product-series',
        'icon' => 'fa  fa-folder-open',
    ],
    [
        'title' => 'Продукти',
        'url' => 'admin/products',
        'icon' => 'fa fa-shopping-cart'
    ],
    // [
    //     'title' => 'Основни гаранции',
    //     'url' => 'admin/base-warranty',
    //     'icon' => 'fa fa-shopping-cart'
    // ],
    [
        'title' => 'Слайдери',
        'icon' => 'fa fa-file-image-o',
        'url' => 'admin/product-sliders',
    ],
    [
        'title' => 'Рекламни материали',
        'url' => 'admin/files/create',
        'icon' => 'fa fa-file-powerpoint-o',
    ],
    [
        'title' => 'За Компанията',
        'icon' => 'fa fa-info-circle',
        'sub_nav' => [
            [
                'title' => 'Списък',
                'url' => 'admin/information-pages',
            ],
            [
                'title' => 'Главна Снимка',
                'url' => 'admin/information-pages/cover-photo',
            ],
            [
                'title' => 'Einhell по света',
                'url' => 'admin/information-pages/map',
            ],
        ]
    ],
    [
        'title' => 'Научни изследвания',
        'icon' => 'fa fa-info-circle',
        'sub_nav' => [
            [
                'title' => 'Списък',
                'url' => 'admin/scientific-researchs',
            ],
            [
                'title' => 'Главна Снимка',
                'url' => 'admin/scientific-researchs/cover-photo',
            ],
        ]
    ],
    // [
    //     'title' => 'Партньори',
    //     'url' => 'admin/partners',
    //     'icon' => 'fa fa-users',
    // ],
    [
        'title' => 'Градове',
        'url' => 'admin/cities',
        'icon' => 'fa fa-university'
    ],
    [
        'title' => 'Търговски представители',
        'url' => 'admin/distributors',
        'icon' => 'fa fa-user-plus'
    ],
    // [
    //     'title' => 'Контакти',
    //     'url' => 'admin/contacts',
    //     'icon' => 'fa fa-envelope',
    // ],
    [
        'title' => 'Системни Файлове',
        'url' => 'admin/system-files/create',
        'icon' => 'fa fa-file',
    ],
    [
        'title' => 'PowerXChange',
        'icon' => 'fa fa-battery-4',
        'sub_nav' => [
            [
                'title' => 'PowerXChange Страница',
                'url' => 'admin/x-change-page',
            ],
            [
                'title' => 'PowerXChange Предимства',
                'url' => 'admin/x-change-advantages',
            ],
        ]
    ],

];
