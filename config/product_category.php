<?php 

return [
    [
    	'id' => '1001',
    	'name' => 'SHOWROOM',
    	'slug' => 'showroom',
    	'picture' => '/img/temp/menu1.png',
    ],
    [
    	'id' => '1002',
    	'name' => 'ИНСТРУМЕНТИ И МАШИНИ',
    	'slug' => 'machines',
    	'picture' => '/img/temp/menu2.png',
    ],
	[
    	'id' => '1003',
    	'name' => 'ГРАДИНСКА ТЕХНИКА',
    	'slug' => 'garden',
    	'picture' => '/img/temp/menu3.png',
    ],
    [
    	'id' => '1004',
    	'name' => 'СЕРИИ',
    	'slug' => 'series',
    	'picture' => '/img/temp/menu4.png',
    ]
];
