
$(document).ready( function() {

	initRange();

	$('.serieFilter').change( function() {

		var value = this.value;

		if(this.checked) {
	    	setFilter(value, 'serie');
	    }else{
	    	unsetFilter(value, 'serie');
	    }

	});

    $('.xChangeFilter').change( function() {

        var value = this.value;

        if(this.checked) {
            setFilter(value, 'xchange');
        }else{
            unsetFilter(value, 'xchange');
        }

    });

	$('.clearFilter').click( function() {
		clearFilters();
	});

	setTimeout(function() {
    	//loadFilters();
    }, 100);

});

function initRange()
{
	// init already defined price ranges from session
}

function setFilter(filter_value , source) {

	var url     = $('#querySet').attr('data-url');
	var data 	= {};
	data.filter = filter_value;
	data.source = source;

    $.ajaxSetup( { headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } } );
    $.ajax({
        method: "POST",
        url: url,
        dataType: "json",
        data: data,
        success: function (response) {
            window.location.reload();
        },
        error: function (response) {
            alert('Error aplying filters');
        },
    });

}

function unsetFilter(filter_value, source) {

	var url     = $('#queryUnset').attr('data-url');

	var data 	= {};
	data.filter = filter_value;
	data.source = source;

    $.ajaxSetup( { headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } } );
    $.ajax({
        method: "POST",
        url: url,
        dataType: "json",
        data: data,
        success: function (response) {
            window.location.reload();
        },
        error: function (response) {
        	
        },
    });

}

function loadFilters()
{
	var url = $('#loadQuery').attr('data-url');

	$.ajaxSetup( { headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } } );
    $.ajax({
        method: "POST",
        url: url,
        dataType: "json",
        data: '',
        success: function (response) {
   //      	if ( (response.results.length > 0) && (typeof response.results != 'string') ) {
   //      		drawResults(response.results);
			// }
        },
        error: function (response) {
            console.log(response);
        },
    });
}

function clearFilters()
{
	var url = $('#clearQuery').attr('data-url');

	$.ajaxSetup( { headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } } );
    $.ajax({
        method: "POST",
        url: url,
        dataType: "json",
        data: '',
        success: function (response) {
        	if ( (response.results.length > 0) && (typeof response.results != 'string') ) {
        		drawResults(response.results);
			}
        },
        error: function (response) {
            console.log(response);
        },
    });
}

// function drawResults(results)
// {
// 	console.log(results);
// }
