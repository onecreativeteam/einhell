$(function() {
    var timer;
    $('#exampleInputAmount').on('keyup', function() {
        clearTimeout(timer);
        timer = setTimeout(function() {
            var data    = {};
            data.search = $('#exampleInputAmount').val();
            $.ajaxSetup( { headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr("content") } } );
            $.ajax({
                method: "POST",
                url: "{{ route('home.search') }}",
                dataType: "json",
                data: data,
                success: function (data) {
                    console.log(data);
                }
            });
        }, 750);
    });
});
