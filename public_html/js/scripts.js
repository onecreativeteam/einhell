$(function() {

    // Create a clone of the menu, right next to original.
    var menu = $('#navbar-main').clone()
    $('#navbar-main').addClass('original')

    $('.sticky-menu .container').append(menu)
    $('.sticky-menu .container').parent().addClass('cloned')


    if($(window).width()>1024){
        $(window).scroll(function(){ 
            if ($(window).scrollTop() >= $('.original').offset().top) {
                $('.original').css('visibility','hidden');
                $('.cloned').addClass('show');
            }else {
                $('.cloned').removeClass('show');
                $('.original').css('visibility','visible');
            }
        })
    }

    function checkIsMobile(){
        if($(window).width()>1024){
            $('.navbar-nav li a.dropdown-toggle').addClass('disabled')
        }
    }

    $( window ).resize(function() {
        checkIsMobile();
    });
    checkIsMobile();
});
