$( document ).ready(function() {

    $('.credential-page .radio').on('change', function() {
        if($("#person").is(':checked')) {
            $(".company-invoice").slideUp();
            // $(".submit.person").fadeIn();
        }else if ($("#legal").is(':checked')) {
            $(".company-invoice").slideDown();
            // $(".submit.person").fadeOut();
        }
    });

    if ($('#bustat').length > 0) {
        var existingBulstat = $('#bustat').val();
    }

    $('#zdds').click(function() {

        if (existingBulstat == '') {
            existingBulstat = $('#bustat').val();
        }

        var string = document.getElementById('bustat').value;

        if(document.getElementById('zdds').checked) {

            var string = document.getElementById('bustat').value;

            var prefix = 'BG';

            var stackUpper = string.startsWith(prefix);

            if (stackUpper === false) {
                $('#bustat').val('BG'+string);
            }

        } else {

            $('#bustat').val(existingBulstat);

        }

    });

});
