$( document ).ready(function() {
    // $(".dropdown").hover(
    //     function() {
    //         $('.dropdown-menu', this).not('.dropdown-menu').stop(true,true).fadeOut();
    //         console.log($(this));
    //         $(this).toggleClass('open');
    //     },
    //     function() {
    //         $('.dropdown-menu', this).not('.dropdown-menu').stop(true,true).fadeIn();
    //         $(this).toggleClass('open');
    //     }
    // );


    var swiper = new Swiper('.home-header-slider', {
       nextButton: '.swiper-button-next-home',
       prevButton: '.swiper-button-prev-home',
       slidesPerView: 1,
       spaceBetween: 0,
       autoplay: 2500,
       speed: 500,
       loop: true
    });
    function returnLoopPromotionSlider(){
        if ($(window).width()>991) {
            if ($('.promotions-slider .swiper-slide').length<5) {
                 return false
            }else {
                return true
            }
        }else{
            return true
        }
    }
    var promotionsSlider = new Swiper('.promotions-slider', {
       nextButton: '.next-home-promotion',
       prevButton: '.prev-home-promotion',
       slidesPerView: 4,
       spaceBetween: 30,
       autoplay: 2000,
       autoplayDisableOnInteraction: true,
       speed: 500,
       loop: returnLoopPromotionSlider(),
       breakpoints: {
            1200: {
                slidesPerView: 3
            },
            768: {
                slidesPerView: 2
            },
            420: {
                slidesPerView: 1
            }
        }
    });
    if ($(window).width()>991) {
        if ($('.promotions-slider .swiper-slide').length<13) {
            if (true) {

            }
            promotionsSlider.destroy(false, false);
            $('.next-home-promotion , .prev-home-promotion').hide();
        }
    }


    function returnLoopBestsellers(){
        if ($(window).width()>991) {
            if ($('.bestsellers .swiper-slide').length<5) {
                 return false
            }else {
                return true
            }
        }else {
            return true
        }
    }
    var bestsellers = new Swiper('.bestsellers', {
        nextButton: '.next-bestsellers',
        prevButton: '.prev-bestsellers',
        slidesPerView: 4,
        spaceBetween: 30,
        autoplay: 2000,
        autoplayDisableOnInteraction: true,
        speed: 500,
        loop: returnLoopBestsellers(),
        breakpoints: {
             1200: {
                 slidesPerView: 3
             },
             768: {
                 slidesPerView: 2
             },
             420: {
                 slidesPerView: 1
             }
         }
    });
    if ($(window).width()>991) {
        if ($('.bestsellers .swiper-slide').length<13) {
            bestsellers.destroy(false, false);
            $('.next-bestsellers , .prev-bestsellers').hide();
        }
    }


    // $(".card-wrapper").hover(
    //
    //     function() {
    //         console.log('vutre');
    //         $('.card-products-wrapper', this).not('.card-products-wrapper').stop(true,true).fadeOut();
    //         console.log($(this));
    //         $('.card-products-wrapper').toggleClass('open');
    //     },
    //     function() {
    //         console.log('vun');
    //         $('.card-products-wrapper', this).not('.card-products-wrapper').stop(true,true).fadeIn();
    //         $('.card-products-wrapper').toggleClass('open');
    //     }
    // );
});
