<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::group([ 'middleware' => ['auth', 'admin'] , 'prefix' => 'admin' ], function() {

	Route::get('/', ['as' => 'admin.home', 'uses' => 'Admin\HomeController@index']);
	Route::get('/orders/list', ['as' => 'admin.orders.list', 'uses' => 'Admin\HomeController@listOrders']);

	Route::post('/ajax/map/{method}', ['as' => 'admin.home.update.map', 'uses' => 'Admin\HomeController@updateMap']);

	Route::get('/statistics/product-quantity', ['as' => 'admin.statistics.quantity', 'uses' => 'Admin\StatisticsController@productQuantity']);
	Route::get('/statistics/new-orders', ['as' => 'admin.statistics.orders', 'uses' => 'Admin\StatisticsController@newOrders']);

	Route::resource('/users', 'Admin\UsersController',
	['names' => [
		'index' => 'admin.list.users',
		'create' => 'admin.create.users',
		'store' => 'admin.store.users',
		'edit' => 'admin.edit.users',
		'update' => 'admin.update.users',
		'destroy' => 'admin.destroy.users'
	],'except' => [
	    'show',
	    //'destroy'
	]]);

	Route::resource('/base-category', 'Admin\BaseCategoriesController',
	['names' => [
		'index' => 'admin.list.base.category',
		'create' => 'admin.create.base.category',
		'store' => 'admin.store.base.category',
		'edit' => 'admin.edit.base.category',
		'update' => 'admin.update.base.category',
		'destroy' => 'admin.destroy.base.category'
	],'except' => [
	    'show',
	    //'destroy'
	]]);

	Route::resource('/contact-theme', 'Admin\ContactThemesController',
	['names' => [
		'index' => 'admin.list.contact.theme',
		'create' => 'admin.create.contact.theme',
		'store' => 'admin.store.contact.theme',
		'edit' => 'admin.edit.contact.theme',
		'update' => 'admin.update.contact.theme',
		'destroy' => 'admin.destroy.contact.theme'
	],'except' => [
	    'show',
	    //'destroy'
	]]);

	Route::resource('/product-category', 'Admin\ProductCategoriesController',
	['names' => [
		'index' => 'admin.list.product.category',
		'create' => 'admin.create.product.category',
		'store' => 'admin.store.product.category',
		'edit' => 'admin.edit.product.category',
		'update' => 'admin.update.product.category',
		'destroy' => 'admin.destroy.product.category',
	],'except' => [
	    'show',
	    //'destroy'
	]]);

	Route::resource('/product-series', 'Admin\ProductSeriesController',
	['names' => [
		'index' => 'admin.list.product.series',
		'create' => 'admin.create.product.series',
		'store' => 'admin.store.product.series',
		'edit' => 'admin.edit.product.series',
		'update' => 'admin.update.product.series',
		'destroy' => 'admin.destroy.product.series',
	],'except' => [
	    'show',
	    //'destroy'
	]]);

	Route::get('/products/change-category', ['as' => 'products.change.cat.manual', 'uses' => 'Admin\ProductsController@manualChangeCategory']);
	Route::post('/products/change-series', ['as' => 'products.change.serie.manual', 'uses' => 'Admin\ProductsController@manualChangeSeries']);
	Route::resource('/products', 'Admin\ProductsController',
	['names' => [
		'index' => 'admin.list.products',
		'create' => 'admin.create.product',
		'store' => 'admin.store.product',
		'edit' => 'admin.edit.product',
		'update' => 'admin.update.product',
		'destroy' => 'admin.destroy.product',
	],'except' => [
	    'show'
	]]);
	Route::get('/products/export', ['as' => 'admin.export.product', 'uses' => 'Admin\ProductsController@export']);
	Route::post('/products/import', ['as' => 'admin.import.product', 'uses' => 'Admin\ProductsController@import']);
	Route::post('/products/pictures', ['as' => 'admin.import.pictures', 'uses' => 'Admin\ProductsController@uploadExcellPictures']);

	Route::get('/products/gallery/export', ['as' => 'admin.export.product.gallery', 'uses' => 'Admin\ProductsController@exportGallery']);
	Route::post('/products/gallery/import', ['as' => 'admin.import.product.gallery', 'uses' => 'Admin\ProductsController@importGallery']);
	Route::post('/products/gallery/pictures', ['as' => 'admin.import.gallery.pictures', 'uses' => 'Admin\ProductsController@uploadExcellGalleryPictures']);
		
	// Route::get('/products/trim', ['as' => 'admin.trim.product.gallery', 'uses' => 'Admin\ProductsController@trim']);
	// Route::post('/products/update-categories', ['as' => 'admin.update.product.categories', 'uses' => 'Admin\ProductsController@updateCategories']);

	Route::resource('/base-warranty', 'Admin\BaseWarrantyController',
	['names' => [
		'index' => 'admin.list.base.warranty',
		'create' => 'admin.create.base.warranty',
		'store' => 'admin.store.base.warranty',
		'edit' => 'admin.edit.base.warranty',
		'update' => 'admin.update.base.warranty',
		'destroy' => 'admin.destroy.base.warranty',
	],'except' => [
	    'show'
	]]);

	Route::resource('/products/{product_id}/embed-gallery', 'Admin\ProductsGalleryController',
	['names' => [
		//'index' => 'admin.list.products.gallery',
		'create' => 'admin.create.product.gallery',
		'store' => 'admin.store.product.gallery',
		//'edit' => 'admin.edit.product.gallery',
		'update' => 'admin.update.product.gallery',
		'destroy' => 'admin.destroy.product.gallery',
	],'except' => [
		'index',
		'edit',
	    'show'
	]]);
	Route::post('/products/embed-gallery/update-order/{id}', ['as' => 'admin.update.product.gallery.order', 'uses' => 'Admin\ProductsGalleryController@updateOrder']);

	Route::resource('/product-sliders', 'Admin\ProductSlidersController',
	['names' => [
		'index' => 'admin.list.product.sliders',
		'create' => 'admin.create.product.sliders',
		'store' => 'admin.store.product.sliders',
		'edit' => 'admin.edit.product.sliders',
		'update' => 'admin.update.product.sliders',
		'destroy' => 'admin.destroy.product.sliders',
	],'except' => [
	    'show',
	    //'destroy'
	]]);

	Route::resource('/files', 'Admin\InformationFilesController',
	['names' => [
		'create' => 'admin.create.information.files',
		'store' => 'admin.store.information.files',
		'destroy' => 'admin.update.information.files'
	],'except' => [
		'index',
	    'show',
	    'edit',
	    'update'
	]]);

	Route::get('/download/{filename}', function($filename)
	{
	    if (file_exists(public_path('uploads' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'information' . DIRECTORY_SEPARATOR . $filename)))
	    {
	        return \Response::download(public_path('uploads' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'information' . DIRECTORY_SEPARATOR . $filename));
	    }
	    else
	    {
	        exit('Requested file does not exist on our server!');
	    }

	    return redirect()->back();
	});

	Route::get('/information-pages/cover-photo', ['as' => 'information.page.cover', 'uses' => 'Admin\InformationPagesController@coverPhoto']);
	Route::get('/information-pages/map', ['as' => 'information.page.map', 'uses' => 'Admin\InformationPagesController@map']);
	Route::post('/information-pages/map/store', ['as' => 'information.page.map.store', 'uses' => 'Admin\MapPageController@store']);

	Route::resource('/information-pages', 'Admin\InformationPagesController',
	['names' => [
		'index' => 'admin.list.information.pages',
		'create' => 'admin.create.information.pages',
		'store' => 'admin.store.information.pages',
		'edit' => 'admin.edit.information.pages',
		'update' => 'admin.update.information.pages',
		'destroy' => 'admin.destroy.information.pages',
	],'except' => [
	    'show',
	    //'destroy'
	]]);

	Route::get('/scientific-researchs/cover-photo', ['as' => 'scientific.research.cover', 'uses' => 'Admin\ScientificResearchsController@coverPhoto']);
	Route::resource('/scientific-researchs', 'Admin\ScientificResearchsController',
	['names' => [
		'index' => 'admin.list.scientific.researchs',
		'create' => 'admin.create.scientific.researchs',
		'store' => 'admin.store.scientific.researchs',
		'edit' => 'admin.edit.scientific.researchs',
		'update' => 'admin.update.scientific.researchs',
		'destroy' => 'admin.destroy.scientific.researchs',
	],'except' => [
	    'show',
	    // 'destroy'
	]]);

	Route::resource('/partners', 'Admin\PartnersController',
	['names' => [
		'index' => 'admin.list.partners',
		'create' => 'admin.create.partners',
		'store' => 'admin.store.partners',
		'edit' => 'admin.edit.partners',
		'update' => 'admin.update.partners',
		'destroy' => 'admin.destroy.partners',
	],'except' => [
	    'show',
	    // 'destroy'
	]]);

	Route::resource('/cities', 'Admin\CitiesController',
	['names' => [
		'index' => 'admin.list.cities',
		'create' => 'admin.create.cities',
		'store' => 'admin.store.cities',
		'edit' => 'admin.edit.cities',
		'update' => 'admin.update.cities',
		'destroy' => 'admin.destroy.cities',
	],'except' => [
	    'show',
	    // 'destroy'
	]]);

	Route::resource('/distributors', 'Admin\DistributorsController',
	['names' => [
		'index' => 'admin.list.distributors',
		'create' => 'admin.create.distributors',
		'store' => 'admin.store.distributors',
		'edit' => 'admin.edit.distributors',
		'update' => 'admin.update.distributors',
		'destroy' => 'admin.destroy.distributors',
	],'except' => [
	    'show',
	    // 'destroy'
	]]);

	Route::resource('/contacts', 'Admin\ContactsController',
	['names' => [
		'index' => 'admin.list.contacts',
		'create' => 'admin.create.contacts',
		'store' => 'admin.store.contacts',
		'edit' => 'admin.edit.contacts',
		'update' => 'admin.update.contacts',
		'destroy' => 'admin.destroy.contacts',
	],'except' => [
	    'show',
	    // 'destroy'
	]]);

	Route::resource('/system-files', 'Admin\SystemFilesController',
	['names' => [
		'create' => 'admin.create.system.files',
		'store' => 'admin.store.system.files',
		'destroy' => 'admin.destroy.system.files'
	],'except' => [
		'index',
	    'show',
	    'edit',
	    'update'
	]]);

	Route::resource('/cover-photos', 'Admin\CoverPhotosController',
	['names' => [
		//'create' => 'admin.create.system.files',
		'store' => 'admin.store.system.files',
		//'destroy' => 'admin.destroy.system.files'
	],'except' => [
		'index',
		'create',
	    'show',
	    'edit',
	    'destroy',
	    'update'
	]]);

	Route::resource('/products/{product_id}/warranty', 'Admin\ProductsWarrantyController',
	['names' => [
		//'index' => 'admin.list.products.warranty',
		'create' => 'admin.create.product.warranty',
		'store' => 'admin.store.product.warranty',
		//'edit' => 'admin.edit.product.warranty',
		'update' => 'admin.update.product.warranty',
		'destroy' => 'admin.destroy.product.warranty',
	],'except' => [
		'index',
		'edit',
	    'show'
	]]);

	Route::resource('/x-change-advantages', 'Admin\AdvantagesController',
	['names' => [
		'index' => 'admin.list.advantages',
		'create' => 'admin.create.advantages',
		'store' => 'admin.store.advantages',
		'edit' => 'admin.edit.advantages',
		'update' => 'admin.update.advantages',
		'destroy' => 'admin.destroy.advantages',
	],'except' => [
	    'show',
	    // 'destroy'
	]]);

	Route::resource('/x-change-page', 'Admin\XChangePageController',
	['names' => [
		'index' => 'admin.list.xchange',
		'create' => 'admin.create.xchange',
		'store' => 'admin.store.xchange',
		'edit' => 'admin.edit.xchange',
		'update' => 'admin.update.xchange',
		'destroy' => 'admin.destroy.xchange',
	],'except' => [
	    // 'index',
	    // 'edit',
	    // 'update',
	    // 'show',
	    // 'destroy'
	]]);

	Route::resource('/orders', 'Admin\OrdersController',
	['names' => [
		// 'index' => 'admin.list.order',
		// 'create' => 'admin.create.order',
		// 'store' => 'admin.store.order',
		'edit' => 'admin.edit.order',
		'update' => 'admin.update.order',
		'destroy' => 'admin.destroy.order',
	],'except' => [
	    'index',
	    'create',
	    'store',
	    'show'
	]]);
	Route::get('/admin/orders/remove/product/{order_product_id}', ['as' => 'admin.order.remove.product', 'uses' => 'Admin\OrdersController@removeOrderProduct']);
	Route::post('/admin/orders/add/product/{order_id}/{product_id}', ['as' => 'admin.order.add.product', 'uses' => 'Admin\OrdersController@addNewOrderProduct']);
	Route::post('/admin/orders/status/{order_id}/{status}', ['as' => 'admin.order.status.change', 'uses' => 'Admin\OrdersController@changeOrderStatus']);
	
	Route::get('/datatables/products', ['as' => 'admin.datatables.products', 'uses' => 'Admin\DataTablesController@productDataTable']);
	Route::get('/datatables/edit-order-products', ['as' => 'admin.datatables.edit.order.products', 'uses' => 'Admin\DataTablesController@editOrderProductDataTable']);
	Route::get('/datatables/users', ['as' => 'admin.datatables.users', 'uses' => 'Admin\DataTablesController@userDataTable']);
	Route::get('/datatables/distributors', ['as' => 'admin.datatables.distributors', 'uses' => 'Admin\DataTablesController@distributorDataTable']);
	Route::get('/datatables/orders', ['as' => 'admin.datatables.orders', 'uses' => 'Admin\DataTablesController@orderDataTable']);
	Route::get('/datatables/new-orders', ['as' => 'admin.datatables.orders.new', 'uses' => 'Admin\DataTablesController@newOrdersDataTable']);

});
