<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('/login', ['as' => 'login.post', 'uses' => 'Auth\LoginController@login']);
Route::get('/logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
// Route::get('/register', ['as' => 'register', 'uses' => 'Auth\RegisterController@showRegistrationForm']);
Route::post('/register', ['as' => 'register.post', 'uses' => 'Auth\RegisterController@register']);
Route::get('/password/reset', ['as' => 'password.reset', 'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
Route::post('/password/email', ['as' => 'password.email', 'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail']);
Route::get('/password/reset/{token}', ['as' => 'password.reset.token', 'uses' => 'Auth\ResetPasswordController@showResetForm']);
Route::post('/password/reset', ['as' => 'password.reset.post', 'uses' => 'Auth\ResetPasswordController@reset']);

// SOCIAL FACEBOOK AUTH
Route::get('/redirect/{provider}', ['as' => 'social.redirect', 'uses' => 'SocialAuthController@redirect']);
Route::get('/callback/{provider}', ['as' => 'social.callback', 'uses' => 'SocialAuthController@callback']);

Route::get('/', ['as' => 'home', 'uses' => 'Home\HomeController@index']);
Route::get('/facture', ['as' => 'facture', 'uses' => 'Home\HomeController@facture']);
Route::get('/language/{language}', ['as' => 'lang.switch', 'uses' => 'Home\LanguageController@index']);

Route::post('/ajax/search', ['as' => 'home.search', 'uses' => 'Home\HomeController@search']);
Route::post('/user/search', ['as' => 'home.search.user', 'uses' => 'Home\HomeController@searchPage']);

Route::group(['middleware' => ['auth']], function() {

	Route::get('/profile', ['as' => 'profile', 'uses' => 'Home\UserProfileController@index']);
	Route::get('/profile/order/{id}', ['as' => 'profile.view.order', 'uses' => 'Home\UserProfileController@viewSingleOrder']);
	Route::get('/profile/shipping', ['as' => 'profile.shipping', 'uses' => 'Home\UserProfileController@indexShipping']);
	Route::post('/profile/update', ['as' => 'profile.update', 'uses' => 'Home\UserProfileController@update']);

});

Route::get('/cart', ['as' => 'cart.index', 'uses' => 'Home\CartController@index']);
Route::get('/cart/order-info', ['as' => 'cart.order.info', 'uses' => 'Home\CartController@orderInfo']);
Route::post('/cart/order-info', ['as' => 'cart.order.info.post', 'uses' => 'Home\CartController@postOrderInfo']);
Route::get('/cart/checkout', ['as' => 'cart.checkout', 'uses' => 'Home\CartController@checkout']);
Route::get('/cart/order', ['as' => 'cart.order.post', 'uses' => 'Home\OrdersController@postOrder']);
Route::get('/thank-you', ['as' => 'cart.thankyou', 'uses' => 'Home\CartController@thankYouPage']);

Route::get('/cart/add/{product_id}', ['as' => 'cart.add.product', 'uses' => 'Home\CartController@add']);
Route::get('/cart/remove/{row_id}', ['as' => 'cart.remove.product', 'uses' => 'Home\CartController@remove']);
Route::get('/cart/update/{row_id}/{qty}', ['as' => 'cart.update.product', 'uses' => 'Home\CartController@updateQuantity']);

Route::get('/contact', ['as' => 'contact.index', 'uses' => 'Home\ContactController@index']);
Route::post('/contact', ['as' => 'contact.post', 'uses' => 'Home\ContactController@store']);

Route::get('/product/series', ['as' => 'product.series', 'uses' => 'Home\ProductsController@series']);
Route::get('/product/promotions', ['as' => 'product.promotions', 'uses' => 'Home\ProductsController@promotions']);

Route::get('/product', ['as' => 'product.index', 'uses' => 'Home\ProductsController@index']);
Route::get('/product/{slug}', ['as' => 'product.inner', 'uses' => 'Home\ProductsController@inner']);
Route::get('/product/category/{slug}', ['as' => 'product.category', 'uses' => 'Home\ProductsController@category']);
Route::get('/product/category/x-change/{slug}', ['as' => 'product.xchange.category', 'uses' => 'Home\ProductsController@xchangeCategory']);
Route::get('/product/category/base/{slug}', ['as' => 'product.base.category', 'uses' => 'Home\ProductsController@baseCategory']);

Route::get('/research', ['as' => 'research.index', 'uses' => 'Home\ResearchController@index']);
Route::get('/about', ['as' => 'about.index', 'uses' => 'Home\AboutController@index']);
Route::get('/partners', ['as' => 'partners.index', 'uses' => 'Home\PartnerController@index']);
Route::get('/power-x-change', ['as' => 'xchange.index', 'uses' => 'Home\HomeController@xChangePage']);

// Query Filters For Hotels Routes
Route::post('/filter/set', ['as' => 'query.filters.set', 'uses' => 'Home\SearchController@setFilter']);
Route::post('/filter/unset', ['as' => 'query.filters.unset', 'uses' => 'Home\SearchController@unsetFilter']);
Route::post('/filter/load', ['as' => 'query.filters.load', 'uses' => 'Home\SearchController@loadFilters']);
Route::post('/filter/clear', ['as' => 'query.filters.clear', 'uses' => 'Home\SearchController@clearFilters']);
